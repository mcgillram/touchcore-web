﻿# How to Use Webcore-Rest

  

>  ## General API

> The general API are listed in this directories text files, the URLs correspond to this general structure:

> 1. **{username}/classdiagram/{cdmName}**

>  > 1.1 **/...** for alterations to elements of the classdiagram (... found in ClassDiagramAPI.txt)

>

>  > 1.2 **/class/...** for alterations to classes (... usage found in ClassAPI.txt)

>

>  > 1.3 **/association/...** for alterations to associatioons (... usage found in Association.txt)

>

>  > 1.4 **/enum/...** for alterations to enums (... usage found in Enum.txt)

>

>  > 1.5 **/implementationClass/...** for alterations to implementation classes (... usage found in ImplementationClass.txt)

>

>  2. **{username}/workspace/..** for viewing and editting access rights to a workspace
>  3. **user**
>  > 3.1 **/public/..** for registering and logging in. These urls are unprotected - i.e they do not require token authentication.
>  > 3.2 **/..** for other authentication-related actions such as logging out or getting current user details.
>  4. **debug/..** for useful debugging apis - to be disabled when deploying WebCore to production as some function poses a security threat.

---

>  ## Long-Polling (on the GET: {username}/classdiagram/{cdmName} endpoint)

> You can poll this endpoint by taking the MD5 hash of one of the previous Responses to calling this enpoint and submitting this as the value for the QueryParameter "classDiagramHash", if the hash is a hash of the current diagram you will poll wait until either 30s is up or another user changes the classdiagram, at which point you will be returned the updated ClassDiagram as a JSON. If the hash is not a hash representation of the current classdiagram or you omit the QueryParameter "classDiagramHash" then the classdiagram as JSON will be returned immediately.

---

>  ## User Guide

> WebCore has user authentication set up. Each user are given a root user directory ( named to their user id ) under the "**users**" folder in **webcore-server**. A user can be the owner of many models - each model is associated with a unique workspace and a workspace is a folder just under the that user directory. For e.g, let's say bob has 2 models: ONE_CLASS and MULTIPLE_CLASSES. Then, the respective associated workspaces will be "users/bob/ONE_CLASS" and "users/bob/MULTIPLE_CLASSES". 
> 
> WebCore also has access rights editting - you can restrict or allow other users to interact with your models. 

> For the authentication. we make use of token-based authentication - specifically Json Web Token (JWT). In essence, almost all of the endpoints requires an extra header "Authorization" set to "Bearer \<yourJWT\>". A JWT has the following features:
> 1. It can encrypt data such as username and can thereby be decrypted/reversed-engineered to find the user associated with that token.
> 2. It is made secure by adding a signature within the token using a specific secret key.
> 3. It can be made to expire by encrypting some expiration date within the token. This further enforces security.

>  ### Step 1 : Registering as a user.

> In this version of WebCore, a person who wants to use the application needs to be registered as a user. To do so, simply send a PUT request to **localhost:8080/public/register** with body **{ "username": "yourUsername", "password" : "yourPassword" }**. You will be returned a JWT token - keep it secret and save it for later.
> Performing this request will automatically create an empty user directory for you, except if it already exists. [ Refer to the note ]
> If you ever lose your token or it expires, simply send a PUT request to **localhost:8080/public/login** with the same aforementioned body.
> Logging out is meant invalidate your JWT and you will require to log in again to get a fresh valid one.

>NOTE: Users are stored in-memory. This means that on server shutdown, all user instances are lost. On server relaunch, each user has to be re-registered manually. However, the user directories/models on disc are persisted. Hence, if on registering, the app detects that a user directory with the same user id exists, it will automatically associate that user directory with the user being created.
>  ### Step 2 : Creating a new model.

> To create a new model and automatically associating a workspace with it, send a PUT request to **localhost:8080/{yourUsername}/classdiagram/{yourModelName}**.
> If bob wants to create a new model "HOSPITAL", then he would do a PUT req to  **localhost:8080/bob/classdiagram/HOSPITAL**.

> NOTE: You must set the "Authorization" header of the request to "Bearer \<yourJWT\>" to be authenticated.

>  ### Step 3 : Interacting with your model.

> For this, please review the various API provided in the various controllers. Generally, any request so as to interact with your model will be a URL preceded by "**{yourUsername}/classdiagram/{yourModelName}**". By default, you can call any request concerning your model as you are identified as the owner. However, if you wish to interact with another user's model, then you would require the appropriate access rights.

>  ### Step 4 : Editting workspace access rights.

> This section is well documented in the **WorkspaceAPI.txt** file. It is important to note that there exists 2 kinds of permissions over a workspace:
> Read access : Users are allowed to perform only GET request pertaining to the model associated with that workspace.
> Write Access: Users are allowed to perform any request ( GET/POST/PUT/DELETE ) pertaining to the model associated with that workspace.
As an example, assume user bob wants to access tom's HOSPITAL model. Tom first needs to send a POST request to **localhost:8080/tom/workspace/HOSPITAL/grantReadAccess** with body **{ "username" : "bob" }** along with the JWT token embedded in the header.
> NOTE: Workspaces are stored in memory as well, they are re-instantiated automatically on server launch by exploring the “users” folder. Some of the workspace meta data can be automatically re-fetched such as the owner, model name and path. However, the permissions and private/public status concerning the workspace will be reset and have to be re-edited manually. By default, on server re-launch, each workspace is completely private with no read/write permissions given to anyone, except the owner.
