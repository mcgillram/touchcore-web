The following URLs are all preceded by: user/

The @param body{} shows the fields of the method related Transfer Object. There is only one kind of
transfer object in this controller: authenticationRequest with body {"username": String, "password": String}
If the body is empty, there is NO associated TO.

All requests should be authenticated by having the "Authorization" header set to "Bearer <user's token>" except for
the URLS preceded by "user/public".

/*
* Creates a new user and then logs them in.
* Technically, it provides the user with a JWT token that they should then
* embed in their header for every subsequent requests to the other rest points, so as
* to be authenticated.
* 
* @param body {"username": String, "password": String}
*/
@PutMapping("/public/register")

/*
* Logs the user in.
* Technically, it provides the user with a JWT token that they should then
* embed in their header for every subsequent requests to the other rest points, so as
* to be authenticated.
* 
* @param body {"username": String, "password": String}
*/
@PostMapping("/public/login")

/*
* Get current user details.
* 
* @param body {}
*/
@GetMapping("/current")

/**
* Logs the user out.
* Technically, it does so by invalidating the JWT token passed in the header
* of the request.
* Also unloads all models for that user from the memory to clear some space
*
* @param body {}
*/
@PostMapping("/logout")