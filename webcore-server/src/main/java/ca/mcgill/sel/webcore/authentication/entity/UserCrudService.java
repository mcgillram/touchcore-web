package ca.mcgill.sel.webcore.authentication.entity;

import java.util.Collection;
import java.util.Optional;

/**
 * User security operations like login and logout, and CRUD operations on {@link User}.
 * 
 * @author Ridwan Kurmally
 *
 */
public interface UserCrudService {

    User save(User user);

    Optional<User> find(String id);

    Optional<User> findByUsername(String username);
    
    Collection<User> getAllUsers();
    
}
