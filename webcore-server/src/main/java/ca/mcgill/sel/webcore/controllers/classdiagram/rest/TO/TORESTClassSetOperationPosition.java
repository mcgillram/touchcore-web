package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

public class TORESTClassSetOperationPosition {
    Integer index;
    public TORESTClassSetOperationPosition(Integer index) {
        super();
        this.index = index;
    }
    public Integer getIndex() {
        return index;
    }
    public void setIndex(Integer index) {
        this.index = index;
    }
}
