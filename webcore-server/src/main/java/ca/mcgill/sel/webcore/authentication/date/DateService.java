package ca.mcgill.sel.webcore.authentication.date;

import org.joda.time.DateTime;

/**
 * Provides the current date.
 * 
 * @author Ridwan kurmally
 */
public interface DateService {

  /**
   * Provides current date at the moment of the call.
   * 
   * @return date
   */
    DateTime now();
}