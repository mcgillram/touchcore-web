package ca.mcgill.sel.webcore.controllers;

import org.json.JSONObject;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

import ca.mcgill.sel.core.COREVisibilityType;
import ca.mcgill.sel.ram.Attribute;
import ca.mcgill.sel.ram.Class;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.controller.ControllerFactory;
import ca.mcgill.sel.webcore.WebResourceMapperManager;
import ca.mcgill.sel.webcore.utils.Utils;

/**
 * Contains all the controllers related to Class.
 * 
 * @author arthurls
 *
 */
@Controller
public class ClassController {
    
    /**
     * Moves a class.
     * @param body {'aspectName': String, 'classId': int, 'xPosition': float, 'yPosition': float}
     */
    @MessageMapping("/moveClassifier")
    public void moveClassifier(String body) {
        System.out.println("/moveClassifier " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int classId = obj.getInt("classId");
        float xPosition = obj.getFloat("xPosition");
        float yPosition = obj.getFloat("yPosition");
        
        Classifier owner = (Classifier) 
                Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), classId);         
        
        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().moveClassifier(owner, xPosition, yPosition);
        MainController.lock.unlock();
    }
    
    /**
     * Changes the visibility of a Class.
     * Visibility values can be: public or concern.
     * @param body {'aspectName': String, 'classId': int, 'visibilityString': String}
     */
    @MessageMapping("/setClassVisibility")
    public void setClassVisibility(String body) {
        System.out.println("/setClassVisibility " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int classId = obj.getInt("classId");
        String visibilityString = obj.getString("visibilityString");
        
        Classifier clazz = (Classifier) 
                Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), classId);
        COREVisibilityType visibility = COREVisibilityType.get(visibilityString);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().changeClassVisibility(clazz, visibility);
        MainController.lock.unlock();
    }

    /**
     * Creates a new Attribute in the specified class.
     * @param body {'aspectName': String, 'classId': int, 'rankIndex': int, 'attributeString': String}
     */
    @MessageMapping("/createAttribute")
    public void createAttribute(String body) {
        System.out.println("/createAttribute " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int classId = obj.getInt("classId");
        int rankIndex = obj.getInt("rankIndex");
        String attributeString = obj.getString("attributeString");
        
        Class owner = (Class) 
                Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), classId);   
        
        if (rankIndex == 0) {
            rankIndex = owner.getAttributes().size();
        }

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().createAttribute(owner, 0, attributeString);
        MainController.lock.unlock();
    }
    
    /**
     * Creates a new Operation in the specified class.
     * @param body {'aspectName': String, 'classId': int, 'rankIndex': int, 'operationString': String}
     */
    @MessageMapping("/createOperation")
    public void createOperation(String body) {
        System.out.println("/createOperation " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int classId = obj.getInt("classId");
        int rankIndex = obj.getInt("rankIndex");
        String operationString = obj.getString("operationString");
        
        Class owner = (Class) Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), classId);
        
        if (rankIndex == 0) {
            rankIndex = owner.getOperations().size();
        }

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().createOperation(owner, rankIndex, operationString);
        MainController.lock.unlock();
    }
    
    /**
     * Creates a getter operation for an attribute.
     * @param body {'aspectName': String, 'attributeId': int}
     */
    @MessageMapping("/createGetterOperation")
    public void createGetterOperation(String body) {
        System.out.println("/createGetterOperation " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int attributeId = obj.getInt("attributeId");
        
        Attribute attribute = 
                (Attribute) Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), attributeId);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().createGetterOperation(attribute);
        MainController.lock.unlock();
    }
    
    /**
     * Creates a setter operation for an attribute.
     * @param body {'aspectName': String, 'attributeId': int}
     */
    @MessageMapping("/createSetterOperation")
    public void createSetterOperation(String body) {
        System.out.println("/createSetterOperation " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int attributeId = obj.getInt("attributeId");
        
        Attribute attribute = 
                (Attribute) Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), attributeId);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().createSetterOperation(attribute);
        MainController.lock.unlock();
    }
    
    /**
     * Creates a getter and a setter operation for an attribute.
     * @param body {'aspectName': String, 'attributeId': int}
     */
    @MessageMapping("/createGetterAndSetterOperation")
    public void createGetterAndSetterOperation(String body) {
        System.out.println("/createGetterAndSetterOperation " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int attributeId = obj.getInt("attributeId");
        
        Attribute attribute = 
                (Attribute) Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), attributeId);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().createGetterAndSetterOperation(attribute);
        MainController.lock.unlock();
    }
    
    /**
     * Creates a class constructor operation.
     * @param body {'aspectName': String, 'classId': int}
     */
    @MessageMapping("/createConstructor")
    public void createConstructor(String body) {
        System.out.println("/createConstructor " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int classId = obj.getInt("classId");
        
        Class owner = (Class) Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), classId);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().createConstructor(owner);
        MainController.lock.unlock();
    }
    
    /**
     * Creates a class destructor operation.
     * @param body {'aspectName': String, 'classId': int}
     */
    @MessageMapping("/createDestructor")
    public void createDestructor(String body) {
        System.out.println("/createDestructor " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int classId = obj.getInt("classId");
        
        Class owner = (Class) Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), classId);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().createDestructor(owner);
        MainController.lock.unlock();
    }
    
    /**
     * Adds a super type to a class.
     * @param body {'aspectName': String, 'subClassId': int, 'superTypeId': int}
     */
    @MessageMapping("/addSuperType")
    public void addSuperType(String body) {
        System.out.println("/addSuperType " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int subClassId = obj.getInt("subClassId");
        int superTypeId = obj.getInt("superTypeId");
        
        Class subClass = (Class) 
                Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), subClassId);
        Classifier superType = 
                (Classifier) Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), superTypeId);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().addSuperType(subClass, superType);
        MainController.lock.unlock();
    }
    
    /**
     * Removes a super type to a class. 
     * Automatically finds out which is the super type.
     * @param body {'aspectName': String, 'classId1': int, 'classId2': int}
     */
    @MessageMapping("/removeSuperType")
    public void removeSuperType(String body) {
        System.out.println("/removeSuperType " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int classId1 = obj.getInt("classId1");
        int classId2 = obj.getInt("classId2");
        
        Class class1 = (Class) Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), classId1);
        Class class2 = (Class) Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), classId2);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().removeSuperType(class1, class2);
        MainController.lock.unlock();
    }
    
    /**
     * Switches the abstract property of an Class.
     * @param body {'aspectName': String, 'classId': int}
     */
    @MessageMapping("/switchAbstract")
    public void switchAbstract(String body) {
        System.out.println("/switchAbstract " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int classId = obj.getInt("classId");
        
        Class owner = (Class) Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), classId);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().switchAbstract(owner);
        MainController.lock.unlock();
    }
}
