package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

public class TORESTClassCreateAttribute {
    String attributeName;
    Integer typeId;
    Integer rankIndex;
    
    public TORESTClassCreateAttribute(String attributeName, Integer typeId, Integer rankIndex){
        super();
        this.attributeName = attributeName;
        this.typeId = typeId;
        this.rankIndex = rankIndex;
    }
    
    public String getAttributeName() {
        return attributeName;
    }
    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }
    public Integer getTypeId() {
        return typeId;
    }
    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }
    public Integer getRankIndex() {
        return rankIndex;
    }
    public void setRankIndex(Integer rankIndex) {
        this.rankIndex = rankIndex;
    }
    
}
