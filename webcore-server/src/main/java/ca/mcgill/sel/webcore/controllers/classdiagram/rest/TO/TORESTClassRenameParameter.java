package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

public class TORESTClassRenameParameter {
    String newName;
    
    public TORESTClassRenameParameter() {
    }
    
    public TORESTClassRenameParameter(String newName) {
        this.newName = newName;
    }
    
    public String getNewName() {
        return this.newName;
    }
    public void setNewName(String newName) {
        this.newName = newName;
    }
}
