package ca.mcgill.sel.webcore.authentication.workspace.controller;

public class UserRequest {
    private String username;
    
    public UserRequest() {
    }
    
    public String getUsername() {
        return this.username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
}
