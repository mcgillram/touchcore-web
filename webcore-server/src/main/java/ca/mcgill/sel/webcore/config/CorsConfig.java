package ca.mcgill.sel.webcore.config;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Cross-Origin Resource Sharing (CORS) configuration for WebCORE to allow being called from
 * frontends hosted elsewhere.
 * 
 * @author Younes Boubekeur
 */
@Configuration
@EnableWebMvc
public class CorsConfig implements WebMvcConfigurer {
  
  /** The CORS options file, located relative to the webcore-server directory. */
  private static final String CORS_OPTIONS_PATH = "cors.xml";

  /** Allows the origins defined in the CORS options file on all mappings. */
  @Override
  public void addCorsMappings(CorsRegistry registry) {
    String[] origins = loadFrom(CORS_OPTIONS_PATH).get("allowedOrigins").replaceAll("\\s+", "").split(",");
    registry.addMapping("/**").allowedOrigins(origins);
  }
  
  /** Returns a map of the keys and values in the specified file. */
  private static Map<String, String> loadFrom(String filename) {
    Map<String, String> result = new HashMap<>();

    try {
      Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new File(filename));
      doc.getDocumentElement().normalize();
      NodeList nodeList = doc.getElementsByTagName("value");
      for (int i = 0; i < nodeList.getLength(); i++) {
        Node node = nodeList.item(i);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
          Element element = (Element) node;
          result.put(element.getAttribute("key"), element.getTextContent());
        }
      }
    } catch (SAXException | IOException | ParserConfigurationException e) {
      System.err.println("An error occurred while parsing the XML file: " + e.getMessage());
    }

    return result;
  }
  
}
