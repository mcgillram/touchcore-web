package ca.mcgill.sel.webcore.controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.json.JSONObject;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

import com.google.gson.Gson;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.CORENamedElement;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.NamedElement;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.RAMVisibilityType;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.StructuralView;
import ca.mcgill.sel.ram.Type;
import ca.mcgill.sel.ram.controller.ControllerFactory;
import ca.mcgill.sel.ram.util.RAMInterfaceUtil;
import ca.mcgill.sel.ram.util.StructuralViewUtil;
import ca.mcgill.sel.webcore.WebResourceMapperManager;
import ca.mcgill.sel.webcore.utils.Utils;

/**
 * Contains all the controllers related to Operation.
 * 
 * @author arthurls
 *
 */
@Controller
public class OperationController {

    /**
     * Removes an Operation.
     * @param body {'aspectName': String, 'operationId': int}
     */
    @MessageMapping("/removeOperation")
    public void removeOperation(String body) {
        System.out.println("/removeOperation " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int operationId = obj.getInt("operationId");
        
        Operation operation = 
                (Operation) Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), operationId);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().removeOperation(operation);
        MainController.lock.unlock();
    }
    
    /**
     * Changes the Operation return type.
     * @param body {'aspectName': String, 'operationId': int, 'operationType': String}
     */
    @MessageMapping("/setOperationType")
    public void setOperationType(String body) {
        System.out.println("/setOperationType " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int operationId = obj.getInt("operationId");
        String operationType = obj.getString("operationType");
        
        Operation operation = 
                (Operation) Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), operationId);
        EStructuralFeature feature = RamPackage.Literals.OPERATION__RETURN_TYPE;
        StructuralView structuralView = 
                EMFModelUtil.getRootContainerOfType(operation, RamPackage.Literals.STRUCTURAL_VIEW);
        Type type = StructuralViewUtil.getTypeByName(operationType, structuralView);

        MainController.lock.lock();
        if (type == null) {
            System.err.println("Return type '" + operationType + "' is not found");
        } else {
            EMFEditUtil.getPropertyDescriptor(operation, feature).setPropertyValue(operation, type);
        }
        MainController.lock.unlock();
    }
    
    /**
     * Set an Operation visibility and changes the class visibility accordingly.
     * @param body {'aspectName': String, 'operationId': int, 'newVisibility': String}
     */
    @MessageMapping("/setOperationVisibility")
    public void setOperationVisibility(String body) {
        System.out.println("/setOperationVisibility " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int operationId = obj.getInt("operationId");
        String newVisibility = obj.getString("newVisibility");
         
        Operation operation = 
                (Operation) Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), operationId);
        Classifier classifier = (Classifier) operation.eContainer();
        RAMVisibilityType visibility = StructuralViewUtil.getRamVisibilityFromStringRepresentation(newVisibility);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController()
                .changeOperationAndClassVisibility(classifier, operation, visibility);
        MainController.lock.unlock();
    }
    

    /**
     * Creates a new Parameter in the specified operation.
     * @param body {'aspectName': String, 'operationId': int, 'rankIndex': int, 'parameterString': String}
     */
    @MessageMapping("/createParameter")
    public void createParameter(String body) {
        System.out.println("/createParameter " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int operationId = obj.getInt("operationId");
        int rankIndex = obj.getInt("rankIndex");
        String parameterString = obj.getString("parameterString");
        
        Operation operation = 
                (Operation) Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), operationId);
        
        if (rankIndex == 0) {
            rankIndex = operation.getParameters().size();
        }

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().createParameter(operation, rankIndex, parameterString);
        MainController.lock.unlock();
    }
    
    /**
     * Removes a Parameter.
     * @param body {'aspectName': String, 'parameterId': int}
     */
    @MessageMapping("/removeParameter")
    public void removeParameter(String body) {
        System.out.println("/removeParameter " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int parameterId = obj.getInt("parameterId");
        
        Parameter parameter = 
                (Parameter) Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), parameterId);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().removeParameter(parameter);
        MainController.lock.unlock();
    }
    
    /**
     * Changes the Parameter type.
     * @param body {'aspectName': String, 'parameterId': int, 'parameterType': String}
     */
    @MessageMapping("/setParameterType")
    public void setParameterType(String body) {
        System.out.println("/setParameterType " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int parameterId = obj.getInt("parameterId");
        String parameterType = obj.getString("parameterType");
        
        Parameter parameter = 
                (Parameter) Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), parameterId);
        EStructuralFeature feature = RamPackage.Literals.PARAMETER__TYPE;
        StructuralView structuralView = 
                EMFModelUtil.getRootContainerOfType(parameter, RamPackage.Literals.STRUCTURAL_VIEW);
        Type type = StructuralViewUtil.getAttributeTypeByName(parameterType, structuralView);

        MainController.lock.lock();
        EMFEditUtil.getPropertyDescriptor(parameter, feature).setPropertyValue(parameter, type);
        MainController.lock.unlock();
    }
    
    /**
     * Switches the abstract property of an Operation.
     * @param body {'aspectName': String, 'operationId': int}
     */
    @MessageMapping("/switchOperationAbstract")
    public void switchOperationAbstract(String body) {
        System.out.println("/switchOperationAbstract " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int operationId = obj.getInt("operationId");
        
        Operation operation = 
                (Operation) Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), operationId);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().switchOperationAbstract(operation);
        MainController.lock.unlock();
    }
    
    /**
     * Switches the static property of an Operation.
     * @param body {'aspectName': String, 'operationId': int}
     */
    @MessageMapping("/switchOperationStatic")
    public void switchOperationStatic(String body) {
        System.out.println("/switchOperationStatic " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int operationId = obj.getInt("operationId");
        
        Operation operation = 
                (Operation) Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), operationId);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().switchOperationStatic(operation);
        MainController.lock.unlock();
    }
    
    /**
     * Gets an operation possible return types.
     * @param body {'aspectName': String, 'eObjectId': int}
     * @return a json array with the possible types for an operation (just the names).
     */
    @MessageMapping("/getPossibleOperationTypes")
    @SendToUser("/queue/getPossibleOperationTypes")
    public String getPossibleOperationTypes(String body) {
        System.out.println("/getPossibleOperationTypes " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int eObjectId = obj.getInt("eObjectId");
        
        EObject eObject = Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), eObjectId);
        
        Collection<EObject> choices = RAMInterfaceUtil.getOperationReturnTypes(eObject);
        List<String> result = new ArrayList<>();
        
        // Iterate once for only get the names 
        for (Iterator<EObject> iterator = choices.iterator(); iterator.hasNext(); ) {
            EObject elem = iterator.next();
            
            result.add(((NamedElement) elem).getName());
        }
        
        // List to json string to properly send it to the client
        Gson gson = new Gson();
        String array = gson.toJson(result);
        
        return array;
    }
    
    /**
     * Gets a parameter possible types.
     * @param body {'aspectName': String, 'eObjectId': int}
     * @return a json array with the possible types for a parameter (just the names).
     */
    @MessageMapping("/getPossibleParameterTypes")
    @SendToUser("/queue/getPossibleParameterTypes")
    public String getPossibleParameterTypes(String body) {
        System.out.println("/getPossibleParameterTypes " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int eObjectId = obj.getInt("eObjectId");
        
        EObject eObject = Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), eObjectId);
        
        Collection<EObject> choices = RAMInterfaceUtil.getAvailableParameterTypes(eObject);
        List<String> result = new ArrayList<>();
        
        // Iterate once for only get the names
        for (Iterator<EObject> iterator = choices.iterator(); iterator.hasNext(); ) {
            EObject elem = iterator.next();
            
            result.add(((NamedElement) elem).getName());
        }
        
        // List to json string to properly send it to the client
        Gson gson = new Gson();
        String array = gson.toJson(result);

        return array;
    }
}
