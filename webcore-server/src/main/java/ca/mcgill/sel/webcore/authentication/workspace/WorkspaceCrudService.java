package ca.mcgill.sel.webcore.authentication.workspace;

import java.util.List;
import java.util.Optional;

/**
 * Service related to {@link Workspace}. Implementation allows saving and finding workspaces 
 * by id or by user name and model name.
 * 
 * @author Ridwan Kurmally
 *
 */
public interface WorkspaceCrudService {
    
    // TODO : Might have to add a remove functionality later. Especially when
    // we will have a rest point for deleting models + workspace
    
    /**
     * Creates a {@link Workspace} according to the passed information.
     * It also saves it for later access via {@link WorkspaceCrudService#find(String owner, String modelName) 
     * find(String, String)}.
     * 
     * @param owner the user name of owner
     * @param modelName the name of the model
     * @param usersWithReadAcess list of names of users with read permissions to that workspace
     * @param usersWithWriteAcess list of names of users with write permissions to that workspace
     * @return the newly created workspace 
     */
    Workspace save(String owner, String modelName, List<String> usersWithReadAcess, List<String> usersWithWriteAcess);
    
    /**
     * Returns the workspace associated to a specific model and user.
     * 
     * @param owner user name of the owner
     * @param modelName name of the model
     * @return Optional value of workspace.
     */
    Optional<Workspace> find(String owner, String modelName);
    
    /**
     * Get all workspaces for every user.
     * 
     * @return list of every workspace
     */
    List<Workspace> getAllWorkspaces();
    
    /**
     * Returns all workspaces belonging to a specific user.
     * 
     * @param owner the user name of the owner
     * @return List of workspaces or null if owner does not exist
     */
    List<Workspace> getAllWorkspaces(String owner);

}
