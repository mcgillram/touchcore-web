package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

public class TORESTClassDiagramExtendModel {
    String newModelName;

    public String getNewModelName() {
        return newModelName;
    }

    public void setNewModelName(String newModelName) {
        this.newModelName = newModelName;
    }

    public TORESTClassDiagramExtendModel(String newModelName) {
        super();
        this.newModelName = newModelName;
    }
    
}
