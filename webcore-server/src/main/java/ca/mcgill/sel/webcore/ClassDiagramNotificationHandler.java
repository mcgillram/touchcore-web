package ca.mcgill.sel.webcore;

import java.util.EventObject;

import org.eclipse.emf.common.command.BasicCommandStack;
import org.eclipse.emf.common.command.CommandStackListener;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.springframework.messaging.MessagingException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.BiMap;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.Association;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.webcore.controllers.MainController;
import ca.mcgill.sel.webcore.utils.Utils;

/**
 * NotificationHandler handles all the notifications coming from the EMF model and the command stack.
 * 
 * @author arthurls
 *
 */
public class ClassDiagramNotificationHandler {
    
    private ClassDiagram aspect;
    private BiMap<EObject, Integer> idMapping;
    private int idCounter;
    private WebResourceMapper webResourceMapper;

    private JsonArray notificationStack;
    
    /**
     * Constructor called from the WebResourceMapper class.
     * 
     * @param webResourceMapper that calls the NotificationHandler
     */
    public ClassDiagramNotificationHandler(WebResourceClassDiagramMapper webResourceMapper) {
        this.webResourceMapper = webResourceMapper;
        this.aspect = webResourceMapper.getClassDiagram();
        this.idMapping = webResourceMapper.getIdMapping();
        this.idCounter = webResourceMapper.getIdCounter();
    }
    
    /**
     * Adds a notification to the notification stack.
     * Allows the API to transform multiple EMF notification into one for the client.
     * The "commandStackChanged" notification sends the stack to the clients.
     * The notificationType parameter may not be the same as the one sent by EMF. See isSpecialCase for more info.
     * @param eObject the eObject to be updated
     * @param notification the EMF notification
     * @param notificationType add, set or remove
     */
    private void stackNotification(EObject eObject, Notification notification, int notificationType) {
        JsonObject jsonNotification = new JsonObject();
        jsonNotification.addProperty("notificationType", Utils.notificationToString(notificationType));
        // makes sure we don't send a notification with an empty containerID
        if (Utils.notificationToString(notificationType).equals("REMOVE")) {
            jsonNotification.addProperty("containerId", idMapping.get(notification.getNotifier()));
            if (idMapping.get(notification.getNotifier()) == null) {
                return;
            }   
        } else {
            jsonNotification.addProperty("containerId", idMapping.get(eObject.eContainer()));
            if (idMapping.get(eObject.eContainer()) == null) {
                return;
            }
        }
        jsonNotification.addProperty("eType", eObject.eClass().getName());
        try {
            JsonParser parser = new JsonParser();
            jsonNotification.add("value", parser.parse(webResourceMapper.toJson(eObject)));
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        
        notificationStack.add(jsonNotification);
    }
    
    /**
     * Handle the EMF notification behavior.
     */
    public void initNotifications() {
        notificationStack = new JsonArray();
        
        BasicCommandStack bcs = EMFEditUtil.getCommandStack(aspect);
        
        bcs.addCommandStackListener(new CommandStackListener() {
            @Override
            public void commandStackChanged(final EventObject event) { 
                if (bcs.getMostRecentCommand() != null) {
                    MainController.flushCDUpdates(aspect, notificationStack);
                    notificationStack = new JsonArray();
                }
            }
        });
        
        // Model notification (Class, Enum, Operation etc...)
        
        aspect.eAdapters().add(new EContentAdapter() {
            @Override
            public void notifyChanged(Notification notification) {
                super.notifyChanged(notification);
                boolean isSpecialCase = handleSpecialCase(notification);
                if (!isSpecialCase) {
                    if (notification.getEventType() == Notification.ADD) {
                        idCounter++;
                        EObject eObj = (EObject) notification.getNewValue();
                        idMapping.put(eObj, idCounter);
                    }
                    EObject modified = getUpdatedObject(notification);
                    if (modified != null) {
                        stackNotification(modified, notification, notification.getEventType());
                    }
                }
            }
        });
        
        // Layout notification (Class creation, Class deletion and Class positional move )
        aspect.getLayout().eAdapters().add(new EContentAdapter() {
            @Override
            public void notifyChanged(Notification notification) {
                super.notifyChanged(notification);
                if (notification.getFeature() == RamPackage.Literals.LAYOUT_ELEMENT__Y) {
                    if (notification.getEventType() == Notification.SET) {
                        EObject eObj = (EObject) notification.getNotifier();
                        stackNotification(eObj, notification, Notification.SET);
                    }
                } else if (notification.getFeature() == RamPackage.Literals.CONTAINER_MAP__VALUE) {
                    switch (notification.getEventType()) {
                        case Notification.ADD:
                            idCounter++;
                            EObject eObj = (EObject) notification.getNewValue();
                            idMapping.put(eObj, idCounter);
                            stackNotification(eObj, notification, Notification.ADD);
                            break;
                        case Notification.REMOVE:
                            EObject eObj2 = (EObject) notification.getOldValue();
                            stackNotification(eObj2, notification, Notification.REMOVE);
                            break;
                        case Notification.SET:
                            break;
                    }
                }
            }
        });
    }
    
    /**
     * Returns the EObject that has been modified.
     * @param notification to handle
     * @return the EObject that has to be updated
     */
    private EObject getUpdatedObject(Notification notification) {        
        switch (notification.getEventType()) {
            case Notification.REMOVE:
                return (EObject) notification.getOldValue();
            case Notification.ADD:
                return (EObject) notification.getNewValue();
            case Notification.SET:
                return (EObject) notification.getNotifier();
            default:
                return null;
        }      
    }

    /**
     * Checks if the notification matches a specific case where the general approach doesn't fit.
     * If it is, it handles the notification and doesn't go though the general approach.
     * @param notification to handle
     * @return True if notification has been handled. False if default behavior works
     */
    private boolean handleSpecialCase(Notification notification) {
        boolean isSpecialCase = false;
        // Creates ids for the parameters as well when a class is created e.g. <+ void sell(int param)>
        if (notification.getFeature() == RamPackage.Literals.CLASSIFIER__OPERATIONS) {
            if (notification.getEventType() == Notification.ADD) {
                Operation operation = (Operation) notification.getNewValue();
                idCounter++;
                idMapping.put(operation, idCounter);
                for (int i = 0; i < operation.getParameters().size(); i++) {
                    idCounter++;
                    idMapping.put(operation.getParameters().get(i), idCounter);
                } 
                stackNotification(operation, notification, Notification.ADD);
                isSpecialCase = true;
            }
        }
        // We change the ADD and REMOVE supertype class notification into SET
        // Since we do not actually create or remove the Class
        else if (notification.getFeature() == RamPackage.Literals.CLASSIFIER__SUPER_TYPES) {
            if (notification.getEventType() == Notification.ADD || notification.getEventType() == Notification.REMOVE) {
                stackNotification((EObject) notification.getNotifier(), notification, Notification.SET);
                return true;
            }
        }
        // Special AssociationEnd creation notification
        // Wait for the Association to be declared (not idMapped yet)
        // Returning a default eObject skips the default behavior
        else if (notification.getFeature() == RamPackage.Literals.CLASSIFIER__ASSOCIATION_ENDS) {
            if (notification.getEventType() == Notification.ADD) {
                idCounter++;
                idMapping.put((EObject) notification.getNewValue(), idCounter);
                isSpecialCase = true;
            }
        }
        // After the AssociationEnd are mapped, we can send the notification for
        // both AssociationEnds and the Association
        else if (notification.getFeature() == RamPackage.Literals.STRUCTURAL_VIEW__ASSOCIATIONS) {
            if (notification.getEventType() == Notification.ADD) {
                Association assoc = (Association) notification.getNewValue();
                idCounter++;
                idMapping.put(assoc, idCounter);
                // (Notification) Add Association End to Class 2
                stackNotification(assoc.getEnds().get(0), notification, Notification.ADD);
                // (Notification) Add Association End to Class 1
                stackNotification(assoc.getEnds().get(1), notification, Notification.ADD);
                // (Notification) Create the Association
                stackNotification(assoc, notification, Notification.ADD);
                isSpecialCase = true;
            }
        }
        return isSpecialCase;
    }

}
