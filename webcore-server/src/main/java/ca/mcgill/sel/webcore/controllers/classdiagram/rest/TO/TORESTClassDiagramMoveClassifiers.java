package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

import java.util.Map;

import ca.mcgill.sel.classdiagram.LayoutElement;

public class TORESTClassDiagramMoveClassifiers {
    Map<Integer, LayoutElement> positionMap;
    public TORESTClassDiagramMoveClassifiers(Map<Integer, LayoutElement> positionMap) {
        super();
        this.positionMap = positionMap;
    }

    public Map<Integer, LayoutElement> getPositionMap() {
        return positionMap;
    }

    public void setPositionMap(Map<Integer, LayoutElement> positionMap) {
        this.positionMap = positionMap;
    }
}
