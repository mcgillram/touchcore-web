package ca.mcgill.sel.webcore.controllers;

import java.io.IOException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import org.eclipse.emf.common.command.BasicCommandStack;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREPartialityType;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.controller.COREControllerFactory;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.webcore.WebResourceClassDiagramMapperManager;
//import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.webcore.WebResourceMapperManager;
import ca.mcgill.sel.webcore.utils.Utils;

/**
 * Main Controller.
 * 
 * @author arthurls
 *
 */
@Controller
@Service
public final class MainController {

    /**
     * Global Lock.
     */
    public static ReentrantLock lock;
    
    private static SimpMessagingTemplate template;
    
    /**
     * Main Controller constructor.
     * @param template 
     */
    @Autowired
    private MainController(SimpMessagingTemplate template) {
        MainController.template = template;
    }
    
    /**
     * Returns the model list from the concern selected by the TouchRam user.
     * Send to /user/queue/getModels the model.
     * @return the model in JSON
     * @throws Exception 
     */
    @MessageMapping("/getModels")
    @SendToUser("/queue/getModels")
    public String getModels() throws Exception {
        System.out.println("/getModels");
        COREConcern concern = null;

        concern = WebResourceClassDiagramMapperManager.getConcern();
        if (concern == null)  concern = WebResourceClassDiagramMapperManager.getConcern();


        if (concern != null) {
            List<COREArtefact> artefacts = concern.getArtefacts();
            JsonArray modelNames = new JsonArray();
            for (COREArtefact artefact : artefacts) {
                if (artefact instanceof COREExternalArtefact) {
                    if (((COREExternalArtefact) artefact).getRootModelElement() instanceof Aspect) {
                        Aspect aspect = (Aspect) ((COREExternalArtefact) artefact).getRootModelElement();
                        modelNames.add(aspect.getName());
                    }else if (((COREExternalArtefact) artefact).getRootModelElement() instanceof ClassDiagram) {
                    	ClassDiagram aspect = (ClassDiagram) ((COREExternalArtefact) artefact).getRootModelElement();
                        modelNames.add(aspect.getName());
                    }
                }
                
            }
            return modelNames.toString();
        }

        return "[]";
    }

    // TODO : Modify body to pass a username to denote a user
    /**
     * Returns the model.
     * Send to /user/queue/getAspect the model.
     * @param sessionId of the user 
     * @param body {'aspectName': String}
     * @return the model in JSON
     * @throws Exception 
     */
    @MessageMapping("/getAspect")
    @SendToUser("/queue/getAspect")
    public String getAspect(@Header ("simpSessionId") String sessionId, String body) throws Exception {
        System.out.println("/getAspect " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        CollabController.setUserAspect(sessionId, aspectName);
        if ( WebResourceMapperManager.getWebResourceMapperDB()!=null &&
        		!WebResourceMapperManager.getWebResourceMapperDB().containsKey(aspectName)) {
            WebResourceMapperManager.addModelManager(aspectName);
            String i = WebResourceMapperManager.getSingleMapper(aspectName).toJson();
            System.out.println(i);
            return i;
        }
        if (WebResourceClassDiagramMapperManager.getWebResourceMapperDB()!=null &&
        		!WebResourceClassDiagramMapperManager.getWebResourceMapperDB().containsKey(aspectName)) {
        	WebResourceClassDiagramMapperManager.addModelManager(aspectName);
        	String i= WebResourceClassDiagramMapperManager.getSingleMapper(aspectName).toJson(); 
        	System.out.println(i);
        	return i;
        }
        return "";
        
    }
        
    /**
    * Sends the changed EObjects stack to the client.
    * Client has to be subscribed to /topic/update.
    * @param aspect notification stack is related to that aspect
    * @param notificationStack the stack of 1 or more notifications
    */
    public static void flushUpdates(Aspect aspect, JsonArray notificationStack) {
        System.out.println("Flush Updates");
        
        JsonObject result = new JsonObject();
        result.addProperty("aspect", aspect.getName());
        result.add("stack", notificationStack);
        
        System.out.println(result.toString());
        template.convertAndSend("/topic/update", result.toString());
    }
    public static void flushCDUpdates(ClassDiagram aspect, JsonArray notificationStack) {
        System.out.println("Flush Updates");
        
        JsonObject result = new JsonObject();
        result.addProperty("aspect", aspect.getName());
        result.add("stack", notificationStack);
        
        System.out.println(result.toString());
        template.convertAndSend("/topic/update", result.toString());
    }
    
    /**
     * Changes the name of an EObjectName (Class, Enum, Operation ...).
     * @param body {'aspectName': String, 'eObjectId': int, 'objectName': String}
     */
    @MessageMapping("/setEObjectName")
    public void setEObjectName(String body) {
        System.out.println("/setEObjectName " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int eObjectId = obj.getInt("eObjectId");
        String objectName = obj.getString("objectName");
        
        EObject eObject = Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), eObjectId);
        EStructuralFeature feature = CorePackage.Literals.CORE_NAMED_ELEMENT__NAME;
        Object value = EcoreUtil.createFromString((EDataType) feature.getEType(), objectName);

        lock.lock();
        EMFEditUtil.getPropertyDescriptor(eObject, feature).setPropertyValue(eObject, value);
        lock.unlock();
    }
    
    /**
     * Changes the partiality of a COREModelElement.
     * Partiality values can be: none, concern or public.
     * Parameters and Implementation Class can not be partial.
     * @param body {'aspectName': String, 'eObjectId': int, 'partiality': String}
     */
    @MessageMapping("/setEObjectPartiality")
    public void setClassPartiality(String body) {
        System.out.println("/setClassPartiality " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int eObjectId = obj.getInt("eObjectId");
        String partialityString = obj.getString("partiality");
        
        EObject eObj = Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), eObjectId);
        COREPartialityType partiality = COREPartialityType.get(partialityString);
        
        // ConcernController extends BaseController which we need
        // We could have used the ContributionController or the FeatureController as well
        lock.lock();
        COREControllerFactory.INSTANCE.getConcernController().setCOREPartialityType(eObj, partiality); 
        lock.unlock();
    }
    
    /**
     * Saves the model in the .ram file.
     * @param body {'aspectName': String}
     * @throws IOException 
     */
    @MessageMapping("/save")
    public void save(String body) throws IOException {
        System.out.println("/save " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        final Map<Object, Object> saveOptions = new HashMap<Object, Object>();
        saveOptions.put(
                Resource.OPTION_SAVE_ONLY_IF_CHANGED,
                Resource.OPTION_SAVE_ONLY_IF_CHANGED_MEMORY_BUFFER);
        WebResourceMapperManager.getSingleAspect(aspectName).eResource().save(saveOptions);

        lock.lock();
        BasicCommandStack commandStack = 
                EMFEditUtil.getCommandStack(WebResourceMapperManager.getSingleAspect(aspectName));
        commandStack.saveIsDone();
        lock.unlock();
    }
    
    /**
     * Undo the last command.
     * Send to /topic/aspect the model.
     * @param body {'aspectName': String}
     * @return notification message
     * @throws Exception 
     */
    @MessageMapping("/undo")
    @SendTo("/topic/aspect")
    public String undo(String body) throws Exception {
        System.out.println("/undo " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        BasicCommandStack bcs = EMFEditUtil.getCommandStack(WebResourceMapperManager.getSingleAspect(aspectName));

        lock.lock();
        if (bcs.canUndo()) {
            bcs.undo();
        }
        lock.unlock();
        return WebResourceMapperManager.getSingleMapper(aspectName).toJson();
    }
    
    /**
     * Redo the last command.
     * Send to /topic/aspect the model.
     * @param body {'aspectName': String}
     * @return notification message
     * @throws Exception 
     */
    @MessageMapping("/redo")
    @SendTo("/topic/aspect")
    public String redo(String body) throws Exception {
        System.out.println("/redo " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        BasicCommandStack bcs = EMFEditUtil.getCommandStack(WebResourceMapperManager.getSingleAspect(aspectName));

        lock.lock();
        if (bcs.canRedo()) {
            bcs.redo();
        }
        bcs.saveIsDone();
        lock.unlock();
        return WebResourceMapperManager.getSingleMapper(aspectName).toJson();
    }    
}
