package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

public class TORESTClassRenameClass {
    String newName;
    
    public TORESTClassRenameClass() {
    }
    
    public TORESTClassRenameClass(String newName) {
        this.newName = newName;
    }
    
    public String getNewName() {
        return newName;
    }
    public void setNewName(String newName) {
        this.newName = newName;
    }
}
