package ca.mcgill.sel.webcore.debug.controller;

import static lombok.AccessLevel.PACKAGE;
import static lombok.AccessLevel.PRIVATE;

import java.io.File;
import java.util.Collection;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ca.mcgill.sel.webcore.WebResourceClassDiagramMapper;
import ca.mcgill.sel.webcore.WebResourceClassDiagramMapperManager;
import ca.mcgill.sel.webcore.authentication.entity.User;
import ca.mcgill.sel.webcore.authentication.entity.UserCrudService;
import ca.mcgill.sel.webcore.authentication.workspace.Workspace;
import ca.mcgill.sel.webcore.authentication.workspace.WorkspaceCrudService;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;

/**
 * Utility rest controller which contains rest points that are useful when debugging WebCore.
 * Note that some of these rest points can pose a privacy/security breach. It is advisable to disable this controller
 * before launching WebCore in production.
 * 
 * @author Ridwan Kurmally
 *
 */
@RestController
@RequestMapping("debug")
@FieldDefaults(level = PRIVATE, makeFinal = true)
@AllArgsConstructor(access = PACKAGE)
public class DebugController {

    @NonNull
    WorkspaceCrudService workspaces;
    
    @NonNull
    UserCrudService users;

    /**
     * Shows all registered users.
     * 
     * @return JSON of all registered users
     */
    @GetMapping("showRegisteredUsers")
    public Collection<User> showRegisteredUsers() {
        return users.getAllUsers();
    }
    
    /**
     * Shows all workspaces for every user. 
     * 
     * @return String representation of all workspaces.
     */
    @GetMapping("showAllWorkspaces")
    public String showAllWorkspaces() {
        StringBuilder result = new StringBuilder();
        for (Workspace workspace : workspaces.getAllWorkspaces()) {
            result.append("-------------\n" + workspace.toString());
        }
        result.append("-------------");
        return result.toString();
    }
    
    /**
     * Shows all users and models.
     * 
     * @return String representation of all users and names of model belonging to each
     */
    @GetMapping("showUsersAndModels")
    String showUsersAndModels() {
        StringBuilder result = new StringBuilder();
        File dir = new File(WebResourceClassDiagramMapperManager.getUserConcernsRootPath());
        File[] usersDir = dir.listFiles();
        for (File userDir : usersDir) {
            result.append(userDir.getName() + "\n");
            File[] userModels = userDir.listFiles();
            for (File userModel : userModels) {
                result.append("\t-" + userModel.getName() + "\n");
            }
        }
        if (result.length() == 0) {
            return "No user workspace.";
        }
        return result.toString();
    }
    
    /**
     * Shows all models and their respective users that are currently loaded in memory.
     * 
     * @return String representation of all models loaded in memory along with their respective user
     */
    @GetMapping("showLoadedModels")
    String showLoadedModels() {
        Map<String, Map<String, WebResourceClassDiagramMapper>> userToWebResourceMapperDB = 
                WebResourceClassDiagramMapperManager.getUserToWebResourceMapperDB();
        StringBuilder result = new StringBuilder();
        for (String username : userToWebResourceMapperDB.keySet()) {
            Map<String, WebResourceClassDiagramMapper> aspectToModelMap = userToWebResourceMapperDB.get(username);
            result.append(username + "\n");
            for (String aspect : aspectToModelMap.keySet()) {
                result.append("\t-" + aspect + "\n");
            }
        }
        if (result.length() == 0) {
            return "No model currently loaded in memory.";
        }
        return result.toString();
    }
    
}
