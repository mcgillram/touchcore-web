package ca.mcgill.sel.webcore.authentication.workspace;

import java.util.ArrayList;
import java.util.List;

import ca.mcgill.sel.webcore.WebResourceClassDiagramMapperManager;
import lombok.Value;
import lombok.experimental.NonFinal;

/**
 * This object represents the workspace for a specific user and model. Each model should have
 * a Workspace instance associated with it in the object {@link ca.mcgill.sel.webcore.WebResourceClassDiagramMapper 
 * WebResourceClassDiagramMapper}. The workspace will keep track of who the owner is and what kind of permissions 
 * other users have concerning the model in that workspace.
 * 
 * A workspace keeps track of users with read access - only allowed to access GET end points. It also keeps
 * track of users with write access - allowed to access any GET/POST/PUT/DELETE end point. The workspace can also
 * be made public for read or write.
 * 
 * @author Ridwan Kurmally
 */
@Value
public class Workspace {
    
    /**
     * Tracks the number of workspaces created so far.
     */
    private static int workspaceIdCounter;
    
    /**
     * Unique Identifier for the workspace.
     */
    private int workspaceId;
    
    /**
     * Name of user who owns this workspace.
     */
    private String owner;
    
    /**
     * Name of model/aspect in that workspace.
     */
    private String modelName;
    
    /**
     * Path to the workspace.
     */
    private String path;
    
    /**
     * List of the name of the users with explicit read access to that workspace.
     * These users are only allowed to perform GET operations on the model in that workspace
     */
    private List<String> usersWithReadAcess;
    
    /**
     * List of the name of the users with explicit write access to that workspace.
     * These users are allowed to perform GET/PUT/POST/DELETE operations on the model in that workspace
     */
    private List<String> usersWithWriteAcess;
    
    /**
     * Whether this workspace can be read by anyone irrespectively.
     */
    @NonFinal private boolean PUBLIC_READ = false;
    
    /**
     * Whether this workspace can be edited by anyone irrespectively.
     */
    @NonFinal private boolean PUBLIC_WRITE = false;
    
    public Workspace(String owner, String modelName) {
        this.owner = owner;
        this.modelName = modelName;
        this.path = WebResourceClassDiagramMapperManager.getPath(owner, modelName);
        this.usersWithReadAcess = new ArrayList<>();
        this.usersWithWriteAcess = new ArrayList<>();
        this.workspaceId = ++workspaceIdCounter;
    }
    
    public Workspace(String owner, String modelName, 
            List<String> usersWithReadAcess, List<String> usersWithWriteAcess) {
        this.owner = owner;
        this.modelName = modelName;
        this.path = WebResourceClassDiagramMapperManager.getPath(owner, modelName);
        this.usersWithReadAcess = usersWithReadAcess;
        this.usersWithWriteAcess = usersWithWriteAcess;
        this.workspaceId = ++workspaceIdCounter;
    }
    
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("Workspace ID : " + this.workspaceId + "\n");
        result.append("Owner : " + this.owner + "\n");
        result.append("Model : " + this.modelName + "\n");
        result.append("Path : " + this.path + "\n");
        result.append("Users with explicit read access rights : " + this.usersWithReadAcess.toString() + "\n");
        result.append("Users with explicit write access rights : " + this.usersWithWriteAcess.toString() + "\n");
        result.append("Public Read Status : " + this.PUBLIC_READ + "\n");
        result.append("Public Write Status : " + this.PUBLIC_WRITE + "\n");
        return result.toString();
    }
    
    /**
     * Return whether the user is the owner.
     * 
     * @param username
     * @return true only if the user is the owner
     */
    public boolean isOwner(String username) {
        return this.owner.equals(username);
    }
    
    /**
     * Return whether the user has read access to that workspace.
     * 
     * @param username
     * @return true only if the user has that permission
     */
    public boolean hasReadAccess(String username) {
        return this.PUBLIC_READ || isOwner(username) || this.usersWithReadAcess.contains(username);
    }
    
    /**
     * Return whether the user has write access to that workspace.
     * 
     * @param username
     * @return true only if the user has that permission
     */
    public boolean hasWriteAccess(String username) {
        return this.PUBLIC_WRITE || isOwner(username) || this.usersWithWriteAcess.contains(username);
    }
    
    /**
     * Grant the user explicit reading permission over that workspace.
     * 
     * @param username
     * @return true if permission successfully granted or false if user already has that permission
     */
    public boolean grantReadAccess(String username) {
        if (!isOwner(username) && !this.usersWithReadAcess.contains(username)) {
            this.usersWithReadAcess.add(username);
            return true;
        } 
        return false;
    }
    
    /**
     * Revokes explicit reading permission from the user over that workspace.
     * 
     * @param username
     * @return true if permission successfully revoked or false if user already does not have that permission or the 
     * user is the owner
     */
    public boolean revokeReadAccess(String username) {
        if (this.usersWithReadAcess.contains(username)) {
            this.usersWithReadAcess.remove(username);
            return true;
        }
        return false;
    }
    
    /**
     * Grant the user explicit writing permission over that workspace.
     * 
     * @param username
     * @return true if permission successfully granted or false if user already has that permission
     */
    public boolean grantWriteAccess(String username) {
        if (!isOwner(username) && !this.usersWithWriteAcess.contains(username)) {
            this.usersWithWriteAcess.add(username);
            return true;
        }
        return false;
    }
    
    /**
     * Revokes explicit writing permission from the user over that workspace.
     * 
     * @param username
     * @return true if permission successfully revoked or false if user already does not have that permission or the 
     * user is the owner
     */
    public boolean revokeWriteAccess(String username) {
        if (this.usersWithReadAcess.contains(username)) {
            this.usersWithReadAcess.remove(username);
            return true;
        }
        return false;
    }
    
    /**
     * Setter for the field PUBLIC_READ.
     * 
     * Pass in true so that anyone can read from this workspace. Note that the list of users with 
     * explicit read access rights, previous to that call, will not be changed and can still be updated. 
     * Pass in false to revert the workspace back to private status and
     * only the owner and those users with explicit read access will be allowed to read from it.
     * 
     * @param status
     */
    public void setPublicRead(boolean status) {
        this.PUBLIC_READ = status;
    }
    
    /**
     * Setter for the field PUBLIC_WRITE.
     * 
     * Pass in true so that anyone can read from/write to this workspace. Note that the list of users with 
     * explicit write access rights, previous to that call, will not be changed and can still be updated. 
     * Pass in false to revert the workspace back to private status and
     * only the owner and those users with explicit write access will be allowed to read from/write to it.
     * 
     * @param status
     */
    public void setPublicWrite(boolean status) {
        this.PUBLIC_WRITE = status;
    }
    
    /**
     * Revokes explicit read access permissions from all users.
     */
    public void revokeAllReadAccess() {
        this.usersWithReadAcess.clear();
    }
    
    /**
     * Revokes explicit write access permissions from all users.
     */
    public void revokeAllWriteAccess() {
        this.usersWithWriteAcess.clear();
    }
    
}
