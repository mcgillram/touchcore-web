package ca.mcgill.sel.webcore.controllers.classdiagram.rest;

import ca.mcgill.sel.classdiagram.ImplementationClass;
import ca.mcgill.sel.classdiagram.Type;
import ca.mcgill.sel.classdiagram.language.controller.ControllerFactory;
import ca.mcgill.sel.webcore.BroadcastContentManagerMapper;
import ca.mcgill.sel.webcore.WebResourceClassDiagramMapperManager;
import ca.mcgill.sel.webcore.authentication.entity.User;
import ca.mcgill.sel.webcore.authentication.workspace.WorkspaceAccessUtils;
import ca.mcgill.sel.webcore.controllers.MainController;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTImplementationClassCreateConstructor;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTImplementationClassCreateOperation;
import ca.mcgill.sel.webcore.utils.Utils;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("{username}/classdiagram/{cdmName}/implementationClass")
public class CdmImplementationClassRestController {

    private static final String PARAMETER_DELIMITER_PATTERN = ",\\s?";
    
    /**
     * Adds a constructor to the implementation class.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param classId id of class
     * @param TO {"constructorName": String, "visibility": VisibilityType, "parameters": List<Parameter>}
     **/
    @PostMapping("{classId}/constructor")
    public void createConstructor(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer classId, 
            @RequestBody TORESTImplementationClassCreateConstructor TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        ImplementationClass owner = (ImplementationClass) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), classId);        

        if (TO.getVisibility() == null) {
            throw new IllegalArgumentException("The visibility is not valid");
        }

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getImplementationClassController()
            .addConstructor(owner, TO.getConstructorName(), TO.getVisibility(), TO.getParameters());
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Adds an operation to the implementation class.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param classId id of class
     * @param TO {"index": Integer, "operationName": String, "visibility": VisibilityType,
     *  "returnTypeId": Integer, "parameters": List<Parameter>, "isStatic": boolean}
     **/
    @PostMapping("{classId}/operation")
    public void createOperation(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer classId, 
            @RequestBody TORESTImplementationClassCreateOperation TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        ImplementationClass owner = (ImplementationClass) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), classId);

        if (TO.getIndex() == 0) {
            TO.setIndex(owner.getOperations().size());
        }

        if (TO.getVisibility() == null) {
            throw new IllegalArgumentException("The visibility is not valid");
        }
        
        Type returnType = (Type) Utils.getEObjectById(WebResourceClassDiagramMapperManager
                .getSingleIdMapping(username, cdmName), TO.getReturnTypeId());

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getImplementationClassController().addOperation(owner,
                TO.getIndex(), TO.getOperationName(), TO.getVisibility(),
                returnType, TO.getParameters(), TO.isStatic());
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
}
