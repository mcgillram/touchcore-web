package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

public class TORESTEnumRenameLiteral {
    String newName;
    
    public TORESTEnumRenameLiteral() {
    }
    
    public TORESTEnumRenameLiteral(String newName) {
        this.newName = newName;
    }
    
    public String getNewName() {
        return this.newName;
    }
    public void setNewName(String newName) {
        this.newName = newName;
    }
}
