package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

public class TORESTClassCreateParameter {
    Integer index;
    String paraName;
    Integer typeId;
    public TORESTClassCreateParameter(Integer index, String paraName, Integer typeId) {
        super();
        this.index = index;
        this.paraName = paraName;
        this.typeId = typeId;
    }
    public Integer getIndex() {
        return index;
    }
    public void setIndex(Integer index) {
        this.index = index;
    }
    public String getParaName() {
        return paraName;
    }
    public void setParaName(String paraName) {
        this.paraName = paraName;
    }
    public Integer getTypeId() {
        return typeId;
    }
    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }
}
