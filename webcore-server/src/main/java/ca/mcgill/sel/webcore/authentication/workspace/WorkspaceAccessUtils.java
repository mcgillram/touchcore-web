package ca.mcgill.sel.webcore.authentication.workspace;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ca.mcgill.sel.webcore.authentication.entity.UserCrudService;

/**
 * Utility class with helper functions for performing access rights checks.
 * 
 * @author Ridwan Kurmally
 */
@Component
public class WorkspaceAccessUtils {
    
    private static WorkspaceCrudService workspaces;
    
    private static UserCrudService users;
    
    /**
     * Auto-wired constructor.
     * This is essential so that we can set the static service {@link ca.mcgill.sel.webcore
     * .authentication.workspace.WorkspaceCrudService WorkspaceCrudService} and {@link ca.mcgill.sel.webcore
     * .authentication.entity.UserCrudService UserCrudService}
     * 
     * @param users
     * @param workspaces
     */
    @Autowired
    public WorkspaceAccessUtils(WorkspaceCrudService workspaces, UserCrudService users) {
        WorkspaceAccessUtils.users = users;
        WorkspaceAccessUtils.workspaces = workspaces;
    }
    
    /**
     * Verifies that the user exists and is the owner.
     * 
     * @param owner owner of a workspace
     * @param user user name
     * @throws IllegalArgumentException if owner or user does not exist or the user is not the owner
     */
    public static void checkOwnerRights(String owner, String user) {
        if (users.findByUsername(user).isEmpty()) {
            throw new IllegalArgumentException(String.format("User '%s' does not exist.",
                    user));
        }
        if (!user.equals(owner)) {
            throw new IllegalArgumentException(String.format("Only the owner '%s' can peform this operation.", 
                    owner));
        }
    }
    
    /**
     * Verifies that the workspace according to the passed owner and cdmName model name does not exist.
     * 
     * @param owner owner of a workspace
     * @param cdmName name of model
     * @throws IllegalArgumentException if this workspace exists
     */
    public static void checkWorkspaceAbsent(String owner, String cdmName) {
        if (workspaces.find(owner, cdmName).isPresent()) {
            throw new IllegalArgumentException(String
                    .format("There is already a model '%s' belonging to the user '%s'.", cdmName, owner));
        }
    }
    
    /**
     * Verifies existence of any workspace corresponding to an owner name and that the user is the owner.
     * Then returns all of these workspaces belonging to the owner.
     * 
     * @param owner owner of the workspaces
     * @param user user name
     * @throws IllegalArgumentException if owner or user does not exist or the user is not the owner
     * @return list of all workspaces
     */
    public static List<Workspace> checkOwnerRightsAndGetWorkspaces(String owner, String user) {
        List<Workspace> userWorkspaces = workspaces.getAllWorkspaces(owner);
        if (userWorkspaces == null) {
            throw new IllegalArgumentException(String.format("The owner '%s' does not exist", 
                    owner));
        }
        if (users.findByUsername(user).isEmpty()) {
            throw new IllegalArgumentException(String.format("User '%s' does not exist.",
                    user));
        }
        if (!user.equals(owner)) {
            throw new IllegalArgumentException(String.format("Only the owner '%s' can peform this operation.", 
                    owner));
        }
        return userWorkspaces;
    }
    
    /**
     * Verifies existence of workspace corresponding to the owner and cdmName model name and that the user is the owner.
     * Then return that workspace.
     * 
     * @param owner owner of the workspace
     * @param cdmName name of model
     * @param user user name
     * @throws IllegalArgumentException if workspace or user does not exist or the user is not the owner
     * @return workspace 
     */
    public static Workspace checkOwnerRightsAndGetWorkspace(String owner, String cdmName, String user) {
        Optional<Workspace> userWorkspace = workspaces.find(owner, cdmName);
        if (userWorkspace.isEmpty()) {
            throw new IllegalArgumentException(String.format("Cannot find workspace as the owner '%s' or model '%s' "
                    + "does not exist", owner, cdmName));
        }
        if (users.findByUsername(user).isEmpty()) {
            throw new IllegalArgumentException(String.format("User '%s' does not exist.",
                    user));
        }
        if (!user.equals(owner)) {
            throw new IllegalArgumentException(String.format("Only the owner '%s' can peform this operation.", 
                    owner));
        }
        return userWorkspace.get();
    }
    
    /**
     * Verifies existence of workspace corresponding to the owner and cdmName model name and that the user has read 
     * access rights over that workspace. Then return that workspace.
     * 
     * @param owner owner of the workspace
     * @param cdmName name of model
     * @param user user name
     * @throws IllegalArgumentException if workspace or user does not exist or the user does not have read rights
     * @return workspace 
     */
    public static Workspace checkReadRightsAndGetWorkspace(String owner, String cdmName, String user) {
        Optional<Workspace> workspace = workspaces.find(owner, cdmName);
        if (workspace.isEmpty()) {
            throw new IllegalArgumentException(String.format("Cannot find workspace as the owner '%s' or model '%s' "
                    + "does not exist", owner, cdmName));
        }
        if (users.findByUsername(user).isEmpty()) {
            throw new IllegalArgumentException(String.format("User '%s' does not exist.",
                    user));
        }
        if (!workspace.get().hasReadAccess(user)) {
            throw new IllegalArgumentException(String.format("The user '%s' does not possess reading permission over "
                    + "that workspace. Request read access from the owner '%s'.", user, owner));
        }
        return workspace.get();
    }
    
    /**
     * Verifies existence of workspace corresponding to the owner and cdmName model name and that the user has write 
     * access rights over that workspace. Then return that workspace.
     * 
     * @param owner owner of the workspace
     * @param cdmName name of model
     * @param user user name
     * @throws IllegalArgumentException if workspace or user does not exist or the user does not have write rights
     * @return workspace 
     */
    public static Workspace checkWriteRightsAndGetWorkspace(String owner, String cdmName, String user) {
        Optional<Workspace> workspace = workspaces.find(owner, cdmName);
        if (workspace.isEmpty()) {
            throw new IllegalArgumentException(String.format("Cannot find workspace as the owner '%s' or model '%s' "
                    + "does not exist", owner, cdmName));
        }
        if (users.findByUsername(user).isEmpty()) {
            throw new IllegalArgumentException(String.format("User '%s' does not exist.",
                    user));
        }
        if (!workspace.get().hasWriteAccess(user)) {
            throw new IllegalArgumentException(String.format("The user '%s' does not possess writing permission over "
                    + "that workspace. Request write access from the owner '%s'.", user, owner));
        }
        return workspace.get();
    }
    
    /**
     * Verifies that the user name is that of a registered user.
     * 
     * @param username user name
     * @throws IllegalArgumentException if user does not exist
     */
    public static void checkIfExist(String username) {
        if (users.findByUsername(username).isEmpty()) {
            throw new IllegalArgumentException(String.format("User '%s' does not exist.",
                    username));
        }
    }
}
