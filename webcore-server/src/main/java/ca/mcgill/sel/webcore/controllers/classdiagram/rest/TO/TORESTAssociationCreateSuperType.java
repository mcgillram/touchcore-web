package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

public class TORESTAssociationCreateSuperType {
    Integer subClassId;
    Integer superClassId;
    public TORESTAssociationCreateSuperType(Integer subClassId, Integer superClassId) {
        super();
        this.subClassId = subClassId;
        this.superClassId = superClassId;
    }
    public Integer getSubClassId() {
        return subClassId;
    }
    public void setSubClassId(Integer subClassId) {
        this.subClassId = subClassId;
    }
    public Integer getSuperClassId() {
        return superClassId;
    }
    public void setSuperClassId(Integer superClassId) {
        this.superClassId = superClassId;
    }
}
