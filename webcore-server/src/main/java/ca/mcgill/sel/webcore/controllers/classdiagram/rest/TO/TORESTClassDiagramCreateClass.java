package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

public class TORESTClassDiagramCreateClass {
    String className;
    boolean dataType;
    boolean isInterface;
    float x;
    float y;
    
    public TORESTClassDiagramCreateClass() {
    }
    
    public TORESTClassDiagramCreateClass(String className, boolean dataType, boolean isInterface, float x, float y) {
        this.className = className;
        this.dataType = dataType;
        this.isInterface = isInterface;
        this.x = x;
        this.y = y;
    }
    public String getClassName() {
        return className;
    }
    public void setClassName(String className) {
        this.className = className;
    }
    public boolean isDataType() {
        return dataType;
    }
    public void setDataType(boolean dataType) {
        this.dataType = dataType;
    }
    public boolean isInterface() {
        return isInterface;
    }
    public void setInterface(boolean isInterface) {
        this.isInterface = isInterface;
    }
    public float getX() {
        return x;
    }
    public void setX(float x) {
        this.x = x;
    }
    public float getY() {
        return y;
    }
    public void setY(float y) {
        this.y = y;
    }
    
}
