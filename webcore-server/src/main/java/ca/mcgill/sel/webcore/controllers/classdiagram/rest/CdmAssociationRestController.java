package ca.mcgill.sel.webcore.controllers.classdiagram.rest;

import ca.mcgill.sel.classdiagram.Association;
import ca.mcgill.sel.classdiagram.AssociationEnd;
import ca.mcgill.sel.classdiagram.Class;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.Classifier;
import ca.mcgill.sel.classdiagram.ReferenceType;
import ca.mcgill.sel.classdiagram.Type;
import ca.mcgill.sel.classdiagram.language.controller.ControllerFactory;
import ca.mcgill.sel.webcore.BroadcastContentManagerMapper;
import ca.mcgill.sel.webcore.WebResourceClassDiagramMapperManager;
import ca.mcgill.sel.webcore.authentication.entity.User;
import ca.mcgill.sel.webcore.authentication.workspace.WorkspaceAccessUtils;
import ca.mcgill.sel.webcore.controllers.MainController;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTAssociationCreateAssociationClass;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTAssociationCreateSuperType;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTAssociationRemoveSuperType;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTAssociationRenameAssociationEnd;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTAssociationSetMultiplicity;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTAssociationSetQualifier;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTAssociationSetReferenceType;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTAssociationSetRoleName;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTAssociationSwitchNavigability;
import ca.mcgill.sel.webcore.utils.Utils;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("{username}/classdiagram/{cdmName}/association")
public class CdmAssociationRestController {
    
    /**
     * Creates a super type association between two classifiers.
     * 
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param TO {"subClassId": Integer, "superTypeId": Integer}
     **/
    @PostMapping("supertype")
    public void createSuperType(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @RequestBody TORESTAssociationCreateSuperType TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Class subClass = (Class)
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), TO.getSubClassId());
        Classifier superType = 
                (Classifier) Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), TO.getSuperClassId());

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().addSuperType(subClass, superType);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Removes the super-type association from a pair of classifiers.
     * 
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param TO {"classId1": Integer, "classId2": Integer}
     **/
    @DeleteMapping("supertype")
    public void removeSuperType(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @RequestBody TORESTAssociationRemoveSuperType TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Class class1 = (Class) Utils.getEObjectById(WebResourceClassDiagramMapperManager
                .getSingleIdMapping(username, cdmName), TO.getClassId1());
        Class class2 = (Class) Utils.getEObjectById(WebResourceClassDiagramMapperManager
                .getSingleIdMapping(username, cdmName), TO.getClassId2());

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().removeSuperType(class1, class2);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Removes the association.
     * 
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param associationId id of association
     **/
    @DeleteMapping("{associationId}")
    public void removeAssociation(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer associationId) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        ClassDiagram owner = WebResourceClassDiagramMapperManager
                .getSingleMapper(username, cdmName).getClassDiagram();
        Association association = (Association) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), associationId);


        MainController.lock.lock();
        ControllerFactory.INSTANCE.getAssociationController().deleteAssociation(owner, association);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Creates an association class for an association.
     * 
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param associationId id of association
     * @param TO {"classId": Integer}
     **/
    @PostMapping("{associationId}/associationClass")
    public void createAssociationClass(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName,
            @PathVariable Integer associationId, 
            @RequestBody TORESTAssociationCreateAssociationClass TO)
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Class owner = (Class) Utils.getEObjectById(WebResourceClassDiagramMapperManager
                .getSingleIdMapping(username, cdmName), TO.getClassId());
        Association association = (Association) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), associationId);


        MainController.lock.lock();
        ControllerFactory.INSTANCE.getAssociationController().setAssociationClass(association, owner);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Removes the association class of an association.
     * 
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param associationId id of association
     **/
    @DeleteMapping("{associationId}/associationClass")
    public void removeAssociationClass(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer associationId) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Association association = (Association) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), associationId);


        MainController.lock.lock();
        ControllerFactory.INSTANCE.getAssociationController().removeAssociationClass(association);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Sets the qualifier at the end of an association.
     * 
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param associationEndId id of association end
     * @param TO {"typeId": Integer}
     **/
    @PutMapping("end/{associationEndId}/qualifier")
    public void setQualifier(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer associationEndId, 
            @RequestBody TORESTAssociationSetQualifier TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        AssociationEnd associationEnd = (AssociationEnd) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), associationEndId);
        Type type = (Type) Utils.getEObjectById(WebResourceClassDiagramMapperManager
                .getSingleIdMapping(username, cdmName), TO.getTypeId());


        MainController.lock.lock();
        ControllerFactory.INSTANCE.getAssociationController().setQualifier(associationEnd, type);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Sets the qualifier at the end of an association to nothing.
     * 
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param associationEndId id of association end
     **/
    @PutMapping("end/{associationEndId}/noQualifier")
    public void removeQualifier(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer associationEndId) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        AssociationEnd associationEnd = (AssociationEnd) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), associationEndId);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getAssociationController().removeQualifier(associationEnd);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Sets the multiplicity of an association end.
     * 
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param associationEndId id of association end
     * @param TO {"lowerBound": Integer, "upperBound": Integer}
     **/
    @PutMapping("end/{associationEndId}/multiplicity")
    public void setMultiplicity(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer associationEndId, 
            @RequestBody TORESTAssociationSetMultiplicity TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        AssociationEnd associationEnd = (AssociationEnd) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), associationEndId);
        ClassDiagram owner = WebResourceClassDiagramMapperManager.getSingleMapper(cdmName).getClassDiagram();

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getAssociationController()
            .setMultiplicity(owner, associationEnd, TO.getLowerBound(), TO.getUpperBound());
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Sets the role name of an association end.
     * 
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param associationEndId id of association end
     * @param TO {"roleName": String}
     **/
    @PutMapping("end/{associationEndId}/rolename")
    public void setRoleName(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer associationEndId, 
            @RequestBody TORESTAssociationSetRoleName TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        AssociationEnd associationEnd = (AssociationEnd) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), associationEndId);
        ClassDiagram owner = WebResourceClassDiagramMapperManager.getSingleMapper(cdmName).getClassDiagram();

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getAssociationController().setRoleName(owner, associationEnd, TO.getRoleName());
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Sets the reference type of an association end.
     * 
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param associationEndId id of association end
     * @param TO {"referenceType": String}
     **/
    @PutMapping("end/{associationEndId}/referencetype")
    public void setReferenceType(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer associationEndId, 
            @RequestBody TORESTAssociationSetReferenceType TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        AssociationEnd associationEnd = (AssociationEnd) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), associationEndId);
        
        ReferenceType type = ReferenceType.get(TO.getReferenceType());

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getAssociationController().setReferenceType(associationEnd, type);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Switches the navigability of the association.
     * 
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param associationEndId id of association end
     * @param TO {"oppositeEndId": Integer}
     **/
    @PutMapping("end/{associationEndId}/navigable")
    public void switchNavigability(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer associationEndId, 
            @RequestBody TORESTAssociationSwitchNavigability TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        AssociationEnd associationEnd = (AssociationEnd) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), associationEndId);
        AssociationEnd oppositeEnd = (AssociationEnd) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), TO.getOppositeEndId());
        ClassDiagram owner = WebResourceClassDiagramMapperManager.getSingleMapper(cdmName).getClassDiagram();


        MainController.lock.lock();
        ControllerFactory.INSTANCE.getAssociationController().switchNavigable(owner, associationEnd,  oppositeEnd);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Switches whether or not the association is static.
     * 
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param associationEndId id of association end
     **/
    @PutMapping("end/{associationEndId}/static")
    public void switchStatic(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer associationEndId) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        AssociationEnd associationEnd = (AssociationEnd) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), associationEndId);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getAssociationController().switchStatic(associationEnd);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Switches the uniqueness of the association.
     * 
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param associationEndId id of association end
     **/
    @PutMapping("end/{associationEndId}/uniqueness")
    public void switchUniqueness(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer associationEndId) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        AssociationEnd associationEnd = (AssociationEnd) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), associationEndId);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getAssociationController().switchUniqueness(associationEnd);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Switches the ordering of the association.
     * 
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param associationEndId id of association end
     **/
    @PutMapping("end/{associationEndId}/order")
    public void switchOrder(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer associationEndId) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        AssociationEnd associationEnd = (AssociationEnd) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), associationEndId);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getAssociationController().switchOrdering(associationEnd);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Rename the association end.
     * 
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param associationEndId id of association end
     * @param TO {"newName": String}
     **/
    @PutMapping("end/{associationEndId}/rename")
    public void renameAssociationEnd(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer associationEndId, 
            @RequestBody TORESTAssociationRenameAssociationEnd TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        AssociationEnd associationEnd = (AssociationEnd) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), associationEndId);   

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getAssociationController().renameAssociationEnd(associationEnd, TO.getNewName());
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
}
