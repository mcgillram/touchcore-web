package ca.mcgill.sel.webcore.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.messaging.SessionConnectEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.gson.JsonObject;

import ca.mcgill.sel.webcore.entity.User;

/**
 * This controller handles every aspect of the collaboration in WebCore.
 * Username and color creation at connexion, highlight and user info management. 
 * 
 * @author arthurls
 *
 */
@Controller
public class CollabController {
    
    private static BiMap<String, User> userDB;
    
    private static List<String> animals;
    private static List<String> stances;
    private static List<String> colors;
        
    private static SimpMessagingTemplate template;

    /**
     * Constructor for the CollabController.
     * 
     * @param template 
     */
    @Autowired
    // TODO: check if necessary to param SimpleMessagingTemplate
    public CollabController(SimpMessagingTemplate template) {
        CollabController.template = template;
        animals = new ArrayList<String>();
        animals.add("Panda");
        animals.add("Eagle");
        animals.add("Chicken");
        animals.add("Puppy");
        animals.add("Turtle");
        animals.add("Cat");
        animals.add("Kangourou");
        animals.add("Koala");
        
        stances = new ArrayList<String>();
        stances.add("Angry");
        stances.add("Happy");
        stances.add("Mad");
        stances.add("Dark");
        stances.add("Wild");
        stances.add("Scary");
        stances.add("Funny");
        stances.add("Crazy");
        stances.add("Unexpected");
        
        colors = new ArrayList<String>();
        colors.add("#007bff");
        colors.add("#6f42c1");
        colors.add("#e83e8c");
        colors.add("#dc3545");
        colors.add("#fd7e14");
        colors.add("#ffc107");
        colors.add("#28a745");
        colors.add("#17a2b8");
        colors.add("#6c757d");
        
        userDB = HashBiMap.create();
    }

    /**
     * Sends to the caller its own sessionId.
     * Message Mapping at /api/getSessionData.
     * Return Mapping at /user/queue/getSessionData
     * 
     * @param sessionId client sessionId located in the header
     * @return sessionId
     */
    @MessageMapping("/getSessionData")
    @SendToUser("/queue/getSessionData")
    private String getSessionData(@Header("simpSessionId") String sessionId) {
        User user = userDB.get(sessionId);
        JsonObject result = new JsonObject();
        result.addProperty("sessionId", user.getSessionId());
        result.addProperty("color", user.getColor());
        result.addProperty("username", user.getUsername());
        return result.toString();
    }
    
    /**
    * Sends the users collaboration info to /topic/collab.
    * Message Mapping at /api/collab.
    */
    @MessageMapping("/collab")
    public static void sendCollabInfo() {
        JsonObject result = new JsonObject();
        JsonObject users = new JsonObject();

        for (Map.Entry<String, User> entry : userDB.entrySet()) {
            users.add(entry.getKey(), entry.getValue().toJson());
        }

        result.add("users", users);
        result.addProperty("userCount", userDB.size());
        template.convertAndSend("/topic/collab", result.toString());
    }
    
    /**
     * Clients informs the server of a change in his class selection (for the highlighting).
     * Message Mapping at /api/highlight.
     * 
     * @param sessionId client sessionId located in the header
     * @param body {'aspectName': String 'selected': Array[selectedClasses], 'unselected': Array[unselectedClasses]}
     */
    @MessageMapping("/highlight")
    private void setHighlightChanges(@Header ("simpSessionId") String sessionId, String body) {
        JSONObject obj = new JSONObject(body);
        List<Object> selected = obj.getJSONArray("selected").toList();
        List<Object> unselected = obj.getJSONArray("unselected").toList();
        
        User user = userDB.get(sessionId);
        for (int i = 0; i < selected.size(); i++) {
            user.getSelectedClass().add((String) selected.get(i));
        }
        
        for (int i = 0; i < unselected.size(); i++) {
            user.getSelectedClass().remove(unselected.get(i));
        }
        
        sendCollabInfo();
    }
    
    /**
     * Handle client connexion event.
     * 
     * @param event connexion info
     */
    @EventListener
    private void handleSessionConnected(SessionConnectEvent event) {
        SimpMessageHeaderAccessor headers = SimpMessageHeaderAccessor.wrap(event.getMessage());
        String sessionId = headers.getSessionId();
        List<String> takenUsernames = getTakenUsernames();
        boolean isNameGenerated = false;
        Random rand = new Random();
    
        while (!isNameGenerated) {
            
            int rand1 = rand.nextInt(stances.size());
            int rand2 = rand.nextInt(animals.size());

            String name = stances.get(rand1) + " " + animals.get(rand2);

            if (!takenUsernames.contains(name)) {
                User user = new User(sessionId, name, pickColor());
                if (user != null) {
                    userDB.put(sessionId, user);
                    isNameGenerated = true;
                }
            }
        }
        System.out.println("New User " + userDB.get(sessionId).getUsername().toString() + " with id " + sessionId);
    }
    
    /**
     * Handle the disconnect of a user.
     * @param event disconnect info
     */
    @EventListener
    private void handleSessionDisconnect(SessionDisconnectEvent event) {
        SimpMessageHeaderAccessor headers = SimpMessageHeaderAccessor.wrap(event.getMessage());
        String sessionId = headers.getSessionId();
                
        userDB.remove(sessionId);
        sendCollabInfo();
    }
    
    /**
     * Returns a color that has not been picked by a user yet.
     * If all the colors have been picked, return one random one in the array.
     * @return user color
     */
    private String pickColor() {

        if (userDB.size() < colors.size()) {
            return colors.get(userDB.size());
        }
        
        Random rand = new Random();
        return colors.get(rand.nextInt(colors.size()));
    }

    /**
     * Returns the list of all the usernames.
     * @return list of username
     */
    private List<String> getTakenUsernames() {
        List<String> result = new ArrayList<String>();
        
        for (Map.Entry<String, User> entry : userDB.entrySet()) {
            result.add(entry.getValue().getUsername());
        }
        
        return result;
    }
    
    /**
     * Sets an aspect name for a user.
     * @param sessionId of the user
     * @param aspectName new aspect name to work on
     */
    public static void setUserAspect(String sessionId, String aspectName) {
        User user = userDB.get(sessionId);
        user.setAspectName(aspectName);
        user.setSelectedClass(new ArrayList<String>());
        sendCollabInfo();
    }
}
