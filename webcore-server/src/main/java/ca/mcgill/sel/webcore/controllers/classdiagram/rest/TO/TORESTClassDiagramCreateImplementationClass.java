package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

import java.util.List;
import java.util.Set;

import ca.mcgill.sel.classdiagram.ImplementationClass;
import ca.mcgill.sel.classdiagram.TypeParameter;

public class TORESTClassDiagramCreateImplementationClass {
    String className;
    List<TypeParameter> generics;
    boolean isInterface;
    boolean isAbstract;
    float x;
    float y;
    Set<String> superTypes;
    Set<ImplementationClass> subTypes;
    public TORESTClassDiagramCreateImplementationClass(String className, List<TypeParameter> generics,
            boolean isInterface, boolean isAbstract, float x, float y, Set<String> superTypes,
            Set<ImplementationClass> subTypes) {
        super();
        this.className = className;
        this.generics = generics;
        this.isInterface = isInterface;
        this.isAbstract = isAbstract;
        this.x = x;
        this.y = y;
        this.superTypes = superTypes;
        this.subTypes = subTypes;
    }
    public String getClassName() {
        return className;
    }
    public void setClassName(String className) {
        this.className = className;
    }
    public List<TypeParameter> getGenerics() {
        return generics;
    }
    public void setGenerics(List<TypeParameter> generics) {
        this.generics = generics;
    }
    public boolean isInterface() {
        return isInterface;
    }
    public void setInterface(boolean isInterface) {
        this.isInterface = isInterface;
    }
    public boolean isAbstract() {
        return isAbstract;
    }
    public void setAbstract(boolean isAbstract) {
        this.isAbstract = isAbstract;
    }
    public float getX() {
        return x;
    }
    public void setX(float x) {
        this.x = x;
    }
    public float getY() {
        return y;
    }
    public void setY(float y) {
        this.y = y;
    }
    public Set<String> getSuperTypes() {
        return superTypes;
    }
    public void setSuperTypes(Set<String> superTypes) {
        this.superTypes = superTypes;
    }
    public Set<ImplementationClass> getSubTypes() {
        return subTypes;
    }
    public void setSubTypes(Set<ImplementationClass> subTypes) {
        this.subTypes = subTypes;
    }
}
