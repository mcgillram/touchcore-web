package ca.mcgill.sel.webcore;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import org.eclipse.emf.common.util.URI;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import ca.mcgill.sel.commons.ResourceUtil;
import ca.mcgill.sel.commons.emf.util.AdapterFactoryRegistry;
import ca.mcgill.sel.commons.emf.util.ResourceManager;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.CORELanguage;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.language.registry.CORELanguageRegistry;
import ca.mcgill.sel.core.provider.CoreItemProviderAdapterFactory;
import ca.mcgill.sel.core.util.CoreResourceFactoryImpl;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.loaders.RamClassLoader;
import ca.mcgill.sel.ram.provider.RamItemProviderAdapterFactory;
//import ca.mcgill.sel.ram.ui.RamApp;
//import ca.mcgill.sel.ram.ui.utils.LoggerUtils;
import ca.mcgill.sel.ram.util.Constants;
import ca.mcgill.sel.ram.util.RamResourceFactoryImpl;
import ca.mcgill.sel.ram.validator.ValidationRulesParser;
import ca.mcgill.sel.webcore.controllers.MainController;

/**
 * Launches TouchCORE and the SpringBoot API server.
 * TouchCORE is the "master". The API cannot be used if the master doesn't select a concern.
 * @author arthurls
 *
 */
@SpringBootApplication
public class DuoCORE {

    /**
     * Main of the project.
     * @param args 
     * @throws IOException 
     */
    public static void main(String[] args) throws IOException {
        
        // TOUCHCORE interaction
        ResourceManager.initialize();
        // Initialize packages.
        RamPackage.eINSTANCE.eClass();
        CorePackage.eINSTANCE.eClass();
        
        // Register resource factories.
        ResourceManager.registerExtensionFactory("ram", new RamResourceFactoryImpl());
        ResourceManager.registerExtensionFactory(Constants.CORE_FILE_EXTENSION, new CoreResourceFactoryImpl());

        // Initialize adapter factories.
        AdapterFactoryRegistry.INSTANCE.addAdapterFactory(RamItemProviderAdapterFactory.class);
        AdapterFactoryRegistry.INSTANCE.addAdapterFactory(CoreItemProviderAdapterFactory.class);

        // Used to import Implementation Classes
        RamClassLoader.INSTANCE.initializeWithJavaClasses();
        
        // Initialize the WebResourceMapperManager
        WebResourceMapperManager.setWebResourceMapperDB(new HashMap<String, WebResourceMapper>());
        WebResourceMapperManager.setDuo(true);
        
        MainController.lock = new ReentrantLock();
        
        // Launch TouchCORE
//        RamApp.initialize();
              
        // Check for existing languages and load them.
//        loadLanguages();        
//        
//        // To parse OCL files and initialize rules.
//        Thread tParser = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                ValidationRulesParser.getInstance();
//                LoggerUtils.info("OCL files parsing terminated.");
//            }
//        }, "Rules Parser Initializer");
//        tParser.start();
//        SpringApplication.run(DuoCORE.class, args);
        printServerDetails();
    }
    
    /**
     * Displays the server info when the application is ready.
     */
    public static void printServerDetails() {
        String ipAddress;
        try {
            InetAddress iP = InetAddress.getLocalHost();
            ipAddress = iP.getHostAddress();
        } catch (UnknownHostException e) {
            ipAddress = "can't find ip";
            e.printStackTrace();
        }
        System.out.println("------------------------------\n\n");
        System.out.println("Open " + ipAddress + ":8080 in your browser to access app");
        System.out.println("\n\n------------------------------");
    }
    
    /**
     * Loads pre-defined languages and registers them in the {@link CORELanguageRegistry}.
     */
//    private static void loadLanguages() {
//        List<String> languages = ResourceUtil.getResourceListing("models/languages/", ".core");
//
//        if (languages != null) {
//            for (String l : languages) {
//                
//                // load existing languages                
//                URI fileURI = URI.createURI(l);
//                COREConcern languageConcern = (COREConcern) ResourceManager.loadModel(fileURI); 
//                // register any languages contained in the loaded concern
//                for (COREArtefact a : languageConcern.getArtefacts()) {
//                    if (a instanceof CORELanguage) {
//                        CORELanguage existingLanguage = (CORELanguage) a;
//                        CORELanguageRegistry.getRegistry().registerLanguage(existingLanguage);                    
//                    }
//                }
//            }
//        }
//    }

}
