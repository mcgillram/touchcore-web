package ca.mcgill.sel.webcore;

import java.io.File;
import java.io.IOException;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.concurrent.locks.ReentrantLock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import ca.mcgill.sel.core.language.registry.CORELanguageRegistry;
import ca.mcgill.sel.classdiagram.CdmPackage;
import ca.mcgill.sel.classdiagram.provider.CdmItemProviderAdapterFactory;
import ca.mcgill.sel.classdiagram.util.CdmResourceFactoryImpl;
import ca.mcgill.sel.commons.emf.util.AdapterFactoryRegistry;
import ca.mcgill.sel.commons.emf.util.ResourceManager;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.provider.CoreItemProviderAdapterFactory;
import ca.mcgill.sel.core.util.CoreResourceFactoryImpl;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.loaders.RamClassLoader;
import ca.mcgill.sel.ram.provider.RamItemProviderAdapterFactory;
import ca.mcgill.sel.ram.util.Constants;
import ca.mcgill.sel.ram.util.RamResourceFactoryImpl;
import ca.mcgill.sel.webcore.controllers.MainController;


/**
 * Main Class of our API.
 * Only launches the SpringBoot API server.
 * @author arthurls
 *
 */
@SpringBootApplication
public class WebCore {
    
    /**
     * Main of the project.
     * @param args 
     * @throws IOException 
     */
    public static void main(String[] args) throws IOException {
        
        // TOUCHCORE interaction
        ResourceManager.initialize();
        // Initialize packages.
        CdmPackage.eINSTANCE.eClass();
        RamPackage.eINSTANCE.eClass();
        CorePackage.eINSTANCE.eClass();
        
        // Register resource factories.
        ResourceManager.registerExtensionFactory("ram", new RamResourceFactoryImpl());
        ResourceManager.registerExtensionFactory("cdm", new CdmResourceFactoryImpl());
        ResourceManager.registerExtensionFactory(Constants.CORE_FILE_EXTENSION, new CoreResourceFactoryImpl());

        // Initialize adapter factories.
        AdapterFactoryRegistry.INSTANCE.addAdapterFactory(RamItemProviderAdapterFactory.class);
        AdapterFactoryRegistry.INSTANCE.addAdapterFactory(CdmItemProviderAdapterFactory.class);
        AdapterFactoryRegistry.INSTANCE.addAdapterFactory(CoreItemProviderAdapterFactory.class);

        // Used to import Implementation Classes
        RamClassLoader.INSTANCE.initializeWithJavaClasses();
        
        // Loading perspective and languages to be able to create new model at runtime.
        COREPerspectiveUtil.INSTANCE.loadPerspectives();
        CORELanguageRegistry.loadLanguages();
        
        // Initialize the WebResourceMapperManager
//        WebResourceMapperManager.setWebResourceMapperDB(new HashMap<String, WebResourceMapper>());
//        WebResourceMapperManager.setDuo(false);
//        COREConcern concern = (COREConcern) ResourceManager.loadModel("D:\\Courses\\project\\touchcore-web\\rama\\Rama.core");
//        WebResourceMapperManager.setConcern(concern);
        
        WebResourceClassDiagramMapperManager.setDuo(false);
        
        /*
         * NOTE: it may be that your machine requires the loaded path to be an absolute path, if so simply find the MULTIPLE_CLASSES.core file
         * on your file system and copy/paste that path onto the path on 1. then un-comment it out, and comment out the use of the relative path below
         */
        
        File f = new File("cores/MULTIPLE_CLASSES/MULTIPLE_CLASSES.core");
        COREConcern concern = (COREConcern) ResourceManager.loadModel(f);
        
//   1. COREConcern concern = (COREConcern) ResourceManager.loadModel("-absolute path of MULTIPLE_CLASSES.core-");
        
        /*
         * END-NOTE
         */
        
        WebResourceClassDiagramMapperManager.setConcern(concern);
        
       
        MainController.lock = new ReentrantLock();
        
        // To parse OCL files and initialize rules.
//        Thread tParser = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                ValidationRulesParser.getInstance();
//                //LoggerUtils.info("OCL files parsing terminated.");
//            }
//        }, "Rules Parser Initializer");
//        tParser.start();

        SpringApplication.run(WebCore.class, args);
        printServerDetails();
        
    }
    
    /**
     * Displays the server info when the application is ready.
     */
    public static void printServerDetails() {
        String ipAddress;
        try {
            InetAddress iP = InetAddress.getLocalHost();
            ipAddress = iP.getHostAddress();
        } catch (UnknownHostException e) {
            ipAddress = "can't find ip";
            e.printStackTrace();
        }
        System.out.println("------------------------------\n\n");
        System.out.println("Open " + ipAddress + ":8080 in your browser to access app");
        System.out.println("\n\n------------------------------");
    }
}
