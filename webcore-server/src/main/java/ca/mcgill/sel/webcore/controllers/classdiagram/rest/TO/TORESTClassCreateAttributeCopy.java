package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

public class TORESTClassCreateAttributeCopy {
    String newName;
    public TORESTClassCreateAttributeCopy(String newName) {
        super();
        this.newName = newName;
    }
    public String getNewName() {
        return newName;
    }
    public void setNewName(String newName) {
        this.newName = newName;
    }
}
