package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

public class TORESTAssociationSwitchNavigability {
    Integer oppositeEndId;
    public TORESTAssociationSwitchNavigability(Integer oppositeEndId) {
        super();
        this.oppositeEndId = oppositeEndId;
    }
    public Integer getOppositeEndId() {
        return oppositeEndId;
    }
    public void setOppositeEndId(Integer oppositeEndId) {
        this.oppositeEndId = oppositeEndId;
    }
}
