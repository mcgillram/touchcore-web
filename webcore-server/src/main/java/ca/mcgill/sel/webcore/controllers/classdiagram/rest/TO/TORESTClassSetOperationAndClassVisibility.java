package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

import ca.mcgill.sel.classdiagram.VisibilityType;

public class TORESTClassSetOperationAndClassVisibility {
    Integer classId;
    VisibilityType visibility;
    public TORESTClassSetOperationAndClassVisibility(Integer classId, VisibilityType visibility) {
        super();
        this.classId = classId;
        this.visibility = visibility;
    }
    public Integer getClassId() {
        return classId;
    }
    public void setClassId(Integer classId) {
        this.classId = classId;
    }
    public VisibilityType getVisibility() {
        return visibility;
    }
    public void setVisibility(VisibilityType visibility) {
        this.visibility = visibility;
    }
}
