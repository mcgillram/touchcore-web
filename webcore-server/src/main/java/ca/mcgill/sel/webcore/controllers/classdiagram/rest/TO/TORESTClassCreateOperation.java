package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

import java.util.List;

import ca.mcgill.sel.classdiagram.Parameter;
import ca.mcgill.sel.classdiagram.Type;
import ca.mcgill.sel.classdiagram.VisibilityType;

public class TORESTClassCreateOperation {
    VisibilityType visibility;
    Integer rankIndex;
    Integer returnObjectTypeId;
    List<Parameter> parameters;
    String operationName;
    public TORESTClassCreateOperation(VisibilityType visibility, Integer rankIndex, Integer returnObjectTypeId,
            List<Parameter> parameters, String operationName) {
        super();
        this.visibility = visibility;
        this.rankIndex = rankIndex;
        this.returnObjectTypeId = returnObjectTypeId;
        this.parameters = parameters;
        this.operationName = operationName;
    }
    public VisibilityType getVisibility() {
        return visibility;
    }
    public void setVisibility(VisibilityType visibility) {
        this.visibility = visibility;
    }
    public Integer getRankIndex() {
        return rankIndex;
    }
    public void setRankIndex(Integer rankIndex) {
        this.rankIndex = rankIndex;
    }
    public Integer getReturnObjectTypeId() {
        return returnObjectTypeId;
    }
    public void setReturnObjectTypeId(Integer returnObjectTypeId) {
        this.returnObjectTypeId = returnObjectTypeId;
    }
    public List<Parameter> getParameters() {
        return parameters;
    }
    public void setParameters(List<Parameter> parameters) {
        this.parameters = parameters;
    }
    public String getOperationName() {
        return operationName;
    }
    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }
}
