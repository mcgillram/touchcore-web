package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

public class TORESTClassDiagramImportClassifier {
    float x;
    float y;
    String importedClassName;
    
    public TORESTClassDiagramImportClassifier(float x, float y, String importedClassName) {
        super();
        this.x = x;
        this.y = y;
        this.importedClassName = importedClassName;
    }
    public float getX() {
        return x;
    }
    public void setX(float x) {
        this.x = x;
    }
    public float getY() {
        return y;
    }
    public void setY(float y) {
        this.y = y;
    }
    public String getImportedClassName() {
        return this.importedClassName;
    }
    public void setImportedClassName(String importedClassName) {
        this.importedClassName = importedClassName;
    }
}
