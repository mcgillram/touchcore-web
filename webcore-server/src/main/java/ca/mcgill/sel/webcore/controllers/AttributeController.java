package ca.mcgill.sel.webcore.controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.json.JSONObject;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

import com.google.gson.Gson;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.CORENamedElement;
import ca.mcgill.sel.ram.Attribute;
import ca.mcgill.sel.ram.NamedElement;
import ca.mcgill.sel.ram.ObjectType;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.StructuralView;
import ca.mcgill.sel.ram.Type;
import ca.mcgill.sel.ram.controller.ControllerFactory;
import ca.mcgill.sel.ram.util.RAMInterfaceUtil;
import ca.mcgill.sel.ram.util.StructuralViewUtil;
import ca.mcgill.sel.webcore.WebResourceMapperManager;
import ca.mcgill.sel.webcore.utils.Utils;

/**
 * Contains all the controllers related to Attribute.
 * 
 * @author arthurls
 *
 */
@Controller
public class AttributeController {
    
    /**
     * Removes an Attribute.
     * @param body {'aspectName': String, 'attributeId': int}
     */
    @MessageMapping("/removeAttribute")
    public void removeAttribute(String body) {
        System.out.println("/removeAttribute " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int attributeId = obj.getInt("attributeId");
        
        Attribute attribute = 
                (Attribute) Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), attributeId);
        
        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().removeAttribute(attribute);
        MainController.lock.unlock();
    }
    
    /**
     * Changes the Attribute type.
     * @param body {'aspectName': String, 'attributeId': int, 'attributeType': String}
     */
    @MessageMapping("/setAttributeType")
    public void setAttributeType(String body) {
        System.out.println("/setAttributeType " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int attributeId = obj.getInt("attributeId");
        String attributeType = obj.getString("attributeType");
        
        Attribute attribute = 
                (Attribute) Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), attributeId);
        EStructuralFeature feature = RamPackage.Literals.ATTRIBUTE__TYPE;
        StructuralView structuralView = 
                EMFModelUtil.getRootContainerOfType(attribute, RamPackage.Literals.STRUCTURAL_VIEW);
        Type type = StructuralViewUtil.getAttributeTypeByName(attributeType, structuralView);

        MainController.lock.lock();
        EMFEditUtil.getPropertyDescriptor(attribute, feature).setPropertyValue(attribute, type);
        MainController.lock.unlock();
    }
    
    
    /**
     * Switches the static property of an Attribute.
     * @param body {'aspectName': String, 'attributeId': int}
     */
    @MessageMapping("/switchAttributeStatic")
    public void switchAttributeStatic(String body) {
        System.out.println("/switchAttributeStatic " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int attributeId = obj.getInt("attributeId");
        
        Attribute attribute = 
                (Attribute) Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), attributeId);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().switchAttributeStatic(attribute);
        MainController.lock.unlock();
    }
    
    /**
     * Gets an attribute possible types.
     * @param body {'aspectName': String, 'eObjectId': int}
     * @return a json array with the possible types for an attribute (just the names).
     */
    @MessageMapping("/getPossibleAttributeTypes")
    @SendToUser("/queue/getPossibleAttributeTypes")
    public String getPossibleAttributeTypes(String body) {
        System.out.println("/getPossibleAttributeTypes " + body);
        JSONObject obj = new JSONObject(body);
        int eObjectId = obj.getInt("eObjectId");
        String aspectName = obj.getString("aspectName");
        
        EObject eObject = Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), eObjectId);
        
        Collection<ObjectType> choices = RAMInterfaceUtil.getAvailableAttributeTypes(eObject);
        List<String> result = new ArrayList<>();
        
        // Iterate once for only get the names
        for (Iterator<ObjectType> iterator = choices.iterator(); iterator.hasNext(); ) {
            EObject elem = iterator.next();
//           NamedElement
            result.add(((NamedElement) elem).getName());  
        }
        
        // List to json string to properly send it to the client
        Gson gson = new Gson();
        String array = gson.toJson(result);
        
        return array;
    }
}
