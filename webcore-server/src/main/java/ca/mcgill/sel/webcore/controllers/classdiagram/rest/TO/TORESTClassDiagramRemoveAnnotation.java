package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

public class TORESTClassDiagramRemoveAnnotation {
    Integer classId;
    public TORESTClassDiagramRemoveAnnotation(Integer classId) {
        super();
        this.classId = classId;
    }
    public Integer getClassId() {
        return classId;
    }
    public void setClassId(Integer classId) {
        this.classId = classId;
    }
}
