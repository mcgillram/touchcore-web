package ca.mcgill.sel.webcore;

import java.io.IOException;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.emfjson.jackson.annotations.EcoreIdentityInfo;
import org.emfjson.jackson.module.EMFModule;
import org.emfjson.jackson.utils.ValueWriter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import ca.mcgill.sel.ram.Aspect;

/**
 * This Class hold every resource needed to handle a model.
 * The Aspect is the EObject referencing the EMF model.
 * The mapper serialize/deserialize EObject into JSON for the client.
 * Has its own {@link NotificationHandler}.
 *
 * @author arthurls
 *
 */
public class WebResourceMapper {
    
    @JsonIgnore
    private Aspect aspect;
    @JsonIgnore
    private ObjectMapper mapper;
    @JsonIgnore
    private BiMap<EObject, Integer> idMapping;
    @JsonIgnore
    private int idCounter;
    
    public WebResourceMapper() {};

    /**
     * Class constructor.
     * @param model the model you want to map
     * @throws IOException 
     */
    public WebResourceMapper(EObject model) {
        
        // Initialize the Class variables
        aspect = (Aspect) model;
        idMapping = HashBiMap.create();
        idCounter = 1;
        
        // Create a mapper to serialize/deserialize objects
        initMapper();
        
        // Create the idMapping
        initIdMapping();
        
        // Initialize Notification Handler
        NotificationHandler notificationHandler = new NotificationHandler(this);
        notificationHandler.initNotifications();
    }
    
    /**
     * For each EObject in the aspect, assign an id and saves it into a map.
     */
    private void initIdMapping() {
        TreeIterator<EObject> it = aspect.eAllContents();
        
        while (it.hasNext()) {
            EObject eObj = it.next();
            idMapping.put(eObj, idCounter++);
            try {
                mapper.writeValueAsString(eObj);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Initialize the mapper.
     * The mapper is used to serialize/deserialize an EObject into JSON.
     */
    private void initMapper() {
        mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        
        // EMFModule handles the serialization within the mapper
        EMFModule module = new EMFModule();
        module.configure(EMFModule.Feature.OPTION_USE_ID, true);
        
        // Handles the ids for EObject. Maps an id to an EObject
        module.setIdentityInfo(new EcoreIdentityInfo("_id",
            new ValueWriter<EObject, Object>() {
                @Override
                public Object writeValue(EObject eObj, SerializerProvider context) {
                    return String.valueOf(idMapping.get(eObj));
                }
            }
        ));
        
        // Handles the reference between EObject. Use the id/EObject mapping
        module.setReferenceSerializer(new JsonSerializer<EObject>() {
            @Override
            public void serialize(EObject eObj, JsonGenerator g, SerializerProvider s) throws IOException {
                g.writeString(String.valueOf(idMapping.get(eObj)));
            }
        });
                
        mapper.registerModule(module);
    }

    /**
     * Return the current loaded aspect.
     * @return the current loaded aspect
     */
    public Aspect getAspect() {
        return aspect;
    }

    /**
     * The idMapping Map holds the EObject to ID mapping.
     * @return idMapping
     */
    public BiMap<EObject, Integer> getIdMapping() {
        return idMapping;
    }
    
    /**
     * Returns the whole aspect as a JSON string.
     * @return the JSON String
     * @throws JsonProcessingException 
     */
    public String toJson() throws JsonProcessingException {
        return mapper.writeValueAsString(aspect);
    }
    
    /**
     * Returns the given EObject into a JSON String.
     * @param eObject to serialize into JSON
     * @return the JSON String
     * @throws JsonProcessingException 
     */
    public String toJson(EObject eObject) throws JsonProcessingException {
        return mapper.writeValueAsString(eObject);
    }
    
    /**
     * This int is incremented for every new EObject added in the model.
     * @return idCounter
     */
    public int getIdCounter() {
        return idCounter;
    }
}
