package ca.mcgill.sel.webcore.authentication.entity;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Setter;
import lombok.Value;
import lombok.experimental.NonFinal;

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;

/**
 * Represent a registered user. It stores the credentials of the user and the current active token.
 * 
 * @author Ridwan Kurmally
 */
@Value
@Builder
public class User implements UserDetails {
    private static final long serialVersionUID = 2396654715019746670L;

    // TODO: id is not used. Should we delete?
    private String id;
    
    private String username;
    private String password;
    
    private @NonFinal String token;

    @JsonCreator
    User(@JsonProperty("id") final String id,
        @JsonProperty("username") final String username,
        @JsonProperty("password") final String password,
        @JsonProperty("token") final String token) {
        super();
        this.id = requireNonNull(id);
        this.username = requireNonNull(username);
        this.password = requireNonNull(password);
    }

    @JsonIgnore
    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return new ArrayList<>();
    }

    //@JsonIgnore
    public String getToken() {
        return this.token;
    }
    
    @JsonIgnore
    public void setToken(String token) {
        this.token = token;
    }
    
    @JsonIgnore
    public String getId() {
        return id;
    }
    
    //@JsonIgnore
    @Override
    public String getUsername() {
        return username;
    }
    
    //@JsonIgnore
    @Override
    public String getPassword() {
        return password;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isEnabled() {
        return true;
    }
}
