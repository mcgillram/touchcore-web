package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

public class TORESTAssociationSetMultiplicity {
    Integer lowerBound;
    Integer upperBound;
    public TORESTAssociationSetMultiplicity(Integer lowerBoud) {
        super();
        this.lowerBound = lowerBoud;
    }
    public Integer getLowerBound() {
        return lowerBound;
    }
    public void setLowerBound(Integer lowerBound) {
        this.lowerBound = lowerBound;
    }
    public Integer getUpperBound() {
        return upperBound;
    }
    public void setUpperBound(Integer upperBound) {
        this.upperBound = upperBound;
    }
}
