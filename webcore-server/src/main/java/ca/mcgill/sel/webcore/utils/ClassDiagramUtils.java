package ca.mcgill.sel.webcore.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.emf.common.util.EList;

import ca.mcgill.sel.classdiagram.Association;
import ca.mcgill.sel.classdiagram.AssociationEnd;
import ca.mcgill.sel.classdiagram.CdmFactory;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.Classifier;
import ca.mcgill.sel.classdiagram.ObjectType;
import ca.mcgill.sel.classdiagram.Parameter;
import ca.mcgill.sel.classdiagram.Type;
//import ca.mcgill.sel.classdiagram.ui.utils.CdmModelUtils;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.controller.COREControllerFactory;
import ca.mcgill.sel.core.controller.ReuseController;
import ca.mcgill.sel.core.util.COREArtefactUtil;
import ca.mcgill.sel.core.util.COREModelUtil;
//import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.util.MetamodelRegex;

//public class ClassDiagramUtils {
//	/**
//	 * The default prefix for role names.
//	 */
//	private static final String PREFIX_ROLE_NAME = "my";
//
//
//	/**
//	 * Creates and returns a parameter based on the given parameter string.
//	 * The parameter needs to match following pattern: <code>"[parameterType] [parameterName]"</code>.
//	 *
//	 * @param parameterString the textual description of the parameter
//	 * @return the {@link Parameter} based on the given string
//	 * @throws IllegalArgumentException if the string does not match the expected pattern
//	 */
//	public static Parameter getParameter(String parameterString, ClassDiagram cd) {
//		Matcher matcher = Pattern.compile(MetamodelRegex.REGEX_ATTRIBUTE_DECLARATION).matcher(parameterString);
//		if (!matcher.matches()) {
//			throw new IllegalArgumentException("The type " + parameterString + " does not exist in the model");
//		}
//
//		Parameter parameter = CdmFactory.eINSTANCE.createParameter();
//		Type type = getType(matcher.group(1), cd);
//
//		parameter.setType(type);
//		parameter.setName(matcher.group(3));
//
//		return parameter;
//	}
//
//	/**
//	 * Returns the type that matches the given type name.
//	 *
//	 * @param typeString the type name
//	 * @return the {@link Type} that matches the name of the type
//	 * @throws IllegalArgumentException if the type does not exist
//	 */
//	public static Type getReturnType(String typeString, ClassDiagram cd) {
//		Type returnType = CdmModelUtils.getTypeByName(typeString,cd);
//		if (returnType == null) {
//			throw new IllegalArgumentException("The type " + typeString + " does not exist in the model");
//		}
//
//		return returnType;
//	}
//
//	public static ObjectType getType(String typeString, ClassDiagram cd) {
//		ObjectType type = CdmModelUtils.getAttributeTypeByName(typeString, cd);
//
//		if (type == null) {
//			throw new IllegalArgumentException("The structural view does not contain a primitive type " + type);
//		}
//		return type;
//	}
//
//	/**
//	 * Creates an association end for the classifier "from" and "to" given.
//	 *
//	 * @param association the {@link StructuralView} the association should be added to
//	 * @param from the first class
//	 * @param to the second class
//	 * @param navigable indicate if we want the end to be navigable.
//	 *
//	 * @return the association end created
//	 */
//	public static AssociationEnd createAssociationEnd(Association association, Classifier from, Classifier to,
//			boolean navigable) {
//		// create an association end
//		AssociationEnd associationEnd = CdmFactory.eINSTANCE.createAssociationEnd();
//		associationEnd.setAssoc(association);
//		associationEnd.setLowerBound(1);
//		associationEnd.setNavigable(navigable);
//
//		// The class could have no name.
//		if (to.getName() != null || to.getName().isEmpty()) {
//			// The default association end name is "my" + name of the target class
//			// To assign a unique name to the association end
//			// we must make sure that the class currently does not have already an
//			// association end with that name
//			String potentialNamePrefix = PREFIX_ROLE_NAME + to.getName();
//			String potentialName = potentialNamePrefix;
//			int index = 1;
//
//			//if from is NULL, we're creating nary association
//			if (from != null) {
//				EList<AssociationEnd> ends = from.getAssociationEnds();
//				if (ends.size() > 0) {
//					int i = 0;
//					AssociationEnd e;
//					while (i < ends.size()) {
//						e = ends.get(i);
//						if (e.getName().equals(potentialName)) {
//							potentialName = potentialNamePrefix + index;
//							index++;
//							i = 0;
//						} else {
//							i++;
//						}
//					}
//				}
//			}
//			associationEnd.setName(potentialName);
//		}
//
//		return associationEnd;
//	}
//
//
//	/**
//	 * Create an extend relationship between extendedModel and extendingModel.
//	 * Does nothing if the creation is not valid.
//	 *
//	 * @param extendingArtefact - The aspect that extends
//	 * @param cdmToExtend - The class diagram to extend.
//	 */
//	public static void extendAspect(final COREArtefact extendingArtefact, final ClassDiagram cdmToExtend) {
//		String errorMessage = null;
//		ClassDiagram extendingCdm = (ClassDiagram)
//				((COREExternalArtefact) extendingArtefact).getRootModelElement();
//		// Look for error cases.
//		if (cdmToExtend.equals(extendingCdm)) {
//			errorMessage = Strings.POPUP_ERROR_SELF_EXTENDS;
//		} else if (COREModelUtil.collectExtendedModels(cdmToExtend).contains(extendingCdm)) {
//			errorMessage = Strings.POPUP_ERROR_CYCLIC_EXTENDS;
//		}
//		// Check if the extendingCdm is already extended.
//		for (COREModelComposition composition : extendingArtefact.getModelExtensions()) {
//			if (composition.getSource() == cdmToExtend) {
//				errorMessage = Strings.POPUP_ERROR_SAME_EXTENDS;
//				break;
//			}
//		}
//		if (errorMessage != null) {
////			RamApp.getActiveScene().displayPopup(errorMessage, PopupType.ERROR);
//			return;
//		}
//		// We can create the composition.
//		ReuseController controller = COREControllerFactory.INSTANCE.getReuseController();
//		controller.createModelExtension(extendingArtefact,
//				COREArtefactUtil.getReferencingExternalArtefact(cdmToExtend));
//	}
//
//}
