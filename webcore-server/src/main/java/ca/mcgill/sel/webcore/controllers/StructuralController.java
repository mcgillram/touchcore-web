package ca.mcgill.sel.webcore.controllers;

import org.json.JSONObject;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.StructuralView;
import ca.mcgill.sel.ram.controller.ControllerFactory;
import ca.mcgill.sel.webcore.WebResourceMapper;
import ca.mcgill.sel.webcore.WebResourceMapperManager;
import ca.mcgill.sel.webcore.utils.Utils;

/**
 * Contains all the controllers related to StructuralView.
 * 
 * @author arthurls
 *
 */
@Controller
public class StructuralController {

    /**
     * Creates a new class in the model.
     * @param body {'aspectName': String, 'className': String,
                    'dataType': boolean, 'xPosition': float, 'yPosition': float}
     */
    @MessageMapping("/createNewClass")
    public void createClass(String body) {
        MainController.lock.lock();
        System.out.println("/createNewClass " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        String className = obj.getString("className");
        boolean dataType = obj.getBoolean("dataType");
        float xPosition = obj.getFloat("xPosition");
        float yPosition = obj.getFloat("yPosition");
        
        StructuralView structView = WebResourceMapperManager.getSingleAspect(aspectName).getStructuralView();
                
        ControllerFactory.INSTANCE.getStructuralViewController()
            .createNewClass(structView, className, dataType, xPosition, yPosition);
        MainController.lock.unlock();
    }
    
    /**
     * Removes a new class in the model.
     * @param body {'aspectName': String, 'classId': int}
     */
    @MessageMapping("/removeClassifier")
    public void removeClassifier(String body) {
        MainController.lock.lock();
        System.out.println("/removeClassifier " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int classId = obj.getInt("classId");
        
        Classifier classifier = 
                (Classifier) Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), classId);
                
        ControllerFactory.INSTANCE.getStructuralViewController().removeClassifier(classifier);
        MainController.lock.unlock();
    }
    
    /**
     * Creates a new association in the model.
     * @param body {'aspectName': String, 'classFromId': int, 'classToId': int, 'bidirectional': boolean}
     */
    @MessageMapping("/createAssociation")
    public void createAssociation(String body) {
        MainController.lock.lock();
        System.out.println("/createAssociation " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int classFromId = obj.getInt("classFromId");
        int classToId = obj.getInt("classToId");
        boolean bidirectional = obj.getBoolean("bidirectional");
        
        WebResourceMapper webResourceMapper = WebResourceMapperManager.getSingleMapper(aspectName);
        
        StructuralView structView = webResourceMapper.getAspect().getStructuralView();
        Classifier classFrom = (Classifier) Utils.getEObjectById(webResourceMapper.getIdMapping(), classFromId);
        Classifier classTo = (Classifier) Utils.getEObjectById(webResourceMapper.getIdMapping(), classToId);
            
        ControllerFactory.INSTANCE.getStructuralViewController()
            .createAssociation(structView, classFrom, classTo, bidirectional);
        MainController.lock.unlock();
    }
    
    /**
     * Creates a new enum in the model.
     * @param body {'aspectName': String, 'enumName': String, 'xPosition': float, 'yPosition': float}
     */
    @MessageMapping("/createEnum")
    public void createEnum(String body) {
        MainController.lock.lock();
        System.out.println("/createEnum " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        String enumName = obj.getString("enumName");
        float xPosition = obj.getFloat("xPosition");
        float yPosition = obj.getFloat("yPosition");
        
        StructuralView structView = WebResourceMapperManager.getSingleAspect(aspectName).getStructuralView();
                
        ControllerFactory.INSTANCE.getStructuralViewController().createEnum(structView, enumName, xPosition, yPosition);
        MainController.lock.unlock();
    }
}
