package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

public class TORESTClassDiagramImportEnum {
    float x;
    float y;
    String enumName;
    public float getX() {
        return x;
    }
    public void setX(float x) {
        this.x = x;
    }
    public float getY() {
        return y;
    }
    public void setY(float y) {
        this.y = y;
    }
    public String getEnumName() {
        return enumName;
    }
    public void setEnumName(String enumName) {
        this.enumName = enumName;
    }
    public TORESTClassDiagramImportEnum(float x, float y, String enumName) {
        super();
        this.x = x;
        this.y = y;
        this.enumName = enumName;
    }
}
