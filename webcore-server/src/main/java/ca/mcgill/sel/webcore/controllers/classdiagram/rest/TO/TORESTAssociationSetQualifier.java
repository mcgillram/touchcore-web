package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

public class TORESTAssociationSetQualifier {
    Integer typeId;
    public TORESTAssociationSetQualifier(Integer typeId) {
        super();
        this.typeId = typeId;
    }
    public Integer getTypeId() {
        return typeId;
    }
    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }
}
