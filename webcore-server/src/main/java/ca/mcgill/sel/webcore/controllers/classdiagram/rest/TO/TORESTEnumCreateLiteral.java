package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

public class TORESTEnumCreateLiteral {
    Integer rankIndex;
    String literalName;
    public TORESTEnumCreateLiteral(Integer rankIndex, String literalName) {
        super();
        this.rankIndex = rankIndex;
        this.literalName = literalName;
    }
    public Integer getRankIndex() {
        return rankIndex;
    }
    public void setRankIndex(Integer rankIndex) {
        this.rankIndex = rankIndex;
    }
    public String getLiteralName() {
        return literalName;
    }
    public void setLiteralName(String literalName) {
        this.literalName = literalName;
    }
}
