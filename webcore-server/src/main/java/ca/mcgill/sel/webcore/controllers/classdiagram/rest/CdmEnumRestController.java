package ca.mcgill.sel.webcore.controllers.classdiagram.rest;

import ca.mcgill.sel.classdiagram.CDEnum;
import ca.mcgill.sel.classdiagram.CDEnumLiteral;
import ca.mcgill.sel.classdiagram.language.controller.ControllerFactory;
import ca.mcgill.sel.webcore.BroadcastContentManagerMapper;
import ca.mcgill.sel.webcore.WebResourceClassDiagramMapperManager;
import ca.mcgill.sel.webcore.authentication.entity.User;
import ca.mcgill.sel.webcore.authentication.workspace.WorkspaceAccessUtils;
import ca.mcgill.sel.webcore.controllers.MainController;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTEnumCreateLiteral;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTEnumRenameEnum;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTEnumRenameLiteral;
import ca.mcgill.sel.webcore.utils.Utils;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("{username}/classdiagram/{cdmName}/enum")
public class CdmEnumRestController {
    
    /**
     * Removes an enum.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param enumId id of enum
     **/
    @DeleteMapping("{enumId}")
    public void removeEnum(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer enumId) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        MainController.lock.lock();        
        CDEnum enums = (CDEnum) Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), enumId);
        
        ControllerFactory.INSTANCE.getEnumController().deleteEnum(enums);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Rename the enum.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param enumId id of enum
     * @param TO {"newName": String}
     **/
    @PutMapping("{enumId}/rename")
    public void renameEnum(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer enumId, 
            @RequestBody TORESTEnumRenameEnum TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        CDEnum enums = 
                (CDEnum) Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), enumId);
        MainController.lock.lock();  
        ControllerFactory.INSTANCE.getEnumController().renameEnum(enums, TO.getNewName());
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Creates a literal.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param enumId id of enum
     * @param TO {"rankIndex": Integer, "literalName": String}
     **/
    @PostMapping("{enumId}/literal")
    public void createLiteral(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer enumId, 
            @RequestBody TORESTEnumCreateLiteral TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        MainController.lock.lock();
        CDEnum owner = (CDEnum) Utils.getEObjectById(WebResourceClassDiagramMapperManager
                .getSingleIdMapping(username, cdmName), enumId);
        
        ControllerFactory.INSTANCE.getEnumController()
            .createREnumLiteral(owner, TO.getRankIndex(), TO.getLiteralName());
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Remove a literal.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param literalId id of literal
     **/
    @DeleteMapping("literal/{literalId}")
    public void removeLiteral(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer literalId) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        MainController.lock.lock();        
        CDEnumLiteral literal = 
                (CDEnumLiteral) Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), literalId);
        
        ControllerFactory.INSTANCE.getEnumController().removeLiteral(literal);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Rename the literal.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param literalId id of literal
     * @param TO {"newName": String}
     **/
    @PutMapping("literal/{literalId}/rename")
    public void renameLiteral(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer literalId, 
            @RequestBody TORESTEnumRenameLiteral TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        CDEnumLiteral literal = 
                (CDEnumLiteral) Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), literalId);
        MainController.lock.lock();  
        ControllerFactory.INSTANCE.getEnumController().renameLiteral(literal, TO.getNewName());
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
}
