package ca.mcgill.sel.webcore.controllers.classdiagram.rest;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ca.mcgill.sel.classdiagram.Attribute;
import ca.mcgill.sel.classdiagram.Class;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.Classifier;
import ca.mcgill.sel.classdiagram.ObjectType;
import ca.mcgill.sel.classdiagram.Operation;
import ca.mcgill.sel.classdiagram.Parameter;
import ca.mcgill.sel.classdiagram.Type;
import ca.mcgill.sel.classdiagram.language.controller.ControllerFactory;
import ca.mcgill.sel.webcore.BroadcastContentManagerMapper;
import ca.mcgill.sel.webcore.WebResourceClassDiagramMapperManager;
import ca.mcgill.sel.webcore.authentication.entity.User;
import ca.mcgill.sel.webcore.authentication.workspace.WorkspaceAccessUtils;
import ca.mcgill.sel.webcore.controllers.MainController;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTClassCreateAttribute;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTClassCreateAttributeCopy;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTClassCreateOperation;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTClassCreateParameter;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTClassRenameAttribute;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTClassRenameClass;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTClassRenameOperation;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTClassRenameParameter;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTClassSetAttributePosition;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTClassSetClassPosition;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTClassSetOperationAndClassVisibility;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTClassSetOperationPosition;
import ca.mcgill.sel.webcore.utils.Utils;

@RestController
@RequestMapping("{username}/classdiagram/{cdmName}/class")
public class CdmClassRestController {
    private static final String PARAMETER_DELIMITER_PATTERN = ",\\s?";
    
    /**
     * Sets the position of the class.
     * 
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param classId id of class
     * @param TO {"xPosition": float, "yPosition": float}
     **/
    @PutMapping("{classId}/position")
    public void setClassPosition(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, @PathVariable Integer classId, 
            @RequestBody TORESTClassSetClassPosition TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Classifier owner = (Classifier) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), classId);         

        MainController.lock.lock();
        //        ControllerFactory.INSTANCE
        ControllerFactory.INSTANCE.getClassController().moveClassifier(owner, TO.getxPosition(), TO.getyPosition());
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Rename the class.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param classId id of class
     * @param TO {"newName": String}
     **/
    @PutMapping("{classId}/rename")
    public void renameClass(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer classId, 
            @RequestBody TORESTClassRenameClass TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        MainController.lock.lock();
        Classifier owner = (Classifier) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), classId);         
        //        ControllerFactory.INSTANCE
        ControllerFactory.INSTANCE.getClassController().renameClass(owner, TO.getNewName());
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Creates an attribute.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param classId id of class
     * @param TO {"rankIndex": Integer, "typeId": Integer, "attributeName": String}
     **/
    @PostMapping("{classId}/attribute")
    public void createAttribute(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer classId, 
            @RequestBody TORESTClassCreateAttribute TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Class owner = (Class) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), classId);
        Integer rankIndex = TO.getRankIndex();
        if (rankIndex == 0) {
            rankIndex = owner.getAttributes().size();
        }

        ObjectType type = (ObjectType) Utils.getEObjectById(WebResourceClassDiagramMapperManager
                .getSingleIdMapping(username, cdmName), TO.getTypeId());

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().createAttribute(owner, 0, TO.getAttributeName(), type);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Removes an attribute.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param attributeId id of attribute
     **/
    @DeleteMapping("attribute/{attributeId}")
    public void removeAttribute(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer attributeId) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Attribute attr = (Attribute) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), attributeId);   

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().removeAttribute(attr);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Renames the given attribute to newName.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param attributeId id of attribute
     * @param TO {"newName": String}
     **/
    @PutMapping("attribute/{attributeId}/rename")
    public void renameAttribute(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer attributeId, 
            @RequestBody TORESTClassRenameAttribute TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Attribute attr = (Attribute) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), attributeId);   

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().renameAttribute(attr, TO.getNewName());
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Creates a copy of an attribute.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param attributeId id of attribute
     * @param TO {"newName": String}
     **/
    @PostMapping("attribute/{attributeId}/copy")
    public void createAttributeCopy(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer attributeId, 
            @RequestBody TORESTClassCreateAttributeCopy TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        ClassDiagram owner = WebResourceClassDiagramMapperManager.getSingleClassDiagram(username, cdmName);   
        Attribute attr = (Attribute) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), attributeId);   

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().createAttributeCopy(owner, attr,  TO.getNewName());
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Sets the attribute position.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param attributeId id of attribute
     * @param TO {"index", Integer}
     **/
    @PutMapping("attribute/{attributeId}/position")
    public void setAttributePosition(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer attributeId, 
            @RequestBody TORESTClassSetAttributePosition TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Attribute attr = (Attribute) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), attributeId);   

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().setAttributePosition(attr, TO.getIndex());
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    /**
     * Creates an operation.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param TO {"visibility": VisibilityType, "rankIndex": Integer, "returnObjectTypeId": Integer
     * , "parameters": List<Parameter>, "operationName": String}
     **/
    @PostMapping("{classId}/operation")
    public void createOperation(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer classId, 
            @RequestBody TORESTClassCreateOperation TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Class owner = (Class) Utils.getEObjectById(WebResourceClassDiagramMapperManager
                .getSingleIdMapping(username, cdmName), classId);
        
        if (TO.getRankIndex() == 0) {
            TO.setRankIndex(owner.getOperations().size());
        }
        if (TO.getVisibility() == null) {
            throw new IllegalArgumentException("The visibility is not valid");
        }
        Type returnType = (Type) Utils.getEObjectById(WebResourceClassDiagramMapperManager
                .getSingleIdMapping(username, cdmName), TO.getReturnObjectTypeId());

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController()
            .createOperation(owner, TO.getRankIndex(), TO.getOperationName(), TO.getVisibility(),
                returnType, TO.getParameters());
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Removes the operation.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param operationId id of operation
     **/
    @DeleteMapping("operation/{operationId}")
    public void removeOperation(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer operationId) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Operation opera = (Operation) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), operationId);   

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().removeOperation(opera);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Rename the operation.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param operationId id of operation
     * @param TO {"newName": String}
     **/
    @PutMapping("operation/{operationId}/rename")
    public void renameOperation(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer operationId, 
            @RequestBody TORESTClassRenameOperation TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Operation opera = (Operation) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), operationId);   

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().renameOperation(opera, TO.getNewName());
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Sets the position of the operation.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param operationId id of operation
     * @param TO {"index", Integer}
     **/
    @PutMapping("operation/{operationId}/position")
    public void setOperationPosition(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer operationId, 
            @RequestBody TORESTClassSetOperationPosition TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Operation op = (Operation) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), operationId);   

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().setOperationPosition(op, TO.getIndex());
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Switches the visibility of an operation and class.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param operationId id of operation
     * @param TO {"classId": Integer, "visibility": VisibilityType}
     **/
    @PutMapping("operation/{operationId}/visibility")
    public void setOperationAndClassVisibility(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer operationId, 
            @RequestBody TORESTClassSetOperationAndClassVisibility TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Class owner = (Class) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), TO.getClassId());   

        Operation op = (Operation) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), operationId);   

        if (TO.getVisibility() == null) {
            throw new IllegalArgumentException("The visibility is not valid");
        }
        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController()
            .changeOperationAndClassVisibility(owner,  op, TO.getVisibility());
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Creates a getter operation for an attribute.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param attributeId id of attribute
     **/
    @PostMapping("attribute/{attributeId}/getter")
    public void createGetter(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer attributeId) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Attribute attribute = 
                (Attribute) Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), attributeId);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().createGetterOperation(attribute);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Creates a setter operation for an attribute.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param attributeId id of attribute
     **/
    @PostMapping("attribute/{attributeId}/setter")
    public void createSetter(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer attributeId) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Attribute attribute = 
                (Attribute) Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), attributeId);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().createSetterOperation(attribute);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Creates both a getter and setter for the operation.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model 
     * @param attributeId id of attribute
     **/
    @PostMapping("attribute/{attributeId}/getterAndSetter")
    public void createGetterAndSetter(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer attributeId) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Attribute attribute = 
                (Attribute) Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), attributeId);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().createGetterAndSetterOperation(attribute);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Creates a constructor for the class.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param classId id of class
     **/
    @PostMapping("{classId}/constructor")
    public void createConstructor(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer classId) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Class owner = (Class) Utils.getEObjectById(WebResourceClassDiagramMapperManager
                .getSingleIdMapping(username, cdmName), classId);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().createConstructor(owner);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Creates a destructor for the class.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param classId id of class
     **/
    @PostMapping("{classId}/destructor")
    public void createDestructor(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            Integer classId) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Class owner = (Class) Utils.getEObjectById(WebResourceClassDiagramMapperManager
                .getSingleIdMapping(username, cdmName), classId);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().createDestructor(owner);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Creates a parameter for an operation.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param operationId id of operation
     * @param TO {"index": Integer, "paraName": String, "typeId": Integer}
     **/
    @PostMapping("operation/{operationId}/parameter")
    public void createParameter(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer operationId, 
            @RequestBody TORESTClassCreateParameter TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Operation op = (Operation) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), operationId);   
        Type type = (Type) Utils.getEObjectById(WebResourceClassDiagramMapperManager
                .getSingleIdMapping(username, cdmName), TO.getTypeId());

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().createParameter(op, TO.getIndex(), TO.getParaName(), type);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Removes a parameter from an operation.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param parameterId id of parameter
     **/
    @DeleteMapping("operation/parameter/{parameterId}")
    public void removeParameter(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer parameterId) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Parameter para = (Parameter) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), parameterId);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().removeParameter(para);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Rename the operation.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param parameterId id of parameter
     * @param TO {"newName": String}
     **/
    @PutMapping("operation/parameter/{parameterId}/rename")
    public void renameParameter(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer parameterId, 
            @RequestBody TORESTClassRenameParameter TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Parameter para = (Parameter) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), parameterId);   

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().renameParameter(para, TO.getNewName());
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Switches the abstract status of a class.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param classId id of class
     **/
    @PutMapping("{classId}/abstract")
    public void switchAbstract(@AuthenticationPrincipal final User user,
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer classId) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Class owner = (Class) Utils.getEObjectById(WebResourceClassDiagramMapperManager
                .getSingleIdMapping(username, cdmName), classId);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().switchAbstract(owner);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Switches the abstract status of an operation.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param operationId id of operation
     **/
    @PutMapping("operation/{operationId}/abstract")
    public void switchOperationAbstract(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer operationId) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Operation op = (Operation) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), operationId);   

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().switchOperationAbstract(op);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Switches the static status of an attribute.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param attributeId id of attribute
     **/
    @PutMapping("attribute/{attributeId}/static")
    public void switchAttributeStatic(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            Integer attributeId) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Attribute attribute = 
                (Attribute) Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), attributeId);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().switchAttributeStatic(attribute);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Switches the static status of an operation.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param operationId id of operation
     **/
    @PutMapping("operation/{operationId}/static")
    public void switchOperationStatic(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            Integer operationId) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Operation op = (Operation) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), operationId);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassController().switchOperationStatic(op);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
}
