package ca.mcgill.sel.webcore.authentication.jwt;

import java.util.Optional;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.NonNull;

import static lombok.AccessLevel.PACKAGE;

/**
 * Token Authentication Provider.
 * 
 * @author Ridwan Kurmally
 */
@Component
public class TokenAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    @NonNull
    private UserAuthenticationService auth;
    
    public TokenAuthenticationProvider(UserAuthenticationService auth) {
        this.auth = auth;
    }

    @Override
    protected void additionalAuthenticationChecks(
            final UserDetails user, final UsernamePasswordAuthenticationToken authentication)
            throws AuthenticationException {
        // Nothing to do

    }

    @Override
    protected UserDetails retrieveUser(final String username, final UsernamePasswordAuthenticationToken authentication)
            throws AuthenticationException {
        final Object token = authentication.getCredentials();
        return Optional
          .ofNullable(token)
          .map(String::valueOf)
          .flatMap(auth::findByToken)
          .orElseThrow(() -> new UsernameNotFoundException("Cannot find user with authentication token=" + token));
    }
}
