package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

public class TORESTClassDiagramCreateNoteWithContent {
    float x;
    float y;
    String content;
    public TORESTClassDiagramCreateNoteWithContent(float x, float y, String content) {
        super();
        this.x = x;
        this.y = y;
        this.content = content;
    }
    public float getX() {
        return x;
    }
    public void setX(float x) {
        this.x = x;
    }
    public float getY() {
        return y;
    }
    public void setY(float y) {
        this.y = y;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
}
