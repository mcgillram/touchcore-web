package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

public class TORESTAssociationRenameAssociationEnd {
    String newName;
    
    public TORESTAssociationRenameAssociationEnd() {
    }
    
    public TORESTAssociationRenameAssociationEnd(String newName) {
        this.newName = newName;
    }
    public String getNewName() {
        return this.newName;
    }
    public void setNewName(String newName) {
        this.newName = newName;
    }
}
