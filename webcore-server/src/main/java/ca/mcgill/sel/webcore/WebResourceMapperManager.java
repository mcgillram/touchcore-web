package ca.mcgill.sel.webcore;

import java.util.Map;

import org.eclipse.emf.ecore.EObject;

import com.google.common.collect.BiMap;

import ca.mcgill.sel.commons.emf.util.ResourceManager;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.ram.Aspect;

/**
 * Holds all the mapping resource in a Map.
 * The key is the aspect name and the value is the WebResourceMapper
 * 
 * @author arthurls
 *
 */
public final class WebResourceMapperManager {

    /**
     * If TouchCORE and WebCore are launched or just WebCore.
     */
    private static boolean isDuo;
    
    /**
     * See {link @WebResourceMapper}.
     */
    private static Map<String, WebResourceMapper> webResourceMapperDB;
    
    /**
     * The current loaded concern. null if no concern is loaded.
     */
    private static COREConcern concern;
    
    /**
     * Prevents default constructor.
     */
    private WebResourceMapperManager() {
    }

    /**
     * Getter for the Model Manager DB.
     * 
     * @return the webResourceMapperDB
     */
    public static Map<String, WebResourceMapper> getWebResourceMapperDB() {
        return webResourceMapperDB;
    }
    
    /**
     * Setter for the Model Manager DB.
     * 
     * @param webResourceMapperDB the new model manager
     */
    public static void setWebResourceMapperDB(Map<String, WebResourceMapper> webResourceMapperDB) {
        WebResourceMapperManager.webResourceMapperDB = webResourceMapperDB;
    }
    
    /**
     * Getter for the current CORE concern.
     * @return the current concern
     */
    public static COREConcern getConcern() {
        return concern;
    }

    /**
     * Setter for the current CORE concern.
     * @param concern the new concern
     */
    public static void setConcern(COREConcern concern) {
        WebResourceMapperManager.concern = concern;
    }

    /**
     * Getter for isDuo.
     * @return isDuo
     */
    public static boolean isDuo() {
        return isDuo;
    }

    /**
     * Setter for isDuo.
     * @param isDuo 
     */
    public static void setDuo(boolean isDuo) {
        WebResourceMapperManager.isDuo = isDuo;
    }

    /**
     * Gets one Model Manager from a Model Name.
     * 
     * @param modelName the model you want to get
     * @return the modelManager matching the name
     */
    public static WebResourceMapper getSingleMapper(String modelName) {
        return webResourceMapperDB.get(modelName);
    }
    
    /**
     * Gets an Aspect from a Model Name.
     * 
     * @param modelName the model you want to get it from
     * @return the Aspect matching the name
     */
    public static Aspect getSingleAspect(String modelName) {
        return webResourceMapperDB.get(modelName).getAspect();
    }
    
    /**
     * Gets an IdMapping from a Model Name.
     * 
     * @param modelName the model you want to get it from
     * @return the mapping matching the name
     */
    public static BiMap<EObject, Integer> getSingleIdMapping(String modelName) {
        return webResourceMapperDB.get(modelName).getIdMapping();
    }
    
    /**
     * Add a Model Manager to the DB.
     * 
     * @param aspectName the model you want to add to the DB
     */
    public static void addModelManager(String aspectName) {
        System.out.println("Adding Resource for " + aspectName);
        if (!webResourceMapperDB.containsKey(aspectName)) {
            for (COREArtefact artefact : concern.getArtefacts()) {
                if (artefact instanceof COREExternalArtefact && artefact.getName().contentEquals(aspectName)) {
                    //EObject model = ResourceManager.loadModel("resources/samples/" + aspectName + ".ram");
                    COREExternalArtefact art = (COREExternalArtefact) artefact;
                    WebResourceMapper webResourceMapper = new WebResourceMapper(art.getRootModelElement());
                    webResourceMapperDB.put(aspectName, webResourceMapper);
                }
            }
        }
    }
}
