package ca.mcgill.sel.webcore.authentication.entity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.springframework.stereotype.Service;

import static java.util.Optional.ofNullable;

import java.util.Collection;

/**
 * Service to save and find users. The users are stored in memory - all users are lost when the server shuts down and 
 * have to be registered manually when it is launched again.
 * 
 * @author Ridwan Kurmally
 */
@Service
public class InMemoryUsers implements UserCrudService {

    private Map<String, User> users = new HashMap<>();

    @Override
    public User save(final User user) {
        return users.put(user.getId(), user);
    }

    @Override
    public Optional<User> find(final String id) {
        return ofNullable(users.get(id));
    }

    @Override
    public Optional<User> findByUsername(final String username) {
        return users
        .values()
        .stream()
        .filter(u -> Objects.equals(username, u.getUsername()))
        .findFirst();
    }
    
    @Override
    public Collection<User> getAllUsers() {
        return users.values();
    }
}
