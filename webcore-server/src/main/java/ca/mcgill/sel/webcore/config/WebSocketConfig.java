package ca.mcgill.sel.webcore.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

/**
 * Simple socket configuration.
 * Uses SockJS and standard socket routes.
 * 
 * @author arthurls
 *
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        // Carry the messages back to the client on destinations prefixed with "/topic".
        config.enableSimpleBroker("/topic", "/queue", "/user");
        
        // Sets all the @MessageMapping annotated methods to default /api/...
        config.setApplicationDestinationPrefixes("/api");
        config.setUserDestinationPrefix("/user");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/webcore")
                .setAllowedOrigins("*")
                .withSockJS();
    }
}
