package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

public class TORESTClassSetAttributePosition {
    Integer index;
    public TORESTClassSetAttributePosition(Integer index) {
        super();
        this.index = index;
    }
    public Integer getIndex() {
        return index;
    }
    public void setIndex(Integer index) {
        this.index = index;
    }
}
