package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

public class TORESTClassDiagramCreateEnum {
    String enumName;
    float x;
    float y;
    public TORESTClassDiagramCreateEnum(String enumName, float x, float y) {
        super();
        this.enumName = enumName;
        this.x = x;
        this.y = y;
    }
    public String getEnumName() {
        return enumName;
    }
    public void setEnumName(String enumName) {
        this.enumName = enumName;
    }
    public float getX() {
        return x;
    }
    public void setX(float x) {
        this.x = x;
    }
    public float getY() {
        return y;
    }
    public void setY(float y) {
        this.y = y;
    }
}
