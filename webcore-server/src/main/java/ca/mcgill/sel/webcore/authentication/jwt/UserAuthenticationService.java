package ca.mcgill.sel.webcore.authentication.jwt;

import java.util.Optional;

import ca.mcgill.sel.webcore.authentication.entity.User;

/**
 * Authentication service interface used to log users in and out of the server.
 * 
 * @author Ridwan Kurmally
 */
public interface UserAuthenticationService {
    
    /**
     * Logs in with the given {@code username} and {@code password}.
     *
     * @param username
     * @param password
     * @return an identifier/token when login succeeds
     */
    Optional<String> login(String username, String password);

    /**
     * Finds a user by its token.
     *
     * @param token user token
     * @return user if found
     */
    Optional<User> findByToken(String token);

    /**
     * Logs out the given input {@code user}.
     *
     * @param user the user to logout
     */
    void logout(User user);
    
}
