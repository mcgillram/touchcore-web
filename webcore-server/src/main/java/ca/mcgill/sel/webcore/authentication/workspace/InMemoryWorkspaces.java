package ca.mcgill.sel.webcore.authentication.workspace;

import static java.util.Optional.ofNullable;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Service;

import ca.mcgill.sel.webcore.WebResourceClassDiagramMapperManager;

/**
 * Service to save and find workspaces. It first preloads all workspaces which it finds on disk when the server
 * is launched. The permissions are not persisted when the server shuts down. 
 * Hence, permissions are reset when restarting.
 * 
 * @author Ridwan Kurmally
 */

@Service
public class InMemoryWorkspaces implements WorkspaceCrudService {
    
    /**
     * Map from user name to model name to workspace. 
     */
    private static Map<String, Map<String, Workspace>> workspaces = new HashMap<>();
    
    static {
        preload();
    }

    /**
     * TODO : Maybe we should create a new directory from this function directly instead of doing it.
     * See {@link ca.mcgill.sel.webcore.controllers.classdiagram
     * .rest.CdmClassDiagramRestController#putClassDiagramWithUser putClassDiagramWithUser}
     */
    @Override
    public Workspace save(String owner, String modelName, List<String> usersWithReadAcess,
            List<String> usersWithWriteAcess) {
        workspaces.putIfAbsent(owner, new HashMap<String, Workspace>());
        Workspace workspace = new Workspace(
                owner, 
                modelName,
                usersWithReadAcess,
                usersWithWriteAcess);
        workspaces.get(owner).put(modelName, workspace);
        return workspace;
    }


    @Override
    public Optional<Workspace> find(String owner, String modelName) {
        if (!workspaces.containsKey(owner)) {
            return ofNullable(null);
        }
        return ofNullable(workspaces.get(owner).get(modelName));
    }
    
    @Override
    public List<Workspace> getAllWorkspaces() {
        List<Workspace> resultWorkspaces = new ArrayList<>();
        for (Map<String, Workspace> map : workspaces.values()) {
            resultWorkspaces.addAll(map.values());
        }
        return resultWorkspaces;
    }
    
    @Override
    public List<Workspace> getAllWorkspaces(String owner) {
        if (!workspaces.containsKey(owner)) {
            return null;
        }
        return new ArrayList<>(workspaces.get(owner).values());
    }
    
    private static void preload() {
        File dir = new File(WebResourceClassDiagramMapperManager.getUserConcernsRootPath());
        File[] usersDir = dir.listFiles();
        for (File userDir : usersDir) {
            workspaces.putIfAbsent(userDir.getName(), new HashMap<String, Workspace>());
            File[] userModels = userDir.listFiles();
            for (File userModel : userModels) {
                Workspace workspace = new Workspace(userDir.getName(), userModel.getName());
                workspaces.get(userDir.getName()).put(userModel.getName(), workspace);
            }
        }
    }

}
