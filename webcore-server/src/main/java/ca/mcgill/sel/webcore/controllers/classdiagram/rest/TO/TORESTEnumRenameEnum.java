package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

public class TORESTEnumRenameEnum {
    String newName;
    
    public TORESTEnumRenameEnum() {
    }
    
    public TORESTEnumRenameEnum(String newName) {
        this.newName = newName;
    }
    
    public String getNewName() {
        return this.newName;
    }
    public void setClassId(String newName){
        this.newName = newName;
    }
}
