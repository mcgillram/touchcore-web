package ca.mcgill.sel.webcore.authentication.jwt;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableMap;

import ca.mcgill.sel.webcore.authentication.date.DateService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Clock;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.impl.compression.GzipCompressionCodec;
import lombok.experimental.FieldDefaults;

import static io.jsonwebtoken.SignatureAlgorithm.HS256;
import static io.jsonwebtoken.impl.TextCodec.BASE64;
import static java.util.Objects.requireNonNull;
import static lombok.AccessLevel.PRIVATE;
import static org.apache.commons.lang3.StringUtils.substringBeforeLast;


/**
 * Service to provide Json Web Token (JWT).
 * 
 * @author Ridwan Kurmally
 */
@Service
@FieldDefaults(level = PRIVATE, makeFinal = true)
final class JWTTokenService implements Clock, TokenService {
    private static final String DOT = ".";
    private static final GzipCompressionCodec COMPRESSION_CODEC = new GzipCompressionCodec();
    
    /**
     * List of tokens that have been blocked from logging out.
     */
    private final List<String> blockedTokens = new ArrayList<>();

    private DateService dates;
    private String issuer;
    private int expirationSec;
    private int clockSkewSec;
    private String secretKey;

    JWTTokenService(final DateService dates,
                  @Value("${jwt.issuer:webcore}") final String issuer,
                  // CHANGE THE DURATION BEFORE A JWT TOKEN EXPIRES HERE
                  @Value("${jwt.expiration-sec:600}") final int expirationSec,
                  @Value("${jwt.clock-skew-sec:300}") final int clockSkewSec,
                  // CHANGE THE SECRET KEY HERE
                  @Value("${jwt.secret:secret}") final String secret) {
        super();
        this.dates = requireNonNull(dates);
        this.issuer = requireNonNull(issuer);
        this.expirationSec = requireNonNull(expirationSec);
        this.clockSkewSec = requireNonNull(clockSkewSec);
        this.secretKey = BASE64.encode(requireNonNull(secret));
    }

    /**
     * Return a JWT according to the attributes which will not expire.
     * 
     * @param attributes
     * @return a permanent JWT
     */
    @Override
    public String permanent(final Map<String, String> attributes) {
        return newToken(attributes, 0);
    }

    /**
     * Return a JWT according to the attributes which can expire.
     * 
     * @param attributes
     * @return an expiring JWT
     */
    @Override
    public String expiring(final Map<String, String> attributes) {
        return newToken(attributes, expirationSec);
    }

    /**
     * Return a JWT according to the attributes which expires given an expiration delay.
     * 
     * @param attributes
     * @param expiresInSec time in second for the token to expire
     * @return a JWT
     */
    private String newToken(final Map<String, String> attributes, final int expiresInSec) {
        final DateTime now = dates.now();
        final Claims claims = Jwts
            .claims()
            .setIssuer(issuer)
            .setIssuedAt(now.toDate());

        if (expiresInSec > 0) {
            final DateTime expiresAt = now.plusSeconds(expiresInSec);
            claims.setExpiration(expiresAt.toDate());
        }
        claims.putAll(attributes);

        return Jwts
      .builder()
      .setClaims(claims)
      .signWith(HS256, secretKey)
      .compressWith(COMPRESSION_CODEC)
      .compact();
    }
    
    /**
     * Verifies that a token is valid and returns the attributes encrypted within it.
     * It does the following checks: 
     * 1.) The token should not be within the block list.
     * 2.) The token should not have expired yet.
     * 3.) The token should have been encrypted using the secret key.
     * 
     * @param token 
     * @return a map of the attributes
     */
    @Override
    public Map<String, String> verify(final String token) {
        
        if (blockedTokens.contains(token)) {
            return ImmutableMap.of();
        }
        
        final JwtParser parser = Jwts
                .parser()
                .requireIssuer(issuer)
                .setClock(this)
                .setAllowedClockSkewSeconds(clockSkewSec)
                .setSigningKey(secretKey);
        return parseClaims(() -> parser.parseClaimsJws(token).getBody());
    }
    
    /**
     * Verifies that a token is valid and returns the attributes encrypted within it.
     * It does the following checks: 
     * 1.) The token should not be within the block list/
     * 2.) The token should not have expired yet.
     * 
     * @param token 
     * @return a map of the attributes
     */
    @Override
    public Map<String, String> untrusted(final String token) {
        
        if (blockedTokens.contains(token)) {
            return ImmutableMap.of();
        }
        
        final JwtParser parser = Jwts
                .parser()
                .requireIssuer(issuer)
                .setClock(this)
                .setAllowedClockSkewSeconds(clockSkewSec);

        // See: https://github.com/jwtk/jjwt/issues/135
        final String withoutSignature = substringBeforeLast(token, DOT) + DOT;
        return parseClaims(() -> parser.parseClaimsJwt(withoutSignature).getBody());
    }

    /**
     * Parse the claims to return the map of attributes.
     * 
     * @param toClaims
     * @return map of attributes
     */
    private static Map<String, String> parseClaims(final Supplier<Claims> toClaims) {
        try {
            final Claims claims = toClaims.get();
            final ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();
            for (final Map.Entry<String, Object> e : claims.entrySet()) {
                builder.put(e.getKey(), String.valueOf(e.getValue()));
            }
            return builder.build();
        } catch (final IllegalArgumentException | JwtException e) {
            return ImmutableMap.of();
        }
    }

    /**
     * Returns the current date.
     */
    @Override
    public Date now() {
        final DateTime now = dates.now();
        return now.toDate();
    }

    /**
     * Invalidates a token by adding it to the blocked list.
     * 
     * @param token 
     */
    @Override
    public void invalidate(String token) {
        this.blockedTokens.add(token);
        
    }
}
