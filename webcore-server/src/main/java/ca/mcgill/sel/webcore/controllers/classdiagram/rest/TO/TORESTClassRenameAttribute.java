package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

public class TORESTClassRenameAttribute {
    String newName;

    public TORESTClassRenameAttribute() {
    }
    
    public TORESTClassRenameAttribute(String newName) {
        this.newName = newName;
    }
    public String getNewName() {
        return this.newName;
    }
    public void setNewName(String newName) {
        this.newName = newName;
    }
}
