package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

public class TORESTAssociationSetReferenceType {
    String referenceType;
    public TORESTAssociationSetReferenceType(String referenceType) {
        super();
        this.referenceType = referenceType;
    }
    public String getReferenceType() {
        return referenceType;
    }
    public void setReferenceType(String referenceType) {
        this.referenceType = referenceType;
    }
}
