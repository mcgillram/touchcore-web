package ca.mcgill.sel.webcore.controllers.classdiagram.rest;

import static lombok.AccessLevel.PACKAGE;
import static lombok.AccessLevel.PRIVATE;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.emf.ecore.EObject;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import ca.mcgill.sel.classdiagram.CDEnum;
import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.classdiagram.Classifier;
import ca.mcgill.sel.classdiagram.language.controller.ControllerFactory;
import ca.mcgill.sel.classdiagram.util.CdmInterfaceUtil;
import ca.mcgill.sel.commons.emf.util.ResourceManager;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.perspective.COREPerspectiveUtil;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.ram.util.Constants;
import ca.mcgill.sel.classdiagram.LayoutElement;
import ca.mcgill.sel.classdiagram.NamedElement;
import ca.mcgill.sel.classdiagram.Note;
import ca.mcgill.sel.classdiagram.impl.NoteImpl;

import ca.mcgill.sel.webcore.BroadcastContentManagerMapper;
import ca.mcgill.sel.webcore.WebResourceClassDiagramMapperManager;
import ca.mcgill.sel.webcore.authentication.entity.User;
import ca.mcgill.sel.webcore.authentication.workspace.Workspace;
import ca.mcgill.sel.webcore.authentication.workspace.WorkspaceAccessUtils;
import ca.mcgill.sel.webcore.authentication.workspace.WorkspaceCrudService;
import ca.mcgill.sel.webcore.controllers.MainController;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTClassDiagramCreateAnnotation;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTClassDiagramCreateAssociation;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTClassDiagramCreateClass;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTClassDiagramCreateEnum;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTClassDiagramCreateImplementationClass;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTClassDiagramCreateNote;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTClassDiagramCreateNoteWithContent;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTClassDiagramEditNote;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTClassDiagramExtendModel;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTClassDiagramImportClassifier;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTClassDiagramImportEnum;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTClassDiagramMoveClassifiers;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTClassDiagramMoveNonClassifiers;
import ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO.TORESTClassDiagramRemoveAnnotation;
//import ca.mcgill.sel.webcore.utils.ClassDiagramUtils;
import ca.mcgill.sel.webcore.utils.Utils;
import eu.kartoffelquadrat.asyncrestlib.ResponseGenerator;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;

@RestController
@RequestMapping("{username}/classdiagram/{cdmName}")
@FieldDefaults(level = PRIVATE, makeFinal = true)
@AllArgsConstructor(access = PACKAGE)
public class CdmClassDiagramRestController {
    
    @NonNull
    WorkspaceCrudService workspaces;
   
    /**
     * Creates a new model in the workspace of the specified user.
     * The class diagram name is the same as the cdmName param.
     * 
     * Access Right: Only Owner.
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @return status message
     **/
    @PutMapping("")
    public String putClassDiagramWithUser(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName) {
        // Only the owner can put a new model in their user directory
        WorkspaceAccessUtils.checkOwnerRights(username, user.getUsername());
        // Cannot create new model if it already exists
        WorkspaceAccessUtils.checkWorkspaceAbsent(username, cdmName);
        COREConcern c = COREModelUtil.createConcern(cdmName);
        String userModelFilePath = WebResourceClassDiagramMapperManager.getPath(username, cdmName) + "/" + cdmName;
        
        // Need to save the model before creating a new scene/cdm
        try {
            ResourceManager.saveModel(c, userModelFilePath.concat("." + Constants.CORE_FILE_EXTENSION));
        } catch (IOException e) {
             // Shouldn't happen.
            e.printStackTrace();
        }
        
        // creating cdm
        COREPerspective p = COREPerspectiveUtil.INSTANCE.getPerspective("Design Modelling");        
        COREPerspectiveUtil.INSTANCE.createNewRealizationSceneAndModel(
                             c, c.getFeatureModel().getRoot(), p, "Design_Model");
        
        // Resaving the model so that the .cdm is linked to the .core file.
        try {
            ResourceManager.saveModel(c, userModelFilePath.concat("." + Constants.CORE_FILE_EXTENSION));
        } catch (IOException e) {
             // Shouldn't happen.
            e.printStackTrace();
        }
        
        // Saving new workspace
        workspaces.save(username, cdmName, new ArrayList<>(), new ArrayList<>());
        
        return String.format("Successful in creating the model '%s' for user '%s'", cdmName, username);
    }
    
    
    /**
     * Returns the specified class diagram in the form of a JSON string in accordance with long-poll protocol
     * 
     * Access Right: Owner and anyone with read access.
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param classDiagramHash
     * @return model as a JSON
     **/
    @GetMapping("")
    public DeferredResult<ResponseEntity<String>> getClassDiagram(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @RequestParam(required = false) String classDiagramHash) throws IllegalArgumentException {
        WorkspaceAccessUtils.checkReadRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        /*
         * No hash provided at all -> return a synced update. 
         * We achieve this by setting a hash that clearly differs from any valid hash.
         */
        if (classDiagramHash == null || classDiagramHash.isEmpty()) {
            classDiagramHash = "-";
        }
        return ResponseGenerator.getHashBasedUpdate(BroadcastContentManagerMapper.TIMETOPOLL, 
                BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName), classDiagramHash);
    }
    /**
     * Imports a classifier.
     * 
     * Access Right: Owner and Anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param TO {"x": float, "y": float, "importedClassName": String}
     **/
    @PostMapping("importClassifier")
    public void importClassifier(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @RequestBody TORESTClassDiagramImportClassifier TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        ClassDiagram cdm =  WebResourceClassDiagramMapperManager.getSingleClassDiagram(username, cdmName); 
        Collection<Classifier> classifiers = CdmInterfaceUtil.getAvailableExternalDatatypes(cdm);
        Classifier importedClassifier = null;
        for (Classifier  cla : classifiers) {
            if (cla.getName().equals(TO.getImportedClassName())) {
                importedClassifier = cla;
                break;
            }
        }
        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassDiagramController()
            .importClassifier(cdm, importedClassifier, TO.getX(), TO.getY());
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Imports an enum.
     * 
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param TO {"x": float, "y": float, "enumName": String}
     **/
    @PostMapping("importEnum")
    public void importEnum(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @RequestBody TORESTClassDiagramImportEnum TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        ClassDiagram cdm =  WebResourceClassDiagramMapperManager.getSingleClassDiagram(username, cdmName); 
        Collection<CDEnum> enums = CdmInterfaceUtil.getAvailableExternalEnums(cdm);
        CDEnum importedEnum = null;
        for (CDEnum  cla : enums) {
            if (cla.getName().equals(TO.getEnumName())) {
                importedEnum = cla;
                break;
            }
        }
        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassDiagramController().importEnum(cdm, importedEnum, TO.getX(), TO.getY());
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
//    /*
//     * Extends the model
//     * @param body {"newModelName": String}
//     */
//    @PutMapping("extendModel")
//    public void extendClassDiagram(@PathVariable String cdmName, @RequestBody TORESTClassDiagramExtendModel TO) {
//        COREConcern concern  = WebResourceClassDiagramMapperManager.getConcern();
//        COREArtefact extendingArtefact = null;
//        List<COREArtefact> artefacts = concern.getArtefacts();
//        for (COREArtefact artefact : artefacts) {
//            if (artefact instanceof COREExternalArtefact) {
//               if (((COREExternalArtefact) artefact).getRootModelElement() instanceof ClassDiagram) {
//                    ClassDiagram cdm = (ClassDiagram) ((COREExternalArtefact) artefact).getRootModelElement();
//                    if (cdm.getName().equals(cdmName)) {
//                        extendingArtefact = artefact;
//                        break;
//                    }
//               }
//            }
//        }
//        ClassDiagram cdmToExtend = null;
//        if (!WebResourceClassDiagramMapperManager.getWebResourceMapperDB().containsKey(TO.getNewModelName())) {
//            WebResourceClassDiagramMapperManager.addModelManager(TO.getNewModelName());
//        }
//        cdmToExtend =  WebResourceClassDiagramMapperManager.getSingleClassDiagram(TO.getNewModelName());
//        /*
//         * Manually tell the observer that the underlying resource has changed
//         */
//        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(cdmName).touch();
//        ClassDiagramUtils.extendAspect(extendingArtefact,  cdmToExtend);
//    }
    /*
     * Saves the current state of the class diagram model
     * @param body {}
     */
//    @PutMapping("save")
//    public void saveClassDiagram(@PathVariable String cdmName) {
//        ClassDiagram element =  WebResourceClassDiagramMapperManager.getSingleClassDiagram(cdmName); 
//        if (displayClassDiagramSceneHandler == null ) {
//            displayClassDiagramSceneHandler= new DefaultClassDiagramSceneHandler();
//        }
//        displayClassDiagramSceneHandler.save(element);
//    }
    /*
     * Undoes the last change to the state of the class diagram
     * @param body {}
     */
//    @PutMapping("undo")
//    public void undoClassDiagram(@PathVariable String cdmName) {
//        ClassDiagram element =  WebResourceClassDiagramMapperManager.getSingleClassDiagram(cdmName); 
//        if (displayClassDiagramSceneHandler == null ) displayClassDiagramSceneHandler= new DefaultClassDiagramSceneHandler();
//        
//        displayClassDiagramSceneHandler.undo(element);
//    }
    /*
     * Re-does the last class diagram element
     * @param body {}
     */
//    @PutMapping("redo")
//    public void redoClassDiagram(@PathVariable String cdmName) {
//        ClassDiagram element =  WebResourceClassDiagramMapperManager.getSingleClassDiagram(cdmName);
//        if (displayClassDiagramSceneHandler == null ) displayClassDiagramSceneHandler= new DefaultClassDiagramSceneHandler();
//        
//        displayClassDiagramSceneHandler.redo(element);
//    }
    
    /**
     * Create a new class.
     * 
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param TO {"className": String, "dataType": boolean, "isInterface": boolean, "x": float, "y": float}
     **/
    @PostMapping("class")
    public void createClassifier(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @RequestBody TORESTClassDiagramCreateClass TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        ClassDiagram owner =  WebResourceClassDiagramMapperManager.getSingleClassDiagram(username, cdmName); 
        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassDiagramController()
            .createNewClass(owner, TO.getClassName(), TO.isDataType(), TO.isInterface(),
                TO.getX(), TO.getY());
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Removes a classifier.
     * 
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param classId id of class
     **/
    @DeleteMapping("class/{classId}")
    public void removeClass(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer classId) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Classifier owner = (Classifier) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), classId);
        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassDiagramController().removeClassifier(owner);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Create a new enum.
     * 
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param TO {"enumName": String, "x": float, "y": float}
     **/
    @PostMapping("enum")
    public void createEnum(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @RequestBody TORESTClassDiagramCreateEnum TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        ClassDiagram cdm =  WebResourceClassDiagramMapperManager.getSingleClassDiagram(username, cdmName); 
        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassDiagramController().createEnum(cdm, TO.getEnumName(), TO.getX(), TO.getY());
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Creates a new implementation class.
     * 
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param TO {"className": String, "generics": List<TypeParameter>, "isInterface": boolean, "isAbstract": boolean, 
     * "x": float, "y": float, "superTypes": Set<String>, "subTypes": Set<ImplementationClass> }
     **/
    @PostMapping("implementationClass")
    public void createImplementationClass(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @RequestBody TORESTClassDiagramCreateImplementationClass TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        ClassDiagram owner =  WebResourceClassDiagramMapperManager.getSingleClassDiagram(username, cdmName); 
        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassDiagramController()
            .createImplementationClass(owner, TO.getClassName(), TO.getGenerics(), 
                TO.isInterface(), TO.isAbstract(), TO.getX(),
                TO.getY(), TO.getSuperTypes(), TO.getSubTypes());
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    /**
     * Move a set of classifiers at once.
     * 
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param TO {"positionMap": Map<Integer, LayoutElement>}
     **/
    @PutMapping("moveClassifiers")
    public void moveClassifiers(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @RequestBody TORESTClassDiagramMoveClassifiers TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        ClassDiagram owner =  WebResourceClassDiagramMapperManager.getSingleClassDiagram(username, cdmName); 
        
        Map<Classifier, LayoutElement> positionMaps = new HashMap<Classifier, LayoutElement>();

        for (Integer id : TO.getPositionMap().keySet()) {
            Classifier classifier = (Classifier) 
                    Utils.getEObjectById(WebResourceClassDiagramMapperManager
                            .getSingleIdMapping(username, cdmName), id);
            positionMaps.put(classifier, TO.getPositionMap().get(id));
        }
        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassDiagramController().moveClassifiers(owner, positionMaps);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Moves a set of non-classifiers.
     * 
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param body {"positionMap": Map<Integer, LayoutElement>}
     **/
    @PutMapping("moveNonClassifiers")
    public void moveNonClassifiers(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @RequestBody TORESTClassDiagramMoveNonClassifiers TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        ClassDiagram owner =  WebResourceClassDiagramMapperManager.getSingleClassDiagram(username, cdmName); 
        
        Map<EObject, LayoutElement> positionMaps = new HashMap<EObject, LayoutElement>();
        
        for (Integer id : TO.getPositionMap().keySet()) {
            EObject obj = Utils.getEObjectById(WebResourceClassDiagramMapperManager
                    .getSingleIdMapping(username, cdmName), id);
            positionMaps.put(obj, TO.getPositionMap().get(id));
        }
        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassDiagramController().moveNonClassifiers(owner, positionMaps);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Creates a note without content.
     *
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param TO {"x": float, "y": float}
     **/
    @PostMapping("note")
    public void createNote(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @RequestBody TORESTClassDiagramCreateNote TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        ClassDiagram cdm =  WebResourceClassDiagramMapperManager.getSingleClassDiagram(username, cdmName); 
        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassDiagramController().createNewNote(cdm, TO.getX(), TO.getY());
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Creates note with content.
     * 
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param TO {"x": float, "y": float, "content": String}
     **/
    @PostMapping("noteWithContent")
    public void createNoteWithContent(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @RequestBody TORESTClassDiagramCreateNoteWithContent TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        ClassDiagram cdm =  WebResourceClassDiagramMapperManager.getSingleClassDiagram(username, cdmName); 
        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassDiagramController()
            .createNewNoteWithContent(cdm, TO.getX(), TO.getY(), TO.getContent());
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Edits the content of a note.
     * 
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param noteId id of note
     * @param TO {"content": String}
     **/
    @PutMapping("note/{noteId}/content")
    public void editNote(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer noteId, 
            @RequestBody TORESTClassDiagramEditNote TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Note note = (Note) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), noteId); 
        
        NoteImpl noteImpl = (NoteImpl) note;
        MainController.lock.lock();
        noteImpl.setContent(TO.getContent());
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Removes a note.
     * 
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param noteId id of note
     **/
    @DeleteMapping("note/{noteId}")
    public void removeNote(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer noteId) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Note note = (Note) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), noteId); 
        
        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassDiagramController().removeNote(note);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Creates an annotation between a note and a class.
     * 
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param noteId id of note
     * @param TO {"classId": Integer}
     **/
    @PostMapping("note/{noteId}/annotation")
    public void createAnnotation(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer noteId, 
            @RequestBody TORESTClassDiagramCreateAnnotation TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        ClassDiagram cdm =  WebResourceClassDiagramMapperManager.getSingleClassDiagram(username, cdmName); 
        Classifier classifier = (Classifier) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), TO.getClassId()); 
        
        NamedElement namedElement = classifier;
        
        Note note = (Note) Utils.getEObjectById(WebResourceClassDiagramMapperManager
                .getSingleIdMapping(username, cdmName), noteId);
        
        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassDiagramController().createAnnotation(cdm, note, namedElement);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Removes an annotation associated with a given note.
     * 
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param noteId id of note
     * @param TO {"classId": Integer}
     **/
    @DeleteMapping("note/{noteId}/annotation")
    public void removeAnnotation(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer noteId, 
            @RequestBody TORESTClassDiagramRemoveAnnotation TO) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Note note = (Note) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), noteId); 
        
        Classifier classifier = (Classifier) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), TO.getClassId()); 
        NamedElement namedElement = classifier;
        
        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassDiagramController().removeAnnotation(note, namedElement);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Removes all annotations associated with a note.
     * 
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param noteId id of note
     **/
    @DeleteMapping("note/{noteId}/annotations")
    public void removeAnnotations(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @PathVariable Integer noteId) 
                    throws IllegalArgumentException {
        WorkspaceAccessUtils.checkWriteRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Note note = (Note) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), noteId); 
            
        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassDiagramController().removeAnnotations(note);
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
    
    /**
     * Creates an association between the specified classes.
     * 
     * Access Right: Owner and anyone with write access
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace/model
     * @param cdmName name of model
     * @param TO {"fromClassId": Integer, "toClassId": Integer, "bidirectional": boolean}
     **/
    @PostMapping("association")
    public void createAssociation(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @RequestBody TORESTClassDiagramCreateAssociation TO) 
                    throws IllegalArgumentException {
        WebResourceClassDiagramMapperManager.addModelManager(username, cdmName);
        Classifier from = (Classifier) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), TO.getFromClassId()); 
        Classifier to = (Classifier) 
                Utils.getEObjectById(WebResourceClassDiagramMapperManager
                        .getSingleIdMapping(username, cdmName), TO.getToClassId());
        ClassDiagram cdm =  WebResourceClassDiagramMapperManager.getSingleClassDiagram(username, cdmName); 
        
        MainController.lock.lock();
        ControllerFactory.INSTANCE.getClassDiagramController().createAssociation(cdm, from, to, TO.isBidirectional());
        /*
         * Manually tell the observer that the underlying resource has changed
         */
        BroadcastContentManagerMapper.INSTANCE.getBroadcastContentManager(username, cdmName).touch();
        MainController.lock.unlock();
    }
}
