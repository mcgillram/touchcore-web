package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

public class TORESTClassRenameOperation {
    String newName;
    
    public TORESTClassRenameOperation() {
    }
    
    public TORESTClassRenameOperation(String newName) {
        this.newName = newName;
    }
    public String getNewName() {
        return this.newName;
    }
    public void setNewName(String newName) {
        this.newName = newName;
    }
}
