package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

public class TORESTClassDiagramEditNote {
    String content;
    public TORESTClassDiagramEditNote(String content) {
        super();
        this.content = content;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
}
