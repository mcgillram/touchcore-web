package ca.mcgill.sel.webcore.authentication.controller;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ca.mcgill.sel.webcore.WebResourceClassDiagramMapperManager;
import ca.mcgill.sel.webcore.authentication.entity.User;
import ca.mcgill.sel.webcore.authentication.entity.UserCrudService;
import ca.mcgill.sel.webcore.authentication.jwt.UserAuthenticationService;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;

import static lombok.AccessLevel.PACKAGE;
import static lombok.AccessLevel.PRIVATE;

import java.util.Optional;

/**
 * Controller which provides rest points related to authentication.
 * This provides rest points for registering, logging in and out.
 * 
 * @author Ridwan Kurmally
 */
@RestController
@RequestMapping("user")
@FieldDefaults(level = PRIVATE, makeFinal = true)
@AllArgsConstructor(access = PACKAGE)
public class AuthenticationController {
    
    @NonNull
    UserAuthenticationService authentication;
    
    @NonNull
    UserCrudService users;
    
    /**
     * Creates a new user and then logs them in.
     * Technically, it provides the user with a JWT token that they should then
     * embed in their header for every subsequent requests to the other rest points, so as
     * to be authenticated.
     * 
     * @param authenticationRequest {"username": String, "password": String}
     * @return message along with the fresh JWT token, if successful.
     */
    @PutMapping("/public/register")
    String register(@RequestBody AuthenticationRequest authenticationRequest) {
        if (users.find(authenticationRequest.getUsername()).isEmpty()) {
            users
                .save(User
                    .builder()
                    .id(authenticationRequest.getUsername())
                    .username(authenticationRequest.getUsername())
                    .password(authenticationRequest.getPassword())
                    .build()
            );
            
            String token = authentication.login(
                    authenticationRequest.getUsername(), 
                    authenticationRequest.getPassword()).get();
                    
            if (!WebResourceClassDiagramMapperManager.hasDirectory(authenticationRequest.getUsername())) {
                WebResourceClassDiagramMapperManager.createDirectory(authenticationRequest.getUsername());
            }
            
            return String.format("User registered. Your authorization token is '%s'. Please embed this token"
                    + " in the header as 'Authorization : Bearer <token>'"
                    + " for the subsequent requests.", token);
        }
        return "This username is already taken. Attempt logging in instead if it belongs to you.";
        
    }

    /**
     * Logs the user in.
     * Technically, it provides the user with a JWT token that they should then
     * embed in their header for every subsequent requests to the other rest points, so as
     * to be authenticated.
     * 
     * @param authenticationRequest {"username": String, "password": String}
     * @return message along with the fresh JWT token, if successful.
     */
    @PostMapping("/public/login")
    String login(@RequestBody AuthenticationRequest authenticationRequest) {
        Optional<String> token = authentication
              .login(authenticationRequest.getUsername(), authenticationRequest.getPassword());
      
        if (token.isEmpty()) {
            return "Invalid login and/or password";
        } 
        return String.format("Logged in. Your authorization token is '%s'. Please embed this token"
                + " in the header as 'Authorization : Bearer <token>'"
                + " for the subsequent requests.", token.get());
      
    }
   
    /**
     * Get current user details.
     * 
     * @param user User automatically recognized through the JWT token
     * @return json representation of the user
     */
    @GetMapping("/current")
    User getCurrent(@AuthenticationPrincipal final User user) {
        return user;
    }

    /**
     * Logs the user out.
     * Technically, it does so by invalidating the JWT token passed in the header
     * of the request.
     * Also unloads all models for that user from the memory to clear some space
     * 
     * @param user User automatically recognized through the JWT token
     * @return Status Message
     */
    @PostMapping("/logout")
    String logout(@AuthenticationPrincipal final User user) {
        authentication.logout(user);
        WebResourceClassDiagramMapperManager.unloadAllModels(user.getUsername());
        return "Successfully logged out. Authorization token invalidated.";
    }
}
