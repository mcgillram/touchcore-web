package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

import java.util.List;

import ca.mcgill.sel.classdiagram.Parameter;
import ca.mcgill.sel.classdiagram.VisibilityType;

public class TORESTImplementationClassCreateConstructor {
    String constructorName;
    VisibilityType visibility;
    List<Parameter> parameters;
    public TORESTImplementationClassCreateConstructor(String constructorName, VisibilityType visibility,
            List<Parameter> parameters) {
        super();
        this.constructorName = constructorName;
        this.visibility = visibility;
        this.parameters = parameters;
    }
    public String getConstructorName() {
        return constructorName;
    }
    public void setConstructorName(String constructorName) {
        this.constructorName = constructorName;
    }
    public VisibilityType getVisibility() {
        return visibility;
    }
    public void setVisibility(VisibilityType visibility) {
        this.visibility = visibility;
    }
    public List<Parameter> getParameters() {
        return parameters;
    }
    public void setParameters(List<Parameter> parameters) {
        this.parameters = parameters;
    }
}
