package ca.mcgill.sel.webcore.controllers;

import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;

/**
 * Contains all the controllers related to error handling.
 * 
 * @author arthurls
 *
 */
@Controller
@ControllerAdvice
public class ErrorController {

    /**
     * Catches and handles IllegalArgument Exception.
     * The exception is sent to the client who created it.
     * @param ex the IllegalArgumentException
     * @return the exception message
     */
    @MessageExceptionHandler
    @SendToUser("/queue/error")
    public String illegalArgumentException(IllegalArgumentException ex) {
        System.err.println("Illegal Argument Exception: " + ex.getMessage());
        if (MainController.lock.isLocked()) {
            MainController.lock.unlock();
        }
        return "Illegal Argument Exception: " + ex.getMessage();
    }
    
    /**
     * Catches and handles classCast Exception.
     * The exception is sent to the client who created it.
     * @param ex the classCastException
     * @return the exception message
     */
    @MessageExceptionHandler
    @SendToUser("/queue/error")
    public String classCastException(ClassCastException ex) {
        System.err.println("Cast Exception (wrong id): " + ex.getMessage());
        if (MainController.lock.isLocked()) {
            MainController.lock.unlock();
        }
        return "Cast Exception (wrong id): " + ex.getMessage();
    }
}
