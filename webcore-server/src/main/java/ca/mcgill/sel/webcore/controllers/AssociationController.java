package ca.mcgill.sel.webcore.controllers;

import org.json.JSONObject;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.Association;
import ca.mcgill.sel.ram.AssociationEnd;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.ReferenceType;
import ca.mcgill.sel.ram.controller.ControllerFactory;
import ca.mcgill.sel.webcore.WebResourceMapperManager;
import ca.mcgill.sel.webcore.utils.Utils;

/**
 * Contains all the controllers related to Association.
 * 
 * @author arthurls
 *
 */
@Controller
public class AssociationController {

    /**
     * Deletes an Association.
     * @param body {'aspectName': String, 'associationId': int}
     */
    @MessageMapping("/deleteAssociation")
    public void deleteAssociation(String body) {
        System.out.println("/deleteAssociation " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int associationId = obj.getInt("associationId");
        
        Association association = (Association) 
                Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), associationId);
        Aspect aspect = EMFModelUtil.getRootContainerOfType(association, RamPackage.Literals.ASPECT);
        
        MainController.lock.lock();
        ControllerFactory.INSTANCE.getAssociationController().deleteAssociation(aspect, association);
        MainController.lock.unlock();
    }
    
    /**
     * Sets the multiplicity of an Association End.
     * @param body {'aspectName': String, 'associationEndId': int, 'lowerBound': int, 'upperBound': int}
     */
    @MessageMapping("/setMultiplicity")
    public void setMultiplicity(String body) {
        System.out.println("/setMultiplicity " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int associationEndId = obj.getInt("associationEndId");
        int lowerBound = obj.getInt("lowerBound");
        int upperBound = obj.getInt("upperBound");
        
        AssociationEnd associationEnd = (AssociationEnd) 
                Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), associationEndId);
        Aspect aspect = EMFModelUtil.getRootContainerOfType(associationEnd, RamPackage.Literals.ASPECT);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getAssociationController()
                                        .setMultiplicity(aspect, associationEnd, lowerBound, upperBound);
        MainController.lock.unlock();
    }
    
    /**
     * Sets the Role of an Association End.
     * @param body {'aspectName': String, 'associationEndId': int, 'roleName': String}
     */
    @MessageMapping("/setRoleName")
    public void setRoleName(String body) {
        System.out.println("/setRoleName " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int associationEndId = obj.getInt("associationEndId");
        String roleName = obj.getString("roleName");
        
        AssociationEnd associationEnd = (AssociationEnd) 
                Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), associationEndId);
        Aspect aspect = EMFModelUtil.getRootContainerOfType(associationEnd, RamPackage.Literals.ASPECT);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getAssociationController().setRoleName(aspect, associationEnd, roleName);
        MainController.lock.unlock();
    }
    
    /**
     * Switches the navigable property of the Association End.
     * @param body {'aspectName': String, 'associationEndId': int, 'oppositeEndId': int, 'isNavigable': boolean}
     */
    @MessageMapping("/setNavigable")
    public void setNavigable(String body) {
        System.out.println("/setNavigable " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int associationEndId = obj.getInt("associationEndId");
        boolean endNavigable = obj.getBoolean("endNavigable");
        int oppositeEndId = obj.getInt("oppositeEndId");
        boolean oppositeEndNavigable = obj.getBoolean("oppositeEndNavigable");
        
        AssociationEnd associationEnd = (AssociationEnd) 
                Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), associationEndId);
        AssociationEnd oppositeEnd = (AssociationEnd) 
                Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), oppositeEndId);
        Aspect aspect = EMFModelUtil.getRootContainerOfType(associationEnd, RamPackage.Literals.ASPECT);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getAssociationController().
            setNavigable(aspect, associationEnd, endNavigable, oppositeEnd, oppositeEndNavigable);
        MainController.lock.unlock();
    }
    
    /**
     * Switches the reference type of the Association End.
     * @param body {'aspectName': String, 'associationEndId': int, 'referenceType': String}
     */
    @MessageMapping("/setReferenceType")
    public void setReferenceType(String body) {
        System.out.println("/setReferenceType " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int associationEndId = obj.getInt("associationEndId");
        String referenceType = obj.getString("referenceType");
        
        AssociationEnd associationEnd = (AssociationEnd) 
                Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), associationEndId);
        ReferenceType type = ReferenceType.get(referenceType);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getAssociationController().setReferenceType(associationEnd, type);
        MainController.lock.unlock();
    }
    
    /**
     * Switches the static property of the Association End.
     * @param body {'aspectName': String, 'associationEndId': int}
     */
    @MessageMapping("/switchStatic")
    public void switchStatic(String body) {
        System.out.println("/switchStatic " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int associationEndId = obj.getInt("associationEndId");
        
        AssociationEnd associationEnd = (AssociationEnd) 
                Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), associationEndId);

        MainController.lock.lock();
        ControllerFactory.INSTANCE.getAssociationController().switchStatic(associationEnd);
        MainController.lock.unlock();
    }
}
