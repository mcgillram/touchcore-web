package ca.mcgill.sel.webcore.controllers;

import org.json.JSONObject;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

import ca.mcgill.sel.ram.REnum;
import ca.mcgill.sel.ram.REnumLiteral;
import ca.mcgill.sel.ram.controller.ControllerFactory;
import ca.mcgill.sel.webcore.WebResourceMapperManager;
import ca.mcgill.sel.webcore.utils.Utils;

/**
 * Contains all the controllers related to Enumeration.
 * 
 * @author arthurls
 *
 */
@Controller
public class EnumController {

    /**
     * Add a new literal in the enum.
     * @param body {'aspectName': String, 'enumId': int, 'rankIndex' 'literalName': String}
     */
    @MessageMapping("/addLiteral")
    public void addLiteral(String body) {
        MainController.lock.lock();
        System.out.println("/addLiteral " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int enumId = obj.getInt("enumId");
        int rankIndex = obj.getInt("rankIndex");
        String literalName = obj.getString("literalName");
        
        REnum owner = (REnum) Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), enumId);
        
        ControllerFactory.INSTANCE.getEnumController().createREnumLiteral(owner, rankIndex, literalName);
        MainController.lock.unlock();
    }
    
    /**
     * Add a new literal in the enum.
     * @param body {'aspectName': String, 'literalId': int}
     */
    @MessageMapping("/removeLiteral")
    public void removeLiteral(String body) {
        MainController.lock.lock();
        System.out.println("/removeLiteral " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        int literalId = obj.getInt("literalId");
        
        REnumLiteral literal = 
                (REnumLiteral) Utils.getEObjectById(WebResourceMapperManager.getSingleIdMapping(aspectName), literalId);
        
        ControllerFactory.INSTANCE.getEnumController().removeLiteral(literal);
        MainController.lock.unlock();
    }
}
