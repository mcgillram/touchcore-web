package ca.mcgill.sel.webcore.authentication.jwt;

import java.util.Map;

/**
 * Creates and validates credentials.
 * 
 * @author Ridwan Kurmally
 */
public interface TokenService {

    /**
     * Return a token, encrypting attributes, which will not expire.
     * 
     * @param attributes
     * @return a new permanent token
     */
    String permanent(Map<String, String> attributes);

    /**
     * Return a token, encrypting attributes, which will expire.
     * 
     * @param attributes
     * @return a new temporary token
     */
    String expiring(Map<String, String> attributes);

  /**
   * Checks the validity of the given credentials.
   *
   * @param token
   * @return attributes if verified
   */
    Map<String, String> untrusted(String token);

  /**
   * Checks the validity of the given credentials.
   *
   * @param token
   * @return attributes if verified
   */
    Map<String, String> verify(String token);
    
    /**
     * Invalidates the given credentials.
     *
     * @param token
     */
    void invalidate(String token);
}
