package ca.mcgill.sel.webcore.authentication.jwt;

import java.util.Objects;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.google.common.collect.ImmutableMap;

import ca.mcgill.sel.webcore.authentication.entity.User;
import ca.mcgill.sel.webcore.authentication.entity.UserCrudService;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;

import static lombok.AccessLevel.PACKAGE;
import static lombok.AccessLevel.PRIVATE;

/**
 * Authentication service which deals with tokens to log users in and out of the server.
 * 
 * @author Ridwan Kurmally
 */
@Service
@AllArgsConstructor(access = PACKAGE)
@FieldDefaults(level = PRIVATE, makeFinal = true)
final class TokenAuthenticationService implements UserAuthenticationService {
    
    @NonNull
    private TokenService tokens;
    @NonNull
    private UserCrudService users;

    /**
     * Gets the token associated with the user corresponding to those credentials.
     * If the credentials are wrong, it returns an empty Optional.
     * If no token is yet associated with that user, it is created and associated.
     * 
     * 
     * @param username
     * @param password
     * @return valid token, if credentials are correct. Else an empty optional
     */
    @Override
    public Optional<String> login(final String username, final String password) {
        Optional<User> optUser = users
                .findByUsername(username)
                .filter(u -> Objects.equals(password, u.getPassword()));
        
        if (optUser.isEmpty()) {
            return Optional.empty();
        }
        
        User user = optUser.get();
        String token = user.getToken();
        
        // If there is no token or an expired token associated with that user, renew it
        if (token == null || tokens.verify(token).isEmpty()) {
            token = tokens.expiring(ImmutableMap.of("username", username));
            user.setToken(token);
        }
        
        return Optional.of(token);
    }

    /**
     * Returns the user associated with that token.
     * 
     * @param token
     * @return user, or empty optional if invalid token
     */
    @Override
    public Optional<User> findByToken(final String token) {
        return Optional
                .of(tokens.verify(token))
                .map(map -> map.get("username"))
                .flatMap(users::findByUsername);
    }

    /**
     * Invalidates the token associated with that user by adding it to the blocked list.
     * 
     * @param user
     */
    @Override
    public void logout(final User user) {
        if (user.getToken() != null) {
            tokens.invalidate(user.getToken());
            user.setToken(null);
        }
    }
}
