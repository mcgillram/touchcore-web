package ca.mcgill.sel.webcore;

import java.io.IOException;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.emfjson.jackson.annotations.EcoreIdentityInfo;
import org.emfjson.jackson.module.EMFModule;
import org.emfjson.jackson.utils.ValueWriter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;


import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.webcore.authentication.workspace.Workspace;
import eu.kartoffelquadrat.asyncrestlib.BroadcastContent;

public class WebResourceClassDiagramMapper extends WebResourceMapper implements BroadcastContent {
    
	private ClassDiagram cd;
	@JsonIgnore
	private Workspace workspace;
	@JsonIgnore
	private ObjectMapper cdMapper;
	@JsonIgnore
    private BiMap<EObject, Integer> cdIdMapping;
	@JsonIgnore
    private int cdIdCounter;
    
    /**
     * Class constructor.
     * @param model the model you want to map
     * @throws IOException 
     */
    public WebResourceClassDiagramMapper(EObject model) {
        // Initialize the Class variables
        cd = (ClassDiagram) model;
        cdIdMapping = HashBiMap.create();
        cdIdCounter = 1;
        
        // Create a mapper to serialize/deserialize objects
        initMapper();
        
        // Create the idMapping
        initIdMapping();
        
        // Initialize Notification Handler
        ClassDiagramNotificationHandler notificationHandler = new ClassDiagramNotificationHandler(this);
        notificationHandler.initNotifications();
    }
    
    public ObjectMapper getCdMapper() {
        return cdMapper;
    }
    
    /**
     * For each EObject in the aspect, assign an id and saves it into a map.
     */
    private void initIdMapping() {
        TreeIterator<EObject> it = cd.eAllContents();
        
        while (it.hasNext()) {
            EObject eObj = it.next();
            cdIdMapping.put(eObj, cdIdCounter++);
            try {
                cdMapper.writeValueAsString(eObj);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Initialize the mapper.
     * The mapper is used to serialize/deserialize an EObject into JSON.
     */
    private void initMapper() {
        cdMapper = new ObjectMapper();
        cdMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        
        // EMFModule handles the serialization within the mapper
        EMFModule module = new EMFModule();
        module.configure(EMFModule.Feature.OPTION_USE_ID, true);
        
        // Handles the ids for EObject. Maps an id to an EObject
        module.setIdentityInfo(new EcoreIdentityInfo("_id",
            new ValueWriter<EObject, Object>() {
                @Override
                public Object writeValue(EObject eObj, SerializerProvider context) {
                    return String.valueOf(cdIdMapping.get(eObj));
                }
            }
        ));
        
        // Handles the reference between EObject. Use the id/EObject mapping
        module.setReferenceSerializer(new JsonSerializer<EObject>() {
            @Override
            public void serialize(EObject eObj, JsonGenerator g, SerializerProvider s) throws IOException {
                g.writeString(String.valueOf(cdIdMapping.get(eObj)));
            }
        });
                
        cdMapper.registerModule(module);
    }
    
    /**
     * Return the current loaded aspect.
     * @return the current loaded aspect
     */
    public ClassDiagram getClassDiagram() {
        return cd;
    }
    
    /**
     * Return the workspace associated to the model.
     * @return the workspace
     */
    public Workspace getWorkspace() {
        return workspace;
    }
    
    /**
     * Setter for the workspace associated to the model.
     */
    public void setWorkspace(Workspace workspace) {
        this.workspace = workspace;
    }

    /**
     * The idMapping Map holds the EObject to ID mapping.
     * @return idMapping
     */
    public BiMap<EObject, Integer> getIdMapping() {
        return cdIdMapping;
    }
    
    /**
     * Returns the whole aspect as a JSON string.
     * @return the JSON String
     * @throws JsonProcessingException 
     */
    public String toJson() throws JsonProcessingException {
        return cdMapper.writeValueAsString(cd);
    }
    
    /**
     * Returns the given EObject into a JSON String.
     * @param eObject to serialize into JSON
     * @return the JSON String
     * @throws JsonProcessingException 
     */
    public String toJson(EObject eObject) throws JsonProcessingException {
        return cdMapper.writeValueAsString(eObject);
    }
    
    /**
     * This int is incremented for every new EObject added in the model.
     * @return idCounter
     */
    public int getIdCounter() {
        return cdIdCounter;
    }
    
    @Override
    @JsonIgnore
    public boolean isEmpty() {
        // TODO Auto-generated method stub
        return false;
    }

}
