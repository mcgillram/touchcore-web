package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

import java.util.List;

import ca.mcgill.sel.classdiagram.Parameter;
import ca.mcgill.sel.classdiagram.VisibilityType;

public class TORESTImplementationClassCreateOperation {
    Integer index;
    String operationName;
    VisibilityType visibility;
    Integer returnTypeId;
    List<Parameter> parameters;
    boolean isStatic;
    public TORESTImplementationClassCreateOperation(Integer index, String operationName, VisibilityType visibility,
            Integer returnTypeId, List<Parameter> parameters, boolean isStatic) {
        super();
        this.index = index;
        this.operationName = operationName;
        this.visibility = visibility;
        this.returnTypeId = returnTypeId;
        this.parameters = parameters;
        this.isStatic = isStatic;
    }
    public Integer getIndex() {
        return index;
    }
    public void setIndex(Integer index) {
        this.index = index;
    }
    public String getOperationName() {
        return operationName;
    }
    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }
    public VisibilityType getVisibility() {
        return visibility;
    }
    public void setVisibility(VisibilityType visibility) {
        this.visibility = visibility;
    }
    public Integer getReturnTypeId() {
        return returnTypeId;
    }
    public void setReturnTypeId(Integer returnTypeId) {
        this.returnTypeId = returnTypeId;
    }
    public List<Parameter> getParameters() {
        return parameters;
    }
    public void setParameters(List<Parameter> parameters) {
        this.parameters = parameters;
    }
    public boolean isStatic() {
        return isStatic;
    }
    public void setStatic(boolean isStatic) {
        this.isStatic = isStatic;
    }
}
