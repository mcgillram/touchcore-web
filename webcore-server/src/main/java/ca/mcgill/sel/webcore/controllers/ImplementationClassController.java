package ca.mcgill.sel.webcore.controllers;

import java.lang.reflect.Modifier;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import org.json.JSONObject;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

import ca.mcgill.sel.ram.ImplementationClass;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.StructuralView;
import ca.mcgill.sel.ram.TypeParameter;
import ca.mcgill.sel.ram.controller.ControllerFactory;
import ca.mcgill.sel.ram.controller.StructuralViewController;
import ca.mcgill.sel.ram.loaders.RamClassLoader;
import ca.mcgill.sel.ram.loaders.RamClassSearcher;
import ca.mcgill.sel.ram.loaders.RamClassUtils;
import ca.mcgill.sel.ram.loaders.exceptions.MissingJarException;
import ca.mcgill.sel.webcore.WebResourceMapperManager;

/**
 * Contains all the controllers related to ImplementationClass.
 * 
 * @author arthurls
 *
 */
@Controller
public class ImplementationClassController {
    
    /**
     * Search for an Implementation Class.
     * Returns an implementation class search list matching the searchString.
     * @param body {'aspectName': String, 'searchString': String}
     * @return list of research result
     */
    @MessageMapping("/getImplementationClassSearch")
    @SendToUser("/queue/getImplementationClassSearch")
    public List<String> getPossibleOperationType(String body) {
        System.out.println("/getImplementationClassSearch " + body);
        JSONObject obj = new JSONObject(body);
        String searchString = obj.getString("searchString");
        List<String> fullRecommendations =
               RamClassSearcher.INSTANCE.getRecommendedLoadable(searchString, RamClassLoader.INSTANCE.getAllLoadable());
        
        Comparator<String> byLength = new Comparator<String>() { 
            @Override public int compare(String s1, String s2) { 
                return s1.length() - s2.length(); 
            } 
        };
        
        Collections.sort(fullRecommendations, byLength);
        
        return fullRecommendations;
    }
    
    /**
     * Import an implementation Class into the model.
     *
     * @param body {'aspectName': String, 'className': String, 'xPosition': float, 'yPosition': float}
     */
    @MessageMapping("/createImplementationClass")
    public void createImplementationClass(String body) {
        System.out.println("/createImplementationClass " + body);
        JSONObject obj = new JSONObject(body);
        String aspectName = obj.getString("aspectName");
        String className = obj.getString("className");
        float xPosition = obj.getFloat("xPosition");
        float yPosition = obj.getFloat("yPosition");
        StructuralView structuralView = WebResourceMapperManager.getSingleAspect(aspectName).getStructuralView();
        
        MainController.lock.lock();
        handleImplementationClassSelection(structuralView, className, xPosition, yPosition);
        MainController.lock.unlock();
    }
     
    /**
     * WARNING This function is an almost duplicate from the GUI StructuralViewHandler
     * Handles the selection of an implementation class to import by the user.
     *
     * @param structuralView the {@link StructuralView} the class should be added to
     * @param className the name
     * @param x the x position of the class
     * @param y the y position of the class
     */
    // TODO: change the location of handleImplementationClassSelection 
    // in the GUI so the API can use it (and avoid code duplication)
    private static void handleImplementationClassSelection(StructuralView structuralView, String className,
            float x, float y) {
        StructuralViewController controller = ControllerFactory.INSTANCE.getStructuralViewController();
        try {
            java.lang.Class<?> loadedClass = RamClassLoader.INSTANCE.retrieveClass(className);
            if (RamClassLoader.INSTANCE.isLoadableEnum(className)) {
                List<String> literals = new ArrayList<String>();
                for (Object literal : loadedClass.getEnumConstants()) {
                    literals.add(literal.toString());
                }
                controller.createImplementationEnum(structuralView, RamClassUtils.extractClassName(className),
                        className, x, y, literals);
            } else {
                List<TypeParameter> typeParameters = new ArrayList<TypeParameter>();
                for (TypeVariable<?> tv : loadedClass.getTypeParameters()) {
                    TypeParameter typeParameter = RamFactory.eINSTANCE.createTypeParameter();
                    typeParameter.setName(tv.getName());
                    typeParameters.add(typeParameter);
                }
                boolean isInterface = RamClassLoader.INSTANCE.isLoadableInterface(className);
                boolean isAbstract = Modifier.isAbstract(loadedClass.getModifiers());
                Set<String> superTypes = RamClassLoader.INSTANCE.getAllSuperClassesFor(className);
                Set<ImplementationClass> subTypes = RamClassLoader.getExistingSubTypes(structuralView, className);
                // If an element is selected create the implementation class with the controller
                controller.createImplementationClass(structuralView, className, typeParameters, isInterface, isAbstract,
                        x, y, superTypes, subTypes);
            }
        } catch (MissingJarException e) {
            // TODO: Error notification
        }
    }

}