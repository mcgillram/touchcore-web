package ca.mcgill.sel.webcore.authentication.workspace.controller;

import java.util.List;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ca.mcgill.sel.webcore.authentication.entity.User;
import ca.mcgill.sel.webcore.authentication.workspace.Workspace;
import ca.mcgill.sel.webcore.authentication.workspace.WorkspaceAccessUtils;

/**
 * Controller associated with every operation pertaining instances of {@link Workspace}.
 * Users can access these rest points to view their workspaces meta data and grant or revoke
 * read/write access permissions to their workspace.
 * 
 * @author Ridwan Kurmally
 *
 */
@RestController
@RequestMapping("{username}/workspace/")
public class WorkspaceController {
    
    /**
     * Shows all workspaces for a specific user.
     * 
     * Access Right: Only Owner.
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspaces
     * @return String representation of all workspaces for a specific user.
     */
    @GetMapping("all")
    public String showWorkspaces(@AuthenticationPrincipal final User user, 
            @PathVariable String username) {
        List<Workspace> userWorkspaces = WorkspaceAccessUtils
                .checkOwnerRightsAndGetWorkspaces(username, user.getUsername());
        StringBuilder result = new StringBuilder();
        for (Workspace workspace : userWorkspaces) {
            result.append("-------------\n" + workspace.toString());
        }
        result.append("-------------");
        return result.toString();
    }
    
    /**
     * Shows workspace details for a specific user and model.
     * 
     * Access Right: Only Owner.
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace
     * @param cdmName name of model
     * @return String representation of the workspace
     */
    @GetMapping("{cdmName}")
    public String showWorkspace(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName) {
        return WorkspaceAccessUtils.checkOwnerRightsAndGetWorkspace(username, cdmName, user.getUsername()).toString();
    }
    
    /**
     * Grant the user explicit read access rights over that workspace.
     * 
     * Access Right: Only Owner.
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace
     * @param cdmName name of model
     * @param request {username: String}
     */
    @PostMapping("{cdmName}/grantReadAccess")
    public void grantReadAccess(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @RequestBody UserRequest request) {
        Workspace userWorkspace = WorkspaceAccessUtils
                .checkOwnerRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WorkspaceAccessUtils.checkIfExist(request.getUsername());
        if (!userWorkspace.grantReadAccess(request.getUsername())) {
            throw new IllegalArgumentException(String.format(
                    "The user '%s' already has read access rights over that workspace.",
                    request.getUsername()));
        }
    }
    
    /**
     * Revokes the user of explicit read access rights over that workspace.
     * 
     * Access Right: Only Owner.
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace
     * @param cdmName name of model
     * @param request {username: String}
     */
    @PostMapping("{cdmName}/revokeReadAccess")
    public void revokeReadAccess(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @RequestBody UserRequest request) {
        Workspace userWorkspace = WorkspaceAccessUtils
                .checkOwnerRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WorkspaceAccessUtils.checkIfExist(request.getUsername());
        if (userWorkspace.isOwner(request.getUsername())) {
            throw new IllegalArgumentException(String.format("Illegal to revoke read access right from the owner '%s'",
                    request.getUsername()));
        } else if (!userWorkspace.revokeReadAccess(request.getUsername())) {
            throw new IllegalArgumentException(String.format(
                    "The user '%s' did not have read access rights over that workspace.",
                    request.getUsername()));
        }
    }
    
    /**
     * Revokes all explicit read access rights of non-owner users over that workspace.
     * 
     * Access Right: Only Owner.
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace
     * @param cdmName name of model
     */
    @PostMapping("{cdmName}/revokeAllReadAccess")
    public void revokeAllReadAccess(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName) {
        Workspace userWorkspace = WorkspaceAccessUtils
                .checkOwnerRightsAndGetWorkspace(username, cdmName, user.getUsername());
        userWorkspace.revokeAllReadAccess();
    }
    
    /**
     * Grant the user explicit write access rights over that workspace.
     * 
     * Access Right: Only Owner.
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace
     * @param cdmName name of model
     * @param request {username: String}
     */
    @PostMapping("{cdmName}/grantWriteAccess")
    public void grantWriteAccess(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @RequestBody UserRequest request) {
        Workspace userWorkspace = WorkspaceAccessUtils
                .checkOwnerRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WorkspaceAccessUtils.checkIfExist(request.getUsername());
        if (!userWorkspace.grantWriteAccess(request.getUsername())) {
            throw new IllegalArgumentException(String.format(
                    "The user '%s' already has write access rights over that workspace.",
                    request.getUsername()));
        }
    }
    
    /**
     * Revokes the user of explicit write access rights over that workspace.
     * 
     * Access Right: Only Owner.
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace
     * @param cdmName name of model
     * @param request {username: String}
     */
    @PostMapping("{cdmName}/revokeWriteAccess")
    public void revokeWriteAccess(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName, 
            @RequestBody UserRequest request) {
        Workspace userWorkspace = WorkspaceAccessUtils
                .checkOwnerRightsAndGetWorkspace(username, cdmName, user.getUsername());
        WorkspaceAccessUtils.checkIfExist(request.getUsername());
        if (userWorkspace.isOwner(request.getUsername())) {
            throw new IllegalArgumentException(String.format("Illegal to revoke read access right from the owner '%s'",
                    request.getUsername()));
        } else if (!userWorkspace.revokeWriteAccess(request.getUsername())) {
            throw new IllegalArgumentException(String.format(
                    "The user '%s' did not have write access rights over that workspace.",
                    request.getUsername()));
        }
    }
    
    /**
     * Revokes all explicit write access rights of non-owner users over that workspace.
     * 
     * Access Right: Only Owner.
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace
     * @param cdmName name of model
     */
    @PostMapping("{cdmName}/revokeAllWriteAccess")
    public void revokeAllWriteAccess(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName) {
        Workspace userWorkspace = WorkspaceAccessUtils
                .checkOwnerRightsAndGetWorkspace(username, cdmName, user.getUsername());
        userWorkspace.revokeAllWriteAccess();
    }
    
    /**
     * Switches the workspace between private and public concerning the read access rights.
     * If public, any users can read from that workspace.
     * If private, only the owner and users with explicit read permissions can read from that workspace.
     * 
     * Access Right: Only Owner.
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace
     * @param cdmName name of model
     */
    @PostMapping("{cdmName}/switchPublicRead")
    public void switchPublicRead(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName) {
        Workspace userWorkspace = WorkspaceAccessUtils
                .checkOwnerRightsAndGetWorkspace(username, cdmName, user.getUsername());
        userWorkspace.setPublicRead(!userWorkspace.isPUBLIC_READ());
    }
    
    /**
     * Switches the workspace between private and public concerning the write access rights.
     * If public, any users can read from/write to that workspace.
     * If private, only the owner and users with explicit write permissions can read from/write to that workspace.
     * 
     * Access Right: Only Owner.
     * 
     * @param user client making a request to that rest point
     * @param username owner of the workspace
     * @param cdmName name of model
     */
    @PostMapping("{cdmName}/switchPublicWrite")
    public void switchPublicWrite(@AuthenticationPrincipal final User user, 
            @PathVariable String username, 
            @PathVariable String cdmName) {
        Workspace userWorkspace = WorkspaceAccessUtils
                .checkOwnerRightsAndGetWorkspace(username, cdmName, user.getUsername());
        userWorkspace.setPublicWrite(!userWorkspace.isPUBLIC_WRITE());
    }
}
