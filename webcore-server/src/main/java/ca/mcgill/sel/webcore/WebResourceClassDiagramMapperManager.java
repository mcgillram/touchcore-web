package ca.mcgill.sel.webcore;

import static lombok.AccessLevel.PACKAGE;
import static lombok.AccessLevel.PRIVATE;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.BiMap;

import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.commons.emf.util.ResourceManager;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.webcore.authentication.entity.UserCrudService;
import ca.mcgill.sel.webcore.authentication.jwt.UserAuthenticationService;
import ca.mcgill.sel.webcore.authentication.workspace.Workspace;
import ca.mcgill.sel.webcore.authentication.workspace.WorkspaceCrudService;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;

/**
 * Holds all the mapping resource in a Map.
 * The key is the aspect name and the value is the WebResourceMapper
 * 
 * @author arthurls
 * @author Ridwan Kurmally
 *
 */

//@FieldDefaults(level = PRIVATE, makeFinal = true)
//@AllArgsConstructor(access = PACKAGE)
@Component
public final class WebResourceClassDiagramMapperManager {
    
    // TODO: Will become obsolete
    private static String concernsRootPath = "cores";
    
    private static String UserConcernsRootPath = "users";
    
    private static WorkspaceCrudService workspaces;
    
    /**
     * Autowired constructor.
     * This is essential so that we can set the static service {@link ca.mcgill.sel.webcore
     * .authentication.workspace.WorkspaceCrudService WorkspaceCrudService} 
     * 
     * @param workspaces
     */
    @Autowired
    public WebResourceClassDiagramMapperManager(WorkspaceCrudService workspaces) {
        WebResourceClassDiagramMapperManager.workspaces = workspaces;
    }
    
    /**
     * If TouchCORE and WebCore are launched or just WebCore.
     */
    private static boolean isDuo;
    
    /**
     * See {link @WebResourceMapper}.
     */
    // Will become obsolete
    private static Map<String, WebResourceClassDiagramMapper> webResourceMapperDB 
        = new HashMap<String, WebResourceClassDiagramMapper>();
    
    
    /**
     * Maps from username to map of aspectname to webResourceClassDiagramMapper.
     */
    private static Map<String, Map<String, WebResourceClassDiagramMapper>> userToWebResourceMapperDB 
        = new HashMap<String, Map<String, WebResourceClassDiagramMapper>>();
    
    /**
     * The current loaded concern. null if no concern is loaded.
     */
    private static COREConcern concern;
    
    /**
     * Getter for the root of the users's model.
     * 
     * @return UserConcernsRootPath
     */
    public static String getUserConcernsRootPath() {
        return UserConcernsRootPath;
    }
    
    /**
     * Getter for the Model Manager DB.
     * 
     * @return the webResourceMapperDB
     */
    public static Map<String, WebResourceClassDiagramMapper> getWebResourceMapperDB() {
        return webResourceMapperDB;
    }
    
    /**
     * Getter for the map of User to Model Manager DB.
     * 
     * @return the userToWebResourceMapperDB
     */
    public static Map<String, Map<String, WebResourceClassDiagramMapper>> getUserToWebResourceMapperDB() {
        return userToWebResourceMapperDB;
    }
    
    /**
     * Setter for the Model Manager DB.
     * 
     * @param webResourceMapperDB the new model manager
     */
    public static void setWebResourceMapperDB(Map<String, WebResourceClassDiagramMapper> webResourceMapperDB) {
        WebResourceClassDiagramMapperManager.webResourceMapperDB = webResourceMapperDB;
    }
    
    /**
     * Setter for the map of User to Model Manager DB.
     * 
     * @param userToWebResourceMapperDB the map for user to model manager
     */
    public static void setUserToWebResourceMapperDB(Map<String, Map<String, WebResourceClassDiagramMapper>> userToWebResourceMapperDB) {
        WebResourceClassDiagramMapperManager.userToWebResourceMapperDB = userToWebResourceMapperDB;
    }
    
    /**
     * Getter for the current CORE concern.
     * @return the current concern
     */
    public static COREConcern getConcern() {
        return concern;
    }

    /**
     * Setter for the current CORE concern.
     * @param concern the new concern
     */
    public static void setConcern(COREConcern concern) {
        WebResourceClassDiagramMapperManager.concern = concern;
    }

    /**
     * Getter for isDuo.
     * @return isDuo
     */
    public static boolean isDuo() {
        return isDuo;
    }

    /**
     * Setter for isDuo.
     * @param isDuo 
     */
    public static void setDuo(boolean isDuo) {
        WebResourceClassDiagramMapperManager.isDuo = isDuo;
    }

    /**
     * Gets one Model Manager from a Model Name.
     * 
     * @param modelName the model you want to get
     * @return the modelManager matching the name
     */
    public static WebResourceClassDiagramMapper getSingleMapper(String modelName) {
        return webResourceMapperDB.get(modelName);
    }
    
    /**
     * Gets one Model Manager from a specific user by a model Name.
     * 
     * @param username the name of the user
     * @param modelName the model you want to get
     * @return the modelManager matching the user name and modelName, or null if not found
     */
    public static WebResourceClassDiagramMapper getSingleMapper(String username, String modelName) {
        Map<String, WebResourceClassDiagramMapper> map = userToWebResourceMapperDB.get(username);
        if (map == null) {
            return null;
        }
        return map.get(modelName);
    }
    
    /**
     * Gets an Aspect from a Model Name.
     * 
     * @param modelName the model you want to get it from
     * @return the Aspect matching the name
     */
    public static ClassDiagram getSingleClassDiagram(String modelName) {
        return webResourceMapperDB.get(modelName).getClassDiagram();
    }
    
    /**
     * Gets an Aspect from a Model Name and user name.
     * 
     * @param username the name of the user
     * @param modelName the model you want to get it from
     * @return the Aspect matching the user name and modelName, or null if not found
     */
    public static ClassDiagram getSingleClassDiagram(String username, String modelName) {
        WebResourceClassDiagramMapper webResourceClassDiagramMapper = getSingleMapper(username, modelName);
        if (webResourceClassDiagramMapper == null) {
            return null;
        }
        return webResourceClassDiagramMapper.getClassDiagram();
    }
    
    /**
     * Gets an IdMapping from a Model Name.
     * 
     * @param modelName the model you want to get it from
     * @return the mapping matching the name
     */
    public static BiMap<EObject, Integer> getSingleIdMapping(String modelName) {
        return webResourceMapperDB.get(modelName).getIdMapping();
    }
    
    /**
     * Gets an IdMapping from a Model Name and user name.
     * 
     * @param username the name of the user
     * @param modelName the model you want to get it from
     * @return the mapping matching the user name and modelName, or null if not found
     */
    public static BiMap<EObject, Integer> getSingleIdMapping(String username, String modelName) {
        WebResourceClassDiagramMapper webResourceClassDiagramMapper = getSingleMapper(username, modelName);
        if (webResourceClassDiagramMapper == null) {
            return null;
        }
        return webResourceClassDiagramMapper.getIdMapping();
    }
    
    /**
     * Gets the workspace corresponding to a model name and user name.
     * 
     * @param username the name of the user
     * @param modelName the name of the model
     * @return the workspace, or null if not found
     */
    public static Workspace getWorkspace(String username, String modelName) {
        WebResourceClassDiagramMapper webResourceClassDiagramMapper = getSingleMapper(username, modelName);
        if (webResourceClassDiagramMapper == null) {
            return null;
        }
        return webResourceClassDiagramMapper.getWorkspace();
    }
    
    // TODO : Will become obsolete
    /**
     * Add a Model Manager to the DB.
     * 
     * @param aspectName the model you want to add to the DB
     */
    public static void addModelManager(String aspectName) {
        System.out.println("Adding Resource for " + aspectName);
        if (!webResourceMapperDB.containsKey(aspectName)) {
            COREConcern concern = loadConcern(aspectName);
            for (COREArtefact artefact : concern.getArtefacts()) {
                if (artefact instanceof COREExternalArtefact && artefact.getName().contentEquals(aspectName)) {
                    //EObject model = ResourceManager.loadModel("resources/samples/" + aspectName + ".ram");
                    COREExternalArtefact art = (COREExternalArtefact) artefact;
                    WebResourceClassDiagramMapper webResourceMapper = new WebResourceClassDiagramMapper(art.getRootModelElement());
                    webResourceMapperDB.put(aspectName, webResourceMapper);
                    /*
                     * webResourceMapperDB and broadcastContentManagers have the same keyset
                     */
                    BroadcastContentManagerMapper.INSTANCE.addBroadcastContentManager(aspectName, webResourceMapper, webResourceMapper.getCdMapper());
                }
            }
        }
    }
    
    /**
     * Add a Model Manager for a specific user to the DB, if absent.
     * 
     * @param username the name of the user
     * @param aspectName the model you want to add to the DB
     * @throws Exception thrown if this is no such user ( no workspace ) or 
     *                   that user's workspace does not contain that model
     */
    public static void addModelManager(String username, String aspectName) throws IllegalArgumentException {
        
        if (contains(username, aspectName)) {
            return;
        }
        
        COREConcern concern;
        try {
            concern = loadConcern(username, aspectName);
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format("Either the user '%s' "
                    + "or the model '%s' does not exist.", username, aspectName));
        }
        
        // Adding the user name to the mapping if it's not already present
        if (!userToWebResourceMapperDB.containsKey(username)) {
            userToWebResourceMapperDB.put(username, new HashMap<String, WebResourceClassDiagramMapper>());
        }
        
        Map<String, WebResourceClassDiagramMapper> webResourceMapperDB 
            = userToWebResourceMapperDB.get(username);
        
        if (!webResourceMapperDB.containsKey(aspectName)) {
            
            //System.out.println("Adding Resource for user " + username + " : " + aspectName);
            
            // TODO : Fix bug that it does not add "empty" concern file if we dont save the concern twice
            for (COREArtefact artefact : concern.getArtefacts()) {
                
                //System.out.print("Artefact :" + artefact);
                
                if (artefact instanceof COREExternalArtefact && artefact.getName().contentEquals(aspectName)) {
                    
                    // Getting the Workspace instance for that user and model
                    Optional<Workspace> workspace = workspaces.find(username, aspectName);
                    if (workspace.isEmpty()) {
                        throw new IllegalArgumentException(String.format("Failed to associate workspace for "
                                + "user '%s' and model '%s'", username, aspectName));
                    }
                    
                    COREExternalArtefact art = (COREExternalArtefact) artefact;
                    WebResourceClassDiagramMapper webResourceMapper 
                        = new WebResourceClassDiagramMapper(art.getRootModelElement());
                    
                    // Associating the workspace with that webResourceMapper 
                    webResourceMapper.setWorkspace(workspace.get());
                    // Loading the webResourceMapper in memory
                    webResourceMapperDB.put(aspectName, webResourceMapper);
                    
                    /*
                     * webResourceMapperDB and broadcastContentManagers have the same keyset
                     */
                    //System.out.print("\t ADDED TO BROADCAST");
                    BroadcastContentManagerMapper.INSTANCE.addBroadcastContentManager(
                            username, aspectName, webResourceMapper, webResourceMapper.getCdMapper());
                }
                //System.out.println();
            }
        }
    }
    
    /**
     * Loads concern directly from disc.
     * 
     * @param aspectName the model you want to the concern to
     * @return the concern
     */
    public static COREConcern loadConcern(String aspectName) {
        File f = new File(concernsRootPath + "/" + aspectName + "/" + aspectName + ".core");
        return (COREConcern) ResourceManager.loadModel(f);
    }
    
    /**
     * Loads concern directly from disc into the static field concern.
     * 
     * @param username the name of the user
     * @param aspectName the model you want to the concern to
     * @return the concern
     */
    public static COREConcern loadConcern(String username, String aspectName) {
        File f = new File(UserConcernsRootPath + "/" + username + "/" + aspectName + "/" + aspectName + ".core");
        
        // Unloading the resource, if already present.
        Resource resource = ResourceManager.getResourceSet().getResource(URI.createFileURI(f.getAbsolutePath()), false);
        if (resource != null) {
            ResourceManager.unloadResource(resource);
        }
        
        return (COREConcern) ResourceManager.loadModel(f);
    }
    
    /**
     * Checks whether a specific mode for a user exists in the map.
     * 
     * @param username the name of the user
     * @param aspectName the model name
     * @return true if the map userToWebResourceMapperDB contains an aspect with name aspectName
     *         belonging to the user with name username
     */
    public static boolean contains(String username, String aspectName) {
        return userToWebResourceMapperDB.containsKey(username)
                && userToWebResourceMapperDB.get(username)
                .containsKey(aspectName);
    }
    
    /**
     * Removes a specific model for a user from the map.
     * 
     * @param username the name of the user
     * @param aspectName the model you want to remove from the map
     * @return true if successful in removing that aspect. False otherwise if it does not exist.
     */
    public static boolean unloadModel(String username, String aspectName) {
        if (contains(username, aspectName)) {
            userToWebResourceMapperDB.get(username).remove(aspectName);
            return true;
        }
        return false;
    }
    
    /**
     * Removes all models for a user from the map. Used when the user logs out to clear space.
     * 
     * @param username the name of the user
     * @return true if successful in removing all models. False otherwise if the user does not exist.
     */
    public static boolean unloadAllModels(String username) {
        if (userToWebResourceMapperDB.containsKey(username)) {
            userToWebResourceMapperDB.remove(username);
            return true;
        }
        return false;
    }
    
    /**
     * Checks whether this user has a directory for storing their models.
     * 
     * @param username the name of the user
     * @return true if that user already has a directory for them.
     */
    public static boolean hasDirectory(String username) {
        return new File(UserConcernsRootPath + "/" + username).exists();
    }
    
    /**
     * Creates a directory for that user.
     * 
     * @param username the name of the user
     */
    public static void createDirectory(String username) {
        new File(UserConcernsRootPath + "/" + username).mkdirs();
    }
    
    /**
     * Returns the path where this model is/should be stored.
     * 
     * @param username the name of the user
     * @param modelName the name of the model
     * @return the path 
     */
    public static String getPath(String username, String modelName) {
        return UserConcernsRootPath + "/" + username + "/" + modelName;
    }
}
