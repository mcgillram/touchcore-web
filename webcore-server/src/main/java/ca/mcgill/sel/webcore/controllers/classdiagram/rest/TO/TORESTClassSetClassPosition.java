package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

public class TORESTClassSetClassPosition {
    float xPosition;
    float yPosition;
    public TORESTClassSetClassPosition(float xPosition, float yPosition) {
        super();
        this.xPosition = xPosition;
        this.yPosition = yPosition;
    }
    public float getxPosition() {
        return xPosition;
    }
    public void setxPosition(float xPosition) {
        this.xPosition = xPosition;
    }
    public float getyPosition() {
        return yPosition;
    }
    public void setyPosition(float yPosition) {
        this.yPosition = yPosition;
    }
}
