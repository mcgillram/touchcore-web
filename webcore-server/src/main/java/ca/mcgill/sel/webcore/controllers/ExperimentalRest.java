package ca.mcgill.sel.webcore.controllers;

import java.util.List;

import org.json.JSONObject;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonArray;

import ca.mcgill.sel.classdiagram.ClassDiagram;
import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.webcore.WebResourceClassDiagramMapperManager;

@RestController
public class ExperimentalRest {
    
    //must contain the aspect name in the body so that the model can be returned in json format
    @PostMapping("/exp")
    public String alteration(@RequestBody String body) throws Exception {
        return body;
    }
}
