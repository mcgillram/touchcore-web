package ca.mcgill.sel.webcore;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.kartoffelquadrat.asyncrestlib.BroadcastContentManager;

/*
 * Singleton class used to map aspects to a corresponding broadcast content manager
 */
//NOTE: This Class SHOULD be refactored by adding its fields and functions into WebResourceClassDiagramMapperManager
public class BroadcastContentManagerMapper {
    /*
     * Creates the one instance of the ContentManagerMapper
     */
    public static final BroadcastContentManagerMapper INSTANCE = 
            new BroadcastContentManagerMapper();
    
    // TODO : Will become obsolete
    /*
     * One field is a map between the aspect and a BroadcastContentManager, has an identical key set to WebResourceClassDiagramMapperManager.webResourceMapperDB
     */
    private Map<String, BroadcastContentManager<WebResourceClassDiagramMapper>> broadcastContentManagers;
    
    /*
     * Mapping from user name to aspect name to actual model manager, 
     * has an identical key set to WebResourceClassDiagramMapperManager.userTowebResourceMapperDB
     */
    private Map<String,  
        Map<String, BroadcastContentManager<WebResourceClassDiagramMapper>>> userToBroadcastContentManagers;

    public static final long TIMETOPOLL = 30000;
    
    public BroadcastContentManagerMapper() {
        this.broadcastContentManagers = new HashMap<String, BroadcastContentManager<WebResourceClassDiagramMapper>>();
        this.userToBroadcastContentManagers = new HashMap<String, 
                Map<String, BroadcastContentManager<WebResourceClassDiagramMapper>>>();
    }
    
    public void addBroadcastContentManager(String aspectName, WebResourceClassDiagramMapper mpr, ObjectMapper configuredMapper) {
        this.broadcastContentManagers.put(aspectName, new BroadcastContentManager<>(configuredMapper, mpr));
    }
    
    public void addBroadcastContentManager(String username, String aspectName, WebResourceClassDiagramMapper mpr, 
            ObjectMapper configuredMapper) {
        if (!this.userToBroadcastContentManagers.containsKey(username)) {
            this.userToBroadcastContentManagers.put(username, 
                    new HashMap<String, BroadcastContentManager<WebResourceClassDiagramMapper>>());
        }
        Map<String, BroadcastContentManager<WebResourceClassDiagramMapper>> broadcastContentManagers
            = this.userToBroadcastContentManagers.get(username);
        broadcastContentManagers.put(aspectName, new BroadcastContentManager<>(configuredMapper, mpr));
    }
    
    public BroadcastContentManager<WebResourceClassDiagramMapper> getBroadcastContentManager(String aspectName){
        if (!WebResourceClassDiagramMapperManager.getWebResourceMapperDB().containsKey(aspectName)) {
            WebResourceClassDiagramMapperManager.addModelManager(aspectName);
        }
        return broadcastContentManagers.get(aspectName);
    }
    
    public BroadcastContentManager<WebResourceClassDiagramMapper> 
        getBroadcastContentManager(String username, String aspectName) throws IllegalArgumentException {
        if (!this.contains(username, aspectName)) {
            WebResourceClassDiagramMapperManager.addModelManager(username, aspectName);
        }
        return userToBroadcastContentManagers.get(username).get(aspectName);
    }
    
    public boolean contains(String username, String aspectName) {
        return !WebResourceClassDiagramMapperManager.getUserToWebResourceMapperDB().containsKey(username)
                || !WebResourceClassDiagramMapperManager.getUserToWebResourceMapperDB().get(username)
                .containsKey(aspectName);
    }
}