package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

public class TORESTAssociationSetRoleName {
    String roleName;
    public TORESTAssociationSetRoleName(String roleName) {
        super();
        this.roleName = roleName;
    }
    public String getRoleName() {
        return roleName;
    }
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
