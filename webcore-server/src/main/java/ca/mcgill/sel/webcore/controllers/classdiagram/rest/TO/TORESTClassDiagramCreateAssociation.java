package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

public class TORESTClassDiagramCreateAssociation {
    Integer fromClassId;
    Integer toClassId;
    boolean bidirectional;
    public TORESTClassDiagramCreateAssociation(Integer fromClassId, Integer toClassId, boolean bidirectional) {
        super();
        this.fromClassId = fromClassId;
        this.toClassId = toClassId;
        this.bidirectional = bidirectional;
    }
    public Integer getFromClassId() {
        return fromClassId;
    }
    public void setFromClassId(Integer fromClassId) {
        this.fromClassId = fromClassId;
    }
    public Integer getToClassId() {
        return toClassId;
    }
    public void setToClassId(Integer toClassId) {
        this.toClassId = toClassId;
    }
    public boolean isBidirectional() {
        return bidirectional;
    }
    public void setBidirectional(boolean bidirectional) {
        this.bidirectional = bidirectional;
    }
}
