package ca.mcgill.sel.webcore.authentication.auth;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.web.RedirectStrategy;

/**
 * Strategy that prevent redirecting because we are using a REST API.
 * 
 * @author Ridwan Kurmally
 */
public class NoRedirectStrategy implements RedirectStrategy {

    @Override
    public void sendRedirect(HttpServletRequest arg0, HttpServletResponse arg1, String arg2) throws IOException {
        // No redirect is required with pure REST
    }

}
