package ca.mcgill.sel.webcore.controllers.classdiagram.rest.TO;

public class TORESTAssociationRemoveSuperType {
    Integer classId1;
    Integer classId2;
    public TORESTAssociationRemoveSuperType(Integer classId1, Integer classId2) {
        super();
        this.classId1 = classId1;
        this.classId2 = classId2;
    }
    public Integer getClassId1() {
        return classId1;
    }
    public void setClassId1(Integer classId1) {
        this.classId1 = classId1;
    }
    public Integer getClassId2() {
        return classId2;
    }
    public void setClassId2(Integer classId2) {
        this.classId2 = classId2;
    }
}
