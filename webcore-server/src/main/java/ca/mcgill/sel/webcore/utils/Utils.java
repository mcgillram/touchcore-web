package ca.mcgill.sel.webcore.utils;

import org.eclipse.emf.ecore.EObject;

import com.google.common.collect.BiMap;

/**
 * A Util class for the whole API.
 * 
 * @author arthurls
 *
 */
public final class Utils {

    /**
     * Prevents default constructor.
     */
    private Utils() {
    }
    
    /**
     * Returns the EObject from an id.
     * We inverse the idMapping Map to get the value. 
     * @param idMapping the map
     * @param eObjectId the id of the EObject
     * @return eObject
     */
    public static EObject getEObjectById(BiMap<EObject, Integer> idMapping, int eObjectId) {   
        EObject eObject = idMapping.inverse().get(eObjectId);               
        return eObject;
    }
    
    /**
     * Changes a notification Int to a readable String defined in {@link Notification}.
     * @param notification int
     * @return notification string
     */
    //CHECKSTYLE:IGNORE ReturnCount: best way to do it
    public static String notificationToString(int notification) {
        switch (notification) {
            case 1:
                return "SET";
            case 2:
                return "UNSET";
            case 3:
                return "ADD";
            case 4:
                return "REMOVE";
            case 5:
                return "ADD_MANY";
            case 6:
                return "REMOVE_MANY";
            case 7:
                return "MOVE";
            case 8:
                return "REMOVING_ADAPTER";
            case 9:
                return "RESOLVE";
            case 10:
                return "EVENT_TYPE_COUNT";
            default:
                return null;
        }        
    }
    
}
