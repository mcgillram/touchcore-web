export interface Updatable {

  addElement(eType, value);
  setElement(eType, value);
  removeElement(eType, value);
}
