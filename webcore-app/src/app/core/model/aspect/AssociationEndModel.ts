import {ViewElement} from "../ViewElement";

export class AssociationEndModel extends ViewElement {

  _id: string;
  assoc: string;
  lowerBound: string;
  name: string;
  navigable: boolean;
  referenceType: string;

  constructor(obj) {
    super();
    this._id = obj._id;
    this.assoc = obj.assoc;
    this.name = obj.name;
    this.navigable = obj.navigable;
    this.lowerBound = obj.lowerBound;
    this.referenceType = obj.referenceType;
  }

  update(value: any) {
    this.name = value.name;
    this.assoc = value.assoc;
    this.navigable = value.navigable;
    this.lowerBound = value.lowerBound;
    this.referenceType = value.referenceType;
  }

  getHTMLLabel(): string {
    return '';
  }
}
