import {AssociationEndModel} from "./AssociationEndModel";

export class AssociationModel {

  _id: string;
  ends: string[];
  name: string;
  source: AssociationEndModel;
  target: AssociationEndModel;
  sourceId: string;
  targetId: string;
  type: string;

  constructor(obj) {
    this._id = obj._id;
    this.ends = obj.ends;
    this.name = obj.name;
    this.type = obj.type;

    //this.source = new AssociationEndModel(obj.source);
    //this.target = new AssociationEndModel(obj.target);
  }

  setAssociationEnd(obj, isSource: boolean) {
    if (isSource) {
      this.source = new AssociationEndModel(obj);
    } else {
      this.target = new AssociationEndModel(obj);
    }
  }

  update(value: any) {
    this.ends = value.ends;
    this.name = value.name;
    this.type = value.type;
  }

  isNavigable(source: boolean) {
    if (!source) {
      if (this.target.navigable !== undefined && this.target.navigable === false) return false;
    } else {
      if (this.source.navigable !== undefined && this.source.navigable === false) return false;
    }
    return true;
  }

  getAssociationData(){
    return {
      start: {
        name: this.target.name,
        bound: this.target.lowerBound,
      },
      end: {
        name: this.source.name,
        bound: this.source.lowerBound,
      }
    };
  }

  getAssociationStyle() {
    let style = {
      start: 'start-',
      end: 'end-'
    };

    if (this.target.referenceType === 'Aggregation') {
      style.end += 'aggregation';
    } else if (this.target.referenceType === 'Composition') {
      style.end += 'composition';
    } else {
      if (this.target.navigable === false) {
        style.end += 'navigable';
      } else {
        style.end += 'default';
      }
    }

    if (this.source.referenceType === 'Aggregation') {
      style.start += 'aggregation';
    } else if (this.source.referenceType === 'Composition') {
      style.start += 'composition';
    } else {
      if (this.source.navigable === false) {
        style.start += 'navigable';
      } else {
        style.start += 'default';
      }
    }

    return style;
  }
}
