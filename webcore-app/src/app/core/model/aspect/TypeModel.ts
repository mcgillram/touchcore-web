import {LiteralModel} from "./LiteralModel";
import {ViewElement} from "../ViewElement";
import {Updatable} from "../Updatable";
import {LayoutValue} from "./layout/LayoutValue";

export class TypeModel extends ViewElement implements Updatable {

  _id: string;
  eClass: string;
  literals: LiteralModel[];
  name: string;
  layout: LayoutValue;

  constructor(obj) {
    super();
    this._id = obj._id;
    this.eClass = obj.eClass;
    this.name = obj.name;
    this.literals = [];
    if (obj.literals) obj.literals.forEach(o => this.literals.push(new LiteralModel(o)));
  }

  isEnum() {
    return this.eClass.split('//')[2] === 'REnum';
  }

  update(value: any) {
    this.name = value.name;
  }

  getHTMLLabel(): string {
    let label = '';
    label += '<< enumeration >></br>';
    label +=  this.name;
    return label;
  }

  addElement(eType, value) {
    if (eType === 'REnumLiteral') {
      this.literals.push(new LiteralModel(value));
    }
    return this;
  }


  removeElement(eType, value) {
    this.literals = this.literals.filter((a) => {
      return a._id !== value._id;
    });
    return this;
  }

  setElement(eType, value) {
    let lit = this.literals.find((a) => {
      return a._id === value._id;
    });
    lit.update(value);
    return this;
  }

}
