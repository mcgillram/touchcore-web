import {AssociationModel} from "./AssociationModel";
import {ClassModel} from "./ClassModel";
import {TypeModel} from "./TypeModel";
import {ElementLayout} from "./layout/ElementLayout";
import {Updatable} from "../Updatable";

export class StructuralViewModel implements Updatable {

  _id: string;
  associations: AssociationModel[];
  classes: ClassModel[];
  types: TypeModel[];

  constructor(object) {
    this._id = object._id;
    this.associations = [];
    this.classes = [];
    this.types = [];

    if (object.associations) object.associations.forEach(o => this.associations.push(new AssociationModel(o)));
    if (object.classes) object.classes.forEach(o => this.classes.push(new ClassModel(o)));
    if (object.types) object.types.forEach(o => this.types.push(new TypeModel(o)));
  }

  /**
   * Returns array of available types in current aspect model
   * (classes are types too)
   */
  getTypes() {
    let types = [];
    for (let t of this.types) {
      if (!t.name) {
        t.name = t.eClass.split('/R')[1].toLowerCase();
        //specific case
        if (t.name === 'string') t.name = 'String';
      }
      types.push({name: t.name, id: t._id});
    }
    for (let c of this.classes) {
      types.push({name: c.name, id: c._id});
    }
    return types;
  }

  /**
   * This methods associates layout data (x, y coords)
   * to aspect elements
   */
  updateLayout(layoutList: ElementLayout[]) {
    for (let layout of layoutList) {
      let clazz = this.getClassById(layout.key);
      if (clazz !== null) {
        clazz.layout = layout.value;
      } else {
        let type = this.getTypeById(layout.key);
        if (type !== null) {
          type.layout = layout.value;
        }
      }
    }
  }

  /**
   * Types in classes are referenced by id. This methods
   * allow to associate string value to types
   */
  updateTypes() {
    let types = this.getTypes();
    this.classes.forEach(c => c.updateTypes(types));
  }

  /**
   * This methods associates association ends to associations,
   * in order to facilitate data access
   */
  updateAssociations() {
    // gather association ends from classes
    let ends = [];
    for (let c of this.classes) {
      if (c.associationEnds) {
        for (let ae of c.associationEnds) {
          ends.push({id: c._id, value: ae});
        }
      }
    }
    // attach ends to their association
    for (let asso of this.associations) {
      if (!asso.source) {
        let s = ends.find(o => o.value._id === asso.ends[0]);
        asso.setAssociationEnd(s.value, true);
        asso.sourceId = s.id;
      }
      if (!asso.target) {
        let t = ends.find(o => o.value._id === asso.ends[1]);
        asso.setAssociationEnd(t.value, false);
        asso.targetId = t.id;
      }
    }
  }

  getClassById(id) {
    for (let cl of this.classes) {
      if (String(cl._id) === String(id)) {
        return cl;
      }
    }
    return null;
  }

  getAssociationById(id) {
    for (let cl of this.associations) {
      if (String(cl._id) === String(id)) {
        return cl;
      }
    }
    return null;
  }

  getTypeById(id) {
    for (let ty of this.types) {
      if (ty._id === id) {
        return ty;
      }
    }
    return null;
  }

  addElement(eType, value) {
    switch (eType) {
      case 'Class':
        let newClass = new ClassModel(value);
        this.classes.push(newClass);
        return newClass;
      case 'Association':
        let newAsso = new AssociationModel(value);
        this.associations.push(newAsso);
        return newAsso;
      case 'REnum':
        let newType = new TypeModel(value);
        this.types.push(newType);
        return newType;
    }
  }

  removeElement(eType, value) {
    let toReturn = null;
    switch (eType) {
      case 'Class':

        let classIndex = this.classes.findIndex((o) => {
          return o._id === value._id;
        });

        if (classIndex !== -1) {
          toReturn = this.classes[classIndex];
          this.classes.splice(classIndex, 1);
        }
        break;
      case 'Association':
        let assoIndex = this.associations.findIndex((o) => {
          return o._id === value._id;
        });
        if (assoIndex !== -1) {
          toReturn = this.associations[assoIndex];
          this.associations.splice(assoIndex, 1);
        }
        break;
      case 'REnum':
        let typeIndex = this.types.findIndex((o) => {
          return o._id === value._id;
        });

        if (typeIndex !== -1) {
          toReturn = this.types[typeIndex];
          this.types.splice(typeIndex, 1);
        }
        break;
    }
    return toReturn;
  }

  setElement(eType, value) {
    switch (eType) {
      case 'Class':
        let clazz = this.classes.find((a) => {
          return a._id === value._id;
        });
        clazz.update(value);
        return clazz;
      case 'Association':
        let assoc = this.associations.find((a) => {
          return a._id === value._id;
        });
        assoc.update(value);
        return assoc;

      case 'REnum':
        let enumeration = this.types.find((a) => {
          return a._id === value._id;
        });
        enumeration.update(value);
        return enumeration;
    }
  }
}
