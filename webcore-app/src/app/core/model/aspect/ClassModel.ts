import {AssociationEndModel} from "./AssociationEndModel";
import {AttributeModel} from "./AttributeModel";
import {OperationModel} from "./OperationModel";
import {ViewElement} from "../ViewElement";
import {LayoutValue} from "./layout/LayoutValue";
import {Updatable} from "../Updatable";

export class ClassModel extends ViewElement implements Updatable {

  _id: string;
  abstract: boolean;
  associationEnds: AssociationEndModel[];
  attributes: AttributeModel[];
  operations: OperationModel[];
  eClass: string;
  name: string;
  visibility: string;
  superTypes: string[];
  partiality: string;
  layout: LayoutValue;
  dataType: boolean;

  constructor(obj) {
    super();
    this._id = obj._id;
    this.abstract = obj.abstract;
    this.eClass = obj.eClass;
    this.name = obj.name;
    if (obj.visibility) this.visibility = obj.visibility;
    else this.visibility = 'concern';
    this.superTypes = obj.superTypes;
    if (obj.partiality) this.partiality = obj.partiality;
    else this.partiality = 'none';

    if (obj.dataType) this.dataType = obj.dataType;
    else this.dataType = false;

    this.associationEnds = [];
    this.attributes = [];
    this.operations = [];

    if (obj.associationEnds) obj.associationEnds.forEach(o => this.associationEnds.push(new AssociationEndModel(o)));
    if (obj.attributes) obj.attributes.forEach(o => this.attributes.push(new AttributeModel(o)));
    if (obj.operations) obj.operations.forEach(o => this.operations.push(new OperationModel(o)));
  }

  update(value: any) {
    this.abstract = value.abstract;
    this.name = value.name;
    this.visibility = value.visibility;
    this.superTypes = value.superTypes;
    this.partiality = value.partiality;
  }


  updateTypes(types) {
    if (this.operations) {
      for (let o of this.operations) {

        if (!o.returnTypeName) {
          let type = types.find(t => t.id === o.returnType);
          o.returnTypeName = type ? type.name : '';
        }

        if (o.parameters) {
          for (let p of o.parameters) {
            if (!p.typeName) {
              let ttype = types.find(t => t.id === p.type);
              p.typeName = ttype ? ttype.name : '';
            }
          }
        }
      }
    }
    if (this.attributes) {
      for (let a of this.attributes) {
        if (!a.typeName) {
          let type = types.find(t => t.id === a.type);
          a.typeName = type ? type.name : '';
        }
      }
    }
  }

  getHTMLLabel() {
    let label = '';
    if (this.dataType) label += '<< dataType >></br>';

    if (this.visibility === 'public') label += '+ ';
    else label += '~ ';
    if (this.partiality === 'public') label += '|';
    else if (this.partiality === 'concern') label += '¦';
    if (this.abstract !== undefined && this.abstract) {
      label += '<i>' + this.name + '</i>';
    } else label += this.name;
    return label;
  }

  addElement(eType, value) {
    switch (eType) {
      case 'Operation':
        let newOperation = new OperationModel(value);
        this.operations.push(newOperation);
        return newOperation;
      case 'Attribute':
        let newAttribute = new AttributeModel(value);
        this.attributes.push(newAttribute);
        return newAttribute;
      case 'AssociationEnd':
        let newAssoEnd = new AssociationEndModel(value);
        this.associationEnds.push(newAssoEnd);
        return newAssoEnd;
    }
  }

  removeElement(eType, value) {
    let returnObject = null;
    switch (eType) {
      case 'Operation':
        this.operations.forEach((a, index) => {
          if (a._id === value._id) {
            returnObject = a;
            this.operations.splice(index, 1);
            return;
          }
        });
        break;
      case 'Attribute':
        this.attributes.forEach((a, index) => {
          if (a._id === value._id) {
            returnObject = a;
            this.attributes.splice(index, 1);
            return;
          }
        });
        break;
      case 'AssociationEnd':
        this.associationEnds.forEach((a, index) => {
          if (a._id === value._id) {
            returnObject = a;
            this.associationEnds.splice(index, 1);
            return;
          }
        });
        break;
    }
    return returnObject;
  }

  setElement(eType, value) {
    switch (eType) {
      case 'Operation':
        let op = this.operations.find((a) => {
          return a._id === value._id;
        });
        op.update(value);
        return op;
      case 'Attribute':
        let attr = this.attributes.find((a) => {
          return a._id === value._id;
        });
        attr.update(value);
        return attr;
      case 'AssociationEnd':
        let assoc = this.associationEnds.find((a) => {
          return a._id === value._id;
        });
        assoc.update(value);
        return assoc;
    }
  }
}

