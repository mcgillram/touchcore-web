import {ViewElement} from "../ViewElement";

export class AttributeModel extends ViewElement{

  _id: string;
  name: string;
  type: string;
  typeName: string;
  abstract: boolean;

  constructor(obj) {
    super();
    this._id = obj._id;
    this.name = obj.name;
    this.type = obj.type;
    this.typeName = obj.typeName;
  }

  update(value: any) {
    this.name = value.name;
    this.type = value.type;
    this.typeName = value.typeName;
  }

  getHTMLLabel(): string {
    return '<span class="visibility">' + '# ' + '</span>' +
      '<span class="type">' + this.typeName + ' </span>' +
      '<span class="name">' + this.name + '</span>';
  }
}
