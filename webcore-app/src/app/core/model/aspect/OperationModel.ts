import {ViewElement} from "../ViewElement";
import {ParameterModel} from "./ParameterModel";
import {Updatable} from "../Updatable";

export class OperationModel extends ViewElement implements Updatable {

  _id: string;
  name: string;
  returnType: string;
  returnTypeName: string;
  visibility: string;
  extendedVisibility: string;
  abstract: boolean;
  partiality: string;
  operationType: string;
  parameters: ParameterModel[];

  constructor(obj) {
    super();
    this._id = obj._id;
    this.name = obj.name;
    this.returnType = obj.returnType;
    this.returnTypeName = obj.returnTypeName;
    this.visibility = obj.visibility;
    this.extendedVisibility = obj.extendedVisibility;
    this.abstract = obj.abstract;
    if (obj.partiality) this.partiality = obj.partiality;
    else this.partiality = 'none';
    this.operationType = obj.operationType;
    this.parameters = [];

    if (obj.parameters) obj.parameters.forEach(o => this.parameters.push(new ParameterModel(o)));
  }


  update(value: any) {
    this.name = value.name;
    this.returnType = value.returnType;
    this.returnTypeName = value.returnTypeName;
    this.visibility = value.visibility;
    this.extendedVisibility = value.extendedVisibility;
    this.abstract = value.abstract;
    this.partiality = value.partiality;
  }

  getHTMLLabel(): string {
    let label = '';

    if (this.visibility === 'public') {
      label += '+ ';
    } else if (this.extendedVisibility === 'private') {
      label += '- ';
    } else if (this.extendedVisibility === 'package') {
      label += '~ ';
    } else if (this.extendedVisibility === 'protected') {
      label += '# ';
    }

    if (this.partiality === 'public') label += '|';
    else if (this.partiality === 'concern') label += '¦';

    if (!this.operationType) label += this.returnTypeName + ' ';
    label += this.name + '(';
    this.parameters.forEach((p, index) => {
      label += p.getHTMLLabel();
      index === this.parameters.length - 1 ? label += '' : label += ', ';
    });
    label += ')';

    if (this.abstract !== undefined && this.abstract) {
      label = '<i>' + label + '</i>';
    }

    return label;

  }

  addElement(eType, value) {
    switch (eType) {
      case 'Parameter':
        this.parameters.push(new ParameterModel(value));
        break;
    }
    return this;
  }

  removeElement(eType, value) {
    switch (eType) {
      case 'Parameter':
        this.parameters = this.parameters.filter((a) => {
          return a._id !== value._id;
        });
        break;
    }
    return this;
  }

  setElement(eType, value) {
    switch (eType) {
      case 'Parameter':
        let param = this.parameters.find((a) => {
          return a._id === value._id;
        });
        param.update(value);
        break;
    }
    return this;
  }
}
