import {LayoutModel} from "./layout/LayoutModel";
import {StructuralViewModel} from "./StructuralViewModel";

export class AspectModel {

  _id: string;
  coreConcern: string;
  eClass: string;
  layout: LayoutModel;
  name: string;
  realizes: string[];
  structuralView: StructuralViewModel;

  constructor(object) {
    this._id = object._id;
    this.coreConcern = object.coreConcern;
    this.eClass = object.eClass;
    this.name = object.name;
    this.realizes = object.realizes;

    this.structuralView = new StructuralViewModel(object.structuralView);
    this.layout = new LayoutModel(object.layout);
  }

  /**
   * Retrieves layout for current structural view
   * and transfers data to it
   */
  updateLayout() {
    let layout = this.layout.containers.find(a => a.key === this.structuralView._id);
    this.structuralView.updateLayout(layout.value);
  }
}
