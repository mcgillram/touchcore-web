import {ViewElement} from "../ViewElement";

export class LiteralModel extends ViewElement {

  _id: string;
  name: string;


  constructor(obj) {
    super();
    this._id = obj._id;
    this.name = obj.name;
  }

  update(value: any){
    this.name = value.name;
  }

  getHTMLLabel(): string {
    return this.name;
  }
}
