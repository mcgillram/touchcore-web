import {ViewLayout} from "./ViewLayout";

export class LayoutModel  {

  _id: string;
  containers: ViewLayout[];

  constructor(object) {
    this._id = object._id;
    this.containers = [];
    if (object.containers) object.containers.forEach(o => this.containers.push(new ViewLayout(o)));
  }

  updateLayoutElement(update) {
    if (update.notificationType === 'SET') {
      return this.containers[0].updateElement(update);
    }
    return null;
  }

  updateLayout(update) {
    let layoutContainer = this.containers.find((a) => {
      return parseInt(a._id, 10) === update.containerId;
    });

    if (layoutContainer) {

      switch (update.notificationType) {
        case 'ADD':
          layoutContainer.addElement(update.value);
          break;
        case 'REMOVE':
          layoutContainer.removeElement(update.value);
          break;
      }
    }
  }


}




