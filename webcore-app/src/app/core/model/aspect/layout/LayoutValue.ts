
export class LayoutValue {

  constructor(object){
    this._id = object._id;
    this.x = object.x;
    this.y = object.y;
  }

  _id: string;
  x: number;
  y: number;


}
