import {ElementLayout} from "./ElementLayout";

export class ViewLayout {

  _id: string;
  key: string;
  value: ElementLayout[];

  constructor(object) {
    this._id = object._id;
    this.key = object.key;
    this.value = [];
    if (object.value) object.value.forEach(o => this.value.push(new ElementLayout(o)));
  }

  addElement(value) {
    let elem = new ElementLayout(value);
    this.value.push(elem);
    return elem;
  }

  removeElement(value) {
    this.value = this.value.filter((a) => {
      return a._id !== value._id;
    });
  }

  updateElement(update) {

    let element = this.value.find((a) => {
      return parseInt(a._id, 10) === update.containerId;
    });


    if (element) {
      element.update(update.value);
    }
    return element;
  }

}
