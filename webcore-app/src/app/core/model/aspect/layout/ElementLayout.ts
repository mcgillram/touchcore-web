import {LayoutValue} from "./LayoutValue";

export class ElementLayout {
  _id: string;
  key: string;
  value: LayoutValue;

  constructor(object) {
    this._id = object._id;
    this.key = object.key;
    this.value = new LayoutValue(object.value);
  }

  update(value) {
    this.value.x = value.x;
    this.value.y = value.y;
  }

}
