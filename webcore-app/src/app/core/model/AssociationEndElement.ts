import {ViewElement} from "./ViewElement";
import {AssociationModel} from "./aspect/AssociationModel";

export class AssociationEndElement extends ViewElement {

  value;
  association: AssociationModel;

  constructor(value, association) {
    super();
    this.value = value;
    this.association = association;
  }

  getHTMLLabel(): string {
    return String(this.value);
  }
}
