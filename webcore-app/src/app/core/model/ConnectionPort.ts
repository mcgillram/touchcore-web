import {ViewElement} from "./ViewElement";

/**
 * A connection port is a cell aside a class view, allowing to
 * create associations
 */
export class ConnectionPort extends ViewElement {

  orientation: string;
  parentId;
  width: number;
  height: number;

  constructor(orientation, parentId, width, height) {
    super();
    this.orientation = orientation;
    this.parentId = parentId;
    this.width = width;
    this.height = height;
  }

  getIconSize() {
    let h;
    let w;
    let x = 15;
    let factor = 2.3;
    switch (this.orientation) {
      case 'west':
        h = factor * x;
        w = x;
        break;
      case 'east':
        h = factor * x;
        w = x;
        break;
      case 'south':
        h = x;
        w = factor * x;
        break;
      case 'north' :
        h = x;
        w = factor * x;
        break;

    }
    return {
      height: h,
      width: w
    };
  }

  getOffset() {
    switch (this.orientation) {
      case 'west':
        return new mxPoint(-this.width, 0);
      case 'east':
        return new mxPoint(0, 0);
      case 'south':
        return new mxPoint(0, 0);
      case 'north':
        return new mxPoint(0, -this.height);
    }

  }


  getHTMLLabel(): string {

    let style = '';
    let size = this.getIconSize();

    let iconStyle = 'opacity:0.4;';

    iconStyle += 'width:' + size.width + 'px;height:' + size.height + 'px;';

    let imagePath = "assets/icons/chevron_" + this.orientation + ".png";

    return "<div style='" + style + "'>" +
      "<img src='" + imagePath + "'" +
      " style='" + iconStyle + "'/>" +
      "</div>";


  }
}
