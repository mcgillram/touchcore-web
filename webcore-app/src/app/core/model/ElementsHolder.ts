/**
 * An ElementsHolder is a container used in swimlane cells,
 * holding child cells
 */
export class ElementsHolder {

  type: string;

  constructor(type) {
    this.type = type;
  }

  getStyle() {
    return this.type + 'Holder';
  }

  getHTMLLabel(): string {
    return "";
  }
}
