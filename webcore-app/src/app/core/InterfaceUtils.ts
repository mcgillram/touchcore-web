export default {

  // width of sidebar in Short mode
  SIDEBAR_SHORT_MODE_WIDTH: 120,

  // width of sidebar in Full mode
  SIDEBAR_FULL_MODE_WIDTH: 250,

  // sidebar is hidden but grabber can be visible if > 0
  SIDEBAR_HIDDEN_MODE_WIDTH: 15,

  // minimum width for displaying sidebar content
  SIDEBAR_MIN_WIDTH_FOR_CONTENT: 115,

  // factor for scroll zoom
  ZOOM_FACTOR_DELTA: 0.05,

  TAP_AND_HOLD_DELAY: 540

};
