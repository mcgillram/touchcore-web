/**
 * Base class for all layout algorithms in mxGraph.
 * https://jgraph.github.io/mxgraph/docs/js-api/files/layout/mxGraphLayout-js.html#mxGraphLayout.graph
 */
declare class mxGraphLayout {

  constructor(graph: mxGraph, b?);

  graph: mxGraph;
  useBoundingBox: boolean;
  parent: mxCell;
  forceConstant;
  fill;
  resizeParent;
  isVertexMovable;


  moveCell(cell: mxCell, x: number, y: number);

  execute(parent: mxCell);

  isVertexIgnored(vertex: mxCell);

  isEdgeIgnored(edge: mxCell);
}

declare class mxMorphing extends mxEventSource {
  constructor(graph, steps?, ease?, delay?);

  steps;
  step;
  ease;
  cells;

  startAnimation();

  updateAnimation();

  show(move);

  animateCell(cell, move?, recurse?);

  stopRecursion(state, delta);

  getDelta(state);

  getOriginForCell(cell);


}


declare class mxHierarchicalLayout extends mxGraphLayout {
}

declare class mxCircleLayout extends mxGraphLayout {
}

declare class mxCompositeLayout extends mxGraphLayout {
  constructor(graph, layouts?, master?);
}

declare class mxParallelEdgeLayout extends mxGraphLayout {
}

declare class mxCompactTreeLayout extends mxGraphLayout {
}

declare class mxFastOrganicLayout extends mxGraphLayout {
  disableEdgeStyle: boolean;
}

declare class mxRadialTreeLayout extends mxGraphLayout {
}

declare class mxStackLayout extends mxGraphLayout {
  constructor(graph, horizontal?, spacing?, x0?, y0?, border?);
  horizontal;
  spacing;
  x0;
  y0;
  border;
  marginTop;
  marginLeft;
  marginRight;
  marginBottom;
  keepFirstLocation;
  fill;
  resizeParent;
  resizeLast;
  wrap;
  borderCollapse;
  isHorizontal();
  moveCell(cell: mxCell, x, y);
  getOarentSize(cell: mxCell);
  execute(cell: mxCell);

}

declare class mxPartitionLayout extends mxGraphLayout {
}

declare class mxEdgeLabelLayout extends mxGraphLayout {

}
