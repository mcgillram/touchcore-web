﻿/******************      View          **************/

declare class mxCellState {

  view: mxGraphView;
  /**
   * Reference to the <mxCell> that is represented by this state.
   */
  cell: mxCell;
  /**
   * Contains an array of key, value pairs that represent the style of the
   * cell.
   */
  style: any;
  x;
  y;
  height;
  width;
  /**
   * Specifies if the state is invalid. Default is true.
   */
  invalid: boolean;
  /**
   * <mxPoint> that holds the origin for all child cells. Default is a new
   * empty <mxPoint>.
   */
  origin: mxPoint;
  /**
   * Holds an array of <mxPoints> that represent the absolute points of an
   * edge.
   */
  absolutePoints: mxPoint[];
  /**
   * <mxPoint> that holds the absolute offset. For edges, this is the
   * absolute coordinates of the label position. For vertices, this is the
   * offset of the label relative to the top, left corner of the vertex.
   */
  absoluteOffset: mxPoint;
  /**
   * Caches the visible source terminal state.
   */
  visibleSourceState: any;
  /**
   * Caches the visible target terminal state.
   */
  visibleTargetState: any;
  /**
   * Caches the distance between the end points for an edge.
   */
  terminalDistance: number;
  /**
   * Caches the length of an edge.
   */
  length: number;
  /**
   * Array of numbers that represent the cached length of each segment of the
   * edge.
   */
  segments: number[];
  /**
   * Holds the <mxShape> that represents the cell graphically.
   */
  shape: mxShape;
  /**
   * Holds the <mxText> that represents the label of the cell. Thi smay be
   * null if the cell has no label.
   */
  text: mxText;
  /**
   * Holds the unscaled width of the state.
   */
  unscaledWidth: any;
  /**
   * Implicit variable declarations
   */
  cellBounds: mxRectangle;
  paintBounds: mxRectangle;
  /**
   * Constructs a new object that represents the current state of the given
   * cell in the specified view.
   *
   * @param view - <mxGraphView> that contains the state.
   * @param cell - <mxCell> that this state represents.
   * @param style - Array of key, value pairs that constitute the style.
   */
  constructor(view: mxGraphView, cell: mxCell, style: any[]);
  /**
   * Returns the <mxRectangle> that should be used as the perimeter of the
   * cell.
   *
   * @param border - Optional border to be added around the perimeter bounds.
   * @param bounds - Optional <mxRectangle> to be used as the initial bounds.
   */
  getPerimeterBounds(border?: number, bounds?: mxRectangle): mxRectangle;
  /**
   * Sets the first or last point in <absolutePoints> depending on isSource.
   *
   * @param point - <mxPoint> that represents the terminal point.
   * @param isSource - Boolean that specifies if the first or last point should
   * be assigned.
   */
  setAbsoluteTerminalPoint(point: mxPoint, isSource: boolean): void;
  /**
   * Sets the given cursor on the style and text style.
   */
  setCursor(cursor: any): void;
  /**
   * Returns the visible source or target terminal cell.
   *
   * @param source - Boolean that specifies if the source or target cell should be
   * returned.
   */
  getVisibleTerminal(source: boolean): any;
  /**
   * Returns the visible source or target terminal state.
   *
   * @param source - Boolean that specifies if the source or target state should be
   * returned.
   */
  getVisibleTerminalState(source: boolean): any;
  /**
   * Sets the visible source or target terminal state.
   *
   * @param terminalState - <mxCellState> that represents the terminal.
   * @param source - Boolean that specifies if the source or target state should be set.
   */
  setVisibleTerminalState(terminalState: mxCellState, source: boolean): void;
  /**
   * Returns the unscaled, untranslated bounds.
   */
  getCellBounds(): mxRectangle;
  /**
   * Returns the unscaled, untranslated paint bounds. This is the same as
   * <getCellBounds> but with a 90 degree rotation if the style's
   * isPaintBoundsInverted returns true.
   */
  getPaintBounds(): mxRectangle;
  /**
   * Updates the cellBounds and paintBounds.
   */
  updateCachedBounds(): void;
  /**
   * Destructor: setState
   *
   * Copies all fields from the given state to this state.
   */
  setState(state: mxCellState): void;
  /**
   * Returns a clone of this <mxCellState>.
   */
  clone(): mxCellState;
  /**
   * Destructor: destroy
   *
   * Destroys the state and all associated resources.
   */
  destroy(): void;

}

declare class mxCellEditor {
  constructor(graph: any);
  /**
   * Creates the <textarea> and installs the event listeners. The key handler
   * updates the <modified> state.
   */
  init(): void;
  /**
   * Called in <stopEditing> if cancel is false to invoke <mxGraph.labelChanged>.
   */
  applyValue(state: any, value: any): void;
  /**
   * Gets the initial editing value for the given cell.
   */
  getInitialValue(state: any, trigger: any): any;
  /**
   * Returns the current editing value.
   */
  getCurrentValue(state: any): any;
  /**
   * Installs listeners for focus, change and standard key event handling.
   */
  installListeners(elt: any): void;
  /**
   * Returns true if the given keydown event should stop cell editing. This
   * returns true if F2 is pressed of if <mxGraph.enterStopsCellEditing> is true
   * and enter is pressed without control or shift.
   */
  isStopEditingEvent(evt: any): boolean;
  /**
   * Returns true if this editor is the source for the given native event.
   */
  isEventSource(evt: any): boolean;
  /**
   * Returns <modified>.
   */
  resize(): void;
  /**
   * Called if the textarea has lost focus.
   */
  focusLost(): void;
  /**
   * Returns the background color for the in-place editor. This implementation
   * always returns null.
   */
  getBackgroundColor(state: any): any;
  /**
   * Starts the editor for the given cell.
   *
   * Parameters:
   *
   * cell - <mxCell> to start editing.
   * trigger - Optional mouse event that triggered the editor.
   */
  startEditing(cell: any, trigger: any): void;
  /**
   * Returns <selectText>.
   */
  isSelectText(): any;
  /**
   * Stops the editor and applies the value if cancel is false.
   */
  stopEditing(cancel: any): void;
  /**
   * Prepares the textarea for getting its value in <stopEditing>.
   * This implementation removes the extra trailing linefeed in Firefox.
   */
  prepareTextarea(): void;
  /**
   * Returns true if the label should be hidden while the cell is being
   * edited.
   */
  isHideLabel(state: any): boolean;
  /**
   * Returns the minimum width and height for editing the given state.
   */
  getMinimumSize(state: any): mxRectangle;
  /**
   * Returns the <mxRectangle> that defines the bounds of the editor.
   */
  getEditorBounds(state: any): mxRectangle;
  /**
   * Returns the initial label value to be used of the label of the given
   * cell is empty. This label is displayed and cleared on the first keystroke.
   * This implementation returns <emptyLabelText>.
   *
   * Parameters:
   *
   * cell - <mxCell> for which a text for an empty editing box should be
   * returned.
   */
  getEmptyLabelText(cell: any): any;
  /**
   * Returns the cell that is currently being edited or null if no cell is
   * being edited.
   */
  getEditingCell(): any;
  /**
   * Destroys the editor and removes all associated resources.
   */
  destroy(): void;

}

declare class mxCellOverlay {
  image: any;
  /**
   * Holds the optional string to be used as the tooltip.
   */
  tooltip: any;
  /**
   * Holds the horizontal alignment for the overlay. Default is
   * <mxConstants.ALIGN_RIGHT>. For edges, the overlay always appears in the
   * center of the edge.
   */
  align: string;
  /**
   * Holds the vertical alignment for the overlay. Default is
   * <mxConstants.ALIGN_BOTTOM>. For edges, the overlay always appears in the
   * center of the edge.
   */
  verticalAlign: string;
  /**
   * Holds the offset as an <mxPoint>. The offset will be scaled according to the
   * current scale.
   */
  offset: any;
  /**
   * Holds the cursor for the overlay. Default is 'help'.
   */
  cursor: any;
  /**
   * Defines the overlapping for the overlay, that is, the proportional distance
   * from the origin to the point defined by the alignment. Default is 0.5.
   */
  defaultOverlap: number;

  /**
   * Constructs a new overlay using the given image and tooltip.
   *
   * @param image - <mxImage> that represents the icon to be displayed.
   * @param tooltip - Optional string that specifies the tooltip.
   * @param align - Optional horizontal alignment for the overlay. Possible
   * values are <ALIGN_LEFT>, <ALIGN_CENTER> and <ALIGN_RIGHT>
   * (default).
   * @param verticalAlign - Vertical alignment for the overlay. Possible
   * values are <ALIGN_TOP>, <ALIGN_MIDDLE> and <ALIGN_BOTTOM>
   * (default).
   * @param offset ?
   * @param cursor ?
   */
  constructor(image: mxImage,
              tooltip ?: string,
              align ?: any,
              verticalAlign ?: any,
              offset ?: mxPoint,
              cursor ?: string);

  /**
   * Returns the bounds of the overlay for the given <mxCellState> as an
   * <mxRectangle>. This should be overridden when using multiple overlays
   * per cell so that the overlays do not overlap.
   *
   * The following example will place the overlay along an edge (where
   * x=[-1..1] from the start to the end of the edge and y is the
   * orthogonal offset in px).
   *
   * (code)
   * overlay.getBounds = function(state)
   * {
   *   var bounds = mxCellOverlay.prototype.getBounds.apply(this, arguments);
   *
   *   if (state.view.graph.getModel().isEdge(state.cell))
   *   {
   *     var pt = state.view.getPoint(state, {x: 0, y: 0, relative: true});
   *
   *     bounds.x = pt.x - bounds.width / 2;
   *     bounds.y = pt.y - bounds.height / 2;
   *   }
   *
   *   return bounds;
   * };
   * (end)
   *
   * Parameters:
   *
   * state - <mxCellState> that represents the current state of the
   * associated cell.
   */
  getBounds(state: any): mxRectangle;

  /**
   * Returns the textual representation of the overlay to be used as the
   * tooltip. This implementation returns <tooltip>.
   */
  toString(): any;
}

declare class mxEdgeStyle {

  /** Implements an entity relation style for edges (as used in database schema diagrams).  At the time
   * the function is called, the result array contains a placeholder (null) for the first absolute point,
   * that is, the point where the edge and source terminal are connected.  The implementation of the style
   * then adds all intermediate waypoints except for the last point, that is, the connection point between
   * the edge and the target terminal.  The first ant the last point in the result array are then replaced
   * with mxPoints that take into account the terminal’s perimeter and next point on the edge.
   */
  static EntityRelation(state: mxCellState, source: mxCell, target: mxCell, points: mxPoint[], result: any): any;

  /** Implements a self-reference, aka. loop. */
  static Loop(state: mxCellState, source: mxCell, target: mxCell, points: mxPoint[], result: any): any;

  /** Uses either SideToSide or TopToBottom depending on the horizontal flag in the cell style.
   * SideToSide is used if horizontal is true or unspecified.  See EntityRelation for a description of the parameters.
   */
  static ElbowConnector(state: mxCellState, source: mxCell, target: mxCell, points: mxPoint[], result: any): any;

  /** Implements a vertical elbow edge.  See EntityRelation for a description of the parameters. */
  static SideToSide(state: mxCellState, source: mxCell, target: mxCell, points: mxPoint[], result: any): any;

  /** Implements a horizontal elbow edge.  See EntityRelation for a description of the parameters */
  static TopToBottom(state: mxCellState, source: mxCell, target: mxCell, points: mxPoint[], result: any): any;

  /** Implements an orthogonal edge style.  Use <mxEdgeSegmentHandler> as an interactive handler for this style. */
  static SegmentConnector(state: mxCellState, source: mxCell, target: mxCell, points: mxPoint[], result: any): any;

  /** Implements a local orthogonal router between the given cells. */
  static OrthConnector(state: mxCellState, source: mxCell, target: mxCell, points: mxPoint[], result: any): any;

  /** Implements our own edges style management */
  static CustomStyle(state: any, source: any, target: any, points: any, result: any);

}

declare class mxCellRenderer {
  static registerShape(a, b);
}

declare class mxPerimeter {
  static RectanglePerimeter(bounds, vertex, next, orthogonal);

  static EllipsePerimeter(bounds, vertex, next, orthogonal);

  static TrianglePerimeter(bounds, vertex, next, orthogonal);
}

/** Defines an object that contains the constraints about how to connect one side of an edge to its terminal. */
declare class mxConnectionConstraint {

  constructor(point, parameter);

  /** mxPoint that specifies the fixed location of the connection point. */
  point;
  /** Boolean that specifies if the point should be projected onto the perimeter of the terminal. */
  perimeter;
  /** Optional string that specifies the name of the constraint. */
  name;
  /**  Optional float that specifies the horizontal offset of the constraint. */
  dx;
  /** Optional float that specifies the vertical offset of the constraint. */
  dy;

  /** Constructs a new connection constraint for the given point and boolean arguments. */
  static mxConnectionConstraint(point, perimeter, name?, dx?, dy?);
}

declare class mxPrintPreview {
  constructor(graph: any, scale?: any, pageFormat?: any, border?: any,
              x0?: any, y0?: any, borderColor?: any, title?: any, pageSelector?: any);
  /**
   * Returns <wnd>.
   */
  getWindow(): any;
  /**
   * Returns the string that should go before the HTML tag in the print preview
   * page. This implementation returns an X-UA meta tag for IE5 in quirks mode,
   * IE8 in IE8 standards mode and edge in IE9 standards mode.
   */
  getDoctype(): string;
  /**
   * Adds the given graph to the existing print preview.
   *
   * Parameters:
   *
   * css - Optional CSS string to be used in the head section.
   * targetWindow - Optional window that should be used for rendering. If
   * this is specified then no HEAD tag, CSS and BODY tag will be written.
   */
  appendGraph(graph: any, scale: any, x0: any, y0: any, forcePageBreaks: any, keepOpen: any): void;
  /**
   * Shows the print preview window. The window is created here if it does
   * not exist.
   *
   * Parameters:
   *
   * css - Optional CSS string to be used in the head section.
   * targetWindow - Optional window that should be used for rendering. If
   * this is specified then no HEAD tag, CSS and BODY tag will be written.
   */
  open(css?: any, targetWindow?: any, forcePageBreaks?: any, keepOpen?: any): any;
  /**
   * Adds a page break to the given document.
   */
  addPageBreak(doc: any): void;
  /**
   * Writes the closing tags for body and page after calling <writePostfix>.
   */
  closeDocument(): void;
  /**
   * Writes the HEAD section into the given document, without the opening
   * and closing HEAD tags.
   */
  writeHead(doc: any, css: any): void;
  /**
   * Called before closing the body of the page. This implementation is empty.
   */
  writePostfix(doc: any): void;
  /**
   * Creates the page selector table.
   */
  createPageSelector(vpages: any, hpages: any): any;
  /**
   * Creates a DIV that prints a single page of the given
   * graph using the given scale and returns the DIV that
   * represents the page.
   *
   * Parameters:
   *
   * w - Width of the page in pixels.
   * h - Height of the page in pixels.
   * dx - Optional horizontal page offset in pixels (used internally).
   * dy - Optional vertical page offset in pixels (used internally).
   * content - Callback that adds the HTML content to the inner div of a page.
   * Takes the inner div as the argument.
   * pageNumber - Integer representing the page number.
   */
  renderPage(w: any, h: any, dx: any, dy: any, content: any, pageNumber: any): HTMLDivElement;
  /**
   * Returns the root cell for painting the graph.
   */
  getRoot(): any;
  /**
   * Adds a graph fragment to the given div.
   *
   * Parameters:
   *
   * dx - Horizontal translation for the diagram.
   * dy - Vertical translation for the diagram.
   * scale - Scale for the diagram.
   * pageNumber - Number of the page to be rendered.
   * div - Div that contains the output.
   * clip - Contains the clipping rectangle as an <mxRectangle>.
   */
  addGraphFragment(dx: any, dy: any, scale: any, pageNumber: any, div: any, clip: any): void;
  /**
   * Returns the link for the given cell state. This returns null.
   */
  getLinkForCellState(state: any): any;
  /**
   * Inserts the background image into the given div.
   */
  insertBackgroundImage(div: any, dx: any, dy: any): void;
  /**
   * Returns the pages to be added before the print output. This returns null.
   */
  getCoverPages(): any;
  /**
   * Returns the pages to be added after the print output. This returns null.
   */
  getAppendices(): any;
  /**
   * Opens the print preview and shows the print dialog.
   *
   * Parameters:
   *
   * css - Optional CSS string to be used in the head section.
   */
  print(css: any): void;
  /**
   * Closes the print preview window.
   */
  close(): void;
}



/******************      View end      **************/
