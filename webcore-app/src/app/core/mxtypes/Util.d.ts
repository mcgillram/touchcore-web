﻿/******************      Util      **************/


/** Common interfaces for Utils */
declare module Util {

  /** Common handler for addListener of EventSource */
  interface EventHandler {
    (sender: any, state: any);
  }

}

declare class mxRubberband {
  constructor(graph: mxGraph);
}

declare class mxStylesheet {
  constructor();
  /**
   * Creates and returns the default vertex style.
   */
  createDefaultVertexStyle(): any;
  /**
   * Creates and returns the default edge style.
   */
  createDefaultEdgeStyle(): any;
  /**
   * Sets the default style for vertices using defaultVertex as the
   * stylename.
   *
   * Parameters:
   * style - Key, value pairs that define the style.
   */
  putDefaultVertexStyle(style: any): void;
  /**
   * Sets the default style for edges using defaultEdge as the stylename.
   */
  putDefaultEdgeStyle(style: any): void;
  /**
   * Returns the default style for vertices.
   */
  getDefaultVertexStyle(): any;
  /**
   * Sets the default style for edges.
   */
  getDefaultEdgeStyle(): any;
  /**
   * Stores the given map of key, value pairs under the given name in
   * <styles>.
   *
   * Example:
   *
   * The following example adds a new style called 'rounded' into an
   * existing stylesheet:
   *
   * (code)
   * var style = new Object();
   * style[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_RECTANGLE;
   * style[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
   * style[mxConstants.STYLE_ROUNDED] = true;
   * graph.getStylesheet().putCellStyle('rounded', style);
   * (end)
   *
   * In the above example, the new style is an object. The possible keys of
   * the object are all the constants in <mxConstants> that start with STYLE
   * and the values are either JavaScript objects, such as
   * <mxPerimeter.RightAngleRectanglePerimeter> (which is in fact a function)
   * or expressions, such as true. Note that not all keys will be
   * interpreted by all shapes (eg. the line style ignores the fill color).
   * The final call to this method associates the style with a name in the
   * stylesheet. The style is used in a cell with the following code:
   *
   * (code)
   * model.setStyle(cell, 'rounded');
   * (end)
   *
   * Parameters:
   *
   * name - Name for the style to be stored.
   * style - Key, value pairs that define the style.
   */
  putCellStyle(name: any, style: any): void;
  /**
   * Returns the cell style for the specified stylename or the given
   * defaultStyle if no style can be found for the given stylename.
   *
   * Parameters:
   *
   * name - String of the form [(stylename|key=value);] that represents the
   * style.
   * defaultStyle - Default style to be returned if no style can be found.
   */
  getCellStyle(name: any, defaultStyle: any): any;
}



/**
 * The mxEventObject is a wrapper for all properties of a single event.  Additionally, it also offers functions
 * to consume the event and check if it was consumed as follows:
 */
declare class mxEventObject {
  constructor(name: string);

  /** Holds the name. */
  name: string;

  /** Holds the properties as an associative array. */
  properties: any;

  /** Returns name */
  getName: () => string;

  /** Returns the property for the given key. */
  getProperty: (key: string) => any;
}

/**
 * A wrapper class for an associative array with object keys.  Note: This implementation uses <mxObjectIdentitiy> to
 * turn object keys into strings.
 */
declare class mxDictionary {

  /** Stores the (key, value) pairs in this dictionary. */
  map: any;

  /**
   * Returns the value for the given key.
   * @param key
   */
  get: (key: any) => any;

  /**
   * Stores the value under the given key and returns the previous value for that key.
   * @param key
   * @param value
   */
  put: (key: any, value: any) => any;

  /** Returns all keys as an array. */
  getKeys: () => string[];
  /** Returns all values as an array. */
  getValues: () => string[];
}

/**
 * Base class for objects that dispatch named events.  To create a subclass that inherits from mxEventSource, the following code is used.
 */
declare class mxEventSource {

  /**
   * Binds the specified function to the given event name.  If no event name is given, then the listener
   * is registered for all events.
   * The parameters of the listener are the sender and an mxEventObject.
   */
  addListener(name: any, func);

}

/**
 * Singleton class that acts as a global converter from string to object values in a style.
 * This is currently only used to perimeters and edge styles.
 */
declare class mxStyleRegistry {

  /**
   * Puts the given object into the registry under the given name.
   * @param name name of the style to set
   * @param obj the style function you want to set
   */
  static putValue: (name: any, obj: mxEdgeStyle) => any;

  /** Maps from strings to objects. */
  values: [];

}

/**
 * Encapsulates the URL, width and height of an image.
 */
declare class mxImage {

  /** Encapsulates the URL, width and height of an image. */
  constructor(src: string, width: number, height: number);

}


/**
 * Cross-browser DOM event support
 */
declare class mxEvent {

  static MOUSE_DOWN;
  static MOUSE_MOVE;
  static MOUSE_UP;
  static ACTIVATE;
  static RESIZE_START;
  static RESIZE;
  static RESIZE_END;
  static MOVE_START;
  static MOVE;
  static MOVE_END;
  static PAN_START;
  static PAN;
  static PAN_END;
  static MINIMIZE;
  static NORMALIZE;
  static MAXIMIZE;
  static HIDE;
  static SHOW;
  static CLOSE;
  static DESTROY;
  static REFRESH;
  static SIZE;
  static SELECT;
  static FIRED;
  static FIRE_MOUSE_EVENT;
  static GESTURE;
  static TAP_AND_HOLD;
  static GET;
  static RECEIVE;
  static CONNECT;
  static DISCONNECT;
  static SUSPEND;
  static RESUME;
  static MARK;
  static ROOT;
  static POST;
  static OPEN;
  static SAVE;
  static BEFORE_ADD_VERTEX;
  static ADD_VERTEX;
  static AFTER_ADD_VERTEX;
  static DONE;
  static EXECUTE;
  static EXECUTED;
  static BEGIN_UPDATE;
  static START_EDIT;
  static END_UPDATE;
  static END_EDIT;
  static BEFORE_UNDO;
  static UNDO;
  static REDO;
  static CHANGE;
  static NOTIFY;
  static LAYOUT_CELLS;
  static CLICK;
  static SCALE;
  static TRANSLATE;
  static SCALE_AND_TRANSLATE;
  static UP;
  static DOWN;
  static ADD;
  static REMOVE;
  static CLEAR;
  static ADD_CELLS;
  static CELLS_ADDED;
  static MOVE_CELLS;
  static CELLS_MOVED;
  static RESIZE_CELLS;
  static CELLS_RESIZED;
  static TOGGLE_CELLS;
  static CELLS_TOGGLED;
  static ORDER_CELLS;
  static CELLS_ORDERED;
  static REMOVE_CELLS;
  static CELLS_REMOVED;
  static GROUP_CELLS;
  static UNGROUP_CELLS;
  static REMOVE_CELLS_FROM_PARENT;
  static FOLD_CELLS;
  static CELLS_FOLDED;
  static ALIGN_CELLS;
  static LABEL_CHANGED;
  static CONNECT_CELL;
  static CELL_CONNECTED;
  static SPLIT_EDGE;
  static FLIP_EDGE;
  static START_EDITING;
  static EDITING_STARTED;
  static EDITING_STOPPED;
  static ADD_OVERLAY;
  static REMOVE_OVERLAY;
  static UPDATE_CELL_SIZE;
  static ESCAPE;
  static DOUBLE_CLICK;
  static START;
  static RESET;

  static consume(event, preventDefault?, stopPropagation?);

  static addMouseWheelListener(i);

  static addListener(one, two?, three?);

  static removeListener();

  static addGestureListeners(node, startListener?, moveListener?, endListener?);

  static isTouchEvent(e);

  static isLeftMouseButton(e);

  static isRightButton(e);

  static isShiftDown();

  static isControlDown();

  static getClientX(e);

  static getClientY(e);

  static isMultiTouchEvent(e);

  static isMouseEvent(e);


  static disableContextMenu(element);
}


declare class mxMouseEvent {
  consumed;
  evt;
  graphX;
  graphY;
  state;

  getEvent();

  getSource();

  isSource(shape);

  getX();

  getY();

  getGraphX();

  getGraphY();

  getState();

  getCell();

  isPopupTrigger();

  isConsumed();

  consume(preventDefault);
}


declare class mxPoint {

  /** Constructs a new point for the optional x and y coordinates.*
   * If no coordinates are given, then the default values for x and y are used.
   */
  constructor(x: number, y: number);

  x: number;
  y: number;

  /**
   * Returns true if the given object equals this point.
   */
  equals(obj: any): boolean;

  clone();

}

declare class mxConstants {

  static DEFAULT_HOTSPOT;
  static MIN_HOTSPOT_SIZE;
  static MAX_HOTSPOT_SIZE;
  static RENDERING_HINT_EXACT;
  static RENDERING_HINT_FASTER;
  static RENDERING_HINT_FASTEST;
  static DIALECT_SVG;
  static DIALECT_VML;
  static DIALECT_MIXEDHTML;
  static DIALECT_PREFERHTML;
  static DIALECT_STRICTHTML;
  static NS_SVG;
  static NS_XHTML;
  static NS_XLINK;
  static SHADOWCOLOR;
  static VML_SHADOWCOLOR;
  static SHADOW_OFFSET_X;
  static SHADOW_OFFSET_Y;
  static SHADOW_OPACITY;
  static NODETYPE_ELEMENT;
  static NODETYPE_ATTRIBUTE;
  static NODETYPE_TEXT;
  static NODETYPE_CDATA;
  static NODETYPE_ENTITY_REFERENCE;
  static NODETYPE_ENTITY;
  static NODETYPE_PROCESSING_INSTRUCTION;
  static NODETYPE_COMMENT;
  static NODETYPE_DOCUMENT;
  static NODETYPE_DOCUMENTTYPE;
  static NODETYPE_DOCUMENT_FRAGMENT;
  static NODETYPE_NOTATION;
  static TOOLTIP_VERTICAL_OFFSET;
  static DEFAULT_VALID_COLOR;
  static DEFAULT_INVALID_COLOR;
  static OUTLINE_HIGHLIGHT_COLOR;
  static OUTLINE_HIGHLIGHT_STROKEWIDTH;
  static HIGHLIGHT_STROKEWIDTH;
  static HIGHLIGHT_SIZE;
  static HIGHLIGHT_OPACITY;
  static CURSOR_MOVABLE_VERTEX;
  static CURSOR_MOVABLE_EDGE;
  static CURSOR_LABEL_HANDLE;
  static CURSOR_TERMINAL_HANDLE;
  static CURSOR_BEND_HANDLE;
  static CURSOR_VIRTUAL_BEND_HANDLE;
  static CURSOR_CONNECT;
  static HIGHLIGHT_COLOR;
  static CONNECT_TARGET_COLOR;
  static INVALID_CONNECT_TARGET_COLOR;
  static DROP_TARGET_COLOR;
  static VALID_COLOR;
  static INVALID_COLOR;
  static EDGE_SELECTION_COLOR;
  static VERTEX_SELECTION_COLOR;
  static VERTEX_SELECTION_STROKEWIDTH;
  static EDGE_SELECTION_STROKEWIDTH;
  static VERTEX_SELECTION_DASHED;
  static EDGE_SELECTION_DASHED;
  static GUIDE_COLOR;
  static GUIDE_STROKEWIDTH;
  static OUTLINE_COLOR;
  static OUTLINE_STROKEWIDTH;
  static HANDLE_SIZE;
  static LABEL_HANDLE_SIZE;
  static HANDLE_FILLCOLOR;
  static HANDLE_STROKECOLOR;
  static LABEL_HANDLE_FILLCOLOR;
  static CONNECT_HANDLE_FILLCOLOR;
  static LOCKED_HANDLE_FILLCOLOR;
  static OUTLINE_HANDLE_FILLCOLOR;
  static OUTLINE_HANDLE_STROKECOLOR;
  static DEFAULT_FONTFAMILY;
  static DEFAULT_FONTSIZE;
  static DEFAULT_TEXT_DIRECTION;
  static LINE_HEIGHT;
  static WORD_WRAP;
  static ABSOLUTE_LINE_HEIGHT;
  static DEFAULT_FONTSTYLE;
  static DEFAULT_STARTSIZE;
  static DEFAULT_MARKERSIZE;
  static DEFAULT_IMAGESIZE;
  static ENTITY_SEGMENT;
  static RECTANGLE_ROUNDING_FACTOR;
  static LINE_ARCSIZE;
  static ARROW_SPACING;
  static ARROW_WIDTH;
  static ARROW_SIZE;
  static PAGE_FORMAT_A4_PORTRAIT;
  static PAGE_FORMAT_A4_LANDSCAPE;
  static PAGE_FORMAT_LETTER_PORTRAIT;
  static PAGE_FORMAT_LETTER_LANDSCAPE;
  static NONE;
  static STYLE_PERIMETER: 'perimeter';
  static STYLE_SOURCE_PORT: 'sourcePort';
  static STYLE_TARGET_PORT: 'targetPort';
  static STYLE_PORT_CONSTRAINT: 'portConstraint';
  static STYLE_PORT_CONSTRAINT_ROTATION: 'portConstraintRotation';
  static STYLE_SOURCE_PORT_CONSTRAINT: 'sourcePortConstraint';
  static STYLE_TARGET_PORT_CONSTRAINT: 'targetPortConstraint';
  static STYLE_OPACITY: 'opacity';
  static STYLE_FILL_OPACITY: 'fillOpacity';
  static STYLE_STROKE_OPACITY: 'strokeOpacity';
  static STYLE_TEXT_OPACITY: 'textOpacity';
  static STYLE_TEXT_DIRECTION: 'textDirection';
  static STYLE_OVERFLOW: 'overflow';
  static STYLE_ORTHOGONAL: 'orthogonal';
  static STYLE_EXIT_X: 'exitX';
  static STYLE_EXIT_Y: 'exitY';
  static STYLE_EXIT_DX: 'exitDx';
  static STYLE_EXIT_DY: 'exitDy';
  static STYLE_EXIT_PERIMETER: 'exitPerimeter';
  static STYLE_ENTRY_X: 'entryX';
  static STYLE_ENTRY_Y: 'entryY';
  static STYLE_ENTRY_DX: 'entryDx';
  static STYLE_ENTRY_DY: 'entryDy';
  static STYLE_ENTRY_PERIMETER: 'entryPerimeter';
  static STYLE_WHITE_SPACE: 'whiteSpace';
  static STYLE_ROTATION: 'rotation';
  static STYLE_FILLCOLOR: 'fillColor';
  static STYLE_POINTER_EVENTS: 'pointerEvents';
  static STYLE_SWIMLANE_FILLCOLOR: 'swimlaneFillColor';
  static STYLE_MARGIN: 'margin';
  static STYLE_GRADIENTCOLOR: 'gradientColor';
  static STYLE_GRADIENT_DIRECTION: 'gradientDirection';
  static STYLE_STROKECOLOR: 'strokeColor';
  static STYLE_SEPARATORCOLOR: 'separatorColor';
  static STYLE_STROKEWIDTH: 'strokeWidth';
  static STYLE_ALIGN: 'align';
  static STYLE_VERTICAL_ALIGN: 'verticalAlign';
  static STYLE_LABEL_WIDTH: 'labelWidth';
  static STYLE_LABEL_POSITION: 'labelPosition';
  static STYLE_VERTICAL_LABEL_POSITION: 'verticalLabelPosition';
  static STYLE_IMAGE_ASPECT: 'imageAspect';
  static STYLE_IMAGE_ALIGN: 'imageAlign';
  static STYLE_IMAGE_VERTICAL_ALIGN: 'imageVerticalAlign';
  static STYLE_GLASS: 'glass';
  static STYLE_IMAGE: 'image';
  static STYLE_IMAGE_WIDTH: 'imageWidth';
  static STYLE_IMAGE_HEIGHT: 'imageHeight';
  static STYLE_IMAGE_BACKGROUND: 'imageBackground';
  static STYLE_IMAGE_BORDER: 'imageBorder';
  static STYLE_FLIPH: 'flipH';
  static STYLE_FLIPV: 'flipV';
  static STYLE_NOLABEL: 'noLabel';
  static STYLE_NOEDGESTYLE: 'noEdgeStyle';
  static STYLE_LABEL_BACKGROUNDCOLOR: 'labelBackgroundColor';
  static STYLE_LABEL_BORDERCOLOR: 'labelBorderColor';
  static STYLE_LABEL_PADDING: 'labelPadding';
  static STYLE_INDICATOR_SHAPE: 'indicatorShape';
  static STYLE_INDICATOR_IMAGE: 'indicatorImage';
  static STYLE_INDICATOR_COLOR: 'indicatorColor';
  static STYLE_INDICATOR_STROKECOLOR: 'indicatorStrokeColor';
  static STYLE_INDICATOR_GRADIENTCOLOR: 'indicatorGradientColor';
  static STYLE_INDICATOR_SPACING: 'indicatorSpacing';
  static STYLE_INDICATOR_WIDTH: 'indicatorWidth';
  static STYLE_INDICATOR_HEIGHT: 'indicatorHeight';
  static STYLE_INDICATOR_DIRECTION: 'indicatorDirection';
  static STYLE_SHADOW: 'shadow';
  static STYLE_SEGMENT: 'segment';
  static STYLE_ENDARROW: 'endArrow';
  static STYLE_STARTARROW: 'startArrow';
  static STYLE_ENDSIZE: 'endSize';
  static STYLE_STARTSIZE: 'startSize';
  static STYLE_SWIMLANE_LINE: 'swimlaneLine';
  static STYLE_ENDFILL: 'endFill';
  static STYLE_STARTFILL: 'startFill';
  static STYLE_DASHED: 'dashed';
  static STYLE_DASH_PATTERN: 'dashPattern';
  static STYLE_FIX_DASH: 'fixDash';
  static STYLE_ROUNDED: 'rounded';
  static STYLE_CURVED: 'curved';
  static STYLE_ARCSIZE: 'arcSize';
  static STYLE_ABSOLUTE_ARCSIZE: 'absoluteArcSize';
  static STYLE_SOURCE_PERIMETER_SPACING: 'sourcePerimeterSpacing';
  static STYLE_TARGET_PERIMETER_SPACING: 'targetPerimeterSpacing';
  static STYLE_PERIMETER_SPACING: 'perimeterSpacing';
  static STYLE_SPACING: 'spacing';
  static STYLE_SPACING_TOP: 'spacingTop';
  static STYLE_SPACING_LEFT: 'spacingLeft';
  static STYLE_SPACING_BOTTOM: 'spacingBottom';
  static STYLE_SPACING_RIGHT: 'spacingRight';
  static STYLE_HORIZONTAL: 'horizontal';
  static STYLE_DIRECTION: 'direction';
  static STYLE_ANCHOR_POINT_DIRECTION: 'anchorPointDirection';
  static STYLE_ELBOW: 'elbow';
  static STYLE_FONTCOLOR: 'fontColor';
  static STYLE_FONTFAMILY: 'fontFamily';
  static STYLE_FONTSIZE: 'fontSize';
  static STYLE_FONTSTYLE: 'fontStyle';
  static STYLE_ASPECT: 'aspect';
  static STYLE_AUTOSIZE: 'autosize';
  static STYLE_FOLDABLE: 'foldable';
  static STYLE_EDITABLE: 'editable';
  static STYLE_BACKGROUND_OUTLINE: 'backgroundOutline';
  static STYLE_BENDABLE: 'bendable';
  static STYLE_MOVABLE: 'movable';
  static STYLE_RESIZABLE: 'resizable';
  static STYLE_RESIZE_WIDTH: 'resizeWidth';
  static STYLE_RESIZE_HEIGHT: 'resizeHeight';
  static STYLE_ROTATABLE: 'rotatable';
  static STYLE_CLONEABLE: 'cloneable';
  static STYLE_DELETABLE: 'deletable';
  static STYLE_SHAPE: 'shape';
  static STYLE_EDGE: 'edgeStyle';
  static STYLE_JETTY_SIZE: 'jettySize';
  static STYLE_SOURCE_JETTY_SIZE: 'sourceJettySize';
  static STYLE_TARGET_JETTY_SIZE: 'targetJettySize';
  static STYLE_LOOP: 'loopStyle';
  static STYLE_ORTHOGONAL_LOOP: 'orthogonalLoop';
  static STYLE_ROUTING_CENTER_X: 'routingCenterX';
  static STYLE_ROUTING_CENTER_Y: 'routingCenterY';
  static FONT_BOLD: 1;
  static FONT_ITALIC: 2;
  static FONT_UNDERLINE: 4;
  static SHAPE_RECTANGLE: 'rectangle';
  static SHAPE_ELLIPSE: 'ellipse';
  static SHAPE_DOUBLE_ELLIPSE: 'doubleEllipse';
  static SHAPE_RHOMBUS: 'rhombus';
  static SHAPE_LINE: 'line';
  static SHAPE_IMAGE: 'image';
  static SHAPE_ARROW: 'arrow';
  static SHAPE_ARROW_CONNECTOR: 'arrowConnector';
  static SHAPE_LABEL: 'label';
  static SHAPE_CYLINDER: 'cylinder';
  static SHAPE_SWIMLANE: 'swimlane';
  static SHAPE_CONNECTOR: 'connector';
  static SHAPE_ACTOR: 'actor';
  static SHAPE_CLOUD: 'cloud';
  static SHAPE_TRIANGLE: 'triangle';
  static SHAPE_HEXAGON: 'hexagon';
  static ARROW_CLASSIC: 'classic';
  static ARROW_CLASSIC_THIN: 'classicThin';
  static ARROW_BLOCK: 'block';
  static ARROW_BLOCK_THIN: 'blockThin';
  static ARROW_OPEN: 'open';
  static ARROW_OPEN_THIN: 'openThin';
  static ARROW_OVAL: 'oval';
  static ARROW_DIAMOND: 'diamond';
  static ARROW_DIAMOND_THIN: 'diamondThin';
  static ALIGN_LEFT: 'left';
  static ALIGN_CENTER: 'center';
  static ALIGN_RIGHT: 'right';
  static ALIGN_TOP: 'top';
  static ALIGN_MIDDLE: 'middle';
  static ALIGN_BOTTOM: 'bottom';
  static DIRECTION_NORTH: 'north';
  static DIRECTION_SOUTH: 'south';
  static DIRECTION_EAST: 'east';
  static DIRECTION_WEST: 'west';
  static TEXT_DIRECTION_DEFAULT: '';
  static TEXT_DIRECTION_AUTO: 'auto';
  static TEXT_DIRECTION_LTR: 'ltr';
  static TEXT_DIRECTION_RTL: 'rtl';
  static DIRECTION_MASK_NONE: 0;
  static DIRECTION_MASK_WEST: 1;
  static DIRECTION_MASK_NORTH: 2;
  static DIRECTION_MASK_SOUTH: 4;
  static DIRECTION_MASK_EAST: 8;
  static DIRECTION_MASK_ALL: 15;
  static ELBOW_VERTICAL: 'vertical';
  static ELBOW_HORIZONTAL: 'horizontal';
  static EDGESTYLE_ELBOW: 'elbowEdgeStyle';
  static EDGESTYLE_ENTITY_RELATION: 'entityRelationEdgeStyle';
  static EDGESTYLE_LOOP: 'loopEdgeStyle';
  static EDGESTYLE_SIDETOSIDE: 'sideToSideEdgeStyle';
  static EDGESTYLE_TOPTOBOTTOM: 'topToBottomEdgeStyle';
  static EDGESTYLE_ORTHOGONAL: 'orthogonalEdgeStyle';
  static EDGESTYLE_SEGMENT: 'segmentEdgeStyle';
  static PERIMETER_ELLIPSE: 'ellipsePerimeter';
  static PERIMETER_RECTANGLE: 'rectanglePerimeter';
  static PERIMETER_RHOMBUS: 'rhombusPerimeter';
  static PERIMETER_HEXAGON: 'hexagonPerimeter';
  static PERIMETER_TRIANGLE: 'trianglePerimeter';
  static TARGET_HIGHLIGHT_COLOR: string;

}


declare class mxUtils {
  errorResource;
  closeRerousce;
  errorImage;

  static convertPoint(ctn, x, y);

  static createImage(src);

  static contains(a, b, c);

  static removeCursors();

  static getCurrentStyle();

  static parseCssNumber();

  static setPrefixedStyle();

  static hasScrollbars();

  static bind(scope, funct);

  static eval();

  static findNode();

  static getFunctionName();

  static indexOf();

  static forEach();

  static remove();

  static isNode();

  static isAncestorNode();

  static getChildNodes();

  static importNode();

  static createXmlDocument();

  static parseXml();

  static clearSelection();

  static removeWhitespace();

  static getTextContent();

  static setTextContent();

  static write();

  static clone(obj, transients?, shallow?);

  static getValue(a, b, c);

  static getColor();

  static getNumber();

  static isNaN();

  static toString();

  static toRadians();

  static toDegree();

  static animateChanges();

  static fadeOut();

  static sortCells();

  static getStylename();

  static getStylenames();

  static addStylename();

  static removeStylename();

  static setCellStyles();

  static setStyle();

  static setStyleFlag();

  static show();

  static printScreen(graph);

  static alert(message);

  static prompt(message, defaultValue?);

  static confirm();

  static error(message, width?, close?, icon?);

  static makeDraggable();


  static extend(a, b);
}

/******************      Util end      **************/
