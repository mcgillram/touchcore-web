import {AspectModel} from "../model/aspect/AspectModel";
import {StructuralViewModel} from "../model/aspect/StructuralViewModel";
import {ClassModel} from "../model/aspect/ClassModel";
import {OperationModel} from "../model/aspect/OperationModel";
import {TypeModel} from "../model/aspect/TypeModel";
import {ViewElement} from "../model/ViewElement";
import {ElementLayout} from "../model/aspect/layout/ElementLayout";
import {AttributeModel} from "../model/aspect/AttributeModel";
import {AssociationModel} from "../model/aspect/AssociationModel";
import {AssociationEndModel} from "../model/aspect/AssociationEndModel";

interface UpdateModel {
  containerId: number;
  eType: string;
  notificationType: string;
  value: any;
}

interface ViewUpdateHolder {
  add: ViewElement[];
  update: ViewElement[];
  remove: ViewElement[];
  move: ElementLayout[];
}

/**
 * This class is responsible of holding Aspect Model,
 * performing data operations & allowing data access
 */
class GraphModel {

  aspect: AspectModel;

  constructor(model) {
    this.aspect = new AspectModel(model);
    this.postProcess();
  }

  /**
   * Postprocess the Aspect to fill the missing fields
   * - add name to types
   * - merge associations and association ends
   * - merge layout and classes
   */
  postProcess() {
    this.aspect.structuralView.updateTypes();
    this.aspect.structuralView.updateAssociations();
    this.aspect.updateLayout();
  }

  updateFullModel(model) {
    this.aspect = new AspectModel(model);
    this.postProcess();
  }

  updateModel(updateStack: UpdateModel[], graphModel: mxGraphModel): ViewUpdateHolder {

    let elementsToUpdate: ViewUpdateHolder = {
      add: [],
      update: [],
      remove: [],
      move: []
    };

    /**
     * To do : some updates can't be achieved because container is not yet created
     * Need to handle it for later process
     */


    for (let update of updateStack) {

      // layout modified (add, remove)
      if (update.eType === 'ElementMap') {
        this.aspect.layout.updateLayout(update);
      }
      // layout element update <=> class has moved
      else if (update.eType === 'LayoutElement') {
        let newLayout = this.aspect.layout.updateLayoutElement(update);
        elementsToUpdate.move.push(newLayout);

      } else {

        if (update.containerId !== null) {
          let containerCell = graphModel.getCell(String(update.containerId));
          let container;

          // if container is not in graphModel we get it from the aspect model
          if (!containerCell) {
            container = this.aspect.structuralView.getClassById(update.containerId);
          } else {
            container = containerCell.value;
          }

          let result;

          switch (update.notificationType) {
            case 'ADD':
              result = container.addElement(update.eType, update.value);
              break;
            case 'REMOVE':
              result = container.removeElement(update.eType, update.value);
              break;
            case 'SET':
              result = container.setElement(update.eType, update.value);
              break;
          }

          this.prepareForView(elementsToUpdate, result, container, update, graphModel);

        }
      }
    }
    this.postProcess();
    this.cleanResponseObject(elementsToUpdate);
    return elementsToUpdate;
  }


  private cleanResponseObject(obj) {
    for (let arr of Object.keys(obj)) {
      obj[arr] = Array.from(new Set(obj[arr]));
    }
  }

  private prepareForView(viewProcess, resultFromUpdate, container, update: UpdateModel, graphModel: mxGraphModel) {
    if (resultFromUpdate === null) return;

    if (container instanceof StructuralViewModel) {

      if (update.notificationType === 'REMOVE') {
        viewProcess.remove.push(resultFromUpdate);
      } else if (update.notificationType === 'ADD') {
        viewProcess.add.push(resultFromUpdate);
      } else {
        viewProcess.update.push(resultFromUpdate);
      }

    } else if (container instanceof ClassModel) {

      if (resultFromUpdate instanceof OperationModel ||
        resultFromUpdate instanceof AttributeModel) {
        viewProcess.update.push(container);
      } else if (resultFromUpdate instanceof AssociationEndModel) {


        if (update.notificationType === 'SET') { //Add and remove ignored

          let assoCell = graphModel.getCell(resultFromUpdate.assoc);

          if (assoCell) {
            let asso = assoCell.value as AssociationModel;
            if (resultFromUpdate._id === asso.source._id) {
              asso.setAssociationEnd(resultFromUpdate, true);
            } else {
              asso.setAssociationEnd(resultFromUpdate, false);
            }
            viewProcess.update.push(asso);
          }


        }
      }

    } else if (container instanceof TypeModel) {

      viewProcess.update.push(resultFromUpdate);

    } else if (container instanceof OperationModel) {

      let cell = graphModel.getCell(container._id);

      if (cell instanceof mxCell) {
        let classModel = cell.parent.parent.getValue();
        viewProcess.update.push(classModel);

      } else {
        console.error('Update not handled by view');
      }

    } else {
      console.error('Update not handled by view');
    }
  }
}

export default GraphModel;
