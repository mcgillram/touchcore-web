import StructuralViewGraph from "./StructuralViewGraph";
import {AspectModel} from "../model/aspect/AspectModel";
import {ViewElement} from "../model/ViewElement";
import {ElementLayout} from "../model/aspect/layout/ElementLayout";
import {ClassModel} from "../model/aspect/ClassModel";
import {AssociationModel} from "../model/aspect/AssociationModel";
import {TypeModel} from "../model/aspect/TypeModel";

interface ViewUpdateHolder {
  add: ViewElement[];
  update: ViewElement[];
  remove: ViewElement[];
  move: ElementLayout[];
}

/**
 * This class is responsible of the visualization of
 * a structural view.
 * The view consists in a custom mxGraph inside an HTML container.
 */
class GraphView {

  graph: StructuralViewGraph;

  constructor(container, uiService) {

    this.graph = new StructuralViewGraph(container, uiService);
  }


  /**
   * Draws an aspect model (a.k.a class diagram) on
   * an empty graph
   */
  drawAspect(model: AspectModel) {

    this.resetView();

    this.graph.model.beginUpdate();

    try {

      let root = this.graph.getDefaultParent();
      let oldId = root.id;
      root.setValue(model.structuralView);
      root.setId(model.structuralView._id);

      delete this.graph.model.cells[oldId];
      this.graph.model.cells[model.structuralView._id] = root;

      model.structuralView.classes.forEach(c => {
        this.graph.drawer.drawClass(c);
      });
      model.structuralView.associations.forEach(a => {
        this.graph.drawer.drawAssociation(a, false);
      });
      model.structuralView.classes.forEach(c => {
        this.graph.drawer.drawInheritanceAssociation(c, false);
      });
      model.structuralView.types.forEach(t => {
        if (t.isEnum()) {
          this.graph.drawer.drawEnumeration(t);
        }
      });
      this.graph.drawer.reorganizeTerminals();
    } finally {
      this.graph.model.endUpdate();
    }
  }

  handleUpdates(viewUpdateHolder: ViewUpdateHolder) {

    this.graph.model.beginUpdate();

    try {

      viewUpdateHolder.add.forEach(element => {
        if (element instanceof ClassModel) {
          this.graph.drawer.drawClass(element);
        } else if (element instanceof AssociationModel) {
          this.graph.drawer.drawAssociation(element, true);
        } else if (element instanceof TypeModel) {
          this.graph.drawer.drawEnumeration(element);
        } else {
          console.error('update to handle :', element);
        }
      });
      viewUpdateHolder.update.forEach(element => {
        if (element instanceof ClassModel) {
          this.graph.drawer.updateClass(element);
        } else if (element instanceof AssociationModel) {
          this.graph.drawer.updateAssociation(element);
        } else if (element instanceof TypeModel) {
          this.graph.drawer.updateEnumeration(element);
        } else {
          console.error('update to handle :', element);
        }

      });
      viewUpdateHolder.remove.forEach(element => {
        this.graph.drawer.removeCell(element);

      });
      viewUpdateHolder.move.forEach(element => {
        let cell = this.graph.model.getCell(element.key);
        if (cell) {
          cell.geometry.x = element.value.x;
          cell.geometry.y = element.value.y;
        }
      });

    } finally {
      this.graph.model.endUpdate();
    }

  }

  /**
   * Clears the view
   */
  resetView() {
    let root = this.graph.getDefaultParent();
    this.graph.removeCells(this.graph.getChildVertices(root), true);
    root.setValue(null);

  }

  switchExpertMode(){
    this.graph.interactionsHandler.switchExpertMode();
  }

  getExpertMode(){
    return this.graph.interactionsHandler.expertMode;
  }

  setExpertMode(value){
    this.graph.interactionsHandler.expertMode = value;
    this.graph.uiService.expertMode = value;
  }


}

export default GraphView;
