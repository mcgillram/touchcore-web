import {ClassModel} from "../model/aspect/ClassModel";
import {AssociationModel} from "../model/aspect/AssociationModel";
import {TypeModel} from "../model/aspect/TypeModel";
import {ViewElement} from "../model/ViewElement";
import {ConnectionPort} from "../model/ConnectionPort";
import {AssociationEndElement} from "../model/AssociationEndElement";
import {ElementsHolder} from "../model/ElementsHolder";


const CLASS_VIEW_WIDTH = 120;
const CLASS_VIEW_HEIGHT = 34;
const CLASS_ELEMENT_HEIGHT = 22;
const CLASS_WIDTH_PADDING = 22;

const PORT_THICKNESS = 30;

/**
 * This class is responsible of the visualization of
 * structural view elements.
 */
class StructuralViewDrawer {

  graph: mxGraph;

  constructor(graph: mxGraph) {
    this.graph = graph;
  }

  /**
   * Draws a bottom line on the cell, considered as a separator line
   */
  private drawBottomLine(cell: mxCell) {
    this.graph.insertVertex(cell, null, null, 0, 1,
      CLASS_VIEW_WIDTH, 2, 'classSeparator', true);
  }


  /**
   * Puts a list of elements as cell inside a cell
   * @param elements : list of View Elements to display
   * @param holderCell : the cell holding the elements
   * @param style : the style of elements' cells
   */
  private drawClassChildren(elements: ViewElement[], holderCell: mxCell, style: string) {
    let childCount = 0;

    for (let element of elements) {
      let yPosition = childCount * CLASS_ELEMENT_HEIGHT;
      let v = this.graph.insertVertex(
        holderCell,
        element._id,
        element,
        0,
        yPosition,
        CLASS_VIEW_WIDTH,
        CLASS_ELEMENT_HEIGHT,
        style,
      );
      childCount++;
    }
  }


  /**
   * Draws a class view and its content
   *
   * A class view contains :
   * - a parent ('swimlane' cell) representing the class
   * - an attributes holder, which contains attribute cells
   * - an operations holder, which contains operation cells
   */
  drawClass(model: ClassModel) {

    let parent = this.graph.getDefaultParent();
    let classVertex = this.graph.insertVertex(
      parent,
      model._id,
      model,
      model.layout.x,
      model.layout.y,
      CLASS_VIEW_WIDTH,
      CLASS_VIEW_HEIGHT,
      "class"
    );

    let attrHolder = new ElementsHolder('attribute');

    let attrHolderCell = this.graph.insertVertex(
      classVertex,
      "99998", // ensure no conflict with model IDs
      attrHolder,
      0,
      CLASS_VIEW_HEIGHT,
      CLASS_VIEW_WIDTH,
      CLASS_ELEMENT_HEIGHT,
      attrHolder.getStyle()
    );

    this.drawClassChildren(model.attributes, attrHolderCell, 'attribute');


    let opeHolder = new ElementsHolder('operation');

    let opeHolderCell = this.graph.insertVertex(
      classVertex,
      "99999", // ensure no conflict with model IDs
      opeHolder,
      0,
      attrHolderCell.geometry.y + attrHolderCell.geometry.height,
      CLASS_VIEW_WIDTH,
      CLASS_ELEMENT_HEIGHT,
      opeHolder.getStyle()
    );


    this.drawClassChildren(model.operations, opeHolderCell, 'operation');

    // line below attribute holder
    this.drawBottomLine(attrHolderCell);

    this.resizeClass(classVertex);
    //this.drawPorts(classVertex);
  }

  /**
   * Draws connection ports around a cell,
   * enabling the cell to create associations
   */
  drawPorts(cell: mxCell) {


    let height = cell.geometry.height;
    let width = cell.geometry.width;

    let p_west = new ConnectionPort('west', cell.id, PORT_THICKNESS, height);
    let p_east = new ConnectionPort('east', cell.id, PORT_THICKNESS, height);
    let p_south = new ConnectionPort('south', cell.id, width, PORT_THICKNESS);
    let p_north = new ConnectionPort('north', cell.id, width, PORT_THICKNESS);


    let pw = this.graph.insertVertex(cell, null, p_west, 0, 0, p_west.width, p_west.height, 'port;', true);
    pw.geometry.offset = p_west.getOffset();

    let pe = this.graph.insertVertex(cell, null, p_east, 1, 0, p_east.width, p_east.height, 'port;', true);
    pe.geometry.offset = p_east.getOffset();

    let ps = this.graph.insertVertex(cell, null, p_south, 0, 1, p_south.width, p_south.height, 'port;', true);
    ps.geometry.offset = p_south.getOffset();

    let pn = this.graph.insertVertex(cell, null, p_north, 0, 0, p_south.width, p_south.height, 'port;', true);
    pn.geometry.offset = p_north.getOffset();
  }

  /**
   * Draws an enumeration and its literals
   */
  drawEnumeration(model: TypeModel) {
    let parent = this.graph.getDefaultParent();
    let vertex = this.graph.insertVertex(
      parent,
      model._id,
      model,
      model.layout.x,
      model.layout.y,
      CLASS_VIEW_WIDTH,
      CLASS_VIEW_HEIGHT,
      "enum"
    );

    let litHolder = new ElementsHolder('literal');

    let litHolderCell = this.graph.insertVertex(
      vertex,
      "100009",
      litHolder,
      0,
      CLASS_VIEW_HEIGHT,
      CLASS_VIEW_WIDTH,
      CLASS_ELEMENT_HEIGHT,
      litHolder.getStyle()

    );


    this.drawClassChildren(model.literals, litHolderCell, 'literal');

    this.resizeClass(vertex);

  }


  /**
   * Resizes cell and its children depending on the highest
   * width value of cells
   */
  resizeClass(cell: mxCell) {

    let prefWidth = CLASS_VIEW_WIDTH;
    let prefHeight = CLASS_VIEW_HEIGHT;

    let classPrefSize = this.graph.getPreferredSizeForCell(cell);

    if (classPrefSize.width > prefWidth) {
      prefWidth = classPrefSize.width;
    }

    if (cell.children) {
      cell.children.forEach(c => {
        if (c.children) {
          // preferred width is the highest width of children in holders
          c.children.forEach(cc => {
            let prefSize = this.graph.getPreferredSizeForCell(cc);
            if (prefSize.width > prefWidth) {
              prefWidth = prefSize.width;
            }
          });
        }
        // height is calculated with holders' height
        if (c.value instanceof ElementsHolder) {
          prefHeight += c.geometry.height;
        }
      });
    }

    cell.geometry.height = prefHeight;
    cell.geometry.width = prefWidth + CLASS_WIDTH_PADDING;

    if (cell.children) {
      cell.children.forEach(c => {
        c.geometry.width = prefWidth + CLASS_WIDTH_PADDING;
        if (c.children) {
          c.children.forEach(cc => {
            cc.geometry.width = prefWidth + CLASS_WIDTH_PADDING;
          });
        }
      });
    }
  }

  /**
   * Draws an association and its 2 ends
   * @param model : association model
   * @param reorganize : if true terminals are
   * reorganized for source and target cells
   */
  drawAssociation(model: AssociationModel, reorganize: boolean) {

    let source = this.graph.getModel().getCell(model.sourceId);
    let target = this.graph.getModel().getCell(model.targetId);

    let edgeStyle = this.getAssociationStyle(source, target, model);

    let parent = this.graph.getDefaultParent();
    let edge = this.graph.insertEdge(parent, model._id, model, source, target, edgeStyle);

    this.drawAssociationEnds(edge, model);
    if (reorganize) {
      this.reorganizeTerminalsForCell(source);
      this.reorganizeTerminalsForCell(target);
    }
  }


  /**
   * Re-draws an association contained in graph
   * Currently the association is removed and re-draw.
   * Could be optimized by refreshing association end cells and
   * updating style properties of the cell
   */
  updateAssociation(model: AssociationModel) {

    let cell = this.graph.model.getCell(model._id);

    this.graph.removeCells([cell]);
    this.drawAssociation(model, false);
  }

  /**
   * Removes a cell and its children from view
   */
  removeCell(model: ViewElement) {
    let cell = this.graph.model.getCell(model._id);
    if (cell) {
      if (cell.value instanceof AssociationModel) {
        let toReorganize = [cell.source, cell.target];
        this.graph.removeCells([cell], false);
        toReorganize.forEach(value => {
          this.reorganizeTerminalsForCell(value);
        });
      } else {
        this.graph.removeCells([cell], true);
      }

    }
  }

  /**
   * Re-draws an enumeration contained in graph
   * Currently the children of the cell are removed and re-drawn
   * and the view is refreshed
   * Could be optimized by re-drawing only new elements of the class
   */
  updateEnumeration(model: TypeModel) {
    let cell = this.graph.model.getCell(model._id);

    if (cell.children) {
      this.graph.removeCells(cell.children);
    }

    let litHolder = new ElementsHolder('literal');

    let litHolderCell = this.graph.insertVertex(
      cell,
      "100009",
      litHolder,
      0,
      CLASS_VIEW_HEIGHT,
      CLASS_VIEW_WIDTH,
      CLASS_ELEMENT_HEIGHT,
      litHolder.getStyle()

    );

    this.drawClassChildren(model.literals, litHolderCell, 'literal');
    this.graph.refresh(cell);
    this.resizeClass(cell);
  }

  /**
   * Re-draws a class contained in graph
   * Currently the children of the cell are removed and re-drawn.
   * Could be optimized by re-drawing only new elements of the class
   */
  updateClass(model: ClassModel) {

    let cell = this.graph.model.getCell(model._id);
    if (cell.children) {
      this.graph.removeCells(cell.children);
    }

    let attrHolder = new ElementsHolder('attribute');


    let attrHolderCell = this.graph.insertVertex(
      cell,
      "100005",
      attrHolder,
      0,
      CLASS_VIEW_HEIGHT,
      CLASS_VIEW_WIDTH,
      CLASS_ELEMENT_HEIGHT,
      attrHolder.getStyle()
    );

    this.drawClassChildren(model.attributes, attrHolderCell, 'attribute');

    let opeHolder = new ElementsHolder('operation');

    let opeHolderCell = this.graph.insertVertex(
      cell,
      "100006",
      opeHolder,
      0,
      attrHolderCell.geometry.y + attrHolderCell.geometry.height,
      CLASS_VIEW_WIDTH,
      CLASS_ELEMENT_HEIGHT,
      opeHolder.getStyle()
    );


    this.drawClassChildren(model.operations, opeHolderCell, 'operation');

    this.drawBottomLine(attrHolderCell);

    this.graph.refresh(cell);
    this.resizeClass(cell);
    //this.drawPorts(cell);


    // redraw inheritance associations
    if (cell.edges) {
      let toRemove = [];
      for (let edge of cell.edges) {
        if (edge.value instanceof ClassModel && edge.value._id === model._id)
          toRemove.push(edge);
      }
      this.graph.removeCells(toRemove, true);
    }
    this.drawInheritanceAssociation(model, true);

  }


  /**
   * Reorganises terminals for all cells which have edges
   * Terminals are spread along the side of cell, avoiding
   * them to overlap
   */
  reorganizeTerminals() {
    let root = this.graph.getDefaultParent();
    if (root.children) {
      for (let cell of root.children) {
        if (cell.isVertex()
          && cell.value instanceof ClassModel
          && cell.edges) {
          this.reorganizeTerminalsForCell(cell);
        }
      }
    }
  }

  /**
   * Reorganises terminals of given cell
   */
  reorganizeTerminalsForCell(cell: mxCell) {
    let arr = {
      N: [],
      S: [],
      E: [],
      W: []
    };
    this.graph.refresh(cell);
    for (let edge of cell.edges) {
      let isSource = edge.source.id === cell.id;
      let style = this.getDirectionFromStyle(edge.style);
      let direction = isSource ? style.source : style.target;
      let supertype = false;

      if (edge.value instanceof ClassModel) {
        if (edge.source.id === cell.id) {
          supertype = true;
        }
      }

      arr[direction].push({
        edge,
        supertype,
        source: isSource,
      });
    }
    for (let direction of Object.keys(arr)) {
      if (arr[direction].length > 0) {
        this.spreadTerminals(cell, direction, arr[direction]);
      }
    }
  }

  /**
   * Terminals are spread across the side of the cell using
   * style properties
   */
  private spreadTerminals(cell: mxCell, direction: string, initialEdges: any[]) {

    let edges = [];
    for (let e of initialEdges) {
      let currId = e.edge.value._id;
      let isIn = false;
      for (let fe of edges) {
        if (fe.edge.value._id === currId) {
          isIn = true;
          break;
        }
      }
      if (!isIn) {
        edges.push(e);
      }

    }


    let slots = 1;
    let supertypeFlag = false;
    for (let c of edges) {
      if (c.supertype === false) {
        slots++;
      } else {
        if (!supertypeFlag) {
          slots++;
          supertypeFlag = true;
        }
      }
    }

    edges.sort((a, b) => {
      //sort edges by coord X or Y depending on direction
      let cellA = a.edge.source.value._id === cell.value._id ? a.edge.target : a.edge.source;
      let cellB = b.edge.source.value._id === cell.value._id ? b.edge.target : b.edge.source;
      let axis = 'x';
      if (direction === 'W' || direction === 'E') axis = 'y';
      return cellA.geometry[axis] - cellB.geometry[axis];
    });

    let spacing = 1 / slots;
    let currentSpacing = spacing;

    let inheritPos;

    for (let e of edges) {
      let axis = 'X';
      if (direction === 'W' || direction === 'E') axis = 'Y';

      let finalPosition;

      if (e.supertype === true) {
        if (!inheritPos) {
          inheritPos = currentSpacing.toFixed(2);
        }
        finalPosition = inheritPos;
      } else {
        finalPosition = currentSpacing.toFixed(2);
      }

      let style = (e.source ? 'exit' : 'entry') + axis + '=' + finalPosition + ';';
      e.edge.style += style;
      this.graph.refresh(e.edge);

      currentSpacing += spacing;

    }
  }


  /**
   * Return formatted style for an association
   */
  getAssociationStyle(source: mxCell, target: mxCell, model: AssociationModel) {

    let directions = this.getConnectionDirections(source, target);
    let assoStyle = model.getAssociationStyle();

    let style = assoStyle.start + ';' + assoStyle.end + ';';
    style += 'exit-' + directions.source + ';';
    style += 'entry-' + directions.target + ';';
    return style;
  }

  /**
   * Adds role name & bounds for Association Ends
   */
  drawAssociationEnds(edge: mxCell, model: AssociationModel) {
    let data = model.getAssociationData();

    let styleClasses = this.getDirectionFromStyle(edge.style);

    let sourceStyle = 'terminal;terminal-' + styleClasses.source;
    let targetStyle = 'terminal;terminal-' + styleClasses.target;
    if (model.isNavigable(false)) {
      this.graph.insertVertex(edge, null, new AssociationEndElement(data.start.name, model),
        -1, 0, 0, 0, sourceStyle + ';terminal-above-line', true);
      this.graph.insertVertex(edge, null, new AssociationEndElement(data.start.bound, model),
        -1, 0, 0, 0, sourceStyle + ';terminal-below-line', true);
    }

    if (model.isNavigable(true)) {
      this.graph.insertVertex(edge, null, new AssociationEndElement(data.end.name, model),
        1, 0, 0, 0, targetStyle + ';terminal-above-line', true);
      this.graph.insertVertex(edge, null, new AssociationEndElement(data.end.bound, model),
        1, 0, 0, 0, targetStyle + ';terminal-below-line', true);
    }
  }


  /**
   * Draws inheritance associations between a class
   * and its supertypes
   * @param reorganize : if true, terminals are reorganized for
   * source and target cells
   */
  drawInheritanceAssociation(model: ClassModel, reorganize: boolean) {
    if (model.superTypes) {
      let parent = this.graph.getDefaultParent();
      model.superTypes.forEach(s => {
        let source = this.graph.getModel().getCell(s);
        let target = this.graph.getModel().getCell(model._id);

        let style = this.getInheritanceStyle(source, target);

        this.graph.insertEdge(parent, null, model, source, target, style);

        if (reorganize) {
          this.reorganizeTerminalsForCell(source);
          this.reorganizeTerminalsForCell(target);
        }
      });
    }
  }


  /**
   * Parse directions from style string
   * (in order to get directions of terminals without saving
   * temporary data, we parse it from the style of a cell
   */
  getDirectionFromStyle(style: string) {
    let res = {
      source: null, //exit
      target: null // entry
    };
    let values = style.split(';');
    for (let v of values) {
      if (v.includes('exit-')) {
        res.source = v.split('-')[1];
      }
      if (v.includes('entry-')) {
        res.target = v.split('-')[1];
      }
    }
    return res;
  }

  getInheritanceStyle(source: mxCell, target: mxCell) {
    let directions = this.getConnectionDirectionsForInheritance(source, target);

    let style = 'inheritance;';
    style += 'exit-' + directions.source + ';';
    style += 'entry-' + directions.target + ';';
    return style;
  }


  /**
   * Determines the source & target side on which association ends
   * have to connect
   * Available directions : N (north), S (south), W (west), E (east)
   */
  getConnectionDirections(source: mxCell, target: mxCell) {
    let sg = source.geometry;
    let tg = target.geometry;


    let dx = tg.x - sg.x;
    let dy = tg.y - sg.y;

    let theta = Math.atan2(dy, dx);
    theta *= 180 / Math.PI;
    theta += 90;
    if (theta < 0) theta += 360;
    let cardinal = this.getCardinality(theta);

    let sourceDirection;
    let targetDirection;

    switch (cardinal) {
      case 'N':
        sourceDirection = 'N';
        targetDirection = 'S';
        break;
      case 'NE':
        sourceDirection = 'E';
        targetDirection = 'S';
        break;
      case 'E':
        sourceDirection = 'E';
        targetDirection = 'W';
        break;
      case 'SE':
        sourceDirection = 'S';
        targetDirection = 'W';
        break;
      case 'S':
        sourceDirection = 'S';
        targetDirection = 'N';
        break;
      case 'SW':
        sourceDirection = 'W';
        targetDirection = 'N';
        break;
      case 'W':
        sourceDirection = 'W';
        targetDirection = 'E';
        break;
      case 'NW':
        sourceDirection = 'N';
        targetDirection = 'E';
        break;
    }

    return {
      source: sourceDirection,
      target: targetDirection,
    };
  }

  /**
   * Determines the source & target side on which association ends
   * have to connect for an inheritance association
   * Available directions : N (north), S (south)
   */
  getConnectionDirectionsForInheritance(source: mxCell, target: mxCell) {
    let sg = source.geometry;
    let tg = target.geometry;
    let dx = tg.x - sg.x;
    let dy = tg.y - sg.y;

    let sourceDirection;
    let targetDirection;

    if (sg.y >= tg.y) {
      sourceDirection = 'N';
      targetDirection = 'S';
    } else {
      sourceDirection = 'S';
      targetDirection = 'N';
    }

    return {
      source: sourceDirection,
      target: targetDirection,
    };
  }

  /**
   * Returns the cardinality of a given angle in degrees
   */
  private getCardinality(angle) {
    let directions = 8;
    let degree = 360 / directions;
    angle = angle + degree / 2;

    if (angle >= 0 && angle < degree)
      return "N";
    if (angle >= degree && angle < 2 * degree)
      return "NE";
    if (angle >= 2 * degree && angle < 3 * degree)
      return "E";
    if (angle >= 3 * degree && angle < 4 * degree)
      return "SE";
    if (angle >= 4 * degree && angle < 5 * degree)
      return "S";
    if (angle >= 5 * degree && angle < 6 * degree)
      return "SW";
    if (angle >= 6 * degree && angle < 7 * degree)
      return "W";
    if (angle >= 7 * degree && angle < 8 * degree)
      return "NW";
    //Should never happen:
    return "N";
  }


}

export default StructuralViewDrawer;


