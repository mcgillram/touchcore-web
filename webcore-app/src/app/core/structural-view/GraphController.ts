import GraphModel from "./GraphModel";
import GraphView from "./GraphView";
import {AttributeModel} from "../model/aspect/AttributeModel";


/**
 * Model-View-Controller Pattern
 *
 * This class controls the data flow into model
 * and updates the view on data changes
 */
class GraphController {

  model: GraphModel;
  view: GraphView;

  constructor(model, view) {
    this.model = model;
    this.view = view;
    this.initializeView();
  }

  /**
   * Draws the model in an empty view
   */
  initializeView() {
    this.view.drawAspect(this.model.aspect);
  }

  /**
   * Updates elements from the view
   */
  updateView(updates) {
    this.view.handleUpdates(updates);
  }

  /**
   * Updates the model and re-draw the concerned view elements
   */
  updateModel(updateStack) {
    let cellsToUpdate = this.model.updateModel(updateStack, this.view.graph.getModel());
    this.updateView(cellsToUpdate);
  }

  /**
   * Model and view are re-initialized
   */
  updateFullModel(model) {
    this.model.updateFullModel(model);
    this.initializeView();
  }

  print() {
    let preview = new mxPrintPreview(this.view.graph);
    preview.open();
  }
}

export default GraphController;
