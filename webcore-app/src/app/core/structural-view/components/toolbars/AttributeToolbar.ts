import ToolbarHandler from "../ToolbarHandler";
import {InteractionsHandler} from "../InteractionsHandler";

class AttributeToolbar extends ToolbarHandler {

  constructor(state: mxCellState, ih: InteractionsHandler) {
    super(state, ih);
  }

  defineProperties() {
    this.iconSize = 24;
    this.iconSpacing = 4;
    this.offsetX = this.state.width + 6;
    this.offsetY = -2;
  }

  defineButtons() {
    this.addButton('assets/icons/edit2.png', 'Edit', () => {
      this.ih.editAttribute(this.cell.getValue());
    });

    this.addButton('assets/icons/delete2.png', 'Delete', () => {
      this.ih.deleteAttribute(this.cell.getValue());
    });
  }

}

export default AttributeToolbar;
