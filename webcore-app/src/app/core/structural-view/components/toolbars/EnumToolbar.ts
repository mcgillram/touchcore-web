import ToolbarHandler from "../ToolbarHandler";
import {InteractionsHandler} from "../InteractionsHandler";

class EnumToolbar extends ToolbarHandler {

  constructor(state: mxCellState, ih: InteractionsHandler) {
    super(state, ih);
  }

  defineProperties() {
    this.iconSize = 26;
    this.iconSpacing = 4;
    this.offsetX = 0;
    this.offsetY = -31;
  }

  defineButtons() {
    let btn1 = this.addButton('assets/icons/add-literal2.png', 'Add literal', (ev) => {
      this.ih.addLiteral(this.cell.getValue(), ev.clientX, ev.clientY);
    });

    let btn3 = this.addButton('assets/icons/delete2.png', 'Delete', () => {
      this.ih.deleteEnum(this.cell.getValue());
    });

    let btn4 = this.addButton('assets/icons/edit2.png', 'Edit', () => {
      this.ih.editEnum(this.cell.getValue());
    });
  }

}

export default EnumToolbar;
