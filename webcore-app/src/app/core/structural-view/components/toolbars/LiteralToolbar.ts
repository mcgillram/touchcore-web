import ToolbarHandler from "../ToolbarHandler";
import {InteractionsHandler} from "../InteractionsHandler";

class LiteralToolbar extends ToolbarHandler {

  constructor(state: mxCellState, ih: InteractionsHandler) {
    super(state, ih);
  }

  defineProperties() {
    this.iconSize = 24;
    this.iconSpacing = 4;
    this.offsetX = this.state.width + 5;
    this.offsetY = -2;
  }

  defineButtons() {

    this.addButton('assets/icons/edit2.png', 'Edit', () => {
      this.ih.editLiteral(this.cell.getValue());
    });

    this.addButton('assets/icons/delete2.png', 'Delete', () => {
      this.ih.deleteLiteral(this.cell.getValue());
    });

  }

}

export default LiteralToolbar;
