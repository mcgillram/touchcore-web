import ToolbarHandler from "../ToolbarHandler";
import {InteractionsHandler} from "../InteractionsHandler";
import GraphModel from "../../GraphModel";

class AssociationToolbar extends ToolbarHandler {

  constructor(state: mxCellState, ih: InteractionsHandler) {
    super(state, ih);
  }

  defineProperties() {
    this.iconSize = 24;
    this.iconSpacing = 4;
    this.offsetX = this.state.width + 6;
    this.offsetY = -2;
  }

  defineButtons() {
    let source = this.graph.model.getCell(this.state.cell.getValue().sourceId);
    let target = this.graph.model.getCell(this.state.cell.getValue().targetId);

    this.addButton('assets/icons/edit2.png', 'Edit', () => {
      this.ih.editAssociation(this.state.cell.getValue(), target.getValue(), source.getValue());
    });

    this.addButton('assets/icons/delete2.png', 'Delete', () => {
      this.ih.deleteAssociation(this.state.cell.getValue());
    });
  }

  getDisplayPointX(): any {
    let absPoints = this.state.absolutePoints;
    return absPoints[1].x;
  }

  getDisplayPointY(): any {
    let absPoints = this.state.absolutePoints;
    return absPoints[1].y;
  }

}

export default AssociationToolbar;
