import ToolbarHandler from "../ToolbarHandler";
import {InteractionsHandler} from "../InteractionsHandler";

class ClassToolbar extends ToolbarHandler {

  constructor(state: mxCellState, ih: InteractionsHandler) {
    super(state, ih);
  }

  defineProperties() {
    this.iconSize = 26;
    this.iconSpacing = 4;
    this.offsetX = 0;
    this.offsetY = -31;
  }

  getPortStyle(direction) {
    let res = {
      left: 0,
      top: 0,
      width: 14
    };
    switch (direction) {
      case 'south':
        res.width = 42;
        res.left = this.state.width / 2 - 21;
        res.top = this.state.height + 40;
        break;
      case 'east':
        res.left = this.state.width + 8;
        res.top = this.state.height / 2 + 15;
        break;
      case 'west':
        res.left = -22;
        res.top = this.state.height / 2 + 15;
        break;
    }
    return res;
  }

  createPort(direction) {

    let img = this.createButtonNode('assets/icons/chevron_' + direction + '.png');
    let style = this.getPortStyle(direction);
    img.style.cursor = 'grab';
    img.style.position = 'absolute';
    img.style.left = style.left + 'px';
    img.style.top = style.top + 'px';
    img.style.width = style.width + 'px';
    img.style.opacity = 0.6;

    mxEvent.addGestureListeners(img,
      mxUtils.bind(this, (evt) => {
        this.graph.popupMenuHandler.hideMenu();
        this.graph.stopEditing(false);

        let pt = mxUtils.convertPoint(this.graph.container,
          mxEvent.getClientX(evt), mxEvent.getClientY(evt));
        this.graph.connectionHandler.start(this.state, pt.x, pt.y);
        this.graph.isMouseDown = true;
        mxEvent.consume(evt);
      })
    );

    mxEvent.addListener(img, 'mouseover', () => {
      img.style.opacity = 0.9;
    });
    mxEvent.addListener(img, 'mouseleave', () => {
      img.style.opacity = 0.6;
    });

    this.domNode.appendChild(img);

  }

  defineButtons() {
    let btn1 = this.addButton('assets/icons/add-attribute2.png', 'Add attribute', (ev) => {
      this.ih.addAttribute(this.cell.getValue(), ev.clientX, ev.clientY);
    });

    let btn2 = this.addButton('assets/icons/add-operation2.png', 'Add operation', (ev) => {
      this.ih.addOperation(this.cell.getValue(), ev.clientX, ev.clientY);
    });

    let btn3 = this.addButton('assets/icons/delete2.png', 'Delete class', (ev) => {
      this.ih.deleteClass(this.cell.getValue());
    });

    let btn4 = this.addButton('assets/icons/edit2.png', 'Edit class', (ev) => {
      this.ih.editClass(this.cell.getValue());
    });

    this.createPort('west');
    this.createPort('east');
    this.createPort('south');


  }

}

export default ClassToolbar;
