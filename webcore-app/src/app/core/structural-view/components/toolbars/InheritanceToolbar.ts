import ToolbarHandler from "../ToolbarHandler";
import {InteractionsHandler} from "../InteractionsHandler";
import GraphModel from "../../GraphModel";

class InheritanceToolbar extends ToolbarHandler {

  constructor(state: mxCellState, ih: InteractionsHandler) {
    super(state, ih);
  }

  defineProperties() {
    this.iconSize = 24;
    this.iconSpacing = 4;
    this.offsetX = this.state.width + 6;
    this.offsetY = -2;
  }

  defineButtons() {

    this.addButton('assets/icons/delete2.png', 'Delete', () => {
      this.ih.deleteSupertypeAssociation(this.state.cell.getValue(), this.state.cell.source.id);
    });

  }

  getDisplayPointX(): any {
    let absPoints = this.state.absolutePoints;
    return absPoints[1].x;
  }

  getDisplayPointY(): any {
    let absPoints = this.state.absolutePoints;
    return absPoints[1].y;
  }

}

export default InheritanceToolbar;
