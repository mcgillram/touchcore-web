import {InteractionsHandler} from "./InteractionsHandler";
import {ConnectionPort} from "../../model/ConnectionPort";

class ToolbarHandler extends mxVertexHandler {

  domNode;
  cell;
  ih: InteractionsHandler;

  iconSize;
  iconSpacing;
  offsetX;
  offsetY;

  constructor(state: mxCellState, ih: InteractionsHandler) {
    super(state);
    this.ih = ih;
    this.cell = state.cell;
  }

  addButton(imageSrc, tooltip, call) {
    let img = this.createButtonNode(imageSrc);
    img.setAttribute('title', tooltip);
    img.style.cursor = 'pointer';
    img.style.width = this.iconSize + 'px';
    img.style.marginRight = this.iconSpacing + 'px';

    mxEvent.addListener(img, 'click',
      mxUtils.bind(this, (evt) => {
        call(evt);
        mxEvent.consume(evt);
      })
    );



    this.domNode.appendChild(img);
    return img;
  }


  createButtonNode(src) {
    let imgNode = null;
    imgNode = document.createElement('img');
    imgNode.setAttribute('src', src);
    return imgNode;
  }

  /**
   * Method 1/2 to override for subclasses
   */
  defineProperties() {
    this.iconSize = 30;
    this.iconSpacing = 4;
    this.offsetX = 0;
    this.offsetY = -35;
  }

  /**
   * Method 2/2 to override for subclasses
   */
  defineButtons() {

  }

  getDisplayPointX() {
    return this.state.x + this.offsetX;
  }

  getDisplayPointY() {
    return this.state.y + this.offsetY;
  }

  init() {
    super.init();

    this.domNode = document.createElement('div');
    this.domNode.style.position = 'absolute';
    this.domNode.style.whiteSpace = 'nowrap';

    this.defineProperties();
    this.defineButtons();

    this.graph.container.appendChild(this.domNode);
    this.redrawTools();
  }

  redraw() {
    super.redraw();
    this.redrawTools();
    //this.showPorts();
  }

  showPorts() {
    if (this.state.cell.children) {
      for (let cell of this.state.cell.children) {
        if (cell.value instanceof ConnectionPort) {
          // north port is hidden because buttons are in foreground
          if (cell.value.orientation !== 'north') {
            cell.style = "port-visible;";
            this.graph.refresh(cell);

          }
        }
      }
    }
  }

  hidePorts() {
    if (this.state.cell.children) {
      for (let cell of this.state.cell.children) {
        if (cell.value instanceof ConnectionPort) {
          cell.style = "port";
          this.graph.refresh(cell);
        }
      }
    }
  }

  redrawTools() {
    if (this.state != null && this.domNode != null) {
      this.domNode.style.left = this.getDisplayPointX() + 'px';
      this.domNode.style.top = this.getDisplayPointY() + 'px';
    }
  }

  destroy() {
    super.destroy();
    if (this.domNode != null) {
      this.domNode.parentNode.removeChild(this.domNode);
      this.domNode = null;
    }
    //this.hidePorts();
  }
}

export default ToolbarHandler;
