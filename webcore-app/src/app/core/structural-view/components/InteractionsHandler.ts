import {UserInteractionsService} from "../../../modules/shared/services/user-interactions.service";
import {CoreAPIService} from "../../../modules/shared/services/core-api.service";
import {ClassModel} from "../../model/aspect/ClassModel";
import {AttributeModel} from "../../model/aspect/AttributeModel";
import {OperationModel} from "../../model/aspect/OperationModel";
import {TypeModel} from "../../model/aspect/TypeModel";
import {LiteralModel} from "../../model/aspect/LiteralModel";
import {AssociationModel} from "../../model/aspect/AssociationModel";
import {InlineTypingHandler} from "./InlineTypingHandler";
import {ViewElement} from "../../model/ViewElement";


/**
 * This class decides which interface to show
 * for a specific interaction
 */
export class InteractionsHandler {

  expertMode: boolean;
  graph: mxGraph;
  uiService: UserInteractionsService;
  apiService: CoreAPIService;
  inlineTyper: InlineTypingHandler;

  defOffsetX = -120;
  defOffsetY = -5;


  constructor(graph, uiService: UserInteractionsService) {
    this.expertMode = false;
    this.graph = graph;
    this.uiService = uiService;
    this.apiService = uiService.coreAPI;
    this.inlineTyper = new InlineTypingHandler(graph);

  }

  switchExpertMode() {
    this.expertMode = !this.expertMode;
  }

  addClass(x, y) {
    if (this.expertMode) {
      this.inlineTyper.setContent(240,
        'Create new class :',
        null,
        'Class name',
        null,
        (value) => {
          value = value[0].toUpperCase() + value.substr(1);
          this.inlineTyper.clearContent();
          this.apiService.send("/api/createNewClass", {
            className: value,
            xPosition: x,
            yPosition: y,
            dataType: false
          });
        });
      this.inlineTyper.show(x + this.inlineTyper.width / 2 - 30, y + 15);
    } else {
      this.uiService.addClass(x, y);
    }
  }

  addEnum(x, y) {
    if (this.expertMode) {
      this.inlineTyper.setContent(240, 'Create new enumeration :', null,
        'Enum name',
        null,
        (value) => {
          this.inlineTyper.clearContent();
          this.apiService.send("/api/createEnum", {
            enumName: value,
            xPosition: x,
            yPosition: y,
          });
        });
      this.inlineTyper.show(x + this.inlineTyper.width / 2 - 30, y + 15);
    } else {
      this.uiService.addEnum(x, y);
    }
  }

  addDatatype(x, y) {
    if (this.expertMode) {
      this.inlineTyper.setContent(240, 'Create new datatype :', null,
        'Datatype name',
        null,
        (value) => {
          this.inlineTyper.clearContent();
          this.apiService.send("/api/createNewClass", {
            className: value,
            xPosition: x,
            yPosition: y,
            dataType: true
          });
        });
      this.inlineTyper.show(x + this.inlineTyper.width / 2 - 30, y + 15);
    } else {
      this.uiService.addDataType(x, y);
    }
  }

  addAttribute(obj: ClassModel, x?, y?) {
    if (this.expertMode) {
      this.inlineTyper.setContent(240, 'Create new attribute :',
        null,
        '',
        '<type> <attribute name>',
        (value) => {
          this.inlineTyper.clearContent();
          this.apiService.send('/api/createAttribute', {
            classId: obj._id,
            rankIndex: 0,
            attributeString: value
          });
        });
      this.inlineTyper.show(x + this.defOffsetX, y + this.defOffsetY);
    } else {
      this.uiService.addAttribute(obj);
    }

  }

  addOperation(obj: ClassModel, x?, y?) {
    if (this.expertMode) {
      this.inlineTyper.setContent(350, 'Create new operation :',
        null,
        '',
        '<visibility> <return type> <name> ([<params>])',
        (value) => {
          this.inlineTyper.clearContent();
          this.apiService.send('/api/createOperation', {
            classId: obj._id,
            rankIndex: 0,
            operationString: value
          });
        });
      this.inlineTyper.show(x + this.defOffsetX, y + this.defOffsetY);
    } else {
      this.uiService.addOperation(obj);
    }

  }

  addLiteral(obj: TypeModel, x?, y?) {
    if (this.expertMode) {
      this.inlineTyper.setContent(240, 'Create new literal :',
        null,
        '',
        '<literal name>',
        (value) => {
          this.inlineTyper.clearContent();
          this.apiService.send('/api/addLiteral', {
            enumId: obj._id,
            rankIndex: 0,
            literalName: value
          });
        });
      this.inlineTyper.show(x + (this.defOffsetX + 60), y + (this.defOffsetY - 5));
    } else {
      this.uiService.addLiteral(obj);
    }
  }


  editClass(obj: ClassModel) {
    this.uiService.editClass(obj);

  }

  editAttribute(obj: AttributeModel) {
    this.uiService.editAttribute(obj);

  }

  editOperation(obj: OperationModel) {
    this.uiService.editOperation(obj);

  }

  editEnum(obj: TypeModel) {
    this.uiService.editEnum(obj);

  }

  editLiteral(obj: LiteralModel) {
    this.uiService.editLiteral(obj);

  }

  editAssociation(obj: AssociationModel, source: ClassModel, target: ClassModel) {

    this.uiService.editAssociation(obj, source, target);

  }

  renameEObject(obj: ViewElement, type, x?, y?) {
    if (this.expertMode) {
      this.inlineTyper.setContent(240, 'Rename ' + type + ' :',
        obj.name,
        null,
        null,
        (value) => {
          this.inlineTyper.clearContent();

          if (type === 'class')
            value = value[0].toUpperCase() + value.substr(1);

          this.apiService.send('/api/setEObjectName', {
            eObjectId: obj._id,
            objectName: value
          });
        });
      this.inlineTyper.show(x + this.defOffsetX, y + (this.defOffsetY - 15));
    } else {
      switch (type) {
        case 'class':
          this.editClass(obj as ClassModel);
          break;
        case 'attribute':
          this.editAttribute(obj as AttributeModel);
          break;
        case 'operation':
          this.editOperation(obj as OperationModel);
          break;
        case 'enumeration':
          this.editEnum(obj as TypeModel);
          break;
        case 'literal':
          this.editLiteral(obj as LiteralModel);
          break;
      }
    }
  }


  deleteClass(obj: ClassModel) {
    if (this.expertMode) {
      this.apiService.send('/api/removeClassifier', {
        classId: obj._id
      });
    } else {
      this.uiService.deleteClass(obj);
    }

  }

  deleteAttribute(obj: AttributeModel) {
    if (this.expertMode) {
      this.apiService.send('/api/removeAttribute', {
        attributeId: obj._id
      });
    } else {
      this.uiService.deleteAttribute(obj);
    }

  }

  deleteOperation(obj: OperationModel) {
    if (this.expertMode) {
      this.apiService.send('/api/removeOperation', {
        operationId: obj._id
      });
    } else {
      this.uiService.deleteOperation(obj);
    }
  }

  deleteEnum(obj: TypeModel) {
    if (this.expertMode) {
      this.apiService.send('/api/removeClassifier', {
        classId: obj._id
      });
    } else {
      this.uiService.deleteEnum(obj);
    }
  }

  deleteLiteral(obj: LiteralModel) {
    if (this.expertMode) {
      this.apiService.send('/api/removeLiteral', {
        literalId: obj._id
      });
    } else {
      this.uiService.deleteLiteral(obj);
    }

  }

  deleteAssociation(obj: AssociationModel) {
    if (this.expertMode) {
      this.apiService.send('/api/deleteAssociation', {
        associationId: obj._id
      });
    } else {
      this.uiService.deleteAssociation(obj);
    }

  }

  deleteSupertypeAssociation(obj: ClassModel, superTypeId) {

    if (this.expertMode) {
      this.apiService.send('/api/removeSuperType', {
        classId1: obj._id,
        classId2: superTypeId
      });
    } else {
      this.uiService.deleteSupertypeAssociation(obj, superTypeId);
    }
  }


}
