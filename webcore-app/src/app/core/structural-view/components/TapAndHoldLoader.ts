export class TapAndHoldLoader {

  domNode;
  graph: mxGraph;
  isVisible = false;
  loaderTimeout;
  imageUrl = '/assets/spinner/spinner2-s4.gif';

  size = 30;

  constructor(graph) {
    this.graph = graph;
    this.init();
  }


  init() {
    this.domNode = document.createElement('img');
    this.domNode.setAttribute('src', this.imageUrl);
    this.domNode.style.position = 'absolute';
    this.domNode.style.whiteSpace = 'nowrap';
    this.domNode.style.width = this.size + 'px';
  }


  show(x, y) {
    this.domNode.style.left = (x - this.size / 2) + 'px';
    this.domNode.style.top = (y - this.size / 2) + 'px';
    this.resetSpinner();
    this.isVisible = true;

    this.domNode.style.visibility = 'hidden';

    this.graph.container.appendChild(this.domNode);

    this.loaderTimeout = setTimeout(() => {
      this.domNode.style.visibility = 'visible';
    }, 200);

  }

  resetSpinner(){
    this.domNode.src = "";
    this.domNode.src = this.imageUrl;
  }

  hide() {
    if (this.isVisible) {
      this.isVisible = false;
      this.domNode.parentNode.removeChild(this.domNode);
      if (this.loaderTimeout) clearTimeout(this.loaderTimeout);
    }
  }

}
