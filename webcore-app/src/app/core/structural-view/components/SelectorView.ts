/**
 * This class allows to define a list of selectable options and associate
 * specific behaviour to click events.
 */
export class SelectorView {

  domNode;
  graph: mxGraph;
  isVisible = false;
  items;
  buttons = [];

  constructor(graph) {
    this.graph = graph;
    this.init();
  }

  /**
   * Adds an option to the view
   * and defines a behaviour for click events
   */
  addMenuOption(value, clickHandler) {
    let elem = document.createElement('button');
    elem.innerHTML = value;
    elem.style.minWidth = '120px';
    elem.style.backgroundColor = '#fff';
    elem.style.border = 'unset';
    elem.style.fontSize = '12px';
    elem.style.display = 'block';
    elem.style.textAlign = 'left';
    elem.style.padding = '3px';
    elem.style.paddingLeft = '10px';
    elem.style.outline = 'none';
    elem.style.borderBottom = '1px solid #dedede';

    if (clickHandler !== null) {
      elem.onclick = (ev) => {
        clickHandler(ev);
        this.hide();
      };
    }

    elem.onmouseenter = (ev) => {
      elem.style.backgroundColor = '#eeeeee';
    };

    elem.onmouseleave = (ev) => {
      elem.style.backgroundColor = '#fff';
    };

    this.buttons.push(elem);
    this.items.appendChild(elem);

  }

  /**
   * Adds a cancel option at the end
   * (only necessary if external events can't hide the view)
   */
  addCancelOption() {
    let elem = document.createElement('button');
    elem.innerHTML = 'Cancel';
    elem.style.color = '#F44336';
    elem.style.minWidth = '120px';
    elem.style.backgroundColor = '#fff';
    elem.style.border = 'unset';
    elem.style.fontSize = '12px';
    elem.style.display = 'block';
    elem.style.textAlign = 'left';
    elem.style.padding = '3px';
    elem.style.paddingLeft = '10px';
    elem.style.outline = 'none';

    elem.onclick = (ev) => {
      this.hide();
    };

    elem.onmouseenter = (ev) => {
      elem.style.backgroundColor = '#e9e9e9';
    };

    elem.onmouseleave = (ev) => {
      elem.style.backgroundColor = '#fff';
    };

    this.items.appendChild(elem);
  }


  /**
   * Resets options of current view
   */
  resetView() {
    //reset background if no `onmouseleave`
    for (let button of this.buttons) {
      button.style.backgroundColor = '#fff';
    }
  }

  /**
   * Initialize parent node which will hold options
   */
  init() {
    this.domNode = document.createElement('div');
    this.domNode.style.position = 'absolute';
    this.domNode.style.whiteSpace = 'nowrap';
    this.domNode.style.border = '1px solid #dedede';
    this.domNode.style.boxShadow = '5px 6px 7px -3px rgba(156,156,156,0.99)';

    this.items = document.createElement('div');

    this.domNode.appendChild(this.items);

  }


  /**
   * Shows view on given coords
   */
  show(x, y) {
    this.domNode.style.left = x + 'px';
    this.domNode.style.top = y + 'px';
    this.isVisible = true;
    this.graph.container.appendChild(this.domNode);
  }

  /**
   * Hides view
   */
  hide() {
    this.isVisible = false;
    this.domNode.parentNode.removeChild(this.domNode);
    this.resetView();
  }

}
