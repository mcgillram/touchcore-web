import {CoreAPIService} from "../../../modules/shared/services/core-api.service";
import {Subscription} from "rxjs";
import {ViewElement} from "../../model/ViewElement";

export class CollaborationHandler {

  graph: mxGraph;
  coreAPI: CoreAPIService;
  collabSubscription: Subscription;
  selectedClasses: {};
  sessionData;
  usersData: {};
  initialized: boolean;

  constructor(graph, coreAPI) {
    this.graph = graph;
    this.coreAPI = coreAPI;

    this.usersData = {};
    this.selectedClasses = {};
    this.initialized = false;

    this.sessionData = this.coreAPI.getSessionData();
    this.initListener();

    this.collabSubscription = this.coreAPI.collabSubject.subscribe(value => {


      /*
      let concernedUsers = Object.keys(value['users']).filter(a =>
        value['users'][a].aspectName === this.coreAPI.currentModel && a !== this.sessionData.sessionId
      );

      concernedUsers.forEach(c => {
        this.usersData[c] = value['users'][c];
      });

      console.log(Object.keys(this.usersData).length + ' users in ' + this.coreAPI.currentModel)

       */

      this.usersData = value['users'];

      if (this.initialized) {
        this.updateHighlight();
      } else {
        this.initHighlight();
        this.initialized = true;
      }
    });
    // get up to date with other users when first connecting
    this.coreAPI.send('/api/highlight', {selected: [], unselected: []});
  }

  private initListener() {

    this.graph.getSelectionModel().addListener(mxEvent.CHANGE, (sender, evt) => {
      let unselectedCells = evt["properties"]["added"];
      let selectedCells = evt["properties"]["removed"];
      let result = {selected: [], unselected: []};

      if (unselectedCells) {
        for (let cell of unselectedCells) {
          if (cell.value instanceof ViewElement && cell.isVertex()) {
            result.unselected.push(cell.value._id);
          }
        }
      }

      if (selectedCells) {
        for (let cell of selectedCells) {
          if (cell.value instanceof ViewElement && cell.isVertex()) {
            result.selected.push(cell.value._id);
          }
        }
      }

      this.coreAPI.send('/api/highlight', result);
    });
  }

  initHighlight() {
    // tslint:disable-next-line:forin
    for (let userIndex in this.usersData) {
      let userColor = this.usersData[userIndex]['color'];
      this.usersData[userIndex]['selectedClasses'].forEach(classId => {
        this.highlight(classId, userColor);
      });
    }
  }

  updateHighlight() {
    // remove highlights
    // for every selected classes, check if the user still highlights it
    // if the user does not select it anymore,
    // delete the class property and unhighlight the class (only one user could have it)
    // tslint:disable-next-line: forin
    for (let classId in this.selectedClasses) {
      this.selectedClasses[classId].forEach(userId => {
        // If user is not connected anymore, unhighlight the class
        if (!this.usersData.hasOwnProperty(userId) ||
          !this.usersData[userId]['selectedClasses'].includes(classId)) {
          delete this.selectedClasses[classId];
          this.unhighlight(classId);
        }
      });
    }
    // add highlight
    // loop over every class selected by every user
    // tslint:disable-next-line: forin
    for (let userIndex in this.usersData) {
      let userSession = this.usersData[userIndex]['sessionId'];
      let userColor = this.usersData[userIndex]['color'];
      if (this.usersData[userIndex]['aspectName'] === this.coreAPI.currentModel) {
        this.usersData[userIndex]['selectedClasses'].forEach(classId => {
          // if the class has is not mapped to a user yet, add the user and highlight the class
          if (!this.selectedClasses.hasOwnProperty(classId) || this.selectedClasses[classId].length === 0) {
            this.selectedClasses[classId] = [userSession];
            if (this.sessionData["sessionId"] !== userSession) {
              this.highlight(classId, userColor);
            }
          }
        });
      }


    }
  }

  highlight(id, color) {
    if (id === null) return;

    let cell = this.graph.model.getCell(id);
    //console.log('show', cell.value.name);
    if (cell) {
      let style = cell.getStyle();
      style = style + ';highlight;strokeColor=' + color;
      this.graph.getModel().setStyle(cell, style);
    }
  }

  unhighlight(id) {
    if (id === null) return;

    let cell = this.graph.model.getCell(id);
    //console.log('hide', cell.value.name);
    if (cell) {
      let style = cell.getStyle();
      style = style.split(';highlight')[0];
      this.graph.getModel().setStyle(cell, style);
    }
  }
}
