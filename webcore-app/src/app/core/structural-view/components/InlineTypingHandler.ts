export class InlineTypingHandler {

  domNode;
  inputField;
  descriptionNode;
  tooltipNode;
  graph: mxGraph;
  isVisible = false;
  defaultWidth = 230;
  actionButtonsWidth = 76;
  width;
  validateAction;
  descriptionText = 'Enter input value :';
  placeholder = 'default value';
  tooltip = 'write ur stuff';


  constructor(graph) {
    this.graph = graph;
    this.width = this.defaultWidth;
    this.init();
  }

  setContent(width, description, value, placeholder, tooltip, action) {
    this.width = width;
    this.domNode.style.width = this.width + 'px';
    this.inputField.style.width = (this.width - this.actionButtonsWidth) + 'px';
    this.placeholder = placeholder ? placeholder : '';
    this.inputField.value = value ? value : '';
    this.descriptionText = description;
    this.tooltip = tooltip;
    this.validateAction = action;
  }


  setTooltip(value) {

    if (value === null) {
      this.tooltipNode.style.display = 'none';
    } else {
      this.tooltipNode.style.display = 'inline-block';
      this.tooltipNode.innerText = value;

    }
  }


  init() {
    this.domNode = document.createElement('div');
    this.domNode.style.position = 'absolute';
    this.domNode.style.whiteSpace = 'nowrap';
    this.domNode.style.width = this.width;
    this.domNode.style.height = 'fit-content';
    this.domNode.style.fontSize = '15px';
    this.domNode.style.padding = '12px';
    this.domNode.style.backgroundColor = '#fff';
    this.domNode.style.border = '3px solid';
    this.domNode.style.borderColor = 'rgba(27,101,208,0.7)';
    this.domNode.style.borderRadius = '4px';
    this.domNode.classList.add('mat-elevation-z3');
    this.domNode.classList.add('scale-in-center');

    this.descriptionNode = document.createElement('p');
    this.descriptionNode.innerText = this.descriptionText;
    this.descriptionNode.style.marginBottom = '8px';
    this.descriptionNode.style.textAlign = 'center';
    this.domNode.appendChild(this.descriptionNode);


    this.tooltipNode = document.createElement('p');
    this.tooltipNode.innerText = this.descriptionText;
    this.tooltipNode.style.marginBottom = '8px';
    this.tooltipNode.style.color = '#8a8a8a';
    this.tooltipNode.style.textAlign = 'center';
    this.tooltipNode.style.width = '100%';
    this.tooltipNode.style.fontSize = '11px';

    this.tooltipNode.style.textAlign = 'center';
    this.domNode.appendChild(this.tooltipNode);


    let inputHolder = document.createElement('div');
    inputHolder.style.margin = '0 auto';

    this.inputField = document.createElement('input');
    this.inputField.placeholder = this.placeholder;
    this.inputField.setAttribute("type", "text");
    this.inputField.style.padding = '2px';
    this.inputField.style.width = (this.width - this.actionButtonsWidth) + 'px';
    this.inputField.style.paddingLeft = '6px';
    this.inputField.style.border = '1px solid #d1d1d1';
    this.inputField.style.outline = 'unset';
    this.inputField.classList.add('cstm-input');
    this.inputField.onkeypress = ev => {
      if (ev.key === 'Enter') this.validate();
    };


    let validateBtn = document.createElement('button');
    validateBtn.innerText = '✔';
    validateBtn.classList.add('sm-btn');
    validateBtn.style.outline = 'none';
    validateBtn.style.color = '#55bf25';
    validateBtn.onclick = () => {
      this.validate();

    };
    let cancelBtn = document.createElement('button');
    cancelBtn.innerText = '✘';
    cancelBtn.classList.add('sm-btn');
    cancelBtn.style.outline = 'none';
    cancelBtn.style.color = '#f44336';

    cancelBtn.onclick = () => {
      this.hide();
    };

    inputHolder.appendChild(this.inputField);
    inputHolder.appendChild(validateBtn);
    inputHolder.appendChild(cancelBtn);
    this.domNode.appendChild(inputHolder);

  }

  validate() {
    if (this.inputField.value !== '') {
      this.inputField.style.border = '1px solid #d1d1d1';

      this.hide();
      if (this.validateAction) {
        this.validateAction(this.inputField.value);
      }
    } else {
      this.inputField.style.border = '1px solid #f44336';
      this.inputField.focus();
    }
  }

  show(x, y) {
    this.domNode.style.left = (x - this.width / 2) + 'px';
    this.domNode.style.top = (y - 70) + 'px';

    this.setVisible(true);

    this.inputField.placeholder = this.placeholder;
    this.descriptionNode.innerText = this.descriptionText;
    this.setTooltip(this.tooltip);
    this.inputField.style.border = '1px solid #d1d1d1';

    this.graph.container.appendChild(this.domNode);
    this.inputField.select();
    this.inputField.focus();
  }

  clearContent() {
    this.validateAction = null;
    this.descriptionText = 'undefined';
    this.placeholder = 'undefined';
    this.inputField.value = '';
    this.domNode.style.width = this.defaultWidth + 'px';
  }

  hide() {
    if (this.isVisible) {
      this.domNode.parentNode.removeChild(this.domNode);
      this.setVisible(false);
    }
  }

  setVisible(value) {
    this.isVisible = value;
  }

}
