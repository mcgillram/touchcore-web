/**
 * This class is responsible of the configuration of styles
 * used by a graph view.
 * Help for understanding mxGraph's styles : https://about.draw.io/shape-styles/
 */

class GraphStyleManager {

  graph: mxGraph;

  constructor(graph) {
    this.graph = graph;
    this.configureConstants();
    this.configureDefaultStyles();
    this.configureCustomStyles();
  }

  registerStyle(selector: string, style: {}) {
    this.graph.getStylesheet().putCellStyle(selector, style);
  }

  configureConstants() {
    //mxConstants.VERTEX_SELECTION_COLOR = '#23b7d0';
    mxConstants.VERTEX_SELECTION_COLOR = 'none';
    mxConstants.VERTEX_SELECTION_STROKEWIDTH = 0;
    mxConstants.DEFAULT_STARTSIZE = 34;

    mxConstants.MIN_HOTSPOT_SIZE = 60;
    mxConstants.DEFAULT_HOTSPOT = 0.8;

    //guidelines
    mxGraphHandler.prototype.guidesEnabled = true;
    mxConstants.GUIDE_COLOR = '#48a3d0';
    mxConstants.GUIDE_STROKEWIDTH = 2;

    mxConstants.HANDLE_SIZE = 16;
    mxConstants.LABEL_HANDLE_SIZE = 7;
    mxEdgeHandler.prototype.tolerance = 10;

    mxConstants.VALID_COLOR = 'rgba(7,255,5,0.7)';
    mxConstants.INVALID_COLOR = 'rgba(7,255,5,0.7)';



    // Larger tolerance and grid for real touch devices
    if (mxClient.IS_TOUCH || navigator.maxTouchPoints > 0 || navigator.msMaxTouchPoints > 0) {
      mxShape.prototype.svgStrokeTolerance = 18;
      mxVertexHandler.prototype.tolerance = 20;
      mxEdgeHandler.prototype.tolerance = 12;
      mxGraph.prototype.tolerance = 12;
    }


  }

  configureDefaultStyles() {

    let vertexStyle = this.graph.getStylesheet().getDefaultVertexStyle();
    vertexStyle.fontFamily = 'Helvetica'; // 'Tahoma', 'Verdana', 'Helvetica', 'Comic Sans MS'
    vertexStyle.strokeColor = "#000"; //"#134791";
    vertexStyle.strokeWidth = 1.4;
    vertexStyle.resizable = 0;
    vertexStyle.editable = 1;
    vertexStyle.rotable = 0;
    vertexStyle.verticalAlign = "middle";

    this.graph.getStylesheet().putDefaultVertexStyle(vertexStyle);

    let edgeStyle = this.graph.getStylesheet().getDefaultEdgeStyle();
    edgeStyle.noLabel = 1;
    edgeStyle.sourcePerimeterSpacing = 10;
    edgeStyle.targetPerimeterSpacing = 10;
    edgeStyle.edgeStyle = "orthogonalEdgeStyle";
    edgeStyle.endSize = 8;
    edgeStyle.startSize = 8;
    edgeStyle.strokeWidth = 2;
    edgeStyle.fillColor = '#FFF';
    //edgeStyle.strokeColor = "#3079d0";
    edgeStyle.strokeColor = "rgb(89,87,87)";

    this.graph.getStylesheet().putDefaultEdgeStyle(edgeStyle);
  }

  configureCustomStyles() {

    // Class view styles

    this.registerStyle('class', {
      fontSize: 13,
      fontColor: '#000',
      fillColor: '#fff',
      swimlaneFillColor: '#fff',
      rounded: 1,
      arcSize: 2,
      autosize: 1,
      fontStyle: 1, // bold
      spacingBottom: 2,
      shape: mxConstants.SHAPE_SWIMLANE,
    });

    this.registerStyle('attribute', {
      fontColor: '#000',
      fillColor: 'none',
      //fillColor: '#c6e3ff',
      rounded: 0,
      align: 'left',
      spacingLeft: 10,
      strokeColor: "rgba(0,0,0,0)"
    });

    this.registerStyle('operation', {
      fontColor: '#000',
      fillColor: 'none',
      //fillColor:  '#a2c3f3',
      rounded: 0,
      align: 'left',
      spacingLeft: 10,
      strokeColor: "rgba(0,0,0,0)"
    });

    this.registerStyle('attributeHolder', {
      //fillColor: '#aee2ff',
      fillColor: 'none',
      strokeColor: 'none',
      noLabel: 1
    });

    this.registerStyle('operationHolder', {
      //fillColor: '#9fcfe9',
      fillColor: 'none',
      strokeColor: 'none',
      noLabel: 1
    });

    this.registerStyle('literalHolder', {
      fillColor: '#fff',
      strokeWidth: 1,
      noLabel: 1
    });

    this.registerStyle('classSeparator', {
      shape: mxConstants.SHAPE_LINE,
      strokeColor: 'rgba(70,70,70,0.7)',
      strokeWidth: 1
    });

    this.registerStyle('highlight', {
      strokeWidth: 2
    });

    this.registerStyle('enum', {
      fontSize: 13,
      fontColor: '#000',
      fillColor: '#c9e3ff',
      swimlaneFillColor: 'red',
      rounded: 1,
      arcSize: 2,
      autosize: 1,
      fontStyle: 1, // bold
      spacingBottom: 2,
      shape: mxConstants.SHAPE_SWIMLANE,
    });

    this.registerStyle('literal', {
      fontColor: '#000',
      fillColor: 'none',
      rounded: 0,
      align: 'center',
      //spacingLeft: 10,
      strokeWidth: 1,
      strokeColor: "#6e6e6e"
    });


    this.registerStyle('port', {
      strokeColor: "none",
      fillColor: "none",
      noLabel: 1,
      //strokeColor: "rgba(255,122,149,0.7)",
    });


    this.registerStyle('port-visible', {
      noLabel: 0,
      fillColor: "none",
      strokeColor: "none"
    });


    //
    // association view styles
    //
    this.registerStyle('start-default', {
      startArrow: 'none',
    });
    this.registerStyle('end-default', {
      endArrow: 'none',
    });
    this.registerStyle('start-navigable', {
      startArrow: 'open',
    });
    this.registerStyle('end-navigable', {
      endArrow: 'open',
    });
    this.registerStyle('start-composition', {
      startArrow: 'diamondThin',
      startFill: 1,
      startSize: 14
    });
    this.registerStyle('end-composition', {
      endArrow: 'diamondThin',
      endFill: 1,
      endSize: 14
    });
    this.registerStyle('start-aggregation', {
      startArrow: 'diamondThin',
      startFill: 0,
      startSize: 14
    });
    this.registerStyle('end-aggregation', {
      endArrow: 'diamondThin',
      endFill: 0,
      endSize: 14
    });
    this.registerStyle('inheritance', {
      startArrow: 'block',
      startFill: 0,
      startSize: 12,
      endArrow: 'none',
    });


    //
    // Terminal styles
    // N = north, S = south, W = west, E = east

    this.registerStyle('entry-N', {
      entryX: 0.5,
      entryY: 0
    });
    this.registerStyle('entry-S', {
      entryX: 0.5,
      entryY: 1
    });
    this.registerStyle('entry-W', {
      entryX: 0,
      entryY: 0.5
    });
    this.registerStyle('entry-E', {
      entryX: 1,
      entryY: 0.5
    });
    this.registerStyle('exit-N', {
      exitX: 0.5,
      exitY: 0
    });
    this.registerStyle('exit-S', {
      exitX: 0.5,
      exitY: 1
    });
    this.registerStyle('exit-W', {
      exitX: 0,
      exitY: 0.5
    });
    this.registerStyle('exit-E', {
      exitX: 1,
      exitY: 0.5
    });


    this.registerStyle('terminal', {
      fontSize: 10,
      fontColor: '#292929'
    });

    this.registerStyle('terminal-N', {
      horizontal: 0,
      align: 'left',
      spacingLeft: 20,
    });
    this.registerStyle('terminal-S', {
      horizontal: 0,
      align: 'right',
      spacingRight: 20,
    });
    this.registerStyle('terminal-W', {
      align: 'right',
      spacingRight: 20,
    });
    this.registerStyle('terminal-E', {
      align: 'left',
      spacingLeft: 20,
    });

    this.registerStyle('terminal-below-line', {
      spacingTop: 17
    });
    this.registerStyle('terminal-above-line', {
      spacingBottom: 22
    });

  }


}

export default GraphStyleManager;
