import {UserInteractionsService} from "../../modules/shared/services/user-interactions.service";
import {ViewElement} from "../model/ViewElement";
import GraphStyleManager from "./GraphStyleManager";
import {ClassModel} from "../model/aspect/ClassModel";
import {InteractionsHandler} from "./components/InteractionsHandler";
import ClassToolbar from "./components/toolbars/ClassToolbar";
import {TypeModel} from "../model/aspect/TypeModel";
import EnumToolbar from "./components/toolbars/EnumToolbar";
import {AttributeModel} from "../model/aspect/AttributeModel";
import AttributeToolbar from "./components/toolbars/AttributeToolbar";
import {OperationModel} from "../model/aspect/OperationModel";
import {LiteralModel} from "../model/aspect/LiteralModel";
import OperationToolbar from "./components/toolbars/OperationToolbar";
import LiteralToolbar from "./components/toolbars/LiteralToolbar";
import {AssociationModel} from "../model/aspect/AssociationModel";
import AssociationToolbar from "./components/toolbars/AssociationToolbar";
import InheritanceToolbar from "./components/toolbars/InheritanceToolbar";
import {TapAndHoldLoader} from "./components/TapAndHoldLoader";
import {SelectorView} from "./components/SelectorView";
import StructuralViewDrawer from "./StructuralViewDrawer";
import {ConnectionPort} from "../model/ConnectionPort";
import {CollaborationHandler} from "./components/CollaborationHandler";
import {AssociationEndElement} from "../model/AssociationEndElement";
import InterfaceUtils from "../InterfaceUtils";
import {ElementsHolder} from "../model/ElementsHolder";

const ZOOM_FACTOR = 0.1;
const CELL_TARGET_COLOR = 'rgba(7,255,5,0.7)';

/**
 * mxGraph with custom behaviour & settings
 */
class StructuralViewGraph extends mxGraph {

  drawer: StructuralViewDrawer;
  styleManager: GraphStyleManager;
  classCreationMenu: SelectorView;
  uiService: UserInteractionsService;
  associationCreationMenu: SelectorView;
  interactionsHandler: InteractionsHandler;
  collaborationHandler: CollaborationHandler;

  constructor(container, uiService) {
    super(container);

    this.drawer = new StructuralViewDrawer(this);
    this.uiService = uiService;
    this.styleManager = new GraphStyleManager(this);
    this.interactionsHandler = new InteractionsHandler(this, this.uiService);
    this.collaborationHandler = new CollaborationHandler(this, this.uiService.coreAPI);
    this.configure();

    this.associationCreationMenu = new AssociationCreationSelector(this);

    this.configureMouseEvents();


  }


  /**
   * Configuration of specific settings for
   * the use we want from mxGraph
   */
  configure() {

    // disabling native unwanted features
    this.setGridEnabled(false);
    this.setDisconnectOnMove(false);
    this.setCellsDisconnectable(false);
    this.setCellsCloneable(false);
    this.setAutoSizeCells(false);
    this.foldingEnabled = false;
    this.dropEnabled = false;
    this.centerZoom = false;

    // enabling specific features
    this.setHtmlLabels(true);
    this.setConnectable(true);
    this.setPanning(true);
    this.panningHandler.useLeftButtonForPanning = true;
    this.keepEdgesInBackground = true;

    //configure connection handler
    /*
    this.connectionHandler.getConnectImage = (state) => {
      let port = state.cell.getValue() as ConnectionPort;
      let size = port.getIconSize();
      return new mxImage('assets/icons/chevron_' + port.orientation + '.png', size.width, size.height);
    };

     */

    //configure event for association creation
    this.connectionHandler.connect = (source, target, evt, dropTarget) => {
      if (source !== null && target !== null) {
        if (source.getValue() instanceof ClassModel &&
          target.getValue() instanceof ClassModel) {
          this.createAssociation(evt, source.value._id, target.value._id);
        }
      }
    };

    //custom cell marker (cell highlight for connections)
    mxCellMarker.prototype.getMarkerColor = (evt, state, isValid) => {
      if (state) {
        let v = state.cell.getValue();
        if (v instanceof ClassModel) {
          return CELL_TARGET_COLOR;
        }
      }
    };
  }


  /**
   * Creates association between a source and a target
   * Calls on buttons click are redefined here with corresponding parameters
   */
  createAssociation(event, from, to) {
    this.associationCreationMenu.buttons[0].onclick = () => {
      this.uiService.coreAPI.send('/api/createAssociation',
        {
          classFromId: from,
          classToId: to,
          bidirectional: false
        });
      this.associationCreationMenu.hide();
    };
    this.associationCreationMenu.buttons[1].onclick = () => {
      this.uiService.coreAPI.send('/api/addSuperType',
        {
          subClassId: from,
          superTypeId: to,
        });
      this.associationCreationMenu.hide();
    };
    this.associationCreationMenu.show(event.clientX - 120, event.clientY);
  }

  click(me): void {
    if (me.state) {
      if (me.state.cell.value instanceof ElementsHolder) {
        if (!this.selectionModel.isSelected(me.state.cell.parent)) {
          this.selectionModel.setCell(me.state.cell.parent);
        }
      }
    }
    return mxGraph.prototype.click.apply(this, [me]);
  }

  /**
   * Defines behaviour on double click :
   * - print value of cell (to remove when on prod)
   * - edit cell action
   */
  dblClick(evt: any, cell: any): void {

    if (!cell) {
      return;
    }

    let value = cell.getValue();
    if (this.model.isVertex(cell)) {
      if (value instanceof ClassModel) {
        this.interactionsHandler.renameEObject(value, 'class', evt.clientX, evt.clientY);
        //this.uiService.editClass(value);
      }
      if (value instanceof AttributeModel) {
        this.interactionsHandler.renameEObject(value, 'attribute', evt.clientX, evt.clientY);

        //this.uiService.editAttribute(value);
      }
      if (value instanceof OperationModel) {
        this.interactionsHandler.renameEObject(value, 'operation', evt.clientX, evt.clientY);
        //this.uiService.editOperation(value);
      }
      if (value instanceof LiteralModel) {
        this.interactionsHandler.renameEObject(value, 'literal', evt.clientX, evt.clientY);
        //this.uiService.editLiteral(value);
      }
      if (value instanceof TypeModel) {
        this.interactionsHandler.renameEObject(value, 'enumeration', evt.clientX, evt.clientY);
        // this.uiService.editEnum(value);
      }
      if (value instanceof AssociationEndElement) {
        let source = this.model.getCell(value.association.sourceId);
        let target = this.model.getCell(value.association.targetId);
        this.uiService.editAssociation(value.association, source.value, target.value);
      }
    } else {
      if (value instanceof AssociationModel) {
        let source = this.model.getCell(value.sourceId);
        let target = this.model.getCell(value.targetId);
        this.uiService.editAssociation(value, source.value, target.value);
      }
    }
  }

  /**
   * Custom mouse events handling :
   * - tap-and-hold
   * - zoom
   */

  configureMouseEvents() {
    mxEvent.disableContextMenu(this.container);

    let getX = (ev) => {
      return ev.clientX - 200; // pixel adjustments
    };
    let getY = (ev) => {
      return ev.clientY - 17; // pixel adjustments
    };


    // defining context menu
    this.popupMenuHandler.factoryMethod = (menu, cell, evt) => {
      menu.addItem('Create Class', null, () => {
        setTimeout(() => {
          this.interactionsHandler.addClass(getX(evt), getY(evt));
        }, 0);
      });
      menu.addSeparator();
      menu.addItem('Create Enum', null, () => {
        setTimeout(() => {
          this.interactionsHandler.addEnum(getX(evt), getY(evt));
        }, 0);
      });
      menu.addSeparator();
      menu.addItem('Create Datatype', null, () => {
        setTimeout(() => {
          this.interactionsHandler.addDatatype(getX(evt), getY(evt));
        }, 0);
      });
    };

    // zoom handling
    mxEvent.addMouseWheelListener((evt, up) => {
      if (up) {
        this.zoomIn();
      } else {
        this.zoomOut();
      }
      mxEvent.consume(evt);
    });

    // show context menu on TAP AND HOLD (for touch devices only)
    this.addListener(mxEvent.TAP_AND_HOLD, (sender, evt) => {
      let x = evt.properties.event.clientX;
      let y = evt.properties.event.clientY;
      this.popupMenuHandler.popup(x, y, null, evt.properties.event);
      evt.consume();
    });

  }


  /**
   * Same behaviour as the super method, but associations are drawn again
   * after cells are translated (with our @StructuralViewDrawer)
   */
  cellsMoved(cells: mxCell[], dx: number, dy: number, disconnect?: boolean, constrain?: boolean, extend?: boolean) {
    if (cells != null && (dx !== 0 || dy !== 0)) {
      extend = (extend != null) ? extend : false;

      let associationsToRedraw = [];
      let inheritanceToRedraw = [];
      let cellsToReorganize = [];

      this.model.beginUpdate();
      try {
        if (disconnect) {
          this.disconnectGraph(cells);
        }

        for (let i = 0; i < cells.length; i++) {
          let cell = cells[i];
          this.translateCell(cell, dx, dy);

          if (cell.edges) {

            for (let e of cell.edges) {
              cellsToReorganize.push(e.source);
              cellsToReorganize.push(e.target);
              if (e.value instanceof AssociationModel) {
                associationsToRedraw.push(e.value);
              } else if (e.value instanceof ClassModel) {
                inheritanceToRedraw.push(e.value);
              }
            }
            this.removeCells(cell.edges, true);
          }


          if (extend && this.isExtendParent(cell)) {
            this.extendParent(cell);
          } else if (constrain) {
            this.constrainChild(cell);
          }

          setTimeout(() => {
            this.uiService.coreAPI.send('/api/moveClassifier',
              {
                classId: cell.value._id,
                xPosition: cell.geometry.x,
                yPosition: cell.geometry.y
              });
          }, 0);


        }

        if (this.resetEdgesOnMove) {
          this.resetEdges(cells);
        }

        associationsToRedraw.forEach(a => {
          this.drawer.drawAssociation(a, false);
        });

        let inheritanceArr = Array.from(new Set(inheritanceToRedraw));
        inheritanceArr.forEach(a => {
          this.drawer.drawInheritanceAssociation(a, false);
        });

        for (let f of Array.from(new Set(cellsToReorganize))) {
          this.drawer.reorganizeTerminalsForCell(f);
        }

      } finally {
        this.model.endUpdate();
      }
    }
  }

  /**
   * Custom Label for View Elements
   */
  convertValueToString(cell: mxCell): string {
    let value = cell.getValue();
    if (value instanceof ViewElement) return value.getHTMLLabel();
    return mxGraph.prototype.convertValueToString.apply(this, [cell]);
  }

  getDropTarget(cells: mxCell[], evt, cell: any, clone: any): any {
    console.log('get drop target');
    return mxGraph.prototype.getDropTarget.apply(this, [cells, evt, cell, clone]);
  }

  isValidConnection(source: mxCell, target: mxCell): boolean {
    if (source && target) {
      if (source.isVertex()
        && target.isVertex()
        && source.value instanceof ClassModel
        && target.value instanceof ClassModel) {
        return true;
      }
    }
    return false;
  }


  isValidSource(cell: mxCell): boolean {
    //if (cell) return cell.getValue() instanceof ClassModel;
    return false;
  }

  isValidTarget(cell: mxCell): boolean {
    if (cell) {
      return cell.isVertex() && cell.getValue() instanceof ClassModel;
    }
    return false;
  }


  isValidDropTarget(cell: any, cells: any, evt: any): boolean {
    return false;
  }

  isCellMovable(cell: mxCell): boolean {
    return this.isSwimlane(cell);
  }


  isCellEditable(cell: mxCell): boolean {
    return false;
  }

  isCellResizable(cell: mxCell): boolean {
    return false;
  }

  /**
   * Shows a specific toolbar when clicked on a cell
   */
  createHandler(state: mxCellState) {
    if (state != null) {
      if (state.cell.isVertex()) {
        if (state.cell.value instanceof ClassModel) {
          return new ClassToolbar(state, this.interactionsHandler);
        }
        if (state.cell.value instanceof TypeModel) {
          return new EnumToolbar(state, this.interactionsHandler);
        }
        if (state.cell.value instanceof AttributeModel) {
          return new AttributeToolbar(state, this.interactionsHandler);
        }
        if (state.cell.value instanceof OperationModel) {
          return new OperationToolbar(state, this.interactionsHandler);
        }
        if (state.cell.value instanceof LiteralModel) {
          return new LiteralToolbar(state, this.interactionsHandler);
        }
      } else if (state.cell.isEdge()) {
        if (state.cell.getValue() instanceof ClassModel) {
          return new InheritanceToolbar(state, this.interactionsHandler);
        }
        if (state.cell.getValue() instanceof AssociationModel) {
          return new AssociationToolbar(state, this.interactionsHandler);
        }

      }
    }
  }

}

/**
 * Custom selector view for association creation
 */
class AssociationCreationSelector extends SelectorView {

  constructor(graph) {
    super(graph);
    //onClick functions isn't defined here, because it needs on-time parameters
    this.addMenuOption('Association', null);
    this.addMenuOption('Inheritance', null);
    this.addCancelOption();
  }

}

export default StructuralViewGraph;
