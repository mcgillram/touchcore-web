import { CoreAPIService } from './modules/shared/services/core-api.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'webcore-app';

  constructor(protected coreAPI: CoreAPIService) {

  }

  ngOnInit() {
    /*
    setTimeout( () => {

    this.coreAPI.sendMessage();
    }, 500);

     */
  }
}
