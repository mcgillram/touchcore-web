import {NgModule} from "@angular/core";
import {AppRoutingModule} from "../../app-routing.module";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {PageNotFoundComponent} from "./components/page-not-found/page-not-found.component";
import {HomeComponent} from "./components/home/home.component";
import {
  MatButtonModule, MatButtonToggleModule,
  MatCardModule, MatDialogModule,
  MatDividerModule, MatFormFieldModule,
  MatIconModule, MatInputModule, MatRippleModule, MatSelectModule,
  MatSlideToggleModule, MatSnackBarModule, MatTooltipModule, MatProgressSpinnerModule
} from "@angular/material";
import {CardElementComponent} from "./components/card-element/card-element.component";
import {FlexibleSidebarComponent} from "./components/flexible-sidebar/flexible-sidebar.component";
import {FloatingTeamPanelComponent} from "./components/floating-team-panel/floating-team-panel.component";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {DisconnectComponent} from "./components/disconnect/disconnect.component";
import {HelpPanelComponent} from "./components/help-panel/help-panel.component";


@NgModule({
  declarations: [
    PageNotFoundComponent,
    HomeComponent,
    CardElementComponent,
    FlexibleSidebarComponent,
    FloatingTeamPanelComponent,
    DisconnectComponent,
    HelpPanelComponent,
  ],
  imports: [
    AppRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatCardModule,
    MatDividerModule,
    MatIconModule,
    MatSnackBarModule,
    MatDialogModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    MatSlideToggleModule,
    MatTooltipModule,
    BrowserAnimationsModule,
    MatRippleModule,
    MatButtonToggleModule,
    MatProgressSpinnerModule,
  ],
  entryComponents: [
    DisconnectComponent,
    HelpPanelComponent
  ],
  exports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PageNotFoundComponent,
    HomeComponent,
    CardElementComponent,
    MatButtonModule,
    MatCardModule,
    MatDividerModule,
    MatIconModule,
    MatSlideToggleModule,
    FlexibleSidebarComponent,
    MatSnackBarModule,
    MatDialogModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    MatSlideToggleModule,
    FloatingTeamPanelComponent,
    MatTooltipModule,
    BrowserAnimationsModule,
    MatRippleModule,
    MatButtonToggleModule,
    MatProgressSpinnerModule,
    DisconnectComponent,
    HelpPanelComponent
  ],
})

export class SharedModule {
}
