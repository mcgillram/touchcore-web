import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FloatingTeamPanelComponent } from './floating-team-panel.component';

describe('FloatingTeamPanelComponent', () => {
  let component: FloatingTeamPanelComponent;
  let fixture: ComponentFixture<FloatingTeamPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FloatingTeamPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FloatingTeamPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
