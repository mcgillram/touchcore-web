import { Subscription } from 'rxjs';
import { CoreAPIService } from './../../services/core-api.service';
import {Component, AfterViewInit, OnDestroy} from '@angular/core';

@Component({
  selector: 'app-floating-team-panel',
  templateUrl: './floating-team-panel.component.html',
  styleUrls: ['./floating-team-panel.component.scss']
})
export class FloatingTeamPanelComponent implements AfterViewInit, OnDestroy {

  collabSubscription: Subscription;
  usersData: {};
  sessionData;
  constructor(protected coreAPI: CoreAPIService) {
    this.usersData = {};
  }

  ngAfterViewInit() {

    this.collabSubscription = this.coreAPI.collabSubject.subscribe(value => {
      this.sessionData = this.coreAPI.getSessionData();
      this.usersData = value['users'];
    });

  }

  ngOnDestroy(): void {
    this.collabSubscription.unsubscribe();
  }

}
