import {CoreAPIService} from "../../services/core-api.service";
import {Component} from "@angular/core";
import {Router} from "@angular/router";
import {animate, keyframes, query, stagger, style, transition, trigger} from "@angular/animations";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
})
/**
 * This component is the entry point of the application
 * Responsible of initializing Websocket connection and fetching available models
 */
export class HomeComponent {
  cards: any[];
  isLoading = true;
  connectError = false;

  constructor(private coreAPIService: CoreAPIService, private router: Router) {

    this.loadModels();

  }

  loadModels() {
    this.isLoading = true;
    this.connectError = false;
    this.cards = [];
    if (!this.coreAPIService.isConnected()) {
      this.coreAPIService.connect(() => {
        this.fetchModels();
      }, () => {
        this.connectError = true;
      });
    } else {
      if (this.coreAPIService.modelList.length === 0) {
        this.fetchModels();
      } else {
        this.cards = this.getCards(this.coreAPIService.modelList);
        this.isLoading = false;
      }
    }
  }

  getCards(modelList) {
    let cards = [];
    modelList.forEach(model => {
      cards.push({
        view: "Structural View",
        name: model,
        desc: "",
        rooter: "/structural-view"
      });
    });
    return cards;
  }

  fetchModels() {
    this.coreAPIService.getModels(modelList => {
      if (modelList.length === 0) {
        setTimeout(this.fetchModels, 300);
      } else {
        this.isLoading = false;
        this.cards = this.getCards(modelList);
      }
    });
  }

  selectCurrentModel(modelCard) {
    this.coreAPIService.currentModel = modelCard.name;
    this.router.navigateByUrl(modelCard.rooter);
  }
}
