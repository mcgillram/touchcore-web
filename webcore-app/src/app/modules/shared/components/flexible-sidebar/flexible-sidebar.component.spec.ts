import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlexibleSidebarComponent } from './flexible-sidebar.component';

describe('FlexibleSidebarComponent', () => {
  let component: FlexibleSidebarComponent;
  let fixture: ComponentFixture<FlexibleSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlexibleSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlexibleSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
