import {Component, EventEmitter, HostListener, Input, OnInit, Output} from '@angular/core';
import InterfaceUtils from "../../../../core/InterfaceUtils";
import {animate, style, transition, trigger} from "@angular/animations";
import {CoreAPIService} from "../../services/core-api.service";
import {UserInteractionsService} from "../../services/user-interactions.service";
import {MatDialog, MatSnackBar} from "@angular/material";
import {HelpPanelComponent} from "../help-panel/help-panel.component";

@Component({
  selector: 'app-flexible-sidebar',
  templateUrl: './flexible-sidebar.component.html',
  animations: [
    trigger(
      'showAnimation',
      [
        transition(
          ':enter', [
            style({transform: 'translateX(100%)', opacity: 0}),
            animate('200ms', style({transform: 'translateX(0)', opacity: 1}))
          ]
        ),
        transition(
          ':leave', [
            style({transform: 'translateX(0)', opacity: 1}),
            animate('80ms', style({transform: 'translateX(100%)', opacity: 0}))]
        )
      ]
    )
  ],
  styleUrls: ['./flexible-sidebar.component.scss']
})
export class FlexibleSidebarComponent implements OnInit {

  @Output() events = new EventEmitter<string>();

  sidebarCanBeHidden = false;
  grabber = false;
  oldSidebarWidth: number;
  currentSidebarWidth: number;
  sideBarWidths: number[];
  minWidthForContent: number;
  expertMode: boolean;


  constructor(private coreAPI: CoreAPIService,
              private snackBar: MatSnackBar,
              private dialog: MatDialog,
              private uiService: UserInteractionsService) {

    this.sideBarWidths = [
      InterfaceUtils.SIDEBAR_HIDDEN_MODE_WIDTH,
      InterfaceUtils.SIDEBAR_SHORT_MODE_WIDTH,
      InterfaceUtils.SIDEBAR_FULL_MODE_WIDTH
    ];

    this.minWidthForContent = InterfaceUtils.SIDEBAR_MIN_WIDTH_FOR_CONTENT;
    this.expertMode = uiService.expertMode;
  }

  getAspect() {
    this.coreAPI.getAspect();
  }


  undo() {
    this.coreAPI.send('/api/undo', {});
  }

  redo() {
    this.coreAPI.send('/api/redo', {});
  }


  hide() {
    this.currentSidebarWidth = InterfaceUtils.SIDEBAR_HIDDEN_MODE_WIDTH;
  }

  show() {
    this.currentSidebarWidth = InterfaceUtils.SIDEBAR_FULL_MODE_WIDTH;
  }

  simpleLayout() {
    this.events.emit('simpleLayout');
  }

  complexLayout() {
    this.events.emit('complexLayout');
  }


  ngOnInit() {
    this.autoResizeSideBar(InterfaceUtils.SIDEBAR_SHORT_MODE_WIDTH);
    // this.autoResizeSideBar(InterfaceUtils.SIDEBAR_FULL_MODE_WIDTH);
  }

  autoResizeSideBar(x: number) {

    const values = [...this.sideBarWidths];
    if (!this.sidebarCanBeHidden) {
      values.shift();
    }

    this.currentSidebarWidth = values.reduce((prev, curr) =>
      (Math.abs(curr - x) < Math.abs(prev - x) ? curr : prev));
  }

  @HostListener('document:mousedown', ['$event'])
  onMouseDown(event: MouseEvent) {
    // @ts-ignore
    if (event.target.id === 'separator') {
      this.grabber = true;
      this.oldSidebarWidth = event.clientX;
    }
  }

  @HostListener('document:mousemove', ['$event'])
  onMouseMove(event: MouseEvent) {
    if (!this.grabber) {
      return;
    }
    this.liveResize(event.clientX - this.oldSidebarWidth);
    this.oldSidebarWidth = event.clientX;
  }

  @HostListener('document:mouseup', ['$event'])
  onMouseUp(event: MouseEvent) {
    if (this.grabber) {
      this.autoResizeSideBar(event.clientX);
      this.grabber = false;
    }
  }

  liveResize(offsetX: number) {
    this.currentSidebarWidth += offsetX;
  }


  switchMode() {
    this.currentSidebarWidth === InterfaceUtils.SIDEBAR_FULL_MODE_WIDTH ?
      this.autoResizeSideBar(InterfaceUtils.SIDEBAR_SHORT_MODE_WIDTH) :
      this.autoResizeSideBar(InterfaceUtils.SIDEBAR_FULL_MODE_WIDTH);
  }

  print() {
    this.events.emit('print');
  }

  switchExpertMode() {
    this.events.emit('switch-expert');
    let snackBarMessage = 'Fast Mode ';
    this.expertMode ? snackBarMessage += 'disabled' : snackBarMessage += 'enabled';
    this.snackBar.open(snackBarMessage, null, {
      duration: 1000
    });
    this.expertMode = !this.expertMode;
  }

  showHelp() {
    this.dialog.open(HelpPanelComponent, {width: '600px'});
  }
}
