import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from "@angular/material";
import {Router} from "@angular/router";

@Component({
  selector: 'app-disconnect',
  templateUrl: './disconnect.component.html',
  styleUrls: ['./disconnect.component.scss']
})
export class DisconnectComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<DisconnectComponent>,
              private router: Router) { }

  ngOnInit() {
  }

  ok() {
    this.dialogRef.close();
    this.router.navigateByUrl('/');
  }
}
