import {Injectable, isDevMode} from "@angular/core";
import * as Stomp from "stompjs";
import * as SockJS from "sockjs-client";
import {Subject} from "rxjs";
import {environment} from "../../../../environments/environment";
import {MatDialog, MatSnackBar} from "@angular/material";
import {DisconnectComponent} from "../components/disconnect/disconnect.component";
import {ActivatedRoute, Router} from "@angular/router";

@Injectable({
  providedIn: "root"
})

export class CoreAPIService {

  serverUrl;
  private sessionData = {
    sessionId: String,
    color: String,
    username: String
  };

  stompClient;

  aspectSubject = new Subject();
  updateSubject = new Subject();
  collabSubject = new Subject();

  modelList = [];
  currentModel;

  constructor(private snackBar: MatSnackBar,
              private route: Router,
              private dialog: MatDialog) {

    if (isDevMode()) {
      this.serverUrl = environment.coreAPI + "/webcore";
    } else {
      this.serverUrl = window.location.origin + "/webcore";
    }

    this.init();

    if (isDevMode()) {
      this.serverUrl = environment.coreAPI + "/webcore";
    } else {
      this.serverUrl = window.location.origin + "/webcore";
    }
  }

  init() {
    let ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(ws);
    this.stompClient.debug = null;
  }

  /**
   * Connects the Socket API and retrieves available models
   */
  connect(onSuccess, onError?) {
    this.init();
    console.log('Connecting to ' + this.serverUrl);
    this.stompClient.connect({}, frame => {

      this.configureListeners();

      this.sendAndSub(
        '/api/getSessionData',
        '/user/queue/getSessionData',
        {},
        (res) => {
          this.sessionData = JSON.parse(res.body);
        });

      this.getModels((models) => {
        this.setCurrentModel(models[0]);
        onSuccess();
      });

    }, () => {

      if (onError) onError();
      if (this.route.url !== '/') this.showDisconnect();
    });
  }

  /**
   * Retrieves the current aspect model
   * If Socket API is not connected, it connects first,
   * ensuring aspect to be fetched
   */
  getAspect() {

    if (!this.isConnected()) {
      this.connect(() => {
        this.getAspect();
      });
    } else {
      this.sendAndSub(
        '/api/getAspect',
        '/user/queue/getAspect',
        {aspectName: this.currentModel},
        (res) => {
          this.emitAspect(JSON.parse(res.body));
        });
    }


  }

  /**
   * Setup of needed subscriptions
   */
  configureListeners() {
    this.stompClient.subscribe("/topic/update", data => {
      this.emitUpdate(JSON.parse(data.body));
      //this.snackBar.open('/update');
    });

    this.stompClient.subscribe("/topic/collab", data => {
      this.emitCollab(JSON.parse(data.body));
    });

    this.stompClient.subscribe("/user/queue/error", data => {
      this.snackBar.open(data.body, 'OK', {duration: 5000});
    });
  }

  /**
   * Sends a message and subscribes to one answer only
   *
   * @param sendPath : API route to request
   * @param subPath : API route to subscribe
   * @param data : optional data for request
   * @param callback : function handling result
   */
  sendAndSub(sendPath, subPath, data, callback) {
    let sub = this.stompClient.subscribe(subPath, (res) => {
      callback(res);
      sub.unsubscribe();
    });

    data.aspectName = this.currentModel;

    this.stompClient.send(sendPath, {}, JSON.stringify(data));
  }

  setCurrentModel(newModel: string) {
    this.currentModel = newModel;
  }

  /**
   * Retrieves available models
   */
  getModels(callback) {
    this.sendAndSub(
      '/api/getModels',
      '/user/queue/getModels',
      {},
      (res) => {
        this.modelList = JSON.parse(res.body);
        callback(this.modelList);
      }
    );
  }

  send(path, data) {
    data.aspectName = this.currentModel;
    this.stompClient.send(path, {}, JSON.stringify(data));
  }

  private emitAspect(model) {
    this.aspectSubject.next(model);
  }

  private emitUpdate(model) {
    if (model.aspect === this.currentModel) {
      this.updateSubject.next(model.stack);
    }

  }

  private emitCollab(collab) {
    this.collabSubject.next(collab);
  }

  isConnected() {
    return this.stompClient.connected;
  }

  getSessionData() {
    return this.sessionData;
  }

  showDisconnect() {
    this.dialog.open(DisconnectComponent, {disableClose: true, closeOnNavigation: true, width: '350px'});
  }


}
