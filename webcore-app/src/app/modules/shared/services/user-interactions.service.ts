import {Injectable} from '@angular/core';
import {MatDialog, MatDialogModule} from "@angular/material";
import {CoreAPIService} from "./core-api.service";
import {AddAttributeComponent} from "../../aspect/components/add-attribute/add-attribute.component";
import {ClassModel} from "../../../core/model/aspect/ClassModel";
import {AddOperationComponent} from "../../aspect/components/add-operation/add-operation.component";
import {DeleteClassComponent} from "../../aspect/components/delete-class/delete-class.component";
import {EditClassComponent} from "../../aspect/components/edit-class/edit-class.component";
import {AttributeModel} from "../../../core/model/aspect/AttributeModel";
import {DeleteClassElementComponent} from "../../aspect/components/delete-class-element/delete-class-element.component";
import {OperationModel} from "../../../core/model/aspect/OperationModel";
import {TypeModel} from "../../../core/model/aspect/TypeModel";
import {AddLiteralComponent} from "../../aspect/components/add-literal/add-literal.component";
import {EditEnumComponent} from "../../aspect/components/edit-enum/edit-enum.component";
import {LiteralModel} from "../../../core/model/aspect/LiteralModel";
import {AssociationModel} from "../../../core/model/aspect/AssociationModel";
import {EditAssociationComponent} from "../../aspect/components/edit-association/edit-association.component";
import {environment} from "../../../../environments/environment";
import {PageNotFoundComponent} from "../components/page-not-found/page-not-found.component";

@Injectable({
  providedIn: 'root'
})
/**
 * This service is responsible of showing input components
 * and calling Core API Service
 *
 */

export class UserInteractionsService {

  isBusy = false;
  expertMode = environment.defaultExpertMode;

  constructor(private dialog: MatDialog,
              public coreAPI: CoreAPIService) {
  }


  showDialog(c, data) {
    this.isBusy = true;
    const d = this.dialog.open(c, {data});
    d.afterClosed().subscribe(res => {
      this.isBusy = false;
    });
  }

  addAttribute(cl: ClassModel) {
    this.showDialog(AddAttributeComponent, {object: cl, mode: 'add'});
  }

  addOperation(cl: ClassModel) {
    this.showDialog(AddOperationComponent, {object: cl, mode: 'add'});
  }

  deleteClass(cl: ClassModel) {
    this.showDialog(DeleteClassComponent, cl);
  }

  editClass(cl: ClassModel) {
    this.showDialog(EditClassComponent, {object: cl, mode: 'edit'});
  }

  addClass(x, y) {
    this.showDialog(EditClassComponent, {mode: 'add', x, y, elementType: "class"});
  }

  addDataType(x, y) {
    this.showDialog(EditClassComponent, {mode: 'add', x, y, elementType: "dataType"});
  }

  addEnum(x, y) {
    this.showDialog(EditClassComponent, {mode: 'add', x, y, elementType: "enumeration"});
  }

  editAttribute(c: AttributeModel) {
    this.showDialog(AddAttributeComponent, {object: c, mode: 'edit'});
  }

  deleteAttribute(c: AttributeModel) {
    this.showDialog(DeleteClassElementComponent, {object: c, type: 'attribute'});
  }

  editOperation(c: OperationModel) {
    this.showDialog(AddOperationComponent, {object: c, mode: 'edit'});
  }

  deleteOperation(c: OperationModel) {
    this.showDialog(DeleteClassElementComponent, {object: c, type: 'operation'});
  }


  addLiteral(cl: TypeModel) {
    this.showDialog(AddLiteralComponent, {object: cl, mode: 'add'});
  }

  editLiteral(cl: LiteralModel) {
    this.showDialog(AddLiteralComponent, {object: cl, mode: 'edit'});
  }

  deleteLiteral(cl: LiteralModel) {
    this.showDialog(DeleteClassElementComponent, {object: cl, type: 'literal'});
  }


  editEnum(cl: TypeModel) {
    this.showDialog(EditEnumComponent, {object: cl, mode: 'edit'});
  }


  deleteEnum(cl: TypeModel) {
    this.showDialog(DeleteClassElementComponent, {object: cl, type: 'enum'});
  }


  deleteAssociation(az: AssociationModel) {
    this.showDialog(DeleteClassElementComponent, {object: az, type: 'asso'});
  }


  editAssociation(obj: AssociationModel, source: ClassModel, target: ClassModel) {
    this.showDialog(EditAssociationComponent, {object: obj, source, target});
  }

  deleteSupertypeAssociation(obj: ClassModel, supertypeId) {
    this.showDialog(DeleteClassElementComponent, {object: obj, type: 'supertype', supertype: supertypeId});
  }
}
