import {Component, Inject, OnInit} from '@angular/core';
import {CoreAPIService} from "../../../shared/services/core-api.service";
import {FormBuilder, FormGroup} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {TypeModel} from "../../../../core/model/aspect/TypeModel";

@Component({
  selector: 'app-edit-enum',
  templateUrl: './edit-enum.component.html',
  styleUrls: ['./edit-enum.component.scss']
})
export class EditEnumComponent {

  enumModel: TypeModel;
  isValid = true;
  form: FormGroup;

  constructor(private coreAPI: CoreAPIService,
              private formBuilder: FormBuilder,
              private dialogRef: MatDialogRef<EditEnumComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {

    this.enumModel = this.data.object;

    let formConfig = {
      name: this.enumModel.name,
    };

    this.form = this.formBuilder.group(formConfig);
  }


  close(){
    if (this.form.value.name === '') {
      this.isValid = false;
      return;
    }

    this.coreAPI.send('/api/setEObjectName', {
      eObjectId: this.enumModel._id,
      objectName: this.form.value.name
    });

    this.dialogRef.close();
  }


}
