import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddLiteralComponent } from './add-literal.component';

describe('AddLiteralComponent', () => {
  let component: AddLiteralComponent;
  let fixture: ComponentFixture<AddLiteralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddLiteralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddLiteralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
