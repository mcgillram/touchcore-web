import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {CoreAPIService} from "../../../shared/services/core-api.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

@Component({
  selector: 'app-add-literal',
  templateUrl: './add-literal.component.html',
  styleUrls: ['./add-literal.component.scss']
})
export class AddLiteralComponent implements OnInit {

  model;
  isValid = true;
  form: FormGroup;

  constructor(private coreAPI: CoreAPIService,
              private formBuilder: FormBuilder,
              private dialogRef: MatDialogRef<AddLiteralComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {

    this.model = data.object;

    let formConfig = {
      name: [''],
    };

    if (this.data.mode === 'edit') {
      formConfig.name = [this.model.name];
    }

    this.form = this.formBuilder.group(formConfig);
  }

  ngOnInit() {

  }

  close() {

    if (this.form.value.name === '') {
      this.isValid = false;
      return;
    }


    if (this.data.mode === 'add') {
      this.coreAPI.send('/api/addLiteral', {
        enumId: this.model._id,
        rankIndex: 0,
        literalName: this.form.value.name
      });


    } else {


      if (this.form.value.name !== this.model.name) {
        this.coreAPI.send('/api/setEObjectName', {
          eObjectId: this.model._id,
          objectName: this.form.value.name
        });

      }

    }

    this.dialogRef.close();
  }
}
