import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {FormBuilder, FormGroup} from "@angular/forms";
import {CoreAPIService} from "../../../shared/services/core-api.service";

@Component({
  selector: 'app-add-attribute',
  templateUrl: './add-attribute.component.html',
  styleUrls: ['./add-attribute.component.scss']
})
export class AddAttributeComponent implements OnInit {

  model;
  typeOptions: string[];
  isValid = true;
  form: FormGroup;

  constructor(private coreAPI: CoreAPIService,
              private formBuilder: FormBuilder,
              private dialogRef: MatDialogRef<AddAttributeComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {

    this.model = data.object;

    let formConfig = {
      name: [''],
      type: ['']
    };

    if (this.data.mode === 'edit') {
      formConfig.name = [this.model.name];
      formConfig.type = [this.model.typeName];
    }

    this.form = this.formBuilder.group(formConfig);
  }

  ngOnInit() {

    this.coreAPI.sendAndSub(
      '/api/getPossibleAttributeTypes',
      '/user/queue/getPossibleAttributeTypes',
      {eObjectId: this.model._id},
      (res) => {
        this.typeOptions = JSON.parse(res.body);
        this.typeOptions.sort();
      });

  }

  close() {

    if (this.data.mode === 'add') {
      this.coreAPI.send('/api/createAttribute', {
        classId: this.model._id,
        rankIndex: 0,
        attributeString: this.form.value.type + ' ' + this.form.value.name
      });
    } else {

      if (this.form.value.name !== this.model.name) {
        this.coreAPI.send('/api/setEObjectName', {
          eObjectId: this.model._id,
          objectName: this.form.value.name
        });
      }

      if (this.form.value.type !== this.model.typeName) {
        this.coreAPI.send('/api/setAttributeType', {
          attributeId: this.model._id,
          attributeType: this.form.value.type
        });
      }
    }

    this.dialogRef.close();
  }
}
