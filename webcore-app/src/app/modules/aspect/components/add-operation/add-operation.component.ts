import {Component, Inject, OnInit} from '@angular/core';
import {CoreAPIService} from "../../../shared/services/core-api.service";
import {FormBuilder, FormGroup} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {ParameterModel} from "../../../../core/model/aspect/ParameterModel";

@Component({
  selector: 'app-add-operation',
  templateUrl: './add-operation.component.html',
  styleUrls: ['./add-operation.component.scss']
})
export class AddOperationComponent implements OnInit {

  opModel = null;
  form: FormGroup;
  isValid = true;
  showAddParam = false;
  returnTypes: string[];
  partialities = ['none', 'public', 'concern'];
  parameterTypes: string[];
  parameters: any[];
  addedParameters: any[];
  removedParameters: any[];
  changesHolder: any;
  constructorDisabled = false;
  destructorDisabled = false;
  addConstructor = false;
  addDestructor = false;

  constructor(private coreAPI: CoreAPIService,
              private formBuilder: FormBuilder,
              private dialogRef: MatDialogRef<AddOperationComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {

    this.opModel = data.object;
    this.changesHolder = {};
    this.parameters = [];
    this.addedParameters = [];
    this.removedParameters = [];

    let formConfig = {
      name: '',
      returnType: '',
      visibility: '+',
      newParamName: '',
      newParamType: '',
      partiality: 'none',
      abstract: false
    };

    if (this.data.mode === 'edit') {
      formConfig.name = this.opModel.name;
      formConfig.returnType = this.opModel.returnTypeName;

      if (this.opModel.extendedVisibility === 'private') {
        formConfig.visibility = '-';
      } else if (this.opModel.extendedVisibility === 'package') {
        formConfig.visibility = '~';
      } else if (this.opModel.extendedVisibility === 'protected') {
        formConfig.visibility = '#';
      }
      this.changesHolder.visibility = formConfig.visibility[0];

      if (this.opModel.partiality) {
        formConfig.partiality = this.opModel.partiality;
      }

      formConfig.abstract = this.opModel.abstract;

      this.opModel.parameters.forEach((p: ParameterModel) => {
        this.parameters.push({
          name: p.name,
          type: p.typeName,
          id: p._id
        });
      });
    } else {
      for (let o of this.opModel.operations) {
        if (o.name === 'create') {
          this.constructorDisabled = true;
        }
        if (o.name === 'destroy') {
          this.destructorDisabled = true;
        }

      }
    }

    this.form = this.formBuilder.group(formConfig);
  }

  ngOnInit() {
    this.getParameterTypes();
    this.getReturnTypes();
  }

  getReturnTypes() {
    this.coreAPI.sendAndSub(
      '/api/getPossibleOperationTypes',
      '/user/queue/getPossibleOperationTypes',
      {eObjectId: this.opModel._id},
      (res) => {
        this.returnTypes = JSON.parse(res.body);
        this.returnTypes.sort().reverse();
      });
  }

  getParameterTypes() {
    this.coreAPI.sendAndSub(
      '/api/getPossibleParameterTypes',
      '/user/queue/getPossibleParameterTypes',
      {eObjectId: this.opModel._id},
      (res) => {
        this.parameterTypes = JSON.parse(res.body);
        this.parameterTypes.sort().reverse();
      });
  }

  createConstructor() {
    this.constructorDisabled = true;
    this.addConstructor = true;
  }

  createDestructor() {
    this.destructorDisabled = true;
    this.addDestructor = true;
  }

  getResult(): string {
    if (this.form.value.name === '' || this.form.value.returnType === '') {
      return '';
    }

    let partiality = '';
    if (this.form.value.partiality === 'public') partiality += '|';
    else if (this.form.value.partiality === 'concern') partiality += '¦';

    let str = '';
    str += this.form.value.visibility + ' ' +
      this.form.value.returnType + ' ' +
      partiality +
      this.form.value.name + '(';

    this.parameters.forEach((value, index) => {
      str += value.type + ' ' + value.name;
      if (index < (this.addedParameters.length - 1)) str += ', ';
    });

    str += ')';

    return str;
  }

  isFormValid() {
    return !(this.form.value.name === '' || this.form.value.returnType === '' || this.form.value.visibility === '');

  }

  close() {

    if (!this.isFormValid() && !this.addDestructor && !this.addConstructor) {
      this.isValid = false;
      return;
    }

    let res = this.getResult();

    if (this.data.mode === 'add') {

      if (this.isFormValid()) {
        this.coreAPI.send('/api/createOperation', {
          classId: this.opModel._id,
          rankIndex: 0,
          operationString: res
        });
      }
      if (this.addConstructor) {
        this.coreAPI.send('/api/createConstructor', {
          classId: this.opModel._id
        });
      }
      if (this.addDestructor) {
        this.coreAPI.send('/api/createDestructor', {
          classId: this.opModel._id
        });
      }

    } else {

      if (this.form.value.name !== this.opModel.name) {
        this.coreAPI.send('/api/setEObjectName', {
          eObjectId: this.opModel._id,
          objectName: this.form.value.name
        });
      }

      if (this.form.value.returnType !== this.opModel.returnTypeName) {
        this.coreAPI.send('/api/setOperationType', {
          operationId: this.opModel._id,
          operationType: this.form.value.returnType
        });
      }

      if (this.form.value.visibility !== this.changesHolder.visibility) {
        this.coreAPI.send('/api/setOperationVisibility', {
          operationId: this.opModel._id,
          newVisibility: this.form.value.visibility
        });
      }

      if (this.form.value.abstract !== null
        && this.form.value.abstract !== this.opModel.abstract) {
        this.coreAPI.send('/api/switchOperationAbstract', {
          operationId: this.opModel._id
        });
      }

      if (this.form.value.partiality !== null
        && this.form.value.partiality !== this.opModel.partiality) {
        this.coreAPI.send('/api/setEObjectPartiality', {
          eObjectId: this.opModel._id,
          partiality: this.form.value.partiality
        });
      }

      this.handleParameter();
    }

    this.dialogRef.close();
  }

  handleParameter() {
    // check all the added parameters
    this.addedParameters.forEach(param => {
      if (!this.removedParameters.includes(param)) {
        this.coreAPI.send('/api/createParameter', {
          operationId: this.opModel._id,
          rankIndex: (this.addedParameters.length - 1),
          parameterString: param.type + ' ' + param.name
        });
      }
    });

    // check all the removed parameters
    this.removedParameters.forEach(param => {
      if (param.hasOwnProperty('id')) {
        this.coreAPI.send('/api/removeParameter', {
          parameterId: param.id
        });
      }
    });
  }

  addParameter() {
    if (this.form.value.newParamType === '' ||
      this.form.value.newParamName === '') {
      return;
    }

    let param = {
      type: this.form.value.newParamType,
      name: this.form.value.newParamName
    };

    this.addedParameters.push(param);
    this.parameters.push(param);
    this.showAddParam = false;
  }

  removeParam(i: number, p) {
    this.removedParameters.push(p);
    this.parameters.splice(i, 1);
  }

  showParamForm() {
    if (this.showAddParam) {
      this.showAddParam = false;
    } else {
      this.form.controls.newParamType.reset('');
      this.form.controls.newParamName.reset('');
      this.showAddParam = true;
    }
  }


}
