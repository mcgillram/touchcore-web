import {Component, Inject, OnInit} from '@angular/core';
import {CoreAPIService} from "../../../shared/services/core-api.service";
import {FormBuilder} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

@Component({
  selector: 'app-delete-class-element',
  templateUrl: './delete-class-element.component.html',
  styleUrls: ['./delete-class-element.component.scss']
})
export class DeleteClassElementComponent implements OnInit {

  model;
  desc;

  constructor(private coreAPI: CoreAPIService,
              private formBuilder: FormBuilder,
              private dialogRef: MatDialogRef<DeleteClassElementComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {

    this.model = this.data.object;
    this.desc = this.getDescription();
  }

  ngOnInit() {
  }

  close() {

    if (this.data.type === 'attribute') {
      this.coreAPI.send('/api/removeAttribute', {
        attributeId: this.model._id
      });
    } else if (this.data.type === 'operation') {
      this.coreAPI.send('/api/removeOperation', {
        operationId: this.model._id
      });
    } else if (this.data.type === 'enum') {
      this.coreAPI.send('/api/removeClassifier', {
        classId: this.model._id
      });
    } else if (this.data.type === 'literal') {
      this.coreAPI.send('/api/removeLiteral', {
        literalId: this.model._id
      });
    } else if (this.data.type === 'asso') {
      this.coreAPI.send('/api/deleteAssociation', {
        associationId: this.model._id
      });
    } else if (this.data.type === 'supertype') {

      this.coreAPI.send('/api/removeSuperType', {
        classId1: this.model._id,
        classId2 : this.data.supertype
      });



    }

    this.dialogRef.close();
  }


  getDescription() {
    let text = "";
    switch (this.data.type) {
      case 'operation':
        text = this.model.name + '()';
        break;
      case 'asso':
        let name = this.model.name.split('_');
        text = "association between " + name[0] + ' & ' + name[1];
        break;
      case 'supertype':
        text = "class " + this.model.name + " inheritance";
        break;
      default:
        text = this.model.name;

    }

    return text;
  }
}
