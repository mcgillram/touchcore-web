import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteClassElementComponent } from './delete-class-element.component';

describe('DeleteClassElementComponent', () => {
  let component: DeleteClassElementComponent;
  let fixture: ComponentFixture<DeleteClassElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteClassElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteClassElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
