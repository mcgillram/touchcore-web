import {
  Component,
  ElementRef, OnDestroy, OnInit,
  ViewChild
} from '@angular/core';
import {CoreAPIService} from "../../../shared/services/core-api.service";
import {Subscription} from "rxjs";
import {UserInteractionsService} from "../../../shared/services/user-interactions.service";
import GraphController from "../../../../core/structural-view/GraphController";
import GraphView from "../../../../core/structural-view/GraphView";
import GraphModel from "../../../../core/structural-view/GraphModel";
import {MatDialog} from "@angular/material";
import {HelpPanelComponent} from "../../../shared/components/help-panel/help-panel.component";

@Component({
  selector: 'app-structural-view',
  templateUrl: './structural-view.component.html',
  styleUrls: ['./structural-view.component.scss']
})
export class StructuralViewComponent implements OnInit, OnDestroy {

  graph: GraphController;
  aspectSubscription: Subscription;
  modelUpdateSubscription: Subscription;

  @ViewChild('graphContainer') graphContainer: ElementRef;
  @ViewChild('sidebar') sidebar: ElementRef;

  constructor(private coreAPIService: CoreAPIService,
              private uiService: UserInteractionsService) {


    this.aspectSubscription = this.coreAPIService.aspectSubject.subscribe(value => {
      if (this.graph) {
        this.graph.updateFullModel(value);
      } else {
        let gView = new GraphView(this.graphContainer.nativeElement, this.uiService);
        let gModel = new GraphModel(value);
        this.graph = new GraphController(gModel, gView);
        gView.setExpertMode(this.uiService.expertMode);
      }
    });


    this.modelUpdateSubscription = this.coreAPIService.updateSubject.subscribe(value => {
      this.graph.updateModel(value);
    });

    this.coreAPIService.getAspect();
  }

  ngOnInit() {

  }


  handleSidebarEvents(event) {
    if (event === 'print') {
      this.graph.print();
    }
    if (event === 'switch-expert') {
      this.graph.view.switchExpertMode();
    }

  }

  rightclickHandler(event) {
    event.preventDefault();
  }

  ngOnDestroy(): void {
    this.aspectSubscription.unsubscribe();
    this.modelUpdateSubscription.unsubscribe();
  }

}
