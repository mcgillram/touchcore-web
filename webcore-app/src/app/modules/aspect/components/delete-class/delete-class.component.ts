import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {ClassModel} from "../../../../core/model/aspect/ClassModel";
import {CoreAPIService} from "../../../shared/services/core-api.service";

@Component({
  selector: 'app-delete-class',
  templateUrl: './delete-class.component.html',
  styleUrls: ['./delete-class.component.scss']
})
export class DeleteClassComponent implements OnInit {

  classModel: ClassModel;

  constructor(private coreAPI: CoreAPIService,
              private formBuilder: FormBuilder,
              private dialogRef: MatDialogRef<DeleteClassComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.classModel = data;
  }

  ngOnInit() {
  }

  close() {


    this.coreAPI.send('/api/removeClassifier', {
      classId: this.classModel._id,
    });


    this.dialogRef.close();
  }


}
