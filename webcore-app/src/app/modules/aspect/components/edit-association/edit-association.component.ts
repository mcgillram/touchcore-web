import {Component, Inject, OnInit} from '@angular/core';
import {CoreAPIService} from "../../../shared/services/core-api.service";
import {FormBuilder, FormGroup} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {AssociationModel} from "../../../../core/model/aspect/AssociationModel";
import {ClassModel} from "../../../../core/model/aspect/ClassModel";

@Component({
  selector: 'app-edit-association',
  templateUrl: './edit-association.component.html',
  styleUrls: ['./edit-association.component.scss']
})
export class EditAssociationComponent implements OnInit {

  model: AssociationModel;
  source: ClassModel;
  target: ClassModel;
  isValid = true;
  sourceValues;
  targetValues;
  sourceValuesOrigin;
  targetValuesOrigin;
  navigability;


  constructor(private coreAPI: CoreAPIService,
              private dialogRef: MatDialogRef<EditAssociationComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {

    this.model = this.data.object;
    this.source = this.data.source;
    this.target = this.data.target;

    this.targetValues = {
      role: this.model.source.name,
      navigable: this.model.source.navigable === undefined ? 'true' : this.model.source.navigable.toString(),
      referenceType: this.model.source.referenceType === undefined ? 'Regular' : this.model.source.referenceType,
      multiplicity: this.model.source.lowerBound
    };
    this.sourceValues = {
      role: this.model.target.name,
      navigable: this.model.target.navigable === undefined ? 'true' : this.model.target.navigable.toString(),
      referenceType: this.model.target.referenceType === undefined ? 'Regular' : this.model.target.referenceType,
      multiplicity: this.model.target.lowerBound
    };

    if (this.sourceValues.navigable === 'true' && this.targetValues.navigable === 'true') {
      this.navigability = 'both';
    }
    if (this.sourceValues.navigable === 'false' && this.targetValues.navigable === 'true') {
      this.navigability = 'right';
    }
    if (this.sourceValues.navigable === 'true' && this.targetValues.navigable === 'false') {
      this.navigability = 'left';
    }

    this.sourceValuesOrigin = JSON.parse(JSON.stringify(this.sourceValues));
    this.targetValuesOrigin = JSON.parse(JSON.stringify(this.targetValues));

  }

  updateNavigability() {
    if (this.navigability === 'both') {
      this.sourceValues.navigable = 'true';
      this.targetValues.navigable = 'true';
    } else if (this.navigability === 'left') {
      this.sourceValues.navigable = 'true';
      this.targetValues.navigable = 'false';
    } else {
      this.sourceValues.navigable = 'false';
      this.targetValues.navigable = 'true';
    }
  }

  ngOnInit() {
  }

  validate() {

    /** Role name **/
    if (this.sourceValues.role !== this.sourceValuesOrigin.role) {
      this.coreAPI.send('/api/setRoleName', {
        associationEndId: this.model.target._id,
        roleName: this.sourceValues.role
      });
    }
    if (this.targetValues.role !== this.targetValuesOrigin.role) {
      this.coreAPI.send('/api/setRoleName', {
        associationEndId: this.model.source._id,
        roleName: this.targetValues.role
      });
    }

    /** Multiplicity **/
    if (this.sourceValues.multiplicity !== this.sourceValuesOrigin.multiplicity) {
      this.coreAPI.send('/api/setMultiplicity', {
        associationEndId: this.model.target._id,
        lowerBound: this.sourceValues.multiplicity,
        upperBound: '1'
      });
    }
    if (this.targetValues.multiplicity !== this.targetValuesOrigin.multiplicity) {
      this.coreAPI.send('/api/setMultiplicity', {
        associationEndId: this.model.source._id,
        lowerBound: this.targetValues.multiplicity,
        upperBound: '1'
      });
    }

    /** Navigable **/
    if ((this.sourceValues.navigable !== this.sourceValuesOrigin.navigable) ||
      (this.targetValues.navigable !== this.targetValuesOrigin.navigable)) {
      this.coreAPI.send('/api/setNavigable', {
        associationEndId: this.model.target._id,
        endNavigable: this.sourceValues.navigable,
        oppositeEndId: this.model.source._id,
        oppositeEndNavigable: this.targetValues.navigable
      });
    }

    /** Reference Type **/
    if (this.sourceValues.referenceType !== this.sourceValuesOrigin.referenceType) {
      this.coreAPI.send('/api/setReferenceType', {
        associationEndId: this.model.target._id,
        referenceType: this.sourceValues.referenceType
      });
    }
    if (this.targetValues.referenceType !== this.targetValuesOrigin.referenceType) {
      this.coreAPI.send('/api/setReferenceType', {
        associationEndId: this.model.source._id,
        referenceType: this.targetValues.referenceType
      });
    }

    this.dialogRef.close();
  }

}
