import {Component, Inject, OnInit} from '@angular/core';
import {CoreAPIService} from "../../../shared/services/core-api.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {ClassModel} from "../../../../core/model/aspect/ClassModel";
import {FormBuilder, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-edit-class',
  templateUrl: './edit-class.component.html',
  styleUrls: ['./edit-class.component.scss']
})
export class EditClassComponent implements OnInit {

  classModel: ClassModel = null;
  isValid = true;
  form: FormGroup;
  visibilities = ['concern', 'public'];
  partialities = ['none', 'public', 'concern'];
  addConstructor = false;
  addDestructor = false;
  constructorDisabled = false;
  destructorDisabled = false;

  constructor(private coreAPI: CoreAPIService,
              private formBuilder: FormBuilder,
              private dialogRef: MatDialogRef<EditClassComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {

    let formConfig = {
      name: '',
      visibility: 'concern',
      abstract: false,
      partiality: 'none'
    };

    if (this.data.mode === 'edit') {
      this.classModel = this.data.object;
      formConfig.name = this.classModel.name;
      if (this.classModel.visibility) {
        formConfig.visibility = this.classModel.visibility;
      }
      formConfig.abstract = this.classModel.abstract;
      if (this.classModel.partiality) {
        formConfig.partiality = this.classModel.partiality;
      }
      for (let o of this.classModel.operations) {
        if (o.name === 'create') {
          this.constructorDisabled = true;
        }
        if (o.name === 'destroy') {
          this.destructorDisabled = true;
        }

      }

    }

    this.form = this.formBuilder.group(formConfig);
  }

  ngOnInit() {

  }


  close() {

    if (this.form.value.name === '') {
      this.isValid = false;
      return;
    }
    let name = this.form.value.name;
    name = name[0].toUpperCase() + name.substr(1);

    if (this.data.mode === 'edit') {
      // Edit a Class
      if (name !== this.classModel.name) {
        this.coreAPI.send('/api/setEObjectName', {
          eObjectId: this.classModel._id,
          objectName: name
        });
      }

      if (this.form.value.visibility !== this.classModel.visibility) {
        this.coreAPI.send('/api/setClassVisibility', {
          classId: this.classModel._id,
          visibilityString: this.form.value.visibility
        });
      }

      if (this.form.value.abstract !== null
        && this.form.value.abstract !== this.classModel.abstract) {
        this.coreAPI.send('/api/switchAbstract', {
          classId: this.classModel._id
        });
      }

      if (this.form.value.partiality !== null
        && this.form.value.partiality !== this.classModel.partiality) {
        this.coreAPI.send('/api/setEObjectPartiality', {
          eObjectId: this.classModel._id,
          partiality: this.form.value.partiality
        });
      }

      if (this.addConstructor) {
        this.coreAPI.send('/api/createConstructor', {
          classId: this.classModel._id
        });
      }
      if (this.addDestructor) {
        this.coreAPI.send('/api/createDestructor', {
          classId: this.classModel._id
        });
      }


    } else {
      // Add a Class, DataType or Enum
      if (this.data.elementType === "enumeration") {
        this.coreAPI.send("/api/createEnum", {
          enumName: name,
          xPosition: this.data.x,
          yPosition: this.data.y,
        });
      } else if (this.data.elementType === "dataType") {
        this.coreAPI.send("/api/createNewClass", {
          className: name,
          xPosition: this.data.x,
          yPosition: this.data.y,
          dataType: true
        });
      } else if (this.data.elementType === "class") {
        this.coreAPI.send("/api/createNewClass", {
          className: name,
          xPosition: this.data.x,
          yPosition: this.data.y,
          dataType: false
        });
      }
    }
    this.dialogRef.close();
  }

}
