import {NgModule} from "@angular/core";
import {SharedModule} from "../shared/shared.module";
import {
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule, MatCheckboxModule,
  MatIconModule,
  MatSidenavModule, MatTooltipModule
} from "@angular/material";
import {StructuralViewComponent} from "./components/structural-view/structural-view.component";
import {AddAttributeComponent} from "./components/add-attribute/add-attribute.component";
import {AddOperationComponent} from "./components/add-operation/add-operation.component";
import {EditClassComponent} from "./components/edit-class/edit-class.component";
import {DeleteClassComponent} from "./components/delete-class/delete-class.component";
import {DeleteClassElementComponent} from "./components/delete-class-element/delete-class-element.component";
import {AddLiteralComponent} from "./components/add-literal/add-literal.component";
import {EditEnumComponent} from "./components/edit-enum/edit-enum.component";
import {EditAssociationComponent} from "./components/edit-association/edit-association.component";


@NgModule({
  declarations: [
    StructuralViewComponent,
    AddAttributeComponent,
    AddOperationComponent,
    EditClassComponent,
    DeleteClassComponent,
    DeleteClassElementComponent,
    AddLiteralComponent,
    EditEnumComponent,
    EditAssociationComponent,
  ],
  entryComponents: [
    AddAttributeComponent,
    AddOperationComponent,
    EditClassComponent,
    DeleteClassComponent,
    DeleteClassElementComponent,
    AddLiteralComponent,
    EditEnumComponent,
    EditAssociationComponent
  ],
  imports: [
    SharedModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatSidenavModule,
    MatButtonToggleModule,
    MatCheckboxModule,
    MatTooltipModule,
  ],
})

export class AspectModule {  }
