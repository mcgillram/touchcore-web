import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {PageNotFoundComponent} from "./modules/shared/components/page-not-found/page-not-found.component";
import {HomeComponent} from "./modules/shared/components/home/home.component";
import {StructuralViewComponent} from "./modules/aspect/components/structural-view/structural-view.component";

const routes: Routes = [
  {path: 'structural-view', component: StructuralViewComponent},
  {path: '', component: HomeComponent},
  {path: '**', component: PageNotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
