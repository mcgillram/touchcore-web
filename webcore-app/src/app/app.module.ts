import { CoreAPIService } from './modules/shared/services/core-api.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {AspectModule} from './modules/aspect/aspect.module';
import {HttpClientModule} from "@angular/common/http";
import {UserInteractionsService} from "./modules/shared/services/user-interactions.service";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AspectModule,
  ],
  providers: [
    CoreAPIService,
    UserInteractionsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
